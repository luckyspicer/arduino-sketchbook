//example use of LCD4Bit library

#include <LCD4Bit.h> 
//create object to control an LCD.  
//number of lines in display=1
LCD4Bit lcd = LCD4Bit(2);
int button = 0;
int buttonState;

void setup() { 
  pinMode(13, OUTPUT);  //we'll use the debug LED to output a heartbeat
  pinMode(0, INPUT);
  lcd.init();
  
}

void loop()
{
if(digitalRead(button) != buttonState) //this keeps the music playing only between button presses instead of all the time!!!
  {
    if(digitalRead(button) == LOW)
      {
        
      }
    else
      {
        digitalWrite(13, HIGH);  //light the debug LED
  
  lcd.clear();
  lcd.printIn("hello, world");
  delay(3000);
  digitalWrite(13, LOW);
  
  //print some dots individually
  for (int i=0; i<3; i++){
    lcd.print('.');
    delay(500);
  }
  
  lcd.clear();
  //print some dots individually
  for (int i=0; i<3; i++){
    lcd.print('.');
    delay(500);
  }
  lcd.printIn("aLl yoUR bAse");
  delay(2000);
  lcd.cursorTo(2, 0);
  lcd.printIn("Is BelONg to uS!!!");
  delay(3000);
  
  //scroll entire display 20 chars to left, delaying 50ms each inc
  lcd.leftScroll(20, 350);
       }
      }
buttonState = digitalRead(button);  
}
