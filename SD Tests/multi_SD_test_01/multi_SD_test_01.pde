#include <SD.h>

SD sd1;

void setup() {
  Serial.begin(9600);
  
  pinMode(53, OUTPUT);
  //pinMode(22, OUTPUT);
  //digitalWrite(22, LOW);
  if (!SD.begin(22)) {
    Serial.println("Initialization Failed!");
    return;
  }
  Serial.println("Initialization SD 1 Successful");
}

void loop() {
  Serial.println("Create a new File called testFile_1!");
  SD.remove("test.txt");
  File testFile_1 = SD.open("test.txt", FILE_WRITE);
  if(SD.exists("test.txt")) {
    Serial.println("The file we created exits!");
  }
  else {
    Serial.println("For some reason the file we created does not exist?");
  }
  testFile_1.println("This is a test! 1");
  delay(1000);
  testFile_1.println("This is a test! 2");
  delay(1000);
  testFile_1.println("This is a test! 3");
  delay(1000);
  testFile_1.println("This is a test! 4");
  delay(1000);
  testFile_1.println("This is a test! 5");
  delay(1000);
  
  testFile_1.close();
  Serial.println("Data written, file closed, end");
  while(1);
}
