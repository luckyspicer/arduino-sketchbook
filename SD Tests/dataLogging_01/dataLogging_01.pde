#include <SD.h>

void setup()
{
  Serial.begin(9600);
  
  // Set up the SD card
  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
}

void loop()
{
  Serial.println("Datalogging! Comma separated file");
  Serial.println("Create File to write log to...");
  
  SD.remove("logFile1.csv");
  SD.remove("timeLog.csv");
  File lightLogFile_1;
  File timeLogFile;
  
  int lightLevel;
  long timeKeeper;
  long timer = millis();
  int startTime = timer/1000;
  Serial.print("Start Time: ");
  Serial.println(startTime);
  
  while((millis()/1000 - startTime) < 10) {
    timeKeeper = millis();
    if(timeKeeper >= (timer + 100)) {
      lightLogFile_1 = SD.open("logFile1.csv", FILE_WRITE);
      lightLevel = analogRead(0);
      Serial.print(timer);
      Serial.print(" ");
      Serial.println(lightLevel);
      lightLogFile_1.print(lightLevel);
      lightLogFile_1.println(",");
      lightLogFile_1.close();
      
      timeLogFile = SD.open("timeLog.csv", FILE_WRITE);
      timeLogFile.print(timer);
      timeLogFile.println(",");
      timeLogFile.close();
      timer = timeKeeper; 
    }
  }
  Serial.print("End Time: ");
  Serial.println(millis()/1000);
  Serial.println("Finished Logging 10 seconds of data");
  while(1);
}
