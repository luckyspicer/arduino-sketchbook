#include <SD.h>

SDClass sd1;
SDClass sd2;

void setup() {
  Serial.begin(9600);
  
  pinMode(53, OUTPUT);
  pinMode(22, OUTPUT);
  pinMode(24, OUTPUT);
  
  
  digitalWrite(22, HIGH);
  if (!sd1.begin(24)) {
    Serial.println("Initialization SD1 Failed!");
    return;
  }
  Serial.println("Initialization SD1 Successful");
  
  /*
  digitalWrite(24, HIGH);
  if (!sd2.begin(22)) {
    Serial.println("Initialization SD2 Failed!");
    return;
  }
  Serial.println("Initialization SD2 Successful");
  */
}

void loop() {
  Serial.println("Create a new File for SD1 called test1.txt!");
  sd1.remove("test1.txt");
  File testFile_1 = sd1.open("test1.txt", FILE_WRITE);
  if(sd1.exists("test1.txt")) {
    Serial.println("The file we created for SD1 now exits!");
  }
  else {
    Serial.println("For some reason the file we created for SD1 didn't happen?");
  }
  
  /*
  Serial.println("Create a new File for SD2 called test2.txt!");
  sd2.remove("test2.txt");
  File testFile_2 = sd2.open("test2.txt", FILE_WRITE);
  if(sd2.exists("test2.txt")) {
    Serial.println("The file we created for SD2 now exits!");
  }
  else {
    Serial.println("For some reason the file we created for SD2 didn't happen?");
  }
  */
  
  
  testFile_1.println("This is a test! 1");
  testFile_1.println("This is a test! 2");
  testFile_1.println("This is a test! 3");
  testFile_1.println("This is a test! 4");
  testFile_1.println("This is a test! 5");
  testFile_1.close();
  
  /*
  testFile_2.println("This is a test! A");
  testFile_2.println("This is a test! B");
  testFile_2.println("This is a test! C");
  testFile_2.println("This is a test! D");
  testFile_2.println("This is a test! E");
  testFile_2.close();
  */
  
  Serial.println("Data written, files closed, END");
  while(1);
}
