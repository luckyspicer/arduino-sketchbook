#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo

void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, distance measure tests!");
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
} 

void loop()
{
  myservo.write(0);
}
