#include <AFMotor.h>

//MOTOR Test STuff
//AF_DCMotor motor1(1, MOTOR12_64KHZ);
//AF_DCMotor motor2(2, MOTOR12_64KHZ);
AF_DCMotor motor3(3, MOTOR12_64KHZ);
AF_DCMotor motor4(4, MOTOR12_64KHZ);

void setup() {
  //motor1.setSpeed(255);
  //motor2.setSpeed(255);
  motor3.setSpeed(255);
  motor4.setSpeed(255);
  
  //motor1.run(FORWARD);
  //motor2.run(FORWARD);
  motor3.run(FORWARD);
  motor4.run(FORWARD);

  delay(4000);
  
  //motor1.run(BACKWARD);
  //motor2.run(BACKWARD);
  motor3.run(BACKWARD);
  motor4.run(BACKWARD);
  
  Serial.begin(9600);
  
}

void loop() {
  Serial.println("Forward");
  ///motor1.run(FORWARD);
  //motor2.run(FORWARD);
  motor3.run(FORWARD);
  motor4.run(FORWARD);

  delay(4000);
  
  Serial.println("Backward!");
  motor3.run(BACKWARD);
  motor4.run(BACKWARD);
  
  delay(5000);
}
