#include <Servo.h> 
#include <AFMotor.h>
 
Servo myservo;  // create servo object to control a servo
AF_DCMotor right_motor(1, MOTOR12_64KHZ);
AF_DCMotor left_motor(2, MOTOR12_64KHZ);

void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, Track Motor Tests!");
  right_motor.setSpeed(255);
  left_motor.setSpeed(255);
  
} 

void loop()
{
  // turn on motor
  right_motor.run(FORWARD);
  left_motor.run(FORWARD);
  
}
