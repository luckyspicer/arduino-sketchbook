/* ******************************
* Driving Functions
*********************************/
// Create Motor Opjects
AF_DCMotor motorRight(2, MOTOR12_64KHZ);
AF_DCMotor motorLeft(1, MOTOR12_64KHZ);

const int MAX_SPEED = 255;

/* DRIVE, REVERSE, STOP */
void drive(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void reverse(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void stop() {
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  return;
}

/*TURNING, forward and backward */
//Strength should be a percent value between 0 and 100
void turnLeft(uint8_t strength) {
 //motorLeft.run(RELEASE);
 //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;   
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void turnRight(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void backLeft(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;   
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void backRight(uint8_t strength) {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void maxLeft() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(FORWARD);
  return;
}

void maxRight() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(BACKWARD);
  return;
}

/* ******************************
* Read Analog Sensor Function (take Average Reading)
*********************************/

int readAnalogSensor(int sensor, int numAvg) {
  int avgVal = 0;
  for(int i = 0; i < numAvg; i++) {
    avgVal = analogRead(sensor) + avgVal;
  }
  avgVal = avgVal/numAvg;
  return avgVal;
}
  
int irRange(int IR) {
  int IRval = readAnalogSensor(IR, 10);
  return 27406.5*(pow(IRval, -1.322086782));
}

/* ******************************
* Read Digital Proximity Sensors
*********************************/
int triggered(int proximitySensor) {
  if (digitalRead(proximitySensor) == 0) {
    return 1;
  }
  else {
    return 0;
  }
}

