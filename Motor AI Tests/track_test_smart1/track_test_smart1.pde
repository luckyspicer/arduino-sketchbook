#include <Servo.h> 
#include <AFMotor.h>

// Create Digital (blue) proximity sensor ports
int left_side = 48;
int left_front = 46;
int right_side = 52;
int right_front = 50;
int rear = 44;

// Create Analog (IR) distance sensor ports
int bottom_IR = 8;
int top_IR = 9;

void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, Track Motor Tests!");
} 

void loop()
{
  //Serial.print("Bottom IR distance = ");
  //Serial.println(irRange(bottom_IR));
  //Serial.print("Top IR distance = ");
  //Serial.println(irRange(top_IR));
  
  // Front IR distance sensor
  if (irRange(bottom_IR) <= 10 || irRange(top_IR) <= 6) {
    Serial.print("IR distance = ");
    Serial.println(irRange(bottom_IR));
    Serial.print("Top IR distance = ");
    Serial.println(irRange(top_IR));
   
   "?;">/LK/ln;kjnb vm stop();
    delay(250);
    if(random(2) == 1) {
      maxLeft();
    }
    else {
      maxRight();
    }
    delay(150 + random(501));
  }
  // Both side sensors triggered
  else if(triggered(left_side) && triggered(right_side)) {
    Serial.println("Both Side Sensors Triggered");
    drive(100);
  }
  // Left side sensor triggered
  else if(triggered(left_side)) {
    Serial.println("Left Side Triggered");
    stop();
    delay(100);
    maxRight();
    delay(150);
  }
  // Right side sensor triggered
  else if(triggered(right_side)) {
    Serial.println("Right Side Triggered");
    stop();
    delay(100);
    maxLeft();
    delay(150);
  }
  // Left side and left front triggered
  else if(triggered(left_side) && triggered(left_front)) {    
    Serial.println("Both Left Sensors Triggered");
    stop();
    delay(100);
    reverse(100);
    delay(200);
    maxRight();
    delay(200);
  }
  // Right side and Right front triggered
  else if(triggered(right_side) && triggered(right_front)) {    
    Serial.println("Both Right Sensors Triggered");
    stop();
    delay(100);
    reverse(100);
    delay(200);
    maxLeft();
    delay(200);
  }
  else if(triggered(left_front)) {
    Serial.println("Left Front Triggered");
    stop();
    delay(100);
    reverse(100);
    delay(200);
    turnLeft(100);
    delay(250);
  }
  else if(triggered(right_front)) {
    Serial.println("Right Front Triggered");
    stop();
    delay(100);
    turnLeft(100);
    delay(250);
  }
  else {
    Serial.println("No Obstacles");
    drive(100);
  }
  //delay(500);
}
