// Lucas Spicer, Spicer Robotics
// September 11th, 2010
// Motor Test 5, Obstacle Avoider "Oscar"
// Attempting 2 front bumpers, sonar and IR obstacle avoidance, 20x2 LCD Display, PhotoResistorEyes and ReadAnalogSensor() function

#include <AFMotor.h>
#include <LiquidCrystal.h>

AF_DCMotor motorLeft(1, MOTOR12_64KHZ);
AF_DCMotor motorRight(2, MOTOR12_64KHZ);

//CONSTANTS
const int FLEE_RADIUS = 12;
const int MAX_TURN_RADIUS = 20;
const int NO_EFFECT_RADIUS = 60;

const int MAX_SPEED = 100;

int IR = 8;
int irVal;
int val = 8;


// pin for pingSensor
int pingSensor = 24;
int SonarRange;
int IRRange;

//variables to store value of left and right bumpers
int leftBump;
int rightBump;

// variable to determine turn direction
int isTurnLeft; // 1 = Turn Left, 0 = Turn Right

//VARIABLE for STATE CONTROL (to keep from getting stuck!

// variables for easy turn
int avgDistance;
int percentTurnStrength;

// pins for the bump sensors
int leftBumper = 22;
int rightBumper = 23;

// Analgo Pins with the photo-resistor eyes
int photoEyeRight = 9;
int photoEyeLeft = 10;
int RightEyeVal;
int LeftEyeVal;
int eyeDifPercent = 10; // %difference between Left and Right Eye cause movement

// initialize the library with the numbers of the interface pins
//LiquidCrystal lcd(rs [orange], R/W [blue], E [green], d4 [red], d5 [gray], d6 [yellow] ,d7 [orange])
// also the single yellow line by itself is the contrast pin!
LiquidCrystal lcd(34, 36, 38, 26, 28, 30, 32);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Hello Oscar!");
  
  // set up the LCD's number of rows and columns: 
  lcd.begin(20, 2);
  lcd.print("   Hello, World!");
  lcd.setCursor(4,1);
  delay(1000);
  lcd.print("I'm Oscar!");
  delay(3000);

  // turn on motor
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
 
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  
  // initialize the bump sensors to be inputs
  pinMode(leftBumper, INPUT);
  pinMode(rightBumper, INPUT);
  
  //turn flag, to make IR/Sonar turns variable
  randomSeed(analogRead(IR));
  isTurnLeft = random(0, 2);
  Serial.print("isTurnLeft = ");
  Serial.println(isTurnLeft);
  
  lcd.clear();
  lcd.print(" L   S   Ir Act   R ");
}

void loop() {
  // Read all the sensors! SENSE
  SonarRange = sonarRange();
  IRRange = irRange();
  LeftEyeVal = readAnalogSensor(photoEyeLeft, 10);
  RightEyeVal = readAnalogSensor(photoEyeRight, 10);
  leftBump = digitalRead(leftBumper);
  rightBump = digitalRead(rightBumper);
  
  // Print some sensor readings to the LCD and the serial interface
  Serial.print(SonarRange);
  Serial.print(" ");
  Serial.println(IRRange);
  
  Serial.print(leftBump);
  Serial.print(" ");
  Serial.println(rightBump);
  Serial.print("Photo-Resistor Right ");
  Serial.println(RightEyeVal);
  Serial.print("Photo-Resistor Left ");
  Serial.println(LeftEyeVal);
  Serial.println("");
  
  lcd.setCursor(0,1);
  lcd.print("                    ");
  lcd.setCursor(0,1);
  lcd.print(LeftEyeVal);
  lcd.setCursor(4, 1);
  lcd.print(SonarRange);
  lcd.setCursor(8, 1);
  lcd.print(IRRange);
  lcd.setCursor(17,1);
  lcd.print(RightEyeVal);
  lcd.setCursor(12, 1);
  
  //Both Bumpers Bumped!
  if (leftBump == 0 && rightBump == 0) {
    lcd.print("Bump");
    //Alrady turning left, continue Turning Left
    if (isTurnLeft == 1) {
      backLeft(25);
    }
    //Already turning right, continue Turning Right
    else if (isTurnLeft == 0) {
      backRight(25);
    }
  }
  //Right Bumper Bumped (Turn hard left!)
  else if (leftBump == 1 && rightBump == 0) {
    lcd.print("Bump");
    maxLeft();
  }
  //Left Bumper Bumped (Turn hard right!)
  else if (leftBump == 0 && rightBump == 1) {
    lcd.print("Bump");
    maxRight();
  }
  //Obstacle too close! Flee!
  else if (SonarRange <= FLEE_RADIUS || IRRange <= FLEE_RADIUS) {
    Serial.println("FLEE!");
    lcd.print("Flee");
    //Alrady turning left, continue Turning Left
    if (isTurnLeft == 1) {
      backLeft(25);
    }
    //Already turning right, continue Turning Right
    else if (isTurnLeft == 0) {
      backRight(25);
    }
  }
  //Obstacle not too close, turn quickly a way from it!
  else if ((SonarRange > FLEE_RADIUS && SonarRange <= MAX_TURN_RADIUS) || (IRRange > FLEE_RADIUS && IRRange <= MAX_TURN_RADIUS)) {
    Serial.println("TURN AWAY HARD!");
    lcd.print("Turn");
    if (isTurnLeft == 1) {
      maxLeft();
    }
    else if (isTurnLeft == 0) {
      maxRight();
    }
  }
  //Obstacle not too close, ease away from it
  else if((SonarRange > MAX_TURN_RADIUS && SonarRange <= NO_EFFECT_RADIUS) || (IRRange > MAX_TURN_RADIUS && IRRange <= NO_EFFECT_RADIUS)) {
    Serial.print("Ease away!");
    lcd.print("Veer");
      avgDistance = (SonarRange + IRRange)/2;
      if(avgDistance >= 60) {
        avgDistance = 60;
      }
      else if(avgDistance <= 20) {
        avgDistance = 20;
      }
      percentTurnStrength = normalize(avgDistance, MAX_TURN_RADIUS, NO_EFFECT_RADIUS);
    if (isTurnLeft == 1) {
      turnLeft(percentTurnStrength);
    }
    else if (isTurnLeft == 0) {
      turnRight(percentTurnStrength);
    }
  }
  //If no bumps or obstacles ahead, try to drive straight
  else {
    Serial.println("DRIVE!");
    lcd.print("Go!!");
    drive(100);
    isTurnLeft = random(0, 2);
    
    //This is the experimental drive to light code!
    if(abs(RightEyeVal-LeftEyeVal)>(eyeDifPercent*min(RightEyeVal, LeftEyeVal)/100)&&min(RightEyeVal, LeftEyeVal)>5) {
      lcd.setCursor(12, 1);
      if(RightEyeVal > LeftEyeVal) {
        lcd.print("Rt_1");
        turnRight(50);
      }
      else if(LeftEyeVal > RightEyeVal) {
        lcd.print("Lf_1");
        turnLeft(50);
      }
    }
  }
  //delay(500);
}

/* ******************************
* Movement algorithms
*********************************/



/* ******************************
* Driving Functions
*********************************/

/* DRIVE, REVERSE, STOP */
void drive(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void reverse(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void stop() {
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  return;
}

/*TURNING, forward and backward */
//Strength should be a percent value between 0 and 100
void turnLeft(uint8_t strength) {
 //motorLeft.run(RELEASE);
 //motorRight.run(RELEASE);
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = MAX_SPEED - strength;
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void turnRight(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = MAX_SPEED - strength;
  Serial.print("Strength of turn: ");
  Serial.println(strength);  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void backLeft(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  
  strength = MAX_SPEED - strength;   
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void backRight(uint8_t strength) {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  
  strength = MAX_SPEED - strength;  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void maxLeft() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(FORWARD);
  return;
}

void maxRight() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(BACKWARD);
  return;
}

/* ******************************
* Sonar Functions
*********************************/

long sonarRange() {
  // establish variables for duration of the ping, 
  // and the distance result in inches and centimeters:
  long duration, inches, cm;
  duration = pingSonar(pingSensor);
  
  // convert the time into a distance
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
  
  return cm;
  
//  Serial.print(inches);
//  Serial.print("in, ");
//  Serial.print(cm);
//  Serial.print("cm");
//  Serial.println();
}

long pingSonar(int pingPin) {
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
  
  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  return pulseIn(pingPin, HIGH);
}

long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

/* ******************************
* Read Analog Sensor Function (take Average Reading)
*********************************/

int readAnalogSensor(int sensor, int numAvg) {
  int avgVal = 0;
  for(int i = 0; i < numAvg; i++) {
    avgVal = analogRead(sensor) + avgVal;
  }
  avgVal = avgVal/numAvg;
  return avgVal;
}
  
int irRange() {
  int IRval = readAnalogSensor(IR, 10);
  return 27406.5*(pow(IRval, -1.322086782));
}

/* ******************************
* Distance, Percent and Normalization Functions
*********************************/
// normalize function returns a percent that x is between min and max
int normalize(int x, int min, int max) {
  return ((x - min)*100)/(max - min);
}

