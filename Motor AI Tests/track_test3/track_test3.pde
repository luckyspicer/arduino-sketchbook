#include <Servo.h> 
#include <AFMotor.h>
 
// Create Motor Opjects
AF_DCMotor right_motor(2, MOTOR12_64KHZ);
AF_DCMotor left_motor(1, MOTOR12_64KHZ);

// Create Digital (blue) proximity sensor ports
int left_side = 48;
int left_front = 46;
int right_side = 52;
int right_front = 50;
int rear = 44;

// Create Analog (IR) distance sensor ports
int front = 8;


void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, Track Motor Tests!");
  right_motor.setSpeed(255);
  left_motor.setSpeed(255);
  
} 

void loop()
{
  if(digitalRead(left_side) == 0) {
    left_motor.run(FORWARD);
    right_motor.run(BACKWARD);
    Serial.println("Left Side Triggered");
  }
  else if(digitalRead(right_side) == 0) {
    left_motor.run(BACKWARD);
    right_motor.run(FORWARD);
    Serial.println("Right Side Triggered");
  }
  else if(digitalRead(left_front) == 0 && digitalRead(right_front) == 0) {
    left_motor.run(BACKWARD);
    left_motor.run(BACKWARD);
    Serial.println("Both Fronts Triggered");
  }
  else if(digitalRead(left_front) == 0) {
    right_motor.setSpeed(0);
    left_motor.setSpeed(255);
    left_motor.run(FORWARD);
    right_motor.run(FORWARD);
    Serial.println("Left Front Triggered");
  }
  else if(digitalRead(right_front) == 0) {
    left_motor.setSpeed(0);
    right_motor.setSpeed(255);
    left_motor.run(FORWARD);
    right_motor.run(FORWARD);
    Serial.println("Right Front Triggered");
  }
  else {
    right_motor.setSpeed(255);
    left_motor.setSpeed(255);
    left_motor.run(FORWARD);
    right_motor.run(FORWARD);
  }
}
