// Lucas Spicer, Spicer Robotics
// August 30th, 2010
// Motor Test 3, Obstacle Avoider "Oscar"
// Attempting 2 front bumpers, sonar and IR obstacle avoidance and stuck, detection

#include <AFMotor.h>

AF_DCMotor motorLeft(1, MOTOR12_64KHZ);
AF_DCMotor motorRight(2, MOTOR12_64KHZ);

//CONSTANTS
const int FLEE_RADIUS = 12;
const int MAX_TURN_RADIUS = 20;
const int NO_EFFECT_RADIUS = 60;

const int MAX_SPEED = 100;

int IR = 8;
int irVal;
int val = 8;
int led13 = 13;

// pin for pingSensor
int pingSensor = 26;
int SonarRange;
int IRRange;

//variables to store value of left and right bumpers
int leftBump;
int rightBump;

// variable to determine turn direction
int isTurnLeft; // 1 = Turn Left, 0 = Turn Right

//VARIABLE for STATE CONTROL (to keep from getting stuck!
int state = 0;
int timeNow;
int drive_time1;
int drive_time2;
int stuck_time = 3; //20 seconds without driving forward means stuck!
int rescue_time = 10; //60 seconds without driving forward means stop trying, get rescued!

// variables for easy turn
int avgDistance;
int percentTurnStrength;

// pins for the bump sensors
int leftBumper = 22;
int rightBumper = 23;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");

  // turn on motor
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
 
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  
  // initialize the bump sensors to be inputs
  pinMode(leftBumper, INPUT);
  pinMode(rightBumper, INPUT);
  
  //turn flag, to make IR/Sonar turns variable
  randomSeed(analogRead(IR));
  isTurnLeft = random(0, 2);
  Serial.print("isTurnLeft = ");
  Serial.println(isTurnLeft);
  
  // initialize the blink led!
  pinMode(led13, OUTPUT);
}

void loop() {
  SonarRange = sonarRange();
  IRRange = irRange();
  leftBump = digitalRead(leftBumper);
  rightBump = digitalRead(rightBumper);
  Serial.print(SonarRange);
  Serial.print(" ");
  Serial.println(IRRange);
  
  Serial.print(leftBump);
  Serial.print(" ");
  Serial.println(rightBump);
  
  //Both Bumpers Bumped! [State number 6]
  if (leftBump == 0 && rightBump == 0 || state == 6 && state != 5 && state != 4 && state != 3 && state != 2 && state != 1 && state != -1) {
    //Alrady turning left, continue Turning Left
    if (isTurnLeft == 1) {
      backLeft(50);
    }
    //Already turning right, continue Turning Right
    else if (isTurnLeft == 0) {
      backRight(50);
    }
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Right Bumper Bumped (Turn hard left!) [State number 5]
  else if (leftBump == 1 && rightBump == 0 || state == 5 && state != 6 && state != 4 && state != 3 && state != 2 && state != 1 && state != -1) {
    maxLeft();
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Left Bumper Bumped (Turn hard right!) [State number 4]
  else if (leftBump == 0 && rightBump == 1 || state == 4 && state != 6 && state != 5 && state != 3 && state != 2 && state != 1 && state != -1) {
    maxRight();
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Obstacle too close! Flee! [State number 3]
  else if (SonarRange <= FLEE_RADIUS || IRRange <= FLEE_RADIUS || state == 3 && state != 6 && state != 5 && state != 4 && state != 2 && state != 1 && state != -1) {
    Serial.println("FLEE!");
    //Alrady turning left, continue Turning Left
    if (isTurnLeft == 1) {
      backLeft(75);
    }
    //Already turning right, continue Turning Right
    else if (isTurnLeft == 0) {
      backRight(75);
    }
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Obstacle not too close, turn quickly a way from it! [State number 2]
  else if ((SonarRange > FLEE_RADIUS && SonarRange <= MAX_TURN_RADIUS) || (IRRange > FLEE_RADIUS && IRRange <= MAX_TURN_RADIUS) || state == 2 && state != 6 && state != 5 && state != 4 && state != 3 && state != 1 && state != -1) {
    Serial.println("TURN AWAY HARD!");
    if (isTurnLeft == 1) {
      maxLeft();
    }
    else if (isTurnLeft == 0) {
      maxRight();
    }
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Obstacle not too close, ease away from it [State number 1]
  else if((SonarRange > MAX_TURN_RADIUS && SonarRange <= NO_EFFECT_RADIUS) || (IRRange > MAX_TURN_RADIUS && IRRange <= NO_EFFECT_RADIUS) || state == 1 && state != 6 && state != 5 && state != 4 && state != 3 && state != 2 && state != -1) {
    Serial.print("Ease away!");
      avgDistance = (SonarRange + IRRange)/2;
      if(avgDistance >= 60) {
        avgDistance = 60;
      }
      else if(avgDistance <= 20) {
        avgDistance = 20;
      }
      percentTurnStrength = normalize(avgDistance, MAX_TURN_RADIUS, NO_EFFECT_RADIUS);
    if (isTurnLeft == 1) {
      turnLeft(percentTurnStrength);
    }
    else if (isTurnLeft == 0) {
      turnRight(percentTurnStrength);
    }
    timeNow = millis()/1000;
    if ((timeNow - drive_time1) > stuck_time) {
      drive_time1 = timeNow;
      state = random(1, 7);
      quickblink();
    }
    if ((timeNow - drive_time2) > rescue_time) {
      state = -1;
    }
  }
  //Totally Stuck, blink until rescue and shut off the motors!
  else if(state == -1) {
    stop();
    for(;;) {
      delay(500);
      digitalWrite(led13, HIGH);
      delay(500);
      digitalWrite(led13, LOW);
    }
  }
  //If no bumps or obstacles ahead, try to drive straight [State number 0]
  else {
    Serial.println("DRIVE!");
    drive(100);
    isTurnLeft = random(0, 2);
    timeNow = millis()/1000;
    drive_time1 = timeNow;
    drive_time2 = timeNow;
  }
  //delay(500);
}

/* ******************************
* Movement algorithms
*********************************/



/* ******************************
* Driving Functions
*********************************/

/* DRIVE, REVERSE, STOP */
void drive(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void reverse(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void stop() {
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  return;
}

/*TURNING, forward and backward */
//Strength should be a percent value between 0 and 100
void turnLeft(uint8_t strength) {
 //motorLeft.run(RELEASE);
 //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;   
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void turnRight(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(FORWARD);
  motorRight.run(FORWARD);
  return;
}

void backLeft(uint8_t strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;   
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(strength);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void backRight(uint8_t strength) {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  if (strength >= 100) {
    strength = 100;
  }
  strength = (strength/100) * MAX_SPEED;
  
  strength = MAX_SPEED - strength;  
  motorLeft.setSpeed(strength);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(BACKWARD);
  return;
}

void maxLeft() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(BACKWARD);
  motorRight.run(FORWARD);
  return;
}

void maxRight() {
  //motorRight.run(RELEASE);
  //motorLeft.run(RELEASE);
  
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
  
  motorLeft.run(FORWARD);
  motorRight.run(BACKWARD);
  return;
}

/* ******************************
* Sonar Functions
*********************************/

long sonarRange() {
  // establish variables for duration of the ping, 
  // and the distance result in inches and centimeters:
  long duration, inches, cm;
  duration = pingSonar(pingSensor);
  
  // convert the time into a distance
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
  
  return cm;
  
//  Serial.print(inches);
//  Serial.print("in, ");
//  Serial.print(cm);
//  Serial.print("cm");
//  Serial.println();
}

long pingSonar(int pingPin) {
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
  
  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  return pulseIn(pingPin, HIGH);
}

long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

/* ******************************
* IR Functions
*********************************/

int readIR() {
  int avgIR = 0;
  for(int i = 0; i < 10; i++) {
    avgIR = analogRead(IR) + avgIR;
  }
  avgIR = avgIR/10;
  return avgIR;
}
  
int irRange() {
  int IRval = readIR();
  return 27406.5*(pow(IRval, -1.322086782));
}

/* ******************************
* Distance, Percent and Normalization Functions
*********************************/
// normalize function returns a percent that x is between min and max
int normalize(int x, int min, int max) {
  return ((x - min)*100)/(max - min);
}

void quickblink() {
  digitalWrite(led13, HIGH);
  delay(300);
  digitalWrite(led13, LOW);
  delay(300);
  digitalWrite(led13, HIGH);
  delay(300);
  digitalWrite(led13, LOW);
  delay(300);
  digitalWrite(led13, HIGH);
  delay(300);
  digitalWrite(led13, LOW);
}



