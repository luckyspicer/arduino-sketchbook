// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor motor1(1, MOTOR12_64KHZ);
AF_DCMotor motor2(2, MOTOR12_64KHZ);
uint8_t wanderTime;
long prevTime;
int IR = 8;
int irVal;
int val = 8;
int pingSensor = 26;
int SonarRange;
int IRRange;
int leftBump;
int rightBump;
boolean isTurnLeft;

// pins for the bump sensors
int leftBumper = 22;
int rightBumper = 23;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");

  // turn on motor
  motor1.setSpeed(255);
  motor2.setSpeed(255);
 
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  // initialize the bump sensors to be inputs
  pinMode(leftBumper, INPUT);
  pinMode(rightBumper, INPUT);
  
  //ramdom motion durint wander
  randomSeed(analogRead(15));
  wanderTime = random(3, 8);
  prevTime = 0;
  
  //turn flag, to make IR/Sonar turns variable
  isTurnLeft = true;
  Serial.println(int(isTurnLeft));
}

void loop() {
  SonarRange = sonarRange();
  IRRange = irRange();
  leftBump = digitalRead(leftBumper);
  rightBump = digitalRead(rightBumper);
  Serial.print(SonarRange);
  Serial.print(" ");
  Serial.println(IRRange);
  
  Serial.print(leftBump);
  Serial.print(" ");
  Serial.println(rightBump);
  
  if(SonarRange <= 25 || IRRange <= 20 || leftBump == 0 || rightBump == 0) {
    if(SonarRange <= 25 || IRRange <= 20 && leftBump == 1 && rightBump == 1) {
      if(SonarRange > 17 && IRRange > 15) {
        if(isTurnLeft) {
          left(255);
        }
        else {
          right(255);
        }
      }
      else {
        backRight(75);
      }
    }
    else if(leftBump == 0 && rightBump == 1) {
      backLeft(255);
    }
    else if(rightBump == 0 && leftBump == 1) {
      backRight(255);
    }
    else if(leftBump == 0 && rightBump == 0) {
      backLeft(75);
    }
  }
  else {
    drive(150);
    isTurnLeft = !isTurnLeft;
  }
  delay(500);
}

/* ******************************
* Movement algorithms
*********************************/

void wander() {
  drive(255);
  if((int(millis()/1000) - prevTime) == wanderTime) {
    Serial.println(wanderTime);
    prevTime = int(millis()/1000);
    wanderTime = random(3, 8);
    int choice1 = random(0, 2);
    if (choice1 == 0) {
      left(random(0, 255));
      Serial.println("Left");
    }
    else {
      right(random(0, 255));
      Serial.println("Right");
    }
    delay(random(0, 3)*1000);  
  }
  return;
}

/* ******************************
* Driving Functions
*********************************/

void drive(uint8_t speed) {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  return;
}

void reverse(uint8_t speed) {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  motor1.setSpeed(speed);
  motor2.setSpeed(speed);
  
  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  return;
}

void stop() {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  return;
}

void left(uint8_t strength) {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  strength = 255 - strength;   
  motor1.setSpeed(strength);
  motor2.setSpeed(255);
  
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  return;
}

void right(uint8_t strength) {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  strength = 255 - strength;  
  motor1.setSpeed(255);
  motor2.setSpeed(strength);
  
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  return;
}

void backLeft(uint8_t strength) {
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  strength = 255 - strength;   
  motor1.setSpeed(strength);
  motor2.setSpeed(255);
  
  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  return;
}

void backRight(uint8_t strength) {
  motor2.run(RELEASE);
  motor1.run(RELEASE);
  
  if (strength <= 0) {
    strength = 0;
  }
  strength = 255 - strength;  
  motor1.setSpeed(255);
  motor2.setSpeed(strength);
  
  motor1.run(BACKWARD);
  motor2.run(BACKWARD);
  return;
}

/* ******************************
* Sonar Functions
*********************************/

long sonarRange() {
  // establish variables for duration of the ping, 
  // and the distance result in inches and centimeters:
  long duration, inches, cm;
  duration = pingSonar(pingSensor);
  
  // convert the time into a distance
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
  
  return cm;
  
//  Serial.print(inches);
//  Serial.print("in, ");
//  Serial.print(cm);
//  Serial.print("cm");
//  Serial.println();
}

long pingSonar(int pingPin) {
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
  
  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  return pulseIn(pingPin, HIGH);
}

long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

/* ******************************
* IR Functions
*********************************/

int readIR() {
  int avgIR = 0;
  for(int i = 0; i < 10; i++) {
    avgIR = analogRead(IR) + avgIR;
  }
  avgIR = avgIR/10;
  return avgIR;
}
  
int irRange() {
  int IRval = readIR();
  return 27406.5*(pow(IRval, -1.322086782));
}

