
int t_button = 17;
int a_button = 16;
int m_button = 15;
int snooze_button = 9;
int h_button = 10;

void setup()
{
  Serial.begin(115200);
  pinMode(h_button, INPUT_PULLUP);
  pinMode(m_button, INPUT_PULLUP);
  pinMode(a_button, INPUT_PULLUP);
  pinMode(t_button, INPUT_PULLUP);
  pinMode(snooze_button, INPUT_PULLUP);
}

void loop()
{
  if (digitalRead(h_button) == 0)
  {
    Serial.println("h_button pressed");
  }
  
  if (digitalRead(m_button) == 0)
  {
    Serial.println("m_button pressed");
  }
  
  if (digitalRead(a_button) == 0)
  {
    Serial.println("a_button pressed");
  }

  if (digitalRead(t_button) == 0)
  {
    Serial.println("t_button pressed");
  }

  if (digitalRead(snooze_button) == 0)
  {
    Serial.println("snooze_button pressed");
  }
  
  delay(100);
}
