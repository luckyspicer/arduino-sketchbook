// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor right_motor(1, MOTOR12_1KHZ);
static int right_speed = 255;

void setup() {
  Serial.begin(115200);           // set up Serial library at 115200 bps
  Serial.println("Motor test!");

  // turn on motor
  right_motor.setSpeed(right_speed);
 
  right_motor.run(RELEASE);
}

void loop() {
  uint8_t i;
  
  Serial.println("Forward");
  
  right_motor.run(FORWARD);
  right_motor.setSpeed(right_speed);
  delay(3000);
  
  Serial.println("Backward");

  right_motor.run(BACKWARD);
  right_motor.setSpeed(right_speed);
  delay(3000);
}
