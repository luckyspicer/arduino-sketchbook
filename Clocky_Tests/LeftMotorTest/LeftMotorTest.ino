// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor left_motor(3, MOTOR34_1KHZ);
static int left_speed = 255;

void setup() {
  Serial.begin(115200);           // set up Serial library at 115200 bps
  Serial.println("Motor test!");

  // turn on motor
  left_motor.setSpeed(left_speed);
 
  left_motor.run(RELEASE);
}

void loop() {
  uint8_t i;
  
  Serial.println("Forward");
  
  left_motor.run(FORWARD);
  left_motor.setSpeed(left_speed);
  delay(3000);
  
  Serial.println("Backward");

  left_motor.run(BACKWARD);
  left_motor.setSpeed(left_speed);
  delay(3000);
}
   
