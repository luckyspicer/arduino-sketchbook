// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor right_motor(1, MOTOR12_1KHZ);
AF_DCMotor left_motor(3, MOTOR34_1KHZ);
static int left_speed = 200;
static int right_speed = 200;

void setup() {
  Serial.begin(115200);           // set up Serial library at 115200 bps
  Serial.println("Motor test!");

  // turn on motor
  left_motor.setSpeed(left_speed);
  right_motor.setSpeed(right_speed);
 
  left_motor.run(RELEASE);
  right_motor.run(RELEASE);
}

void loop() {
  uint8_t i;
  
  Serial.println("Forward");
  
  right_motor.run(FORWARD);
  left_motor.run(FORWARD);
  right_motor.setSpeed(right_speed);
  left_motor.setSpeed(left_speed);
  delay(1500);
  
  Serial.println("Backward");

  right_motor.run(BACKWARD);
  left_motor.run(BACKWARD);
  right_motor.setSpeed(right_speed);
  left_motor.setSpeed(left_speed);
 delay(1500);
}
