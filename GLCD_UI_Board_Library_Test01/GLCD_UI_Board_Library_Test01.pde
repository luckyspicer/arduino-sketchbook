#include <Interrupt_Types.h>
#include <UIButtons.h>

// Local declarations
static uint32_t buttonPressTimes[UIButtons::NumberButtons];
static const uint16_t LeftButton	= 38;
static const uint16_t UpButton		= 39;
static const uint16_t DownButton	= 40;
static const uint16_t RightButton	= 41;
static const uint16_t SelectButton	= 43;

static UIButtons displayButtons(Interrupt_Four, Interrupt_Falling, UpButton, LeftButton, RightButton, DownButton, SelectButton);

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Start of test of the UIButtons Library Test!"));
  displayButtons.Begin();
}

void loop()
{
	uint16_t buttonNum = displayButtons.WhichUIButtonPressed();
	if (buttonNum != UIButtons::NoButton)
	{
		if (buttonNum ==  UIButtons::UpButton) {
			Serial.print(F("Upbutton "));
		} else if (buttonNum ==  UIButtons::LeftButton) {
			Serial.print(F("LeftButton "));
		} else if (buttonNum == UIButtons::RightButton) {
			Serial.print(F("RightButton "));
		} else if (buttonNum == UIButtons::DownButton) {
			Serial.print(F("DownButton "));
		} else if (buttonNum == UIButtons::SelectButton) {
			Serial.print(F("SelectButton "));
		}
		Serial.print(F("Pressed "));
		Serial.print(++buttonPressTimes[buttonNum]);
		Serial.println(F(" times!"));
	}
}