/* A test to see if Arduino has a scanf equivalent and other things like that
 *
 *
 *
 */
int x = 0;
int y = 0;
 
void setup()
{
  Serial.begin(9600);
}

void loop()
{
 
 if (Serial.available() > 0)
 {
   Serial.println("\nMy favorite number is: ");
 
   x = Serial.read();
   
   Serial.print(x, BYTE);
   Serial.println(" in BYTE form");
   Serial.print(x, DEC);
   Serial.println(" in Decimal");
   Serial.print(x, HEX);
   Serial.println(" in Hex Code");
   Serial.print(x, OCT);
   Serial.println(" in Octal");
   Serial.print(x, BIN);
   Serial.println(" in Binary");
   
   Serial.print("My number + 15 =");
   y = x + 15;
   Serial.print(y);
   Serial.flush();
   delay(1000);
 }
}
