/* Lesson4_Hello World
*
*This program teaches how to do serial communication from the arduino to the PC
*Luke Spicer, Jan 8, 2009
*
*/

#include "WProgram.h"
void setup();
void loop();
void setup()                       // run once, when the sketch starts
{
  Serial.begin(9600);              // set up Serial library at 9600 bps
  
  Serial.println("Hello world!"); // print hello wtih ending line break
}

void loop()                        // loop forever
{
  Serial.println("Hello world!"); // print hello wtih ending line break
  delay(1000);
}
  
int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

