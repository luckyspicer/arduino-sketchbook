/*
*
*Use two potentiometers to control the bi-color LED!!!!
*
*/

#include "WProgram.h"
void setup();
void loop();
int switch0 = 0;            // Switch connected to digital pin 2
int val0;
int switch1 = 1;
int val1;
int Led11 = 11;
int Led10 = 10;


void setup()
{
}

void loop()
{
  val0 = analogRead(switch0);
  val1 = analogRead(switch1);
  
  analogWrite(Led11, val0);
  analogWrite(Led10, val1);
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

