/*
 * Lesson3_RGB_Blink
 *
 * The basic Arduino example.  Turns on an LED on for one second,
 * then off for one second, and so on...  We use pin 13 because,
 * depending on your Arduino board, it has either a built-in LED
 * or a built-in resistor so that you need only an LED.
 *
 * http://www.arduino.cc/en/Tutorial/Blink
 */

#include "WProgram.h"
void setup();
void loop();
int redLed = 12;                // red LED connected to digital pin 12
int greenLed = 11;              // green Led connected to digital pin 11
int blueLed = 10;               // blue Led connected to digital pin 10

void setup()                    // run once, when the sketch starts
{
  pinMode(redLed, OUTPUT);      // sets the digital pins as output
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
}

void loop()                     // run over and over again
{
  int delayTime = 2500;
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, LOW);   
  delay(delayTime);            //red light only
  
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, HIGH);   
  delay(delayTime);            //purple light
  
digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, HIGH);   
  delay(delayTime);           //blue light only
  
  digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, HIGH);   
  delay(delayTime);          //cyan light
  
digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, LOW);   
  delay(delayTime);         //greeen light only
  
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, LOW);   
  delay(delayTime);         //yellow light
  
digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, HIGH);   
  delay(delayTime);        //white light
  
  digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, LOW);   
  delay(delayTime);   
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

