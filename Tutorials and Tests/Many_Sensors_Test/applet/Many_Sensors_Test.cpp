/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

#include "WProgram.h"
void setup();
void loop();
int button = 2;            // Switch connected to digital pin 2
int val;
int buttonState;
int magSens = 3;
int reflectSens = 1;
int reflectVal;
int pirSens = 2;
int pirVal;
int led = 7;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(button, INPUT);  // sets the digital pin as input
  pinMode(magSens, INPUT);
  pinMode(led, OUTPUT);
  buttonState = digitalRead(button);
}

void loop()
{
  reflectVal = 0;
  pirVal = 0;
  val = digitalRead(button);
  if(val != buttonState)
  {
    if(val == LOW)
      {
        
      }
    else
      { 
        digitalWrite(led, HIGH);
        delay(500);
        digitalWrite(led, LOW);
        
        Serial.print("Hall Effect (Magnet) Sensor Reads: ");
        Serial.println(digitalRead(magSens));
        
        for(int i = 0; i < 10; i++)
        {
          reflectVal += analogRead(reflectSens);
        }
        Serial.print("Reflective IR sensor Reads: ");
        Serial.println(reflectVal/10);
        
        for(int i = 0; i < 10; i++)
        {
          pirVal += analogRead(pirSens);
        }
        Serial.print("Passive Infrared Sensor (PIR) Sensor Reads: ");
        Serial.println(pirVal/10);
        Serial.println("\n");
      }
  }
  buttonState = val;
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

