/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int button = 2;            // Switch connected to digital pin 2
int val;
int buttonState;
int magSens = 3;
int reflectSens = 1;
int pir = 2;
int led = 7;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  pinMode(magSens, INPUT);
  pinMode(led, OUTPUT);
  buttonState = digitalRead(button);
}

void loop()
{
  val = digitalRead(button);
  if(val != buttonState)
  {
    if(val == LOW)
      {
        
      }
    else
      { 
        digitalWrite(led, HIGH);
        delay(500);
        digitalWrite(led, LOW);
        
        Serial.print("Hall Effect (Magnet) Sensor Reads: ");
        Serial.println(digitalRead(magSens));
        
        Serial.print("Reflective IR sensor Reads: ");
        Serial.println(analogRead(reflecSens));
        
        Serial.print("Passive Infrared Sensor (PIR) Sensor Reads: ");
        Serial.println(analogRead(pir));
        Serial.println("\n");
      }
  }
  buttonState = val;
}
