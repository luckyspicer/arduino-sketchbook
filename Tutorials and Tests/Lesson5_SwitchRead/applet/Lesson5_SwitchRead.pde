/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int switchPin = 2;            // Switch connected to digital pin 2

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
}

void loop()
{
  Serial.print("Read switch input: ");
  Serial.println(digitalRead(switchPin));    //Read the pin and display the value
  delay(100);
}
  
