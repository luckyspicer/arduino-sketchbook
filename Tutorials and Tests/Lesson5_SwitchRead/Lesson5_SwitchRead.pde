/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int switchPin = 2;            // Switch connected to digital pin 2
int greenLed = 12;
int redLed = 13;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  pinMode(greenLed, OUTPUT);
  pinMode(redLed, OUTPUT);
}

void loop()
{
  Serial.print("Read switch input: ");
  Serial.println(digitalRead(switchPin));    //Read the pin and display the value
  if(digitalRead(switchPin) == LOW)
    {
      digitalWrite(greenLed, HIGH);
      digitalWrite(redLed, LOW);
    }
  else if(digitalRead(switchPin) == HIGH)
    {
      digitalWrite(greenLed, LOW);
      digitalWrite(redLed, HIGH);
    }
  delay(2000);
}
  
