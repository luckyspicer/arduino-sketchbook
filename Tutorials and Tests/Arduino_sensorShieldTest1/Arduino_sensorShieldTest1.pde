int analog0 = 0;
int analog1 = 1;
int analog2 = 2;
int analog3 = 3;
int analog4 = 4;
int analog5 = 5;

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.print("Analog 0 = ");
  Serial.println(analogRead(analog0));
  
  Serial.print("Analog 1 = ");
  Serial.println(analogRead(analog1));
  
  Serial.print("Analog 2 = ");
  Serial.println(analogRead(analog2));
  
  Serial.print("Analog 3 = ");
  Serial.println(analogRead(analog3));
  
  Serial.print("Analog 4 = ");
  Serial.println(analogRead(analog4));
  
  Serial.print("Analog 5 = ");
  Serial.println(analogRead(analog5));
  Serial.println("");
  delay(750);
}
