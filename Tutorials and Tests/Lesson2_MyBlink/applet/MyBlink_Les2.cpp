/*
 * ProtoShield Blink
 *
 * The basic Arduino example.  Turns on an LED on for one second,
 * then off for one second, and so on...  We use pin 13 because,
 * depending on your Arduino board, it has either a built-in LED
 * or a built-in resistor so that you need only an LED.
 *
 * http://www.arduino.cc/en/Tutorial/Blink
 */

#include "WProgram.h"
void setup();
void loop();
int redLed = 13;                // red LED connected to digital pin 13
int greenLed = 12;              // green Led connected to digital pin 12

void setup()                    // run once, when the sketch starts
{
  pinMode(redLed, OUTPUT);      // sets the digital pins as output
  pinMode(greenLed, OUTPUT);
}

void loop()                     // run over and over again
{
  digitalWrite(redLed, HIGH);   // sets the red LED on
  digitalWrite(greenLed, LOW);  // sets the green Led off
  delay(1000);                  // waits for a second
  digitalWrite(redLed, LOW);    // sets the red LED off
  digitalWrite(greenLed, HIGH); //sets the green Led on
  delay(1000);                  // waits for a second
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

