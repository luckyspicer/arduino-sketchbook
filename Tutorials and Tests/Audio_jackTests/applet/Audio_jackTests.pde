/*
*
*A sketch to use serial.read to test microphone/speaker inputs/outputs
*/

int out10 = 10;
int out11 = 11;
int value;
int anin0 = 0;
int anin1 = 1;


void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(out10, OUTPUT);
  pinMode(out11, INPUT);
  
}

void loop()
{
  /*
  //This test headphone output, makes a weird wining buzzing sound.
  for(value = 0 ; value <= 255; value+=1) // fade in (from min to max) 
  { 
    analogWrite(out10, value);           // sets the value (range from 0 to 1024) 
    delay(10);                            // waits for 30 milli seconds to see the dimming effect
    Serial.println("\nValue on out10 is:");
    Serial.println(value); 
  }
  analogWrite(out10, 0);
  for(value = 0; value <= 255; value+=4)   // fade out (from max to min) 
  { 
    analogWrite(out11, value); 
    delay(1000);
    Serial.println("\nValue on out11 is:");
    Serial.println(value); 
  }
  analogWrite(out11, 0);
  
  /* //this is to test audio inputs!
  Serial.println("\n\nAnalog Input 0 reads:");
  Serial.println(analogRead(anin0));
  Serial.println("\nAnalog Input 1 reads:");
  Serial.println(analogRead(anin1));
  */
  /*
  analogWrite(out10, analogRead(anin0));
  Serial.println(analogRead(anin0));
  */
  
  digitalWrite(out10, digitalRead(out11));
  Serial.println(digitalRead(out11));
  delay(200);
}
