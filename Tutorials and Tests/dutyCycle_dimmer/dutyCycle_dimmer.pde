/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int knobPin = 0;
int val;
int Led12 = 12;


void setup()
{
  Serial.begin(9600);         // Setting up serial library

  pinMode(Led12, OUTPUT);
  val = analogRead(knobPin);
}

void loop()
{
  val = analogRead(knobPin);
  digitalWrite(Led12, LOW);
  delay(.0099);
  digitalWrite(Led12, HIGH);
  delay(.0001);
}
