/*
*
*A sketch to use serial.read to test microphone/speaker inputs/outputs
*/

#include "WProgram.h"
void setup();
void loop();
int out10 = 10;
int out11 = 11;


void setup()
{
  Serial.begin(9600);         // Setting up serial library

  pinMode(out10, OUTPUT);
  pinMode(out11, OUTPUT);
}

void loop()
{
  digitalWrite(out10, HIGH);
  delay(1000);
  digitalWrite(out10, LOW);
  digitalWrite(out11, HIGH);
  delay(1000);
  digitalWrite(out11, LOW);
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

