/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int switchPin = 2;            // Switch connected to digital pin 2
int val;
int buttonState;
int mode = 0;
int Led13 = 13;


void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  buttonState = digitalRead(switchPin);
  
  pinMode(Led13, OUTPUT);
}

void light()
{
  digitalWrite(Led13, HIGH);
}

void loop()
{
  val = digitalRead(switchPin);
  if(val != buttonState)
  {
    if(val == LOW)
      {
        Serial.println("\nButton has been pressed!");
        light();
      }
  }   
  buttonState = val;
}
