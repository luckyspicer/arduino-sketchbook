/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int switchPin = 2;            // Switch connected to digital pin 2
int knobPin = 0;
int time = 1023;
int val;
int buttonState;
int mode = 0;
int Led12 = 12;
int Led11 = 11;
int Led10 = 10;
int Led9 = 9;
int Led8 = 8;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  buttonState = digitalRead(switchPin);
  
  pinMode(Led12, OUTPUT);
  pinMode(Led11, OUTPUT);
  pinMode(Led10, OUTPUT);
  pinMode(Led9, OUTPUT);
  pinMode(Led8, OUTPUT);
  
  time = analogRead(knobPin);
}

void mode1()
{
  digitalWrite(Led12, HIGH);
  digitalWrite(Led11, HIGH);
  digitalWrite(Led10, HIGH);
  digitalWrite(Led9, HIGH);
  digitalWrite(Led8, HIGH);
}

void loop()
{
  val = digitalRead(switchPin);
  time = analogRead(knobPin);
  if(val != buttonState)
  {
    if(val == LOW)
      {
        Serial.println("\nButton has been pressed!");
        if(mode != 10)
        {
          mode = mode++;
        }
        else if(mode == 10)
        {
          mode = 0;
        }
        Serial.println("Now in Mode");
        Serial.println(mode);
        Serial.println("Current Delay time is");
        Serial.println(time);
      }
  }
  if(mode == 1)
    {
      mode1();
    }
   else if(mode == 2)
     {
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/2);
       
       time = analogRead(knobPin);   
       digitalWrite(Led12, LOW);
       digitalWrite(Led11, LOW);
       digitalWrite(Led10, LOW);
       digitalWrite(Led9, LOW);
       digitalWrite(Led8, LOW);
       delay(time/2); 
      }
    else if(mode == 3)
      {
        time = analogRead(knobPin);
        digitalWrite(Led12, HIGH);
        digitalWrite(Led11, LOW);
        digitalWrite(Led10, HIGH);
        digitalWrite(Led9, LOW);
        digitalWrite(Led8, HIGH);
        delay(time/2);
        
        time = analogRead(knobPin);  
        digitalWrite(Led12, LOW);
        digitalWrite(Led11, HIGH);
        digitalWrite(Led10, LOW);
        digitalWrite(Led9, HIGH);
        digitalWrite(Led8, LOW);
        delay(time/2);
       }
     else if(mode == 4)
       {
         time = analogRead(knobPin);
         digitalWrite(Led12, HIGH);
         delay(time/5);
         digitalWrite(Led11, HIGH);
         delay(time/5);
         time = analogRead(knobPin);
         digitalWrite(Led10, HIGH);
         delay(time/5);
         digitalWrite(Led9, HIGH);
         delay(time/5);
         digitalWrite(Led8, HIGH);
         delay(time/5);
         
         time = analogRead(knobPin); 
         digitalWrite(Led12, LOW);
         delay(time/5);
         digitalWrite(Led11, LOW);
         delay(time/5);
         time = analogRead(knobPin);
         digitalWrite(Led10, LOW);
         delay(time/5);
         digitalWrite(Led9, LOW);
         delay(time/5);
         digitalWrite(Led8, LOW);
         delay(time/5);
          
     }
     else if(mode == 5)
     {
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       delay(time/5);
       digitalWrite(Led11, HIGH);
       delay(time/5);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led12, LOW);
       delay(time/5);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led11, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led8, HIGH);
       digitalWrite(Led10, LOW);
       delay(time/5);
       digitalWrite(Led9, LOW);        
       delay(time/5);
       digitalWrite(Led8, LOW);
       delay(time/5);
     }
     else if(mode == 6)
     {
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, LOW);
       delay(time/5);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led12, LOW);
       delay(time/5);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led10, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led11, LOW);
       delay(time/5);
       digitalWrite(Led8, HIGH);
       digitalWrite(Led9, LOW);
       delay(time/5);
     }
     else if(mode == 7)
     {
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/5);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       delay(time/5);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       digitalWrite(Led10, HIGH);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       delay(time/5);
       digitalWrite(Led10,LOW);
       delay(time/5);
     }
     else if(mode == 8)
     {
       time = analogRead(knobPin);
       digitalWrite(Led10, HIGH);
       delay(time/5);
       digitalWrite(Led10, LOW);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/5);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/5);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       digitalWrite(Led10, HIGH);
       delay(time/5);
       digitalWrite(Led10, LOW);
       delay(time/5);
     }
     else if(mode == 9)
     {
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/5);
       digitalWrite(Led10, LOW);
       delay(time/5);
       digitalWrite(Led10, HIGH);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       delay(time/5);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/5);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       delay(time/5);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       digitalWrite(Led10, LOW);
       delay(time/5);
       digitalWrite(Led10, HIGH);
       delay(time/5);
     }
     else if(mode == 10)
     {
       time = analogRead(knobPin);
       digitalWrite(Led10, HIGH);
       delay(time/3);
       digitalWrite(Led11, HIGH);
       digitalWrite(Led9, HIGH);
       delay(time/10);
       digitalWrite(Led10, LOW);
       delay(time/5);
       time = analogRead(knobPin);
       digitalWrite(Led12, HIGH);
       digitalWrite(Led8, HIGH);
       delay(time/10);
       digitalWrite(Led11, LOW);
       digitalWrite(Led9, LOW);
       delay(time/5);
       digitalWrite(Led12, LOW);
       digitalWrite(Led8, LOW);
       delay(time/10);
     }
     else
     {
       digitalWrite(Led12, LOW);
       digitalWrite(Led11, LOW);
       digitalWrite(Led10, LOW);
       digitalWrite(Led9, LOW);
       digitalWrite(Led8, LOW);      
     }    
  buttonState = val;
}
