/*
 * Lesson3_RGB_Blink
 *
 * The basic Arduino example.  Turns on an LED on for one second,
 * then off for one second, and so on...  We use pin 13 because,
 * depending on your Arduino board, it has either a built-in LED
 * or a built-in resistor so that you need only an LED.
 *
 * http://www.arduino.cc/en/Tutorial/Blink
 */

int redLed = 12;                // red LED connected to digital pin 12
int greenLed = 11;              // green Led connected to digital pin 11
int blueLed = 10;               // blue Led connected to digital pin 10

void setup()                    // run once, when the sketch starts
{
  pinMode(redLed, OUTPUT);      // sets the digital pins as output
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  Serial.begin(9600);
}

void loop()                     // run over and over again
{
  int delayTime = 3000;
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, LOW);
  Serial.println("I'm Making Red Light!"); //the arduino prints the color it is producing
  delay(delayTime);            //red light only
  
  
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, HIGH);
  Serial.println("I'm Making Purple Light!"); //the arduino prints the color it is producing
  delay(delayTime);            //purple light
  
digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, HIGH);
  Serial.println("I'm Making Blue Light!"); //the arduino prints the color it is producing
  delay(delayTime);           //blue light only
  
  digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, HIGH);
  Serial.println("I'm Making Cyan Light!"); //the arduino prints the color it is producing 
  delay(delayTime);          //cyan light
  
digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, LOW);
  Serial.println("I'm Making Green Light!"); //the arduino prints the color it is producing  
  delay(delayTime);         //greeen light only
  
  digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, LOW);
  Serial.println("I'm Making Yellowish Light!"); //the arduino prints the color it is producing 
  delay(delayTime);         //yellow light
  
digitalWrite(redLed, HIGH);    
  digitalWrite(greenLed, HIGH);  
  digitalWrite(blueLed, HIGH);
  Serial.println("I'm Making Sorta white Light!"); //the arduino prints the color it is producing  
  delay(delayTime);        //white light
  
  digitalWrite(redLed, LOW);    
  digitalWrite(greenLed, LOW);  
  digitalWrite(blueLed, LOW);
  Serial.println("I'm Making NO Light Now!"); //the arduino prints the color it is producing 
  delay(delayTime);   
}
