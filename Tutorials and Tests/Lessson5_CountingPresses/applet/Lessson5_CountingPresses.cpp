/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

#include "WProgram.h"
void setup();
void loop();
int switchPin = 2;            // Switch connected to digital pin 2
int val;
int buttonState;
int presses = 0;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  buttonState = digitalRead(switchPin);
}

void loop()
{
  val = digitalRead(switchPin);
  
  if(val != buttonState)
  {
    if(val == LOW)
      {
        
     
      }
    else
      {
        presses++;
        Serial.println("Button has been released ");
        Serial.println(presses);
        Serial.println(" times");
      }
  }
  
  buttonState = val;
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

