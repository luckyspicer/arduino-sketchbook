/*
*
*A sketch to use serial.read to get read the value of a digital switch
*
*/

int switchPin = 2;            // Switch connected to digital pin 2
int val;
int buttonState;

void setup()
{
  Serial.begin(9600);         // Setting up serial library
  pinMode(switchPin, INPUT);  // sets the digital pin as input
  buttonState = digitalRead(switchPin);
}

void loop()
{
  val = digitalRead(switchPin);
  
  if(val != buttonState)
  {
    if(val == LOW)
      {
        Serial.println("Button just pressed");
      }
      else
      {
        Serial.println("Button just released");
      }
  }
  
  buttonState = val;
}
