int volumePin = 2;

void setup() {
  pinMode(volumePin, OUTPUT);
}

void loop() {
  analogWrite(volumePin, 127);
}
