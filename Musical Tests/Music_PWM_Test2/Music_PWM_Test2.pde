/* Piezo Music using PWM (pulse width modulation)
 * Luke Spicer, Jan 29th, 2009
 *
 *
 *
 */
 
//Now we define Note by period and frequency =====================================
//      note  period    frequency
const int C3 = 7645;    // 131 Hz
const int D3 = 6811;    // 147 Hz
const int E3 = 6068;    // 165 Hz
const int F3 = 5727;    // 175 Hz
const int G3 = 5102;    // 196 Hz
const int A3 = 4545;    // 220 Hz
const int B3 = 4050;    // 246 Hz
const int c4 = 3831;    // 261 Hz
const int d4 = 3401;    // 294 Hz
const int e4 = 3039;    // 329 Hz
const int f4 = 2865;    // 349 Hz
const int g4 = 2551;    // 392 Hz
const int a4 = 2273;    // 440 Hz
const int b4 = 2020;    // 493 Hz
const int c5 = 1912;    // 523 Hz
const int d5 = 1703;    // 587 Hz
const int R = 0;        // Defines R as a rest!

// SETUP ========================================================================
int speaker = 9; //setup speaker on PWM outputpin 9
int bspeaker = 10; //setup bass on PWM outputpin 10
int button = 2;
int buttonState;

void setup()
{
  pinMode(speaker, OUTPUT);
  pinMode(bspeaker, OUTPUT);
  pinMode(button, INPUT);
  Serial.begin(9600);  //setting up serial port for debugging.
  
  buttonState = digitalRead(button);
}

// Melody and Timing =============================================================
int melody[] = {  b4,  b4,  c5,  d5,  d5,  c5,  b4,  a4,  g4,  g4,  a4,  b4,  b4,  a4,  a4,
                  b4,  b4,  c5,  d5,  d5,  c5,  b4,  a4,  g4,  g4,  a4,  b4,  a4,  g4,  g4,
                  a4,  a4,  b4,  g4,  a4,  b4,  c5,  b4,  g4,  a4,  b4,  c5,  b4,  a4,  g4,  a4,  d4,
                  b4,  b4,  c5,  d5,  d5,  c5,  b4,  a4,  g4,  g4,  a4,  b4,  a4,  g4,  g4};
                  
int beats[]  = {  16,  16,  17,  18,  18,  17,  16,  15,  14,  14,  15,  16,  20,   7,  28,
                  16,  16,  17,  18,  18,  17,  16,  15,  14,  14,  14,  16,  18,   9,  26,
                  15,  15,  16,  14,  15,   8,   8,  16,  14,  15,   8,   8,  16,  15,  14,  15,  25,
                  16,  16,  17,  18,  18,  17,  16,  15,  14,  14,  14,  16,  18,   9,  30};
                  

//Bass and Timing ================================================================
int bass[]   = {  B3,  B3,  B3,  B3,  D3,  D3,  D3,  D3,  G3,  G3,  G3,  G3,  B3,  B3,  B3,
                  B3,  B3,  B3,  B3,  D3,  D3,  D3,  D3,  G3,  G3,  G3,  G3,  B3,  B3,  B3,
                  B3,  B3,  B3,  B3,  D3,  D3,  D3,  D3,  G3,  G3,  G3,  G3,  B3,  B3,  B3,
                  B3,  B3,  B3,  B3,  D3,  D3,  D3,  D3,  G3,  G3,  G3,  G3,  B3,  B3,  B3};

int bbeats[] = {   8,   8,   7,   8,   4,   5,   4,   4,   6,   7,   6,   6,   9,   9,  10,
                   8,   8,   7,   8,   4,   5,   4,   4,   6,   7,   6,   6,   9,   9,  10,
                   8,   8,   7,   8,   4,   5,   4,   4,   6,   7,   6,   6,   9,   9,  10,
                   8,   8,   7,   8,   4,   5,   4,   4,   6,   7,   6,   6,   9,   9,  10};
                   
int MAX_COUNT = sizeof(melody) / 2; //Melody length for looping.

// Set overall tempo
long tempo = 30000;
// Set lenght of pause between notes
int pause = 500;
// Loop variables to increase Rest length
int rest_count = 100;

int tone = 0;
int btone = 0;
int beat = 0;
int bbeat = 0;
long duration = 0;

// PlAY TONE =================================================================
// Pulse the speaker to play the tone for a particular duration
void playTone()
{
  long elapsed_time = 0;
  if (tone > 0) {
    while (elapsed_time < duration) {
      digitalWrite(speaker, HIGH);
      digitalWrite(bspeaker, HIGH);
      delayMicroseconds(tone / 2);
      
      digitalWrite(speaker, LOW);
      digitalWrite(bspeaker, LOW);
      delayMicroseconds(tone / 2);
      
      elapsed_time += (tone);
    }
  }
  
  else {
    for (int j = 0; j < rest_count; j++) {
      delayMicroseconds(duration);
    }
  }
}

//PLAY ME MY MUSIC!!!================================
void loop()
{
  
  if(digitalRead(button) != buttonState) //this keeps the music playing only between button presses instead of all the time!!!
  {
    if(digitalRead(button) == LOW)
      {
        
      }
    else
      {
        for (int i = 0; i < MAX_COUNT; i++) {
        tone = melody[i];
        beat = beats[i];
        btone = bass[i];
        bbeat = bbeats[i];
    
        duration = beat * tempo;
        //bduration = bbeat * tempo;
    
        playTone();
        // A pause between notes...
         delayMicroseconds(pause);
    
        Serial.print(i);
        Serial.print(":");
        Serial.print("beat ");
        Serial.print(tone);
        Serial.print(" ");
        Serial.println(duration);
       }
      }
  }
  buttonState = digitalRead(button);
}
