/* Working on the functions and such to control music with a piezo element
 * through a concise library!!!
 * Luke Spicer, March 15, 2009
 *
 *
 *
 */
 
/* Now we define Note by period and frequency =====================================
         note   period      frequency==============================================*/

#include "WProgram.h"
void setup();
void playMusic(int song[], int length);
void loop();
const int c4  = 3822;    // 261 Hz ===================MIDDLE C======================
const int c4s = 3608;    // 277 Hz
const int d4  = 3405;    // 294 Hz
const int e4b = 3214;    // 311 Hz
const int e4  = 3034;    // 329 Hz
const int f4  = 2863;    // 349 Hz
const int f4s = 2703;    // 370 Hz  
const int g4  = 2551;    // 392 Hz
const int g4s = 2408;    // 415 Hz
const int a4  = 2273;    // 440 Hz
const int b4b = 2145;    // 466 Hz
const int b4  = 2025;    // 493 Hz

const int q   = 100;     //quarter note

const int R = 0;       // Defines R as a rest!

// SETUP ========================================================================
int speaker = 18; //setup speaker on PWM outputpin 9

void setup()
{
  pinMode(speaker, OUTPUT);
  Serial.begin(9600);  //setting up serial port for debugging.
}

// Melody and Timing =============================================================
int melody[] = {120,  c4,q,  c4s,q,  d4,q,  R,q,  e4b,q,  e4,q,  f4,q,  R,q,  f4s,q,  g4,q,  g4s,q,  R,q,  a4,q,  b4b,q,  b4,q,  R,q};


void playMusic(int song[], int length)     //void means this method returns nothing                                            
{
  length /= sizeof(song[0]);         //use song array length and note size to get # of notes in song
  long tempo = 600000/song[0];       //tempo is determined by song[0] = BPM
  for(int i = 1; i < length; i += 2) //notes start at song[1]
  {
    int period = song[i];             //the period of our note from Music.h
    long duration = long(song[i+1]) * tempo; //actual duration, from tempo
    if(period == 0)                   //If the period is zero, it's a rest
    {
      Serial.println("Should be resting now?");      //If the note is a rest, play nothing
      //duration /= 1000;
      ///Serial.println(duration);
      delay(duration/1000);    //Play nothing for the duration requested
    }
    else                              //Else the note is not a rest, play it!
    {
      long t = 0;                     //t is how long the note has been played
      while(t <= duration)            //if t is less than duration, play on!
      {
        digitalWrite(speaker, HIGH); //turn on piezo element
        delayMicroseconds(period/2);  //on for half of period
        digitalWrite(speaker, LOW);  //turn off piezo element
        delayMicroseconds(period/2);  //off for half of period
        t += period;                  // t = t + period, play time so far
      }
      digitalWrite(speaker, LOW);    //turn off the piezo after note completed
      delay(15);                      //the delay makes our notes discrete
    }
  }
}

//PLAY ME MY MUSIC!!!================================
void loop()
{
  playMusic(melody, sizeof(melody));
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

