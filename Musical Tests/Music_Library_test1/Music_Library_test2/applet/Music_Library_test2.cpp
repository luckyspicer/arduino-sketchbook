/* Now we define Note by period and frequency
====================================================
======= note  period(usec.) frequency ==============*/
         
#include "WProgram.h"
void playSong(int song[]);
void setup();
void loop();
const int C1  = 30581;   // 33  Hz  C
const int Cs1 = 28860;   // 35  Hz  D flat / C sharp
const int D1  = 27241;   // 37  Hz  D
const int Eb1 = 25714;   // 38  Hz  D sharp / E flat
const int E1  = 24272;   // 41  Hz  E
const int F1  = 22910;   // 44  Hz  F
const int Fs1 = 21622;   // 46  Hz  G flat / F sharp
const int G1  = 20408;   // 49  Hz  G
const int Gs1 = 19264;   // 52  Hz  A flat / G sharp
const int A1  = 18182;   // 55  Hz  A
const int Bb1 = 17161;   // 58  Hz  B flat
const int Bn1 = 16197;   // 62  Hz  B (natural)

const int C2  = 15288;   // 65  Hz  C
const int Cs2 = 14430;   // 70  Hz  D flat / C sharp
const int D2  = 13620;   // 73  Hz  D
const int Eb2 = 12857;   // 78  Hz  D sharp / E flat
const int E2  = 12134;   // 82  Hz  E
const int F2  = 11453;   // 87  Hz  F
const int Fs2 = 10811;   // 93  Hz  G flat / F sharp
const int G2  = 10204;   // 98  Hz  G
const int Gs2 = 9631;    // 104 Hz  A flat / G sharp
const int A2  = 9091;    // 110 Hz  A
const int Bb2 = 8583;    // 117 Hz  B flat
const int B2  = 8099;    // 123 Hz  B

const int C3  = 7645;    // 131 Hz  C
const int Cs3 = 7216;    // 138 Hz  D flat / C sharp
const int D3  = 6811;    // 147 Hz  D
const int Eb3 = 6428;    // 156 Hz  D sharp / E flat
const int E3  = 6068;    // 165 Hz  E
const int F3  = 5727;    // 175 Hz  F
const int Fs3 = 5405;    // 185 Hz  G flat / F sharp
const int G3  = 5102;    // 196 Hz  G
const int Gs3 = 4816;    // 208 Hz  A flat / G sharp 
const int A3  = 4545;    // 220 Hz  A
const int Bb3 = 4290;    // 233 Hz  B flat
const int B3  = 4050;    // 246 Hz  B

const int c4  = 3822;    // 261 Hz ===================MIDDLE C======================
const int cs4 = 3608;    // 277 Hz  D flat / C sharp 
const int d4  = 3405;    // 294 Hz  D 
const int eb4 = 3214;    // 311 Hz  D sharp / E flat
const int e4  = 3034;    // 329 Hz  E
const int f4  = 2863;    // 349 Hz  F
const int fs4 = 2703;    // 370 Hz  G flat / F sharp  
const int g4  = 2551;    // 392 Hz  G
const int gs4 = 2408;    // 415 Hz  A flat / G sharp
const int a4  = 2273;    // 440 Hz  A
const int bb4 = 2145;    // 466 Hz  B flat
const int b4  = 2025;    // 493 Hz  B

const int c5  = 1911;    // 523 Hz  C 
const int cs5 = 1804;    // 554 Hz  D flat / C sharp 
const int d5  = 1703;    // 587 Hz  D 
const int eb5 = 1607;    // 622 Hz  D sharp / E flat
const int e5  = 1517;    // 659 Hz  E
const int f5  = 1432;    // 698 Hz  F
const int fs5 = 1351;    // 740 Hz  G flat / F sharp
const int g5  = 1276;    // 784 Hz  G
const int gs5 = 1204;    // 831 Hz  A flat / G sharp
const int a5  = 1136;    // 880 Hz  A
const int bb5 = 1073;    // 932 Hz  B flat
const int b5  = 1012;    // 988 Hz  B

const int c6  = 956;     // 1047 Hz  C 
const int cs6 = 902;     // 1109 Hz  D flat / C sharp 
const int d6  = 851;     // 1175 Hz  D 
const int eb6 = 804;     // 1245 Hz  D sharp / E flat
const int e6  = 758;     // 1319 Hz  E
const int f6  = 716;     // 1397 Hz  F
const int fs6 = 676;     // 1480 Hz  G flat / F sharp
const int g6  = 638;     // 1568 Hz  G
const int gs6 = 602;     // 1661 Hz  A flat / G sharp
const int a6  = 568;     // 1760 Hz  A
const int bb6 = 536;     // 1865 Hz  B flat
const int b6  = 506;     // 1976 Hz  B

const int R = 0;         // R is a rest, no tone is played

/*===========================================================
                Now we define the Note durations
If you wish to play a note of duration longer than these,
or of some other multiple like a dotted quarter note, just add
them up. Example: dotted quarter = q + e
         Example: dotted eighth = e + s
===========================================================*/
const int q = 100;  // Quarter Note
const int h = 200;  // Half Note
const int W = 400;  // Whole Note
const int e = 50;   // Eight Note
const int s = 25;   // Sixteenth Note
//==========================================================

int speaker = 7;

int tune[] = {120, b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  b4,q+e,  a4,e,  a4,h,
                  b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h,
                  a4,q,  a4,q,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,  a4,q,  g4,q,  a4,q,  d4,h,
                  b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h};


void playSong(int song[])
{
  int length = (sizeof(tune)-1) / 2;
  long tempo = 600000/song[0];
  for(int i = 1; i <= length; i += 2)
  {
    int period = song[i];
    long duration = long(song[i+1]) * tempo;
    Serial.println(length);
    if(period == 0)
    {
      digitalWrite(speaker, LOW);
      delayMicroseconds(duration);
    }
    else
    {
      long t = 0;
      while(t <= duration)
      {
        digitalWrite(speaker, HIGH);
        delayMicroseconds(period/2);
        digitalWrite(speaker, LOW);
        delayMicroseconds(period/2);
        t += period;
      }
      digitalWrite(speaker, LOW);
      delay(15);
    }
  }
}
void setup()
{
  pinMode(speaker, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  playSong(tune);
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

