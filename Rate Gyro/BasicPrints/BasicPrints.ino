#include <Wire.h>
#include <L3G.h>

L3G gyro;

void setup() {
  Serial.begin(115200);
  Wire.begin();

  if (!gyro.init())
  {
    Serial.println("Failed to autodetect gyro type!");
    while (1);
  }

  gyro.enableDefault();
  //gyro.writeReg(L3G_CTRL_REG1, 0x0F);
  //gyro.writeReg(L3G_CTRL_REG5, 0x2);
}

void loop() {
  gyro.read();

//  Serial.print("G ");
//  Serial.print("X: ");
  Serial.print((int)gyro.g.x);
  Serial.print(" ");
  Serial.print((int)gyro.g.y);
  Serial.print(" ");
  Serial.println((int)gyro.g.z);
//  Serial.print(" Y: ");
//  Serial.print((int)gyro.g.y);
//  Serial.print(" Z: ");
//  Serial.println((int)gyro.g.z);
  delayMicroseconds(10525);
}
