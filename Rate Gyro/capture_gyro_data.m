s = serial('COM36', 'BaudRate', 115200); % Set up serial port
fopen(s); % open serial port

for i = 1:(95 * 60 * 5)     % Run for 3 minutes (17100 samples) (95 * 60)
    data = fscanf(s, '%d');
    gyroData(i, :) = data';
%     status(i) = data(1);
%     temp(i) = data(2);
%     x(i) = data(3);
%     y(i) = data(4);
%     z(i) = data(5);
end

fclose(s);  % close serial port
