//#define _DEFINED

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("does this work?");
  printStatement();
  delay(1500);
}

#ifdef _DEFINED
   void printStatement() {
     Serial.println("Defined!");
   }
#else
   void printStatement() {
     Serial.println("Not Defined!");
   }
#endif

