int motion_sensor = 5;

void setup() {
  pinMode(motion_sensor, INPUT);
}

void loop() {
  if(digitalRead(motion_sensor) == 1) {
    digitalWrite(13, HIGH);
  }
  else {
    digitalWrite(13, LOW);
  }
}
