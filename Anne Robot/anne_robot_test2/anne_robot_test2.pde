int H2 = 2;
int H3 = 3;
int H4 = 4;
int H5 = 5;

void setup() {
  Serial.begin(9600);
  Serial.println("Anne's Robot is Go!");
}

void loop() {
  
  Serial.println("Forward Motor 1");
  digitalWrite(H5, LOW);
  digitalWrite(H4, HIGH);
  
  Serial.println("Forward Motor 2");
  digitalWrite(H3, LOW);
  digitalWrite(H2, HIGH);
  
  delay(3000);
  
  Serial.println("Stop Motor 1");
  digitalWrite(H5, LOW);
  digitalWrite(H4, LOW);
  
  Serial.println("Stop Motor 2");
  digitalWrite(H3, LOW);
  digitalWrite(H2, LOW);
  
  delay(1000);
  
  Serial.println("Reverse Motor 1");
  digitalWrite(H5, LOW);
  digitalWrite(H4, HIGH);
  
  Serial.println("Reverse Motor 2");
  digitalWrite(H2, LOW);
  digitalWrite(H3, HIGH);
  
  delay(3000);
  
  Serial.println("Stop Motor 1");
  digitalWrite(H5, LOW);
  digitalWrite(H4, LOW);
  
  Serial.println("Stop Motor 2");
  digitalWrite(H3, LOW);
  digitalWrite(H2, LOW);
  delay(1000);
}
