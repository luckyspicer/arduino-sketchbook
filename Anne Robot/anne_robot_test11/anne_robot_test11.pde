#include <AFMotor.h>
#include <Music.h> 

// Define Speaker Output pin and tunes
Music speaker = 7;

int startupTune[] = {300, g4,e,  a4,e,  b4,e,  c5,e,  g4,e, a4,e,  b4,e,  c5,e,  g4,e, a4,e,  b4,e,  c5,e};
int distressTune[] = {120,  d5,q,  d5,q,  d5,q,  d5,e,  c5,e,  d5,h};

int guitarTune[] = {200, B3,q,  d4,q,  g4,q,  B3,e,
                     c4,q,  d4,q,  g4,q,  c4,e,
                     d4,q,  d4,e,  g4,q,  d4,q,  d4,q,  d4,e,  g4,q,  d4,q,
                     g4,q,  d4,q,  g4,q,  g4,e,  fs4,q,  d4,q,  g4,q,  fs4,e,
                     a4,q,  d4,q,  g4,q,  a4,e,
                     b4,q,  d4,q,  g4,q,  b4,q,
                   
                     d5,q,  g4,q,  b4,q,  d5,e,
                     c5,q,  g4,q,  b4,q,  c5,e,
                     b4,q,  g4,q,  b4,q,  b4,e,
                     a4,q,  g4,q,  b4,q,  g4,q};
                     
int happyBirthdayTune[] = {140, c4,e+s,  c4,s,  d4,q,  c4,q,  f4,q,  e4,h,  c4,e+s,  c4,s,  d4,q,  c4,q,  g4,q,  f4,h,
                                c4,e+s,  c4,s,  c5,q,  a4,q,  f4,q,  e4,q,  d4,h,  bb4,e+s,  bb4,s,  a4,q,  f4,q,  g4,q, f4,h+q};
                                
int drunkSailorTune[] = {120, a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                              a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  c4,e,  e4,e,  g4,e,
                              a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                              c4,e,  a4,e,  g4,e,  e4,e,  d4,q,  d4,q,
                              a4,q,  a4,e+s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                              g4,q,  g4,e+s,  g4,s,  g4,e,  c4,e,  e4,e,  g4,e,
                              a4,q,  a4,e+s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                              c5,e,  a4,e,  g4,e,  e4,e,  d4,q, d4,q};

int scarboroughFairTune[] = {140, e4,h,  d4,q,  b4,h,  b4,q,  fs4,q+e,  g4,e,  fs4,q,  e4,h+q,  R,q,  b4,q,  d5,q,  e5,h,  d5,q,
                                   b4,q,  cs5,q,  a4,q,  b4,h+q,  R,h,  e5,q,  e5,h,  e5,q,  d5,h,  b4,q,  b4,q,  a4,q,  g4,q,
                                   fs4,e,  d4,e,  d4,h,  d4,h,  d4,h+q,  e4,h,  b4,q,  a4,h,  g4,q,  fs4,q,  e4,q,  d4,q,  e4,h+q};

int nobodyKnows_Tune[] = {120,  b4,e,  d4,q,  e4,e,  g4,q+e,  a4,e,  b4,e,  b4,s,  b4,e+s,  b4,q+e,
                               R,q,  b4,e,  d4,q,  e4,s,  g4,q,  g4,q,  e4,e,  d4,e+h, R,q}; 

// Define Motors
AF_DCMotor leftMotor(4, MOTOR12_64KHZ);
AF_DCMotor rightMotor(3, MOTOR12_64KHZ);

// Define Sensors
int rightBumper = 2;
int leftBumper = 3;
int centerIR = 16;
int motion_sensor = 18;

// Define LEDS
int rightFrontLED = 15;  //digital pin 15, analog 1
int rightBackLED = 14;   //digital pin 14, analog 0
int leftFrontLED = 9;    //digital pin 9,
int leftBackLED = 10;    //digital pin 10,
int yellowLED = 17;

// Define stopwatch timers
unsigned long bumpTimer;
unsigned long stuckTimer;
unsigned long curiosityTimer;
unsigned long restTimer;

int min_bump_time = 2500;
int max_bump_number = 7;
int recent_bumps;
int max_no_obstacle_time = 12000;
int needs_rescue_time = 40000;

// Define logical connectors
int isOBSTACLE;
int isSTUCK_IN_LOOP;
int isSTUCK_ON_OBSTACLE;
int isTIME_UP;
int isNEED_RESCUE;

void setup() {
  //Play StartupTune!
  playGuitarTune();
  reset_restTime();
  reset_stuckTime();
  reset_curiosityTime();
  delay(2000);
  
  //Initialize Motors
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);

  //Initialize Serial Interface
  Serial.begin(9600);

  //Initialize LEDs
  initialize_LEDS();

  //Initialize Random Number Generator
  randomSeed(analogRead(5));
}

void loop() {
  // Check the obstacle sensors
  isOBSTACLE = 0;
  if (centerEye() == 0) {
    random_avoid();
    isOBSTACLE = 1;
  }
  if (leftBump() == 0) {
    left_avoid();
    isOBSTACLE = 1;
  }
  if (rightBump() == 0) {
    right_avoid();
    isOBSTACLE = 1;
  }
  
  // Meta sensing
  if (isOBSTACLE) {
    reset_stuckTime();
    if (bumpTime() < min_bump_time) {             //last bump recent?
      if (recent_bumps == max_bump_number - 1) {  //yes; many recent?  
        isSTUCK_IN_LOOP = 1;
        reset_bumpTime();
        recent_bumps = 0;
        //Serial.println("I'm stuck in a loop!");
      }
      else {                                      //not stuck yet; increment, and reset
        isSTUCK_IN_LOOP = 0;
        recent_bumps++;
        reset_bumpTime();
      }
    }
    else {                                        //no recent bumps, start bump counter, not stuck yet
      isSTUCK_IN_LOOP = 0;
      reset_bumpTime();
      recent_bumps = 1;
    }
  }
  else if (!isOBSTACLE) {
    if (stuckTime() > max_no_obstacle_time) {
      isSTUCK_ON_OBSTACLE = 1;
      //Serial.println("I'm stuck on something!");
    }
  }
  
  // Get out of loops or get unstuck
  if (isSTUCK_IN_LOOP) {
    halt();
    playStartupTune();
    delay(1500);
    random_avoid();
    isSTUCK_IN_LOOP = 0;
    reset_curiosityTime();
  }

  if (isSTUCK_ON_OBSTACLE) {
    halt();
    playDistressTune();
    delay(1500);
    freak_out();
    isSTUCK_ON_OBSTACLE = 0;
    reset_stuckTime();
    reset_curiosityTime();
  }
 
  // If things are too quiet for too long, do something new! (go crazy little robot!)
  if(curiosityTime() >= (random(20000) + 30000)) {
    driveBackward();
    delay(1500);
    if(random(3) == 1) {
      spinRight();
    }
    else {
      spinLeft();
    }
    int songNumber = random(3);
    if(songNumber == 0) {
      playGuitarTune();
    }
    else if(songNumber == 1) {
      playHappyBirthdayTune();
    }
    else if(songNumber == 2) {
      playDrunkSailorTune();
    }
    else {
      playScarboroughFairTune();
    }
    reset_curiosityTime();
    reset_stuckTime();
  }
  
  /*
  if (motion() == 1) {
    digitalWrite(yellowLED, HIGH);
  }
  else {
    digitalWrite(yellowLED, LOW);
  }
  */
  
  // All the time features
  driveForward();
}

// Functions!

// Obstacle Avoidance Functions
// general avoid behavior if left bumper is hit
void left_avoid()
{
  driveBackward();
  delay(350);
  spinLeft();
  delay(250);
  halt();
}

// general avoid behavior if right bumper is hit
void right_avoid()
{
  driveBackward();
  delay(350);
  spinRight();
  delay(250);
  halt();
}

// general avoid process if sonar sees obstacle (random turn avoid)
void random_avoid()
{
  driveBackward();
  delay(550);
  if(random(3) == 1) {
    spinRight();
  }
  else {
    spinLeft();
  }
  delay(random(500) + 0.15);
  halt();
}

// try to get unstuck from obstacles
void freak_out() {
  spinLeft();
  delay(250);
  spinRight();
  delay(250);
  spinLeft();
  delay(250);
  spinRight();
  delay(250);
  driveBackward();
  delay(500);
  
  spinLeft();
  delay(250);
  spinRight();
  delay(250);
  spinLeft();
  delay(250);
  spinRight();
  delay(250);
  driveBackward();
  delay(500);
  
  random_avoid();
}
  

// Motor Drive Functions
void driveForward() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void driveBackward() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(155);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

void spinLeft() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void spinRight() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

void halt() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(RELEASE);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(RELEASE);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, LOW);
}

//Sensor Functions
int leftBump() {
  return digitalRead(leftBumper);
}

int rightBump() {
  return digitalRead(rightBumper);
}

int centerEye() {
  return digitalRead(centerIR);
}

int motion() {
  return digitalRead(motion_sensor);
}

//LED functions
void initialize_LEDS() {
  pinMode(rightFrontLED, OUTPUT);
  pinMode(rightBackLED, OUTPUT);
  pinMode(leftFrontLED, OUTPUT);
  pinMode(leftBackLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
}

//Music functions
void playStartupTune() {
  speaker.playMusic(startupTune, sizeof(startupTune));
}

void playDistressTune() {
  speaker.playMusic(distressTune, sizeof(distressTune));
}

void playGuitarTune() {
  speaker.playMusic(guitarTune, sizeof(guitarTune));
}

void playHappyBirthdayTune() {
  speaker.playMusic(happyBirthdayTune, sizeof(happyBirthdayTune));
}

void playDrunkSailorTune() {
  speaker.playMusic(drunkSailorTune, sizeof(drunkSailorTune));
}

void playScarboroughFairTune() {
  speaker.playMusic(scarboroughFairTune, sizeof(scarboroughFairTune));
}

void play_NobodyKnowsTune() {
  speaker.playMusic(nobodyKnows_Tune, sizeof(nobodyKnows_Tune));
}

//Time/Timer (stopwatch) functions
void reset_bumpTime() {
  bumpTimer = millis();
}

unsigned long bumpTime() {
  return millis() - bumpTimer;
}

void reset_stuckTime() {
  stuckTimer = millis();
}

unsigned long stuckTime() {
  return millis() - stuckTimer;
}

void reset_curiosityTime() {
  curiosityTimer = millis();
}

unsigned long curiosityTime() {
  return millis() - curiosityTimer;
}

void reset_restTime() {
  restTimer = millis();
}

unsigned long restTime() {
  return millis() - restTimer;
}


