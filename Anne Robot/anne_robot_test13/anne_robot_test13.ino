#include <AFMotor.h>
#include <Music.h> 

// Built for Duemilanove 168

// Define Speaker Output pin and tunes
Music speaker = 7;

static const int startupTune[] = {300, g4,e,  a4,e,  b4,e,  c5,e,  g4,e, a4,e,  b4,e,  c5,e,  g4,e, a4,e,  b4,e,  c5,e};
static const int distressTune[] = {120,  d5,q,  d5,q,  d5,q,  d5,e,  c5,e,  d5,h};

static const int guitarTune[] = {200, B3,q,  d4,q,  g4,q,  B3,e,
                                 c4,q,  d4,q,  g4,q,  c4,e,
                                 d4,q,  d4,e,  g4,q,  d4,q,  d4,q,  d4,e,  g4,q,  d4,q,
                                 g4,q,  d4,q,  g4,q,  g4,e,  fs4,q,  d4,q,  g4,q,  fs4,e,
                                 a4,q,  d4,q,  g4,q,  a4,e,
                                 b4,q,  d4,q,  g4,q,  b4,q,
                               
                                 d5,q,  g4,q,  b4,q,  d5,e,
                                 c5,q,  g4,q,  b4,q,  c5,e,
                                 b4,q,  g4,q,  b4,q,  b4,e,
                                 a4,q,  g4,q,  b4,q,  g4,q};
                     
static const int happyBirthdayTune[] = {140, c4,e+s,  c4,s,  d4,q,  c4,q,  f4,q,  e4,h,  c4,e+s,  c4,s,  d4,q,  c4,q,  g4,q,  f4,h,
                                        c4,e+s,  c4,s,  c5,q,  a4,q,  f4,q,  e4,q,  d4,h,  bb4,e+s,  bb4,s,  a4,q,  f4,q,  g4,q, f4,h+q};
                                
static const int drunkSailorTune[] = {120, a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                                      a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  c4,e,  e4,e,  g4,e,
                                      a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                                      c4,e,  a4,e,  g4,e,  e4,e,  d4,q,  d4,q,
                                      a4,q,  a4,e+s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                                      g4,q,  g4,e+s,  g4,s,  g4,e,  c4,e,  e4,e,  g4,e,
                                      a4,q,  a4,e+s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                                      c5,e,  a4,e,  g4,e,  e4,e,  d4,q, d4,q};
                              
// Define Motors
AF_DCMotor leftMotor(4, MOTOR12_64KHZ);
AF_DCMotor rightMotor(3, MOTOR12_64KHZ);

// Define Sensors
static int rightBumper = 2;
static int leftBumper = 3;
static int centerIR = 16;
static int motion_sensor = 18;

// Define LEDS
static int rightFrontLED = 15;  //digital pin 15, analog 1
static int rightBackLED = 14;   //digital pin 14, analog 0
static int leftFrontLED = 9;    //digital pin 9,
static int leftBackLED = 10;    //digital pin 10,
static int yellowLED = 17;

// Define stopwatch timers
unsigned long bumpTimer;
unsigned long stuckTimer;
unsigned long curiosityTimer;
unsigned long restTimer;

static const int min_bump_time = 2500;
static const int max_bump_number = 7;
static int recent_bumps;
static const int max_no_obstacle_time = 12000;
static const unsigned long sleep_time = 180000;

// Define logical connectors
static int isOBSTACLE;
static int isSTUCK_IN_LOOP;
static int isSTUCK_ON_OBSTACLE;
static int isTIME_UP;
static int isNEED_RESCUE;

void setup() {
  //Play StartupTune!
  //playStartupTune();
  //playGuitarTune();
  //playHappyBirthdayTune();
  reset_restTime();
  reset_stuckTime();
  reset_curiosityTime();
  delay(2000);
  
  //Initialize Motors
  leftMotor.setSpeed(15);
  rightMotor.setSpeed(255);

  //Initialize Serial Interface
  Serial.begin(9600);

  //Initialize LEDs
  initialize_LEDS();

  //Initialize Random Number Generator
  randomSeed(analogRead(5));
}

void loop() {
  while(restTime()/10000 <= 12) {
    // Check the obstacle sensors
    isOBSTACLE = 0;
    if (centerEye() == 0) {
      random_avoid();
      isOBSTACLE = 1;
    }
    if (leftBump() == 0) {
      left_avoid();
      isOBSTACLE = 1;
    }
    if (rightBump() == 0) {
      right_avoid();
      isOBSTACLE = 1;
    }
    
    // Meta sensing
    if (isOBSTACLE) {
      reset_stuckTime();
      if (bumpTime() < min_bump_time) {             //last bump recent?
        if (recent_bumps == max_bump_number - 1) {  //yes; many recent?  
          isSTUCK_IN_LOOP = 1;
          reset_bumpTime();
          recent_bumps = 0;
          //Serial.println("I'm stuck in a loop!");
        }
        else {                                      //not stuck yet; increment, and reset
          isSTUCK_IN_LOOP = 0;
          recent_bumps++;
          reset_bumpTime();
        }
      }
      else {                                        //no recent bumps, start bump counter, not stuck yet
        isSTUCK_IN_LOOP = 0;
        reset_bumpTime();
        recent_bumps = 1;
      }
    }
    else if (!isOBSTACLE) {
      if (stuckTime() > max_no_obstacle_time) {
        isSTUCK_ON_OBSTACLE = 1;
        //Serial.println("I'm stuck on something!");
      }
    }
    
    // Get out of loops or get unstuck
    if (isSTUCK_IN_LOOP) {
      halt();
      playStartupTune();
      delay(1500);
      random_avoid();
      isSTUCK_IN_LOOP = 0;
      reset_curiosityTime();
    }
  
    if (isSTUCK_ON_OBSTACLE) {
      halt();
      playDistressTune();
      delay(1500);
      freak_out();
      isSTUCK_ON_OBSTACLE = 0;
      reset_stuckTime();
      reset_curiosityTime();
    }
   
    // If things are too quiet for too long, do something new! (go crazy little robot!)
    if(curiosityTime() >= (random(20000) + 30000)) {
      driveBackward();
      delay(1500);
      if(random(3) == 1) {
        spinRight();
      }
      else {
        spinLeft();
      }
      int songNumber = random(3);
      if(songNumber == 0) {
        playGuitarTune();
      }
      else if(songNumber == 1) {
        playHappyBirthdayTune();
      }
      else if(songNumber == 2) {
        playDrunkSailorTune();
      }
      reset_curiosityTime();
      reset_stuckTime();
    }
    // All the time feature
    driveForward();
  }
  
  //Time for Sleep
  halt();
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, HIGH);

  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, HIGH);
  delay(10000);
  
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, LOW);

  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, LOW);
  
  // Motion Sensor Wakes the bot up again!
  while(1) {
    if (motion() == 1) {
      digitalWrite(yellowLED, LOW);
      playStartupTune();
      reset_restTime();
      reset_stuckTime();
      reset_curiosityTime();
      break;
    }
    else {
      digitalWrite(yellowLED, HIGH);
    }
  }
}

// Functions!

// Obstacle Avoidance Functions
// general avoid behavior if left bumper is hit
static void left_avoid()
{
  driveBackward();
  delay(1550);
  spinLeft();
  delay(550);
  halt();
}

// general avoid behavior if right bumper is hit
static void right_avoid()
{
  driveBackward();
  delay(1550);
  spinRight();
  delay(550);
  halt();
}

// general avoid process if sonar sees obstacle (random turn avoid)
static void random_avoid()
{
  driveBackward();
  delay(1650);
  if(random(3) == 1) {
    spinRight();
  }
  else {
    spinLeft();
  }
  delay(random(500) + 450);
  halt();
}

// try to get unstuck from obstacles
static void freak_out() {
  spinLeft();
  delay(450);
  spinRight();
  delay(450);
  spinLeft();
  delay(450);
  spinRight();
  delay(450);
  driveBackward();
  delay(1200);
  
  spinLeft();
  delay(450);
  spinRight();
  delay(450);
  spinLeft();
  delay(450);
  spinRight();
  delay(450);
  driveBackward();
  delay(1200);
  
  random_avoid();
}
  

// Motor Drive Functions
static void driveForward() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

static void driveBackward() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(255);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

static void spinLeft() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(255);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

static void spinRight() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

static void halt() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(255);
  leftMotor.run(RELEASE);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(RELEASE);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, LOW);
}

//Sensor Functions
static int leftBump() {
  return digitalRead(leftBumper);
}

static int rightBump() {
  return digitalRead(rightBumper);
}

static int centerEye() {
  return digitalRead(centerIR);
}

static int motion() {
  return digitalRead(motion_sensor);
}

//LED functions
static void initialize_LEDS() {
  pinMode(rightFrontLED, OUTPUT);
  pinMode(rightBackLED, OUTPUT);
  pinMode(leftFrontLED, OUTPUT);
  pinMode(leftBackLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
}

//Music functions

static void playStartupTune() {
  speaker.playMusic((int*)startupTune, sizeof(startupTune));
}

static void playDistressTune() {
  speaker.playMusic((int*)distressTune, sizeof(distressTune));
}

static void playGuitarTune() {
  speaker.playMusic((int*)guitarTune, sizeof(guitarTune));
}

static void playHappyBirthdayTune() {
  speaker.playMusic((int*)happyBirthdayTune, sizeof(happyBirthdayTune));
}

static void playDrunkSailorTune() {
  speaker.playMusic((int*)drunkSailorTune, sizeof(drunkSailorTune));
}

//Time/Timer (stopwatch) functions
static void reset_bumpTime() {
  bumpTimer = millis();
}

static unsigned long bumpTime() {
  return millis() - bumpTimer;
}

static void reset_stuckTime() {
  stuckTimer = millis();
}

static unsigned long stuckTime() {
  return millis() - stuckTimer;
}

static void reset_curiosityTime() {
  curiosityTimer = millis();
}

static unsigned long curiosityTime() {
  return millis() - curiosityTimer;
}

static void reset_restTime() {
  restTimer = millis();
}

static unsigned long restTime() {
  return millis() - restTimer;
}
