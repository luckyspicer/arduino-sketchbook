int H1 = 0;
int H0 = 1;
int H2 = 2;
int H3 = 3;


void setup() {
  Serial.begin(9600);
  Serial.println("Anne's Robot is Go!");
}

void loop() {
  Serial.println("Forward Motor 1");
  digitalWrite(H2, LOW);
  digitalWrite(H1, HIGH);
  delay(3000);
  
  Serial.println("Stop Motor 1");
  digitalWrite(H2, LOW);
  digitalWrite(H1, LOW);
  delay(1000);
  
  Serial.println("Reverse Motor 1");
  digitalWrite(H1, LOW);
  digitalWrite(H2, HIGH);
  delay(3000);
  
  Serial.println("Stop Motor 1");
  digitalWrite(H2, LOW);
  digitalWrite(H1, LOW);
  delay(1000);
}
