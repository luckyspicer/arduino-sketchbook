/* Music Library Arduino Sketchbook Example
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element (or speakers) and your Arduino!
 */
 
#include <Music.h>    //including the library that this code exemplifies seems like a good idea

Music speaker = 7;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */

int guitarTune[] = {200, B3,q,  d4,q,  g4,q,  B3,e,
                     c4,q,  d4,q,  g4,q,  c4,e,
                     d4,q,  d4,e,  g4,q,  d4,q,  d4,q,  d4,e,  g4,q,  d4,q,
                     g4,q,  d4,q,  g4,q,  g4,e,  fs4,q,  d4,q,  g4,q,  fs4,e,
                     a4,q,  d4,q,  g4,q,  a4,e,
                     b4,q,  d4,q,  g4,q,  b4,q,
                   
                     d5,q,  g4,q,  b4,q,  d5,e,
                     c5,q,  g4,q,  b4,q,  c5,e,
                     b4,q,  g4,q,  b4,q,  b4,e,
                     a4,q,  g4,q,  b4,q,  g4,q};
                     
int happyBirthdayTune[] = {120, c4,e+s,  c4,s,  d4,q,  c4,q,  f4,q,  e4,h,  c4,e+s,  c4,s,  d4,q,  c4,q,  g4,q,  f4,h,
                                c4,e+s,  c4,s,  c5,q,  a4,q,  f4,q,  e4,q,  d4,h,  bb4,e+s,  bb4,s,  a4,q,  f4,q,  g4,q, f4,h+q};
                                
int drunkSailorTune[] = {120, a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                              a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  c4,e,  e4,e,  g4,e,
                              a4,e,  a4,s,  a4,s,  a4,e,  a4,s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                              c4,e,  a4,e,  g4,e,  e4,e,  d4,q,  d4,q,
                              a4,q,  a4,e+s,  a4,s,  a4,e,  d4,e,  f4,e,  a4,e,
                              g4,q,  g4,e+s,  g4,s,  g4,e,  c4,e,  e4,e,  g4,e,
                              a4,q,  a4,e+s,  a4,s,  a4,e,  b4,e,  c5,e,  d5,e,
                              c5,e,  a4,e,  g4,e,  e4,e,  d4,q, d4,q};

int scarboroughFairTune[] = {120, e4,h,  d4,q,  b4,h,  b4,q,  fs4,q+e,  g4,e,  fs4,q,  e4,h+q,  R,q,  b4,q,  d5,q,  e5,h,  d5,q,
                                   b4,q,  cs5,q,  a4,q,  b4,h+q,  R,h,  e5,q,  e5,h,  e5,q,  d5,h,  b4,q,  b4,q,  a4,q,  g4,q,
                                   fs4,e,  d4,e,  d4,h,  d4,h,  d4,h+q,  e4,h,  b4,q,  a4,h,  g4,q,  fs4,q,  e4,q,  d4,q,  e4,h+q};

int nobodyKnowsTune[] = {75,  b4,e,  d4,q,  e4,e,  g4,q+e,  a4,e,  b4,e,  b4,s,  b4,e+s,  b4,q+e,
                               R,q,  b4,e,  d4,q,  e4,s,  g4,q,  g4,q,  e4,e,  d4,e+h}; 

void setup()       //If you only want your arduino to play music, no further setup is required
{
}

void loop()
{
  playGuitarTune();      //nameOfYourPiezo.playMusic(nameOfYourSong[], sizeof(nameOfYourSong), this is all you need to make some music, Enjoy!
  delay(1000);
  playHappyBirthdayTune();
  delay(1000);
  playDrunkSailorTune();
  delay(1000);
  playScarboroughFairTune();
  delay(1000);
  playNobodyKnowsTune();
  delay(1000);
  
}

void playGuitarTune() {
  speaker.playMusic(guitarTune, sizeof(guitarTune));
}

void playHappyBirthdayTune() {
  speaker.playMusic(happyBirthdayTune, sizeof(happyBirthdayTune));
}

void playDrunkSailorTune() {
  speaker.playMusic(drunkSailorTune, sizeof(drunkSailorTune));
}

void playScarboroughFairTune() {
  speaker.playMusic(scarboroughFairTune, sizeof(scarboroughFairTune));
}

void playNobodyKnowsTune() {
  speaker.playMusic(nobodyKnowsTune, sizeof(nobodyKnowsTune));
}
