#include <AFMotor.h>

AF_DCMotor motor3(3, MOTOR12_64KHZ);
AF_DCMotor motor4(4, MOTOR12_64KHZ);

void setup() {
  motor3.setSpeed(255);
  motor4.setSpeed(255);
  
  Serial.begin(9600);
  
}

void loop() {
  Serial.println("Forward");
  motor3.run(FORWARD);
  digitalWrite(12, HIGH);
  digitalWrite(11, LOW);
  
  motor4.run(FORWARD);
  digitalWrite(10, HIGH);
  digitalWrite(9, LOW);

  delay(2000);
  
  Serial.println("Backward!");
  motor3.run(BACKWARD);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  
  motor4.run(BACKWARD);
  digitalWrite(10, LOW);
  digitalWrite(9, HIGH);
  
  delay(2000);
  
  Serial.println("Left!");
  motor3.run(BACKWARD);
  digitalWrite(12, LOW);
  digitalWrite(11, HIGH);
  
  motor4.run(FORWARD);
  digitalWrite(10, HIGH);
  digitalWrite(9, LOW);
  
  delay(1000);
  
  Serial.println("Right!");
  motor3.run(FORWARD);
  digitalWrite(12, HIGH);
  digitalWrite(11, LOW);
  
  motor4.run(BACKWARD);
  digitalWrite(10, LOW);
  digitalWrite(9, HIGH);
  
  delay(1000);
}
