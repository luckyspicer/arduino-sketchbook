/* Music Library Arduino Sketchbook Example
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element (or speakers) and your Arduino!
 */
 
#include <Music.h>    //including the library that this code exemplifies seems like a good idea

Music speaker = 7;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */
                              
int happyChirp1[] = {300,  g4,q,  d5,q,  b4,q};
int happyChirp2[] = {350,  a4,q,  a4,q,  g4,q, a4,q};
int happyChirp3[] = {300,  c5,q,  d5,q,  c5,e, a4,q}; 
int happyChirp4[] = {350,  e4,e,  e4,e,  e4,e, e4,e,  c4,q}; 
int happyChirp5[] = {300,  b4,q,  c5,q,  e4,q};

int happyChirp6[] = {300,  b4,q,  d5,q,  g4,q};
int happyChirp7[] = {350,  g4,q,  g4,q,  a4,q, g4,q};
int happyChirp8[] = {300,  a4,q,  c5,q,  c4,e, d5,q}; 
int happyChirp9[] = {350,  c4,e,  c4,e,  c4,e, c4,e,  e4,q}; 
int happyChirp10[] = {300,  e4,q,  c5,q,  b4,q}; 

int happyChirp11[] = {300,  bb4,h,  b4,q}; 
int happyChirp12[] = {300,  d5,h+q, d5,s, d5,s, d5,s, d5,q}; 
int happyChirp13[] = {300,  g4,W}; 

void setup()       //If you only want your arduino to play music, no further setup is required
{
}

void loop()
{ 
  playChirp();
  delay(2000); 
}

void playChirp1() {
  speaker.playMusic(happyChirp1, sizeof(happyChirp1));
}

void playChirp2() {
  speaker.playMusic(happyChirp2, sizeof(happyChirp2));
}

void playChirp3() {
  speaker.playMusic(happyChirp3, sizeof(happyChirp3));
}

void playChirp4() {
  speaker.playMusic(happyChirp4, sizeof(happyChirp4));
}

void playChirp5() {
  speaker.playMusic(happyChirp5, sizeof(happyChirp5));
}

void playChirp6() {
  speaker.playMusic(happyChirp6, sizeof(happyChirp6));
}

void playChirp7() {
  speaker.playMusic(happyChirp7, sizeof(happyChirp7));
}

void playChirp8() {
  speaker.playMusic(happyChirp8, sizeof(happyChirp8));
}

void playChirp9() {
  speaker.playMusic(happyChirp9, sizeof(happyChirp9));
}

void playChirp10() {
  speaker.playMusic(happyChirp10, sizeof(happyChirp10));
}

void playChirp11() {
  speaker.playMusic(happyChirp11, sizeof(happyChirp11));
}

void playChirp12() {
  speaker.playMusic(happyChirp12, sizeof(happyChirp12));
}

void playChirp13() {
  speaker.playMusic(happyChirp13, sizeof(happyChirp13));
}

void playChirp() {
  int chirpNumber = random(12);
  if (chirpNumber == 0) {
    playChirp1();
  }
  else if(chirpNumber == 1) {
    playChirp2();
  }
  else if(chirpNumber == 2) {
    playChirp3();
  }
  else if(chirpNumber == 3) {
    playChirp4();
  }
  else if(chirpNumber == 4) {
    playChirp5();
  }
  else if(chirpNumber == 5) {
    playChirp6();
  }
  else if(chirpNumber == 6) {
    playChirp7();
  }
  else if(chirpNumber == 7) {
    playChirp8();
  }
  else if(chirpNumber == 8) {
    playChirp9();
  }
  else if(chirpNumber == 9) {
    playChirp10();
  }
  else if(chirpNumber == 10) {
    playChirp11();
  }
  else if(chirpNumber == 11) {
    playChirp12();
  }
  else if(chirpNumber == 12) {
    playChirp13();
  }
}
