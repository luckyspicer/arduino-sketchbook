int motor0 = 0;
int motor1 = 1;

void setup() {
  Serial.begin(9600);
  Serial.println("Anne's Robot is Go!");
}

void loop() {
  
  Serial.println("Forward Motor 0");
  forward(motor0);
  
  Serial.println("Forward Motor 2");
  forward(motor1);
  
  delay(3000);
  
  Serial.println("Stop Motor 0");
  stop(motor0);
  
  Serial.println("Stop Motor 1");
  stop(motor1);
  
  delay(1000);
  
  Serial.println("Reverse Motor 0");
  backward(motor0);
  
  Serial.println("Reverse Motor 1");
  backward(motor1);
  
  delay(3000);
  
  Serial.println("Stop Motor 0");
  stop(motor0);
  
  Serial.println("Stop Motor 1");
  stop(motor1);
  delay(1000);
}

void forward(int motor) {
  if(motor == 0) {
    digitalWrite(3, LOW);
    digitalWrite(2, HIGH);
  }
  else if(motor == 1) {
    digitalWrite(5, LOW);
    digitalWrite(4, HIGH);
  }
}

void backward(int motor) {
  if(motor == 0) {
    digitalWrite(2, LOW);
    digitalWrite(3, HIGH);
  }
  else if(motor == 1) {
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);
  }
}

void stop(int motor) {
  if(motor == 0) {
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
  }
  else if(motor == 1) {
    digitalWrite(4, LOW);
    digitalWrite(5, LOW);
  }
}
