#include <AFMotor.h>

// Define Motors
AF_DCMotor leftMotor(4, MOTOR12_64KHZ);
AF_DCMotor rightMotor(3, MOTOR12_64KHZ);

// Define Sensors
int rightBumper = 2;
int leftBumper = 3;
int centerIR = 16;
int motion_sensor = 18;

// Define LEDS
int rightFrontLED = 15;  //digital pin 15, analog 1
int rightBackLED = 14;   //digital pin 14, analog 0
int leftFrontLED = 9;    //digital pin 9,
int leftBackLED = 10;    //digital pin 10,
int yellowLED = 17;

void setup() {
  //Initialize Motors
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);

  //Initialize Serial Interface
  Serial.begin(9600);

  //Initialize LEDs
  initialize_LEDS();

  //Initialize Random Number Generator
  randomSeed(analogRead(5));
}

void loop() {
  driveForward();
  if (centerEye() == 0) {
    IR_avoid();
  }
  if (leftBump() == 0) {
    left_avoid();
  }
  if (rightBump() == 0) {
    right_avoid();
  }
  if (motion() == 1) {
    digitalWrite(yellowLED, HIGH);
  }
  else {
    digitalWrite(yellowLED, LOW);
  }

}

// Functions!

// Obstacle Avoidance Functions
// general avoid behavior if left bumper is hit
void left_avoid()
{
  driveBackward();
  delay(350);
  spinRight();
  delay(250);
  halt();
}

// general avoid behavior if right bumper is hit
void right_avoid()
{
  driveBackward();
  delay(350);
  spinLeft();
  delay(250);
  halt();
}

// general avoid process if sonar sees obstacle
void IR_avoid()
{
  driveBackward();
  delay(550);
  if(random(2) == 1) {
    spinLeft();
  }
  else {
    spinRight();
  }
  delay(450);
  halt();
}

// Motor Drive Functions
void driveForward() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void driveBackward() {
  leftMotor.setSpeed(155);
  rightMotor.setSpeed(155);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

void spinLeft() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);

  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void spinRight() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

void halt() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  leftMotor.run(RELEASE);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, LOW);

  rightMotor.run(RELEASE);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, LOW);
}

//Sensor Functions
int leftBump() {
  return digitalRead(leftBumper);
}

int rightBump() {
  return digitalRead(rightBumper);
}

int centerEye() {
  return digitalRead(centerIR);
}

int motion() {
  return digitalRead(motion_sensor);
}

//LED functions
void initialize_LEDS() {
  pinMode(rightFrontLED, OUTPUT);
  pinMode(rightBackLED, OUTPUT);
  pinMode(leftFrontLED, OUTPUT);
  pinMode(leftBackLED, OUTPUT);
  pinMode(yellowLED, OUTPUT);
}

