#include <AFMotor.h>

// Define Motors
AF_DCMotor leftMotor(4, MOTOR12_64KHZ);
AF_DCMotor rightMotor(3, MOTOR12_64KHZ);

// Define LEDS
int rightFrontLED = 15;  //digital pin 15, analog 1
int rightBackLED = 14;   //digital pin 14, analog 0
int leftFrontLED = 9;    //digital pin 9,
int leftBackLED = 10;    //digital pin 10,

void setup() {
  leftMotor.setSpeed(255);
  rightMotor.setSpeed(255);
  
  Serial.begin(9600);

  initialize_LEDS();
}

void loop() {
  Serial.println("Forward");
  driveForward();
  delay(2000);
  
  Serial.println("Backward!");
  driveBackward();
  delay(2000);
  
  Serial.println("Left!");
  turnLeft();
  delay(1000);
  
  Serial.println("Right!");
  turnRight();
  delay(1000);
}

// Functions!

// Motor Drive Functions
void driveForward() {
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);
  
  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void driveBackward() {
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);
  
  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

void turnLeft() {
  leftMotor.run(BACKWARD);
  digitalWrite(leftFrontLED, LOW);
  digitalWrite(leftBackLED, HIGH);
  
  rightMotor.run(FORWARD);
  digitalWrite(rightFrontLED, HIGH);
  digitalWrite(rightBackLED, LOW);
}

void turnRight() {
  leftMotor.run(FORWARD);
  digitalWrite(leftFrontLED, HIGH);
  digitalWrite(leftBackLED, LOW);
  
  rightMotor.run(BACKWARD);
  digitalWrite(rightFrontLED, LOW);
  digitalWrite(rightBackLED, HIGH);
}

//LED functions
void initialize_LEDS() {
  pinMode(rightFrontLED, OUTPUT);
  pinMode(rightBackLED, OUTPUT);
  pinMode(leftFrontLED, OUTPUT);
  pinMode(leftBackLED, OUTPUT);
}
