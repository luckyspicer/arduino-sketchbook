/********************************
JPEG Camera Example
Luke Spicer 6/8/2011
Modified from:

JPEG Camera Example Sketch
The sketch will take a picture on the JPEG Serial Camera and store the jpeg to an SD card
on an SD Shield
Written by Ryan Owens
SparkFun Electronics
*********************************/

/********************************
Hardware Notes:
This sketch assumes the arduino has the microSD shield from SparkFun attached.
The camera Rx/Tx should be attached to pins 2 and 3.
********************************/

//This example requires the CameraUart and SD libraries
#include <CameraUart.h>
#include <SD.h>

//Create an instance of the camera
CameraUart camera;

void setup()
{
  //Setup the camera, serial port and memory card
  camera.begin();
  Serial.begin(9600);
    
  // Set up the SD card
  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
    
  //Reset the camera
  camera.reset();
  delay(3000);
  
  //camera.setImageSize(LOW);
  
  //Take a picture
  Serial.println("Taking a picture! The response follows");
  camera.takePicture();
  //Print the response to the 'TAKE_PICTURE' command.
  Serial.write((const uint8_t*)camera.getLastResponse(), camera.getLastResponseCount());
  Serial.println();
  
  //Get the size of the picture
  camera.readSize();
  //Print the size
  Serial.print("Size: ");
  Serial.println(camera.getSize());
  
  //Create a file called 'test.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  //The file has to be renamed to .jpg when copied to a computer.
  SD.remove("image.jpg");
  Serial.println("\nCreating image.jpg...");
  File imageFile = SD.open("image.jpg", FILE_WRITE);
  
  long timeSaveStart;
  timeSaveStart = millis();
  camera.savePicture(imageFile);
  /*
  if(!camera.savePicture(imageFile)) {
    Serial.println("Image could not be saved!");
  }
  else {
    Serial.println("The image, image.txt, was successfully saved to the SD card!");
  }
  */
  Serial.print("Time to save picture: ");
  Serial.print(millis() - timeSaveStart);
  Serial.println(" mSec");
  //Close the file
  imageFile.close();
  Serial.println("Closing image.jpg...");
  Serial.println("Done.");
}

void loop()
{

}
