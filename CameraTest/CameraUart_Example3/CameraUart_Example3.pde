/********************************
JPEG Camera Example
Luke Spicer 6/8/2011
Modified from:

JPEG Camera Example Sketch
The sketch will take a picture on the JPEG Serial Camera and store the jpeg to an SD card
on an SD Shield
Written by Ryan Owens
SparkFun Electronics
*********************************/

/********************************
Hardware Notes:
This sketch assumes the arduino has the microSD shield from SparkFun attached.
The camera Rx/Tx should be attached to pins 2 and 3.
********************************/

//This example requires the CameraUart and SD libraries
#include <CameraUart.h>
#include <SD.h>

//Create an instance of the camera
CameraUart camera;

//Create a String Object to hold the image filename
String imageFileString;
String imageNum;
char imageFileCharArray[13];
int i; // to keep track of the picture we're on

int LED = 31;
int button = 4;

void setup()
{
  //Setup the camera, serial port and memory card
  camera.begin();
  Serial.begin(9600);
    
  // Set up the SD card
  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
    
  //Reset the camera
  camera.reset();
  delay(3000);
}

void loop()
{
    while(digitalRead(button) == 1);
    if(digitalRead(button) == 0) {
      delay(20);
      while(digitalRead(button) != 1);
      Serial.println("Button Pushed! Now take a picture!");
    }
    
    //Take a picture
    Serial.println("Taking a picture! The response follows");
    camera.takePicture();
    
    //Get the size of the picture
    camera.readSize();
    //Print the size
    Serial.print("Size: ");
    Serial.println(camera.getSize());
    
    //Create a file called 'testi.txt' on the SD card.
    //NOTE: The memoryCard libary can only create text files.
    //The file has to be renamed to .jpg when copied to a computer.
    imageNum = String(i);
    imageFileString = String("image" + imageNum + ".jpg");
    imageFileString.toCharArray(imageFileCharArray, imageFileString.length()+1);
    
    SD.remove(imageFileCharArray);
    Serial.print("\nCreating ");
    Serial.print(imageFileCharArray);
    Serial.println("...");
    File imageFile = SD.open(imageFileCharArray, FILE_WRITE);
    
    long timeSaveStart;
    timeSaveStart = millis();
    camera.savePicture(imageFile);
    Serial.print("Time to save picture: ");
    Serial.print(millis() - timeSaveStart);
    Serial.println(" mSec");
    
    camera.stopPictures();
    
    //Close the file
    imageFile.close();
    Serial.print("Closing ");
    Serial.print(imageFileCharArray);
    Serial.println("...");
    Serial.println("Done.");
    i++;
}
