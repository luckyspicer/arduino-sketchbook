/********************************
JPEG Camera Example
Luke Spicer 6/8/2011
Modified from:

JPEG Camera Example Sketch
The sketch will take a picture on the JPEG Serial Camera and store the jpeg to an SD card
on an SD Shield
Written by Ryan Owens
SparkFun Electronics
*********************************/

/********************************
Hardware Notes:
This sketch assumes the arduino has the microSD shield from SparkFun attached.
The camera Rx/Tx should be attached to pins 2 and 3.
********************************/

//This example requires the CameraUart and SD libraries
#include <CameraUart.h>
#include <SD.h>

// Also include the VFD library so you can use that display for debugging
#include <VFD.h>

//Create an instance of the camera
CameraUart camera;

// Create an instance of the VFD object
VFD svfdisp2(SERIAL2);

//Create a String Object to hold the image filename
String imageFileString;
String imageNum;
char imageFileCharArray[13];
int i; // to keep track of the picture we're on

int LED = 31;
int button = 4;

void setup()
{
  // Setup the camera
  camera.begin();
  
  // Setup the serial VFD
  svfdisp2.clear();
  svfdisp2.brightness(3);
  svfdisp2.print(" VFD/Camera/SD Test");
  delay(1000); 
    
  //Clear display again for debug information  
  // Set up the SD card
  svfdisp2.newLine();
  svfdisp2.print("Init SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    svfdisp2.newLine();
    svfdisp2.print("initialization failed!");
    return;
  }
  svfdisp2.newLine();
  svfdisp2.print("Initialization done.");
    
  //Reset the camera
  camera.reset();
  delay(3000);
}

void loop()
{
  if(digitalRead(5) == 0) {
  while(1) {
    while(digitalRead(button) == 1);
    if(digitalRead(button) == 0) {
      delay(20);
      while(digitalRead(button) != 1);
      svfdisp2.clear();
      svfdisp2.print("Button Pushed!");
      svfdisp2.newLine();
      svfdisp2.print("Now take a picture!");
    }
    
    //Take a picture
    camera.takePicture();
    
    //Get the size of the picture
    camera.readSize();
    delay(1000);
    
    //Create a file called 'testi.txt' on the SD card.
    //NOTE: The memoryCard libary can only create text files.
    //The file has to be renamed to .jpg when copied to a computer.
    imageNum = String(i);
    imageFileString = String("image" + imageNum + ".jpg");
    imageFileString.toCharArray(imageFileCharArray, imageFileString.length()+1);
    
    // This while loop check for pictures already on the SD card and increments the index until
    // we have a unique filename, that is the next image number
    while (SD.exists(imageFileCharArray)) {
      i++;
      imageNum = String(i);
      imageFileString = String("image" + imageNum + ".jpg");
      imageFileString.toCharArray(imageFileCharArray, imageFileString.length()+1);
    }
    svfdisp2.clear();
    svfdisp2.print("Open  ");
    svfdisp2.print(imageFileCharArray);
    svfdisp2.println("...");
    File imageFile = SD.open(imageFileCharArray, FILE_WRITE);
    
    long timeSaveStart;
    timeSaveStart = millis();
    camera.savePicture(imageFile);
    svfdisp2.setCursor(0, 1);
    svfdisp2.print("Time to save ");
    svfdisp2.print(millis() - timeSaveStart);
    svfdisp2.print(" mS");
    
    camera.stopPictures();
    
    //Close the file
    imageFile.close();
    svfdisp2.setCursor(0, 2);
    svfdisp2.print("Close ");
    svfdisp2.print(imageFileCharArray);
    svfdisp2.print("...");
    svfdisp2.setCursor(0, 3);
    svfdisp2.print("Done");
    i++;
  }
  }
  else {
      svfdisp2.clear();
      svfdisp2.println("Datalogging! Comma separated file");
      svfdisp2.setCursor(0,1);
      svfdisp2.println("Create File to write log to...");
      
      SD.remove("logFile1.csv");
      File lightLogFile_1;
      
      int lightLevel;
      long timeKeeper;
      long timer = millis();
    while(1) {
        timeKeeper = millis();
        if(timeKeeper >= (timer + 100)) {
          lightLogFile_1 = SD.open("logFile1.csv", FILE_WRITE);
          lightLevel = analogRead(0);

          lightLogFile_1.print(lightLevel);
          lightLogFile_1.println(",");
          lightLogFile_1.close();
     
          timer = timeKeeper; 
       }
    } 
  }
}
