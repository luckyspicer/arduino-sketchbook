#include <SD.h>

void setup()
{
  //Setup the serial port and memory card
  Serial.begin(9600);
    
  // Set up the SD card
  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  //Create a file called 'test.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  //The file has to be renamed to .jpg when copied to a computer.
  SD.remove("tester.txt");
  Serial.println("\nCreating tester.txt...");
  File imageFile = SD.open("tester.txt", FILE_WRITE);
  
  long timePrintStart;
  long timePrintEnd;
  uint8_t printVal = 1;
  
  uint8_t printArray[1024];
  for(int i = 0; i < 1024; i++) {
    printArray[i] = 'A';
  }
 
 timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 32);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 32 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 64);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 64 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 128);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 128 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 256);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 256 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 512);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 512 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  timePrintStart = millis();
 for(int i = 0; i < 115; i++) {
   imageFile.write(printArray, 1024);
 }
  timePrintEnd = millis();
  Serial.print("Time to write 115 bunches 1024 byte buffers: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  //Close the file
  imageFile.close();
  Serial.println("Closing tester.txt...");
  Serial.println("Done.");
  
  while(1);
}

void loop()
{

}
