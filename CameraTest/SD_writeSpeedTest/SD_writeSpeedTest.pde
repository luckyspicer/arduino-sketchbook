#include <SD.h>

void setup()
{
  //Setup the serial port and memory card
  Serial.begin(9600);
    
  // Set up the SD card
  Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  //Create a file called 'test.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  //The file has to be renamed to .jpg when copied to a computer.
  SD.remove("tester.txt");
  Serial.println("\nCreating tester.txt...");
  File imageFile = SD.open("tester.txt", FILE_WRITE);
  
  long timePrintStart;
  long timePrintEnd;
  uint8_t printVal = 1;
  timePrintStart = millis();
  imageFile.print(printVal);
  timePrintEnd = millis();
  Serial.print("Time to print single val: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  printVal = 2;
  timePrintStart = millis();
  imageFile.print(printVal, BYTE);
  timePrintEnd = millis();
  Serial.print("Time to print single val as BYTE: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  printVal = 3;
  timePrintStart = millis();
  imageFile.write(printVal);
  timePrintEnd = millis();
  Serial.print("Time to write single val: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  printVal = 4;
  timePrintStart = millis();
  for(int i = 0; i < 100; i++) {
    imageFile.print(printVal);
  }
  timePrintEnd = millis();
  Serial.print("Time to print 100 values: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  printVal = 5;
  timePrintStart = millis();
  for(int i = 0; i < 100; i++) {
    imageFile.print(printVal, BYTE);
  }
  timePrintEnd = millis();
  Serial.print("Time to print 100 values as BYTES: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  printVal = 6;
  timePrintStart = millis();
  for(int i = 0; i < 100; i++) {
    imageFile.write(printVal);
  }
  timePrintEnd = millis();
  Serial.print("Time to write 100 values using For Loop: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  uint8_t printArray[5000];
  for(int i = 0; i < 100; i++) {
    printArray[i] = 7;
  }
  timePrintStart = millis();
  imageFile.write(printArray, 100);
  timePrintEnd = millis();
  Serial.print("Time to write 100 values as using buffer: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  /*printArray[256];
  for(int i = 0; i < 256; i++) {
    printArray[i] = 7;
  }*/
  timePrintStart = millis();
  imageFile.write(printArray, 256);
  timePrintEnd = millis();
  Serial.print("Time to write 256 values as using buffer: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  /*printArray[500];
  for(int i = 0; i < 500; i++) {
    printArray[i] = 7;
  }*/
  timePrintStart = millis();
  imageFile.write(printArray, 500);
  timePrintEnd = millis();
  Serial.print("Time to write 500 values as using buffer: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  /*printArray[1024];
  for(int i = 0; i < 1024; i++) {
    printArray[i] = 7;
  }*/
  timePrintStart = millis();
  imageFile.write(printArray, 1024);
  timePrintEnd = millis();
  Serial.print("Time to write 1024 values as using buffer: ");
  Serial.print(timePrintEnd - timePrintStart);
  Serial.println(" mSec\n");
  
  //Close the file
  imageFile.close();
  Serial.println("Closing tester.txt...");
  Serial.println("Done.");
  
  while(1);
}

void loop()
{

}
