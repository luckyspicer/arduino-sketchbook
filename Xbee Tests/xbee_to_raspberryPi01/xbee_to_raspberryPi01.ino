static uint32_t timer = 0;
static char rxBuff[64];
void setup()
{
  Serial.begin(115200);
  Serial3.begin(38400);
  Serial.println("Hello World, I am arduino!");
  Serial3.println("Hello Raspberry Pi, I am Arduino!");
  timer = millis();
}

void loop()
{
  Serial3.print("Time: ");
  Serial3.println(millis());
  while ((millis() - timer) < 1000)
  {
    int bytesAvailable = Serial3.available();
    if (bytesAvailable > 0)
    {
      int bytesRead = Serial3.readBytesUntil(13, rxBuff, bytesAvailable);
      rxBuff[bytesRead] = '\0';
      Serial.print(rxBuff);
    }
  }
  timer = millis();
} 
