// Lucas Spicer, Spicer Robotics
// September 11th, 2010
// Motor Test 5, Obstacle Avoider "Oscar"
// Attempting 2 front bumpers, sonar and IR obstacle avoidance, 20x2 LCD Display, PhotoResistorEyes and ReadAnalogSensor() function

#include <AFMotor.h>
#include <LiquidCrystal.h>
#include <PID_v1.h>

AF_DCMotor motorLeft(1, MOTOR12_64KHZ);
AF_DCMotor motorRight(2, MOTOR12_64KHZ);

const int MAX_SPEED = 100;

// pin for pingSensor
int pingSensor = 24;

// Define Variables we'll be connecting the PID to
double SonarRange, SetDistance, MotorPower;

PID simplePID(&SonarRange, &MotorPower, &SetDistance, 5, 10, 3, REVERSE);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("PID Test");

  // turn on motor
  motorLeft.setSpeed(MAX_SPEED);
  motorRight.setSpeed(MAX_SPEED);
 
  motorLeft.run(RELEASE);
  motorRight.run(RELEASE);
  
  SetDistance = 10;
  simplePID.SetOutputLimits(-255, 255);
  simplePID.SetMode(AUTOMATIC);
}

void loop() {
  // Read all the sensors! SENSE
  SonarRange = sonarRange();
  
  simplePID.Compute();
  
  drive(MotorPower);
  
  Serial.print("MotorPower: ");
  Serial.println(MotorPower);
  //delay(100);
  /*
  // Print some sensor readings to the LCD and the serial interface
  Serial.print("Sonar Range Value is: ");
  Serial.println(SonarRange);
  delay(250);
  */
}

/* ******************************
* Movement algorithms
*********************************/



/* ******************************
* Driving Functions
*********************************/

/* DRIVE, REVERSE, STOP */
void drive(int strength) {
  //motorLeft.run(RELEASE);
  //motorRight.run(RELEASE);
  
  if (strength < 0) {
    motorLeft.setSpeed(-strength + 75);
    motorRight.setSpeed(-strength + 75);
    motorLeft.run(BACKWARD);
    motorRight.run(BACKWARD);
  }
  else if(strength == 0) {
    motorLeft.setSpeed(0);
    motorRight.setSpeed(0);
    motorLeft.run(RELEASE);
    motorRight.run(RELEASE);
  }
  else if (strength > 0) {
    motorLeft.setSpeed(strength + 75);
    motorRight.setSpeed(strength + 75);
    motorLeft.run(FORWARD);
    motorRight.run(FORWARD);
  }
}

/* ******************************
* Sonar Functions
*********************************/

long sonarRange() {
  // establish variables for duration of the ping, 
  // and the distance result in inches and centimeters:
  long duration, inches, cm;
  duration = pingSonar(pingSensor);
  
  // convert the time into a distance
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);
  
  return cm;
  
//  Serial.print(inches);
//  Serial.print("in, ");
//  Serial.print(cm);
//  Serial.print("cm");
//  Serial.println();
}

long pingSonar(int pingPin) {
  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);
  
  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  return pulseIn(pingPin, HIGH);
}

long microsecondsToInches(long microseconds)
{
  // According to Parallax's datasheet for the PING))), there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

