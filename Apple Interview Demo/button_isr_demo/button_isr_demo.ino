static const int button = 18;
static const int green_led = 19;
static const int DEBOUNCE_TIME_MS = 100;

static void button_isr(void);
static int push_counter = 0;
static int button_state = 0;
static int button_timer = 0;

void setup()
{
  Serial.begin(115200);
  Serial.println("Begin button isr demo");
  
  pinMode(button, INPUT_PULLUP);
  pinMode(green_led, OUTPUT);
  attachInterrupt(5, button_isr, FALLING);
}

void loop()
{
  if ((button_timer != 0) && ((millis() - button_timer) > DEBOUNCE_TIME_MS) && !digitalRead(button))
  {
    push_counter++;
    button_state = !button_state;
    Serial.print("push_counter = ");
    Serial.print(push_counter);
    Serial.print(" state = ");
    Serial.println(button_state);
    digitalWrite(green_led, button_state);
    button_timer = 0;
  }
}

static void button_isr(void)
{
  button_timer = millis();
}
