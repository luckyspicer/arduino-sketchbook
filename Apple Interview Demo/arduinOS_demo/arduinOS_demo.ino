#include "Arduino.h"
#include "DuinOS/DuinOS.h"

static const int GRNLED = 19;
static const int HEARTBEAT = 13;

static void vGreen(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    Serial.println(F("Green On"));
    digitalWrite(GRNLED, LOW);
    vTaskDelay( 500 );
    Serial.println(F("Green Off"));
    digitalWrite(GRNLED, HIGH);
    vTaskDelay( 500 );
  }
}

void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Arduino FreeRTOS Test"));
  pinMode(GRNLED, OUTPUT);
  pinMode(HEARTBEAT, OUTPUT);
  xTaskCreate( Heartbeat, ( signed char * ) "Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  xTaskCreate( vGreen, ( signed char * ) "Green", configMINIMAL_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  vTaskStartScheduler(); 
}

void loop()
{
}
