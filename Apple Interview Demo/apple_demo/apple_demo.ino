#include <Arduino.h>
#include <DuinOS/DuinOS.h>
#include <LiquidCrystal_arduinOS.h>
#include <VFD.h>  //including VFD.h allows access to the special vfd library functions
#include <avr/pgmspace.h> //including avar/pgmspace.h allows static array (like for images) to be saved in flash memory instead of RAM
#include <InterruptConstants.h>
#include <Ping.h>

static const int GRNLED = 19;
static const int HEARTBEAT = 13;
static const int configSTANDARD_STACK_SIZE = 256;
static const char* VERSION_STR = "Apple Demo v1.2";

/********************/
/***** 20x2 LCD *****/
/********************/
// initialize the library with the numbers of the interface pins
//LiquidCrystal lcd(rs [orange], R/W [blue], E [green], d4 [red], d5 [gray], d6 [yellow] ,d7 [orange])
// also the single yellow line by itself is the contrast pin!
static LiquidCrystal lcd_20x2(40, 38, 36, 34, 32, 30, 28);
static Ping sonar(InterruptConstants::Interrupt_Three);
static const char* hello_apple_str = " Hello, Apple!";
static const char* lcd_20x2_line2 = "Up:    Range:     cm";
static const byte apple[8] = {
  B00010,
  B00100,
  B01010,
  B11111,
  B11110,
  B11111,
  B01010,
};

static const byte smiley[8] = {
  B00000,
  B01010,
  B00000,
  B11111,
  B10001,
  B01110,
  B00000,
};

static void setup_lcd_20x2(void)
{
  Serial.println("setup_lcd_20x2");
  lcd_20x2.createChar(0, (byte*)apple);
  lcd_20x2.createChar(1, (byte*)smiley);
  // set up the LCD's number of rows and columns: 
  lcd_20x2.begin(20, 2);
  lcd_20x2.clear();
  lcd_20x2.home();
  lcd_20x2.print(VERSION_STR);
}

static void vtask_lcd_20x2(void *pvParameters){
  (void) pvParameters;
  static int first_time = 1;
  static long int range_sum = 0;
  static int range_counter = 0;
  while(1)
  {
    if (first_time)
    {
      lcd_20x2.clear();
      lcd_20x2.setCursor(2, 0);
      // Print a message to the LCD.
      lcd_20x2.write(byte(1));
      lcd_20x2.print(hello_apple_str);
      lcd_20x2.write(byte(0));
      lcd_20x2.setCursor(0, 1);
      lcd_20x2.print(lcd_20x2_line2);
      first_time = 0;
    }
    // Get Sonar Range
    sonar.SendPing(Ping::StartPing);
    while(sonar.GetPingState() != Ping::Done)
    {
      vTaskDelay(1);
    }
    range_sum += (int)Ping::uSecondsToCM(sonar.GetPulseWidth());
    range_counter++;
    if (range_counter >= 10)
    {
      lcd_20x2.setCursor(3, 1);
      // print the number of seconds since reset:
      lcd_20x2.print(millis()/1000);
      lcd_20x2.setCursor(13, 1);
      lcd_20x2.print(F("     "));
      lcd_20x2.setCursor(13, 1);
      lcd_20x2.print(range_sum / 10);
      range_counter = 0;
      range_sum = 0;
    }
    vTaskDelay(100);
  }
}

/********************/
/***** 20x4 LCD *****/
/********************/
// initialize the library with the numbers of the interface pins
// rs, enable, d0, d1, d2, d3
static LiquidCrystal lcd_20x4(26, 27, 43, 45, 47, 49);
static int apple_quote_idx = 0;
struct apple_quote_s {
  const char* apple_quote;
  const char* apple_quoter;
  int char_col;
  int char_row;
};
static const struct apple_quote_s apple_quotes[] = {
  {"Byte into an Apple", "-Apple, late 1970's", 0, 2},
  {"Design is not just what it looks like and feels like. Design is how it works.",  "--Steve Jobs", 3, 1},
  {"Simplicity is the Ultimate Sophistication", "-Apple, 1970/80's", 1, 2},
  {"Innovation distinguishes between a leader and a follower.", "--Steve Jobs", 3, 1},
  {"The Computer for the rest of us", "-Apple, 1984", 3, 2},
  {"Quality is much better than quantity. 1 home run is much better than 2 doubles.", "--Steve Jobs", 3, 1},
  {"Think different", "-Apple, 1997-2002", 2, 2},
  {"Stay hungry, stay foolish", "--Steve Jobs", 3, 1}
};

static void setup_lcd_20x4(void)
{
  Serial.println("setup_lcd_20x4");
  
  randomSeed(analogRead(0));
  apple_quote_idx = random(0, (sizeof(apple_quotes)/sizeof(apple_quotes[0])));
  // set up the LCD's number of rows and columns: 
  lcd_20x4.begin(20, 4);
  lcd_20x4.clear();
  lcd_20x4.home();
  lcd_20x4.print(VERSION_STR);
}

static void display_next_apple_quote(void)
{
  // Display the apple quote
  lcd_20x4.clear();
  lcd_20x4.home();
  lcd_20x4.cursor();
  for(int i = 0; i < strlen(apple_quotes[apple_quote_idx].apple_quote); i++)
  {
    if (((i % 20) == 0) && i < 80)
    {
      lcd_20x4.setCursor(0, i / 20);
    }
    lcd_20x4.write(*(apple_quotes[apple_quote_idx].apple_quote + i));
    vTaskDelay(150);
  }
  vTaskDelay(2000);
  lcd_20x4.clear();
  lcd_20x4.noCursor();
  lcd_20x4.setCursor(apple_quotes[apple_quote_idx].char_col, apple_quotes[apple_quote_idx].char_row);
  lcd_20x4.print(apple_quotes[apple_quote_idx].apple_quoter);
  vTaskDelay(2500);
  
  // Advance to next apple quote
  apple_quote_idx++;
  if (apple_quote_idx >= (sizeof(apple_quotes)/sizeof(apple_quotes[0])))
  {
    apple_quote_idx = 0;
  }
}

static void vtask_lcd_20x4(void *pvParameters){
  (void) pvParameters;
  while(1)
  {
    display_next_apple_quote();
  }
}

/********************/
/******** VFD *******/
/********************/
// Create a new instance of the VFD object, this one is named vfdisp, but choose whatever you want
// The constructor needs the pin numbers of the pins: (wr, rd, d0, d1, d2, d3, d4, d5, d6, d7)
// The pins are numbered 5 through 14 on the VFD pcb and spec sheet
VFD vfdisp(PARALLEL, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
// buffer for bitmap pixel data stored in flash memory
static uint8_t vfd_imageBuffer[560];
static void imageFromFlash(uint8_t buffer[], uint8_t image[], uint8_t x, uint8_t y);
static void setup_vfd(void)
{
  Serial.println("setup_vfd");
  vfdisp.clear();
  vfdisp.print(VERSION_STR);
}

static void vtask_vfd(void *pvParameters){
  (void) pvParameters;
  while(1)
  {
    vfd_loop();
  }
}

/********************/
/***** Button *******/
/********************/
static const int button = 18;
static const int DEBOUNCE_TIME_MS = 100;
static void button_isr(void);
static int push_counter = 0;
static const int t2s_led = 19;
static int t2s_state = 0;
static long int button_timer = 0;
#define emicSerial Serial3
static char t2s_buff[1024];

static void button_isr(void)
{
  button_timer = millis();
}

static void setup_button(void)
{
  Serial.println("setup_button");
  pinMode(button, INPUT_PULLUP);
  attachInterrupt(5, button_isr, FALLING);
}

static void vtask_button(void *pvParameters){
  (void) pvParameters;
  while(1)
  {
    if ((button_timer != 0) && ((millis() - button_timer) > DEBOUNCE_TIME_MS) && !digitalRead(button))
    {
      push_counter++;
      t2s_state = !t2s_state;
      button_timer = 0;
      if (t2s_state)
      {
        // Stop speaking
        emicSerial.print('X');
        Serial.println("Enter text for me to speak:");
      }
      else
      {
        Serial.println("I'll tell you about Apple now");
      }
    }
    digitalWrite(t2s_led, t2s_state);
    vTaskDelay(50);
  }
}

/********************/
/*** Text2Speech ****/
/********************/
static const char* apple_facts_str[] = {
"Apple Incorporated is an American multi-national corporation headquartered in Cupertino California.",
"Apple Inc designs, develops, and sells consumer electronics, computer software, online services, and personal computers.",
"Its best-known hardware products are the Mack line of computers, the eye-Pod media player, the eye-Phone smart-phone, and the eye-Pad tablet computer.",
"Its online services include eye-Cloud, eye-Tunes Store, and App Store.",
"Apple's consumer software includes the Oh-S X and eye-oh-s operating systems, the eye-Tunes media browser, the Suhfaree web browser, and the eye-Life and eye-Work creativity and productivity suites.",
"Apple was founded by Steve Jobs, Steve Wozniak, and Ronald Wayne, on April 1, 1976, to develop and sell personal computers.",
"It was incorporated as Apple Computer Inc, on January 3, 1977, and was renamed as Apple Inc on January 9, 2007, to reflect its shifted focus towards consumer electronics.",
"Apple is the world's second-largest information technology company by revenue after Samsung Electronics, and the world's third-largest mobile phone maker.",
"On November 25, 2014, in addition to being the largest publicly traded corporation in the world by market capitalization, Apple became the first US company to be valued at over 700 billion dollars.",
"As of 2014, Apple employs 72,800 permanent full-time employees, and maintains 437 retail stores in fifteen countries.",
"Apple also operates the online Apple Store and eye-Tunes Store, the latter of which is the world's largest music retailer.",
"Apple's worldwide annual revenue in 2014 totaled 182 billion dollars.",
"Apple enjoys a high level of brand loyalty and according to the 2014 edition of the Interbrand Best Global Brands report, is the world's most valuable brand with a valuation of 118.9 billion dollars."};

static void speak_next_apple_fact(void)
{
  static int apple_facts_idx = 0;
  Serial.println(apple_facts_str[apple_facts_idx]);
  speak_string(apple_facts_str[apple_facts_idx]);
  if(!t2s_state)
  {
    apple_facts_idx++;
  }
  if (apple_facts_idx >= (sizeof(apple_facts_str)/sizeof(apple_facts_str[0])))
  {
    apple_facts_idx = 0;
  }
}

static void setup_text2speech(void)
{
  Serial.println("setup_text2speech");
  pinMode(t2s_led, OUTPUT);
  // set the data rate for the SoftwareSerial port
  emicSerial.begin(9600);
  /*
    When the Emic 2 powers on, it takes about 3 seconds for it to successfully
    intialize. It then sends a ":" character to indicate it's ready to accept
    commands. If the Emic 2 is already initialized, a CR will also cause it
    to send a ":"
  */
  emicSerial.print('\n');             // Send a CR in case the system is already up
  while (emicSerial.read() != ':');   // When the Emic 2 has initialized and is ready, it will send a single ':' character, so wait here until we receive it
  delay(10);                          // Short delay
  emicSerial.flush();                 // Flush the receive buffer
}

void static speak_string(const char* text)
{
  // Speak some text
  emicSerial.print('S');
  emicSerial.print(text);  // Send the desired string to convert to speech
  emicSerial.print('\n');
  while(emicSerial.read() != ':')     // Wait here until the Emic 2 responds with a ":" indicating it's ready to accept the next command
  {
    vTaskDelay(100);
  }
}

static void vtask_text2speech(void *pvParameters){
  (void) pvParameters;
  while(1)
  {
    if(t2s_state == 0)
    {
      speak_next_apple_fact();
      vTaskDelay(1000); // Debug, replace with apple facts
    }
    else if (Serial.available() > 0)
    {
      int num_bytes = Serial.readBytesUntil('\n', t2s_buff, 1023);
      t2s_buff[num_bytes] = '\0';
      Serial.println(t2s_buff);
      speak_string(t2s_buff);
    }
    vTaskDelay(25);
  }
}

/********************/
/**** Heartbeat *****/
/********************/
void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void setup()
{
  // Serial Port Initialization
  Serial.begin(115200);
  Serial.println(VERSION_STR);
  
  // LCD_20x2 Init
  setup_lcd_20x2();
  // LCD_20x4 Init
  setup_lcd_20x4();
  // VFD Init
  setup_vfd();
  // Button Init
  setup_button();
  // Text-2-Speech Init
  setup_text2speech();
  delay(3000);
  Serial.println("Done with setup");
  
  // Create the OS tasks
  pinMode(HEARTBEAT, OUTPUT);
  xTaskCreate(Heartbeat, (signed char*)"Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  xTaskCreate(vtask_lcd_20x2, (signed char*)"lcd_20x2", configSTANDARD_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate(vtask_lcd_20x4, (signed char*)"lcd_20x4", configSTANDARD_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate(vtask_vfd, (signed char*)"vfd", configSTANDARD_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate(vtask_button, (signed char*)"button", configSTANDARD_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate(vtask_text2speech, (signed char*)"text2speech", configSTANDARD_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  
  // Start the OS (task scheduler)
  Serial.println("Starting vTaskStartScheduler\n");
  LiquidCrystal::set_os_running_flag();
  vTaskStartScheduler(); 
}

// Never gets executed once the task scheduler starts
void loop()
{
}
