#include <InterruptConstants.h>
#include <Ping.h>

Ping sonar(InterruptConstants::Interrupt_Three);

void setup()
{
  Serial.begin(115200);
  Serial.println("Begin PING library demo");
}

void loop()
{
  sonar.SendPing(Ping::StartPing);
  while(sonar.GetPingState() != Ping::Done)
  {
    delay(Ping::PulseMin_us);
  }
  uint32_t pulse_width = sonar.GetPulseWidth();
  Serial.print("Time: ");
  Serial.print(pulse_width);
  Serial.print(" uSec ");
  Serial.print("Distance: ");
  Serial.print(Ping::uSecondsToCM(pulse_width));
  Serial.println(" cm");
  delay(900);
}
