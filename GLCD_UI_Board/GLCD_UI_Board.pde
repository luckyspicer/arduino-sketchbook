// Local declarations
static unsigned long UITimer = 0;
static const unsigned long UIDebounceTime_ms = 50;
static int UIInterruptPin = 19;
static unsigned int buttonPressTimes[5];
static int buttonNum = 0;
typedef enum {
	LeftButton		= 38,
	UpButton		= 39,
	DownButton		= 40,
	RightButton		= 41,
	SelectButton	= 43
} UIButtonPins_t;

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Start of test of the GLCD UI Board 03!"));
  //pinMode(UIInterruptPin, INPUT);
  attachInterrupt(4, UIBoard_ISR, FALLING);
}

void loop()
{
	if (IsUIBoardPressed())
	{
		if (!digitalRead(UpButton)) {
			Serial.print(F("Upbutton "));
			buttonNum = 0;
		} else if (!digitalRead(LeftButton)) {
			Serial.print(F("LeftButton "));
			buttonNum = 1;
		} else if (!digitalRead(RightButton)) {
			Serial.print(F("RightButton "));
			buttonNum = 2;
		} else if (!digitalRead(DownButton)) {
			Serial.print(F("DownButton "));
			buttonNum = 3;
		} else if (!digitalRead(SelectButton)) {
			Serial.print(F("SelectButton "));
			buttonNum = 4;
		}
		Serial.print(F("Pressed "));
		Serial.print(++buttonPressTimes[buttonNum]);
		Serial.println(F(" times!"));
	}
}


void UIBoard_ISR (void) 
{
	UITimer = millis();
}

// returns true if key pressed
boolean IsUIBoardPressed()
{
  if ((UITimer != 0) && ((millis() - UITimer) > UIDebounceTime_ms) && !digitalRead(UIInterruptPin))
  {
    UITimer = 0;
    return true;
  }
  return false;
}