#include <ks0108.h>
//#include "Arial14.h"         // proportional font
//#include "SystemFont5x7.h"   // system font
#include "baskerville.h"     // homemade big font
#include "cardinal.h"

//////////////////////////////////// SETUP
void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  
}

/////////////////////////////////// LOOP
void loop() {
  int i;
  /*for(i = 0; i < 10; i++)
  {
    victimDisplay(i, i, i, i);
    delay(2000);
  }*/
  
  GLCD.ClearScreen();
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
  while(1);
}

void victimDisplay(int room, int x, int y, int status)
{
  GLCD.ClearScreen();
  GLCD.GotoXY(0, 7);
  GLCD.SelectFont(baskerville);
  GLCD.PrintNumber(room);
  GLCD.GotoXY(33, 7);
  GLCD.PrintNumber(x);
  GLCD.GotoXY(66, 7);
  GLCD.PrintNumber(y);
  GLCD.GotoXY(99, 7);
  GLCD.PrintNumber(status);
}
