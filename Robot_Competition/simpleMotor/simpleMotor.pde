#include <AFMotor.h>

//Create the Motor Devices!
AF_DCMotor motor1(4, MOTOR12_64KHZ);
AF_DCMotor motor3(3, MOTOR12_64KHZ);


void setup() {
  maxSpeed(255);
  
  Serial.begin(9600);
}

void loop() {
  Serial.println("This is a test");
  
  forward();
  delay(7000);
  
  backward();
  delay(5000);
  
  left();
  delay(3000);
  
  right();
  delay(3000);                                             
}

void maxSpeed(int speed) {
  motor1.setSpeed(speed);
  motor3.setSpeed(speed);
}

void forward() {
  motor1.run(FORWARD);
  motor3.run(FORWARD);
}

void backward() {
  motor1.run(BACKWARD);
  motor3.run(BACKWARD);
}

void left() {
  motor1.run(BACKWARD);
  motor3.run(FORWARD);
}

void right() {
  motor1.run(FORWARD);
  motor3.run(BACKWARD);
}
