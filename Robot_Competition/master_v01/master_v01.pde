//************ INCLUDES ***************//
//Display
#include <ks0108.h>
#include "SystemFont5x7.h"   // system font
#include "baskerville.h"     // homemade big font
#include "cardinal.h"

//Audio
#include "WaveHC.h"
#include "WaveUtil.h"

//Motors
#include <AFMotor.h>
//************ VARIABLES ***************//
//Audio
char filename[13];

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
FatReader file;   // This object represent the WAV file for a pi digit or period
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
/*
 * Define macro to put error messages in flash memory
 */
#define error(msg) error_P(PSTR(msg))

//Motor
AF_DCMotor motorL(1, MOTOR12_64KHZ);
AF_DCMotor motorR(3, MOTOR12_64KHZ);

//Sensors
int bottom = 16;
int top = 15;

int left_bump = 19;
int right_bump = 18;

int L1 = 10;
int L2 = 12;
int R1 = 9;
int R2 = 14;

int back = 13;

//Pushbutton
int start_button = 17;

//Timer
int start_time = 0.0;
int end_time = 20;

//************* SETTUP ****************//
void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  //Display
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  
  //Audio
  PgmPrintln("Rescue Robot Voice Sartup");
  
  if (!card.init()) {
    error("Card init. failed!");
  }
  if (!vol.init(card)) {
    error("No partition!");
  }
  if (!root.openRoot(vol)) {
    error("Couldn't open dir");
  }

  PgmPrintln("Files found:");
  root.ls();
  
  //Motors
  maxSpeed(100);
  
  
}

//*************** LOOP ****************//
void loop() {
  // This is the wait for button/switch to start loop
  Serial.println("You must flip the switch/push the button to start!");
  DisplayCardinal();
  while(digitalRead(start_button) == 0){};
  
  // Initialize the start_time
  start_time = millis()/1000;
  
    DisplayTeam();
    delay(5000);
    
    //forward();
    //delay(2000);
    
    DisplayVictim(3, 3, 6, 2);
    
    // Say "Victim Located"
    playcomplete("VICTIM.WAV");
    delay(50);
    
    // Say Room Number
    playcomplete("ROOM3.WAV");
    delay(50);
    
    // Say Position
    playcomplete("POSITION.WAV");
    delay(50);
    playnum(3);
    delay(50);
    playnum(6);
    delay(50);
    
    // Say Status
    playcomplete("STATUS.WAV");
    delay(50);
    
    playcomplete("PORTAL3.WAV");
    delay(50);
    
    // This is outside of the timer loop, end!
    Serial.println("This is the end, time's up!");
    DisplayCardinal();
    playcomplete("BROWN.WAV");
    while(1);
}

//************ FUNCTIONS ***************//

//DISPLAY
void DisplayTeam() {
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");
}

void DisplayCardinal() {
  GLCD.ClearScreen();
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
}

void DisplayVictim(int room, int x, int y, int status)
{
  GLCD.ClearScreen();
  GLCD.GotoXY(0, 7);
  GLCD.SelectFont(baskerville);
  GLCD.PrintNumber(room);
  GLCD.GotoXY(33, 7);
  GLCD.PrintNumber(x);
  GLCD.GotoXY(66, 7);
  GLCD.PrintNumber(y);
  GLCD.GotoXY(99, 7);
  GLCD.PrintNumber(status);
}

//AUDIO
void playnum(int number) {
  switch (number) {
    case 0:
      playcomplete("0.WAV");
      break;
    case 1:
      playcomplete("1.WAV");
      break;
    case 2:
      playcomplete("2.WAV");
      break;
    case 3:
      playcomplete("3.WAV");
      break;
    case 4:
      playcomplete("4.WAV");
      break;
    case 5:
      playcomplete("5.WAV");
      break;
    case 6:
      playcomplete("6.WAV");
      break;
     case 7:
      playcomplete("7.WAV");
      break;
     case 8:
      playcomplete("8.WAV");
      break;
     case 9:
      playcomplete("9.WAV");
      break;
  }
}
/*
 * print error message and halt
 */
void error_P(const char *str)
{
  PgmPrint("Error: ");
  SerialPrint_P(str);
  sdErrorCheck();
  while(1);
}
/*
 * print error message and halt if SD I/O error
 */
void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  PgmPrint("\r\nSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  PgmPrint(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
/*
 * Play a file and wait for it to complete
 */
void playcomplete(char *name) {
  playfile(name);
  while (wave.isplaying);
  
  // see if an error occurred while playing
  sdErrorCheck();
}
/*
 * Open and start playing a WAV file
 */
void playfile(char *name) 
{
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  if (!file.open(root, name)) {
    PgmPrint("Couldn't open file ");
    Serial.print(name); 
    return;
  }
  if (!wave.create(file)) {
    PgmPrintln("Not a valid WAV");
    return;
  }
  // ok time to play!
  wave.play();
}

//MOTORS
void maxSpeed(int speed) {
  motorL.setSpeed(speed);
  motorR.setSpeed(speed);
}

void forward() {
  motorL.run(BACKWARD);
  motorR.run(FORWARD);
}

void backward() {
  motorL.run(FORWARD);
  motorR.run(BACKWARD);
}

void left() {
  motorL.run(FORWARD);
  motorR.run(FORWARD);
}

void right() {
  motorL.run(BACKWARD);
  motorR.run(BACKWARD);
}

int time() {
  return millis()/1000 - start_time;
}

//SENSORS
float sYellow(int pin)
{
  return pow((analogRead(pin))*.0048828125,-1.146)*29.331;    // read the input pin
}

float avg_sYellow(int pin)
{
  float avg = 0;
  int i = 0;
  for(i=0;i<10;i++)
  {
      avg = sYellow(pin) + avg;
  }
  return avg/10;
}

float sWhite(int pin)
{
  return pow((analogRead(pin))*.0048828125,-1.184)*27.385;    // read the input pin
}

float avg_sWhite(int pin)
{
  float avg = 0;
  int i = 0;
  for(i=0;i<10;i++)
  {
      avg = sWhite(pin) + avg;
  }
  return avg/10;
}
