/*
 * Play .wav files for robot voice
 * Output form:
 * "Room Number" a "Position" b "x" c "y" 
 * "The Victim is councious/unconcious/dead" 
 */
#include "WaveHC.h"
#include "WaveUtil.h"


char filename[13];

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
FatReader file;   // This object represent the WAV file for a pi digit or period
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
/*
 * Define macro to put error messages in flash memory
 */
#define error(msg) error_P(PSTR(msg))

//////////////////////////////////// SETUP

void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  PgmPrintln("Rescue Robot Voice Sartup");
  
  if (!card.init()) {
    error("Card init. failed!");
  }
  if (!vol.init(card)) {
    error("No partition!");
  }
  if (!root.openRoot(vol)) {
    error("Couldn't open dir");
  }

  PgmPrintln("Files found:");
  root.ls();
}

/////////////////////////////////// LOOP

unsigned digit = 0;
void loop() {
 
  //"Room Number w"
  strcpy_P(filename, PSTR("MAJESTY.WAV"));
  playcomplete(filename);
  delay(50);
  
  /*
  RobotSpeak(2,6,22,1);
  RobotSpeak(4,12,3,2);
  RobotSpeak(1,10,14,3);
  */
}

/////////////////////////////////// HELPERS

void RobotSpeak(int w, int x, int y, int z)
{
  char aud [2];
 
  //"Room Number w"
  strcpy_P(filename, PSTR("ROOMNU~1.WAV"));
  playcomplete(filename);
  delay(50);
  itoa(w, aud , 10); 
  speaknum(aud[0]);
 
  //"Position" x "x" y "y"
  delay(300);
  strcpy_P(filename, PSTR("POSITION.WAV"));
  playcomplete(filename);
  delay(100);
  itoa(x, aud , 10); 
  speaknum(aud[0]);
  speaknum(aud[1]);
  strcpy_P(filename, PSTR("X.WAV"));
  playcomplete(filename);
  delay(150);
  itoa(y, aud , 10); 
  speaknum(aud[0]);
  speaknum(aud[1]);
  strcpy_P(filename, PSTR("Y.WAV"));
  playcomplete(filename);
  delay(150);
 
  //"The Victim is" concious, unconcious, dead
  if(z == 1){
  strcpy_P(filename, PSTR("CONCIOUS.WAV"));
  playcomplete(filename);
  }
  else if(z == 2){
  strcpy_P(filename, PSTR("UNCONCIOUS.WAV"));
  playcomplete(filename); 
  }
  else if(z == 3){
  strcpy_P(filename, PSTR("DEAD.WAV"));
  playcomplete(filename);
  }
}

void speaknum(char c) 
{   uint8_t i=0;
    strcpy_P(filename, PSTR("             "));
    strcpy_P(filename, PSTR("1.WAV"));
    filename[0] = c;
    i = 1;

  playcomplete(filename);
}
/*
 * print error message and halt
 */
void error_P(const char *str)
{
  PgmPrint("Error: ");
  SerialPrint_P(str);
  sdErrorCheck();
  while(1);
}
/*
 * print error message and halt if SD I/O error
 */
void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  PgmPrint("\r\nSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  PgmPrint(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
/*
 * Play a file and wait for it to complete
 */
void playcomplete(char *name) {
  playfile(name);
  while (wave.isplaying);
  
  // see if an error occurred while playing
  sdErrorCheck();
}
/*
 * Open and start playing a WAV file
 */
void playfile(char *name) 
{
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  if (!file.open(root, name)) {
    PgmPrint("Couldn't open file ");
    Serial.print(name); 
    return;
  }
  if (!wave.create(file)) {
    PgmPrintln("Not a valid WAV");
    return;
  }
  // ok time to play!
  wave.play();
}
