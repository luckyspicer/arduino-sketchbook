int start_time = 0.0;
int start_button = 7;

void setup() {
  // settup the serial port
  Serial.begin(9600);
}

void loop() {
  // This is the wait for button/switch to start loop
  Serial.println("You must flip the switch/push the button to start!");
  while(digitalRead(start_button) == 0){};
  
  // Initialize the start_time
  start_time = millis()/1000;
  
  Serial.print("Start time = ");
  Serial.println(start_time);
  
  // This is the timer while loop
  while(time() < 15) {
    Serial.print("Time = ");
    Serial.println(time());
    delay(1000);
  }
  
  // This is outside of the timer loop, end!
  Serial.println("This is the end, time's up!");
  while(1);
}

int time() {
  return millis()/1000 - start_time;
}

