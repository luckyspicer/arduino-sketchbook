//************ INCLUDES ***************//
//Display
#include <ks0108.h>
#include "SystemFont5x7.h"   // system font
#include "baskerville.h"     // homemade big font
#include "cardinal.h"

//Audio
#include "WaveHC.h"
#include "WaveUtil.h"

//Motors
#include <AFMotor.h>
//************ VARIABLES ***************//
//Display
int isVictim = 1;

//Audio
char filename[13];

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
FatReader file;   // This object represent the WAV file for a pi digit or period
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
/*
 * Define macro to put error messages in flash memory
 */
#define error(msg) error_P(PSTR(msg))

//Motor
AF_DCMotor motorL(1, MOTOR12_64KHZ);
AF_DCMotor motorR(3, MOTOR12_64KHZ);

//Sensors
int bottom = 16;
int bottom_val;
int top = 15;
int top_val;

int left_bump = 19;
int left_bump_val;
int right_bump = 18;
int right_bump_val;
int buff = 3;

int L1 = 10;
float L1_val;
int L2 = 12;
float L2_val;
int R1 = 9;
float R1_val;
int R2 = 13;
float R2_val;

int back = 11;

//Pushbutton
int start_button = 17;

//Timer
int start_time = 0.0;
int end_time = 240.0;

//************* SETTUP ****************//
void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  //Display
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  
  //Audio
  PgmPrintln("Rescue Robot Voice Sartup");
  
  if (!card.init()) {
    error("Card init. failed!");
  }
  if (!vol.init(card)) {
    error("No partition!");
  }
  if (!root.openRoot(vol)) {
    error("Couldn't open dir");
  }

  PgmPrintln("Files found:");
  root.ls();
  
  //Motors
  maxSpeed(115);
  
  //Random number generator
  randomSeed(analogRead(2));
}

//*************** LOOP ****************//
void loop() {
  // This is the wait for button/switch to start loop
  Serial.println("You must flip the switch/push the button to start!");
  DisplayCardinal();
  while(digitalRead(start_button) == 0){};
  
  // Initialize the start_time
  start_time = millis()/1000;
  
  // This is the timer while loop
  while(time() < end_time) 
  {
    playcomplete("BROWN.WAV");
    delay(50);
    if(isVictim == 1) {
      DisplayTeam();
      isVictim = 0;
    }
    forward();
    left_bump_val = digitalRead(left_bump);
    right_bump_val = digitalRead(right_bump);
    top_val = digitalRead(top);
    bottom_val = digitalRead(bottom);
    
    if(left_bump_val == 0) {
      Serial.println("Left Bump!");
      halt();
      right();
      delay(250);
    }
    else if(right_bump_val == 0) {
      Serial.println("Right Bump!");
      halt();
      left();
      delay(250);
    }
    else if(top_val == 0 || bottom_val == 0) {
      Serial.println("Something in Front!");
      halt();
      if(top_val == 0) {
        Serial.println("Obstacle!");
        if(random(0,2) == 1) {
          halt();
          left();
        }
        else {
          halt();
          right();
        }
        delay(750);
      }
      else
      {
        //VICTIM FOUND!!!!
        Serial.println("Victim Found!");
        //Straigten Up, till straight enough
        while(1)
        {
           Serial.println("Straightening Up!");
           L1_val = avg_sYellow(L1);
           L2_val = avg_sYellow(L2);
           R1_val = avg_sYellow(R1);
           R2_val = avg_sYellow(R2);
           Serial.print(R1_val);
           Serial.print("   ");
           Serial.println(R2_val);
           
            if(L1_val+L2_val < R1_val+R2_val)
            { 
              Serial.println("Left side is closer!");
              if (((L1_val-1.5) - L2_val) < -buff)
              {
                right();
              }
              else if (((avg_sYellow(L1)-1.5) - avg_sYellow(L2)) > buff)
              {
                left();
              }
              else
              {
                 //we're straight!
                 break;
              }
            }
            else
            {
               Serial.println("Right Side is Closer!");
               if ((R1_val - (R2_val-.5)) > buff)
               {
                 right();
               }
               else if ((R1_val - (R2_val-.5)) < -buff)
               {
                 left();
               }
               else
               {
                 //we're straight!
                 break;
               }
            }
        }
        
        DisplayVictim(3, 3, 6, 2);
        isVictim = 1;
    
        // Say "Victim Located"
        playcomplete("VICTIM.WAV");
        delay(50);
        
        // Say Room Number
        playcomplete("ROOM3.WAV");
        delay(50);
        
        // Say Position
        playcomplete("POSITION.WAV");
        delay(50);
        playnum(3);
        delay(50);
        playnum(6);
        delay(50);
        
        // Say Status
        playcomplete("STATUS.WAV");
        delay(50);
      }
    }
  }
  
  // This is outside of the timer loop, end!
  Serial.println("This is the end, time's up!");
  DisplayCardinal();
  while(1);
}

//************ FUNCTIONS ***************//

//DISPLAY
void DisplayTeam() {
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");
}

void DisplayCardinal() {
  GLCD.ClearScreen();
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
}

void DisplayVictim(int room, int x, int y, int status)
{
  GLCD.ClearScreen();
  GLCD.GotoXY(0, 7);
  GLCD.SelectFont(baskerville);
  GLCD.PrintNumber(room);
  GLCD.GotoXY(33, 7);
  GLCD.PrintNumber(x);
  GLCD.GotoXY(66, 7);
  GLCD.PrintNumber(y);
  GLCD.GotoXY(99, 7);
  GLCD.PrintNumber(status);
}

//AUDIO
void playnum(int number) {
  switch (number) {
    case 0:
      playcomplete("0.WAV");
      break;
    case 1:
      playcomplete("1.WAV");
      break;
    case 2:
      playcomplete("2.WAV");
      break;
    case 3:
      playcomplete("3.WAV");
      break;
    case 4:
      playcomplete("4.WAV");
      break;
    case 5:
      playcomplete("5.WAV");
      break;
    case 6:
      playcomplete("6.WAV");
      break;
     case 7:
      playcomplete("7.WAV");
      break;
     case 8:
      playcomplete("8.WAV");
      break;
     case 9:
      playcomplete("9.WAV");
      break;
  }
}
/*
 * print error message and halt
 */
void error_P(const char *str)
{
  PgmPrint("Error: ");
  SerialPrint_P(str);
  sdErrorCheck();
  while(1);
}
/*
 * print error message and halt if SD I/O error
 */
void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  PgmPrint("\r\nSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  PgmPrint(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
/*
 * Play a file and wait for it to complete
 */
void playcomplete(char *name) {
  playfile(name);
  while (wave.isplaying);
  
  // see if an error occurred while playing
  sdErrorCheck();
}
/*
 * Open and start playing a WAV file
 */
void playfile(char *name) 
{
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  if (!file.open(root, name)) {
    PgmPrint("Couldn't open file ");
    Serial.print(name); 
    return;
  }
  if (!wave.create(file)) {
    PgmPrintln("Not a valid WAV");
    return;
  }
  // ok time to play!
  wave.play();
}

//MOTORS
void maxSpeed(int speed) {
  motorL.setSpeed(speed);
  motorR.setSpeed(speed);
}

void forward() {
  motorL.run(BACKWARD);
  motorR.run(FORWARD);
}

void backward() {
  motorL.run(FORWARD);
  motorR.run(BACKWARD);
}

void left() {
  motorL.run(FORWARD);
  motorR.run(FORWARD);
}

void right() {
  motorL.run(BACKWARD);
  motorR.run(BACKWARD);
}

void halt() {
  motorL.run(RELEASE);
  motorR.run(RELEASE);
  delay(500);
}

//TIMER

int time() {
  return millis()/1000 - start_time;
}

//SENSORS
float sYellow(int pin)
{
  return pow((analogRead(pin))*.0048828125,-1.146)*29.331;    // read the input pin
}

float avg_sYellow(int pin)
{
  float avg = 0;
  int i = 0;
  for(i=0;i<10;i++)
  {
      avg = sYellow(pin) + avg;
  }
  return avg/10;
}

float sWhite(int pin)
{
  return pow((analogRead(pin))*.0048828125,-1.184)*27.385;    // read the input pin
}

float avg_sWhite(int pin)
{
  float avg = 0;
  int i = 0;
  for(i=0;i<10;i++)
  {
      avg = sWhite(pin) + avg;
  }
  return avg/10;
}
