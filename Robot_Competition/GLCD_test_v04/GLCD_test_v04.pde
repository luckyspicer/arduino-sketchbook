#include <ks0108.h>
//#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font
#include "baskerville.h"     // homemade big font
#include "cardinal.h"

//////////////////////////////////// SETUP
void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  
}

/////////////////////////////////// LOOP
void loop() {
  DisplayTeam();
  delay(5000);
  
  int i;
  for(i = 0; i < 10; i++)
  {
    DisplayVictim(i, i, i, i);
    delay(2000);
  }
  
  DisplayCardinal();
  while(1);
}

//**************** Display Functions ****************//
void DisplayTeam() {
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");
}

void DisplayCardinal() {
  GLCD.ClearScreen();
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
}

void DisplayVictim(int room, int x, int y, int status)
{
  GLCD.ClearScreen();
  GLCD.GotoXY(0, 7);
  GLCD.SelectFont(baskerville);
  GLCD.PrintNumber(room);
  GLCD.GotoXY(33, 7);
  GLCD.PrintNumber(x);
  GLCD.GotoXY(66, 7);
  GLCD.PrintNumber(y);
  GLCD.GotoXY(99, 7);
  GLCD.PrintNumber(status);
}
