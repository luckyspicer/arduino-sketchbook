#include <ks0108.h>
#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font

//////////////////////////////////// SETUP
void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");
}

/////////////////////////////////// LOOP

unsigned digit = 0;
void loop() {
  GLCD.SetInverted(0);
  
  delay(3000);
  
  GLCD.SetInverted(1);
  
  delay(3000);
}
