// This is a test code for a panning SHARP GP2D12 sensor
// That seeks out victims from the IEEE SoutheastCon 2011
// Hardware Competition
// Luke Spicer 11/4/2010

#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 

int IRsensor0 = 8;
int distance;
int pos = 0;    // variable to store the servo position 
int LED = 13;

void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, distance measure tests!");
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
} 
 
 
void loop() 
{  
  int turnDirection;
  for(pos = 0; pos > -1; pos = pos + turnDirection)  // goes from 0 degrees to 180 degrees 
  { 
    //This if/else switches the servo's direction of panning
    //If at 180 degrees pan back down to 0
    if (pos == 180) {
      turnDirection = -1;
    }
    //If at 0 degrees pan back up to 180
    else if(pos == 0) {
      turnDirection = 1;
    }
    myservo.write(pos);
    
    //Every 5 degrees take a range measurement
    if(pos%5 == 0) {
      distance = irRange(IRsensor0);
      //print the distance to the screen
      Serial.println(distance);
    }
    
    if(distance <= 20) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
    delay(5);                       // waits 3ms for the servo to reach the position 
  } 
}

/**** Functions to read and average the sensors! ****/
int avgSensor(int Sensor, int numAvg) {
  int avg = 0;
  for(int i = 0; i < numAvg; i++) {
    avg = analogRead(Sensor) + avg;
  }
  avg = avg/numAvg;
  return avg;
}
  
int irRange(int IR) {
  int dis;
  while(1) {
    int IRval = avgSensor(IR, 3);
    dis = 9148.23997*(pow(IRval, -1.09443));
    if(dis <= 100) {
      break;
    }
  }
  return dis; 
}
