int IR = 0;
int IRdigital = 2;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Simple IR test!");
}

void loop() {
 Serial.print("IR sensor analog: ");
 Serial.println(analogRead(IR));
 Serial.print("IR sensor digital: ");
 Serial.println(digitalRead(IRdigital));
 Serial.println("");
 delay(750); 
}
