int photoResistor = 9;
int photoResistor2 = 10;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("PhotoResistorEyes!");
}

void loop() {
 Serial.print("Photo-Resistor Right ");
 Serial.println(readPR(photoResistor));
 Serial.print("Photo-Resistor Left ");
 Serial.println(readPR(photoResistor2));
 Serial.println("");
 delay(750); 
}

int readPR(int photoR) {
  int avgPR = 0;
  for(int i = 0; i < 10; i++) {
    avgPR = analogRead(photoR) + avgPR;
  }
  avgPR = avgPR/10;
  return avgPR;
}
