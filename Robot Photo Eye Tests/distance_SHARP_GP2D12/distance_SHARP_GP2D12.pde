int IRsensor0 = 0;
int distance;

void setup() {
  Serial.begin(9600);
  Serial.println("Hello, distance measure tests!");
}

void loop() {
  distance = irRange(IRsensor0);
  Serial.print("Distance measure is: ");
  Serial.println(distance + distance/10);
  Serial.println("");
  
  delay(2000);
}

int avgSensor(int Sensor, int numAvg) {
  int avg = 0;
  for(int i = 0; i < numAvg; i++) {
    avg = analogRead(Sensor) + avg;
  }
  avg = avg/numAvg;
  return avg;
}
  
int irRange(int IR) {
  //int IRval = readAnalogSensor(IR, 10);
  int IRval = avgSensor(IR, 3);
  //return 27406.5*(pow(IRval, -1.322086782));
  return 9148.23997*(pow(IRval, -1.09443));
}
