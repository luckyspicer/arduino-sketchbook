// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int IRsensor0 = 0;
int distance;
int pos = 0;    // variable to store the servo position 
int LED = 13;
 
void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, distance measure tests!");
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
} 
 
 
void loop() 
{  
  for(pos = 0; pos < 180; pos++)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    if(pos%5 == 0) {
      distance = avgSensor(IRsensor0, 5);
      Serial.println(distance + distance/10);
    }
    if(distance <= 20) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
    delay(100);                       // waits 15ms for the servo to reach the position 
  } 
  for(pos = 180; pos>=1; pos--)     // goes from 180 degrees to 0 degrees 
  {                                
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    if(pos%5 == 0) {
      distance = avgSensor(IRsensor0, 5);
      Serial.println(distance + distance/10);
    }
    if(distance <= 20) {
      digitalWrite(LED, HIGH);
    }
    else {
      digitalWrite(LED, LOW);
    }
    delay(100);                       // waits 15ms for the servo to reach the position 
  }
}

int avgSensor(int Sensor, int numAvg) {
  int avg = 0;
  for(int i = 0; i < numAvg; i++) {
    avg = analogRead(Sensor) + avg;
  }
  avg = avg/numAvg;
  return avg;
}
  
int irRange(int IR) {
  //int IRval = readAnalogSensor(IR, 10);
  int IRval = avgSensor(IR, 3);
  //return 27406.5*(pow(IRval, -1.322086782));
  return 9148.23997*(pow(IRval, -1.09443));
}
