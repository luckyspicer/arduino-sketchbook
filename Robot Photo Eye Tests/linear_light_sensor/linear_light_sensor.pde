int lightSensor = 0;
int ambientLevel;

int lightHighTime;
int lightLowTime;
int lightHighFlag = -1;
int lightLowFlag = -1;

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Linear Light Sensor Test!");
  
  ambientLevel = avgSensor(lightSensor);
}

void loop() {
 
 Serial.print("Light Sensor Output ");
 Serial.println(avgSensor(lightSensor));
 Serial.println("");
 delay(100);
 
 
 if(avgSensor(lightSensor) >= ambientLevel + 15) {
   digitalWrite(13, HIGH);
   
   if(lightHighFlag != 0) {
     lightHighTime = millis();
     lightHighFlag = 0;
     lightLowFlag = 1;
   }
 }
 
 else {
   digitalWrite(13, LOW);
   
   if(lightLowFlag != 0) {
     lightLowTime = millis();
     lightLowFlag = 0;
     lightHighFlag = 1;
   }
 }
 
 if(lightHighFlag != -1 && lightLowFlag != -1) {
   Serial.print("Frequency = ");
   Serial.println(500/abs(lightLowTime-lightHighTime));
 }
}

int avgSensor(int Sensor) {
  int avg = 0;
  int numAvg = 25;
  for(int i = 0; i < numAvg; i++) {
    avg = analogRead(Sensor) + avg;
  }
  avg = avg/numAvg;
  return avg;
}
