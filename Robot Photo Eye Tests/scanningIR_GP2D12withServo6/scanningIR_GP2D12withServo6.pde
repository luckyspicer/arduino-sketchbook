// This is a test code for a panning SHARP GP2D12 sensor
// That seeks out victims from the IEEE SoutheastCon 2011
// Hardware Competition
// Luke Spicer 11/4/2010

#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 

int IRsensor0 = 8;
int minDistance = 500;
int Distance;
int pos = 0;    // variable to store the servo position 
int LED = 13;
int minPos = 0;

int rightPos;
int leftPos;

int sweepCount;

void setup() 
{ 
  Serial.begin(9600);
  Serial.println("Hello, distance measure tests!");
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
  myservo.write(0);
  delay(1500);
} 
 
 
void loop() 
{  
  int turnDirection;
  for(pos = 0; pos > -1; pos = pos + turnDirection)  // goes from 0 degrees to 180 degrees 
  { 
    //This if/else switches the servo's direction of panning
    //If at 180 degrees pan back down to 0
    if (pos == 180) {
      turnDirection = -1;
      myservo.write(minPos);
      delay(1000);
     
     for(pos = minPos; pos <= 180; pos++) {
       myservo.write(pos);
       delay(3);
       Distance = irRange(IRsensor0);
       if(Distance > minDistance + 5) {
         rightPos = pos;
         break;
       }
     }
     //Serial.print("Right Edge = ");
     //Serial.println(rightPos);
     myservo.write(minPos);
     delay(250);
     for(pos = minPos; pos >= 0; pos--) {
       myservo.write(pos);
       delay(3);
       Distance = irRange(IRsensor0);
       if(Distance > minDistance + 5) {
         leftPos = pos;
         break;
       }
     }
     //Serial.print("Left Edge = ");
     //Serial.println(leftPos);
     
     minPos = (rightPos + leftPos)/2;
     myservo.write(minPos);
     delay(500);
     Serial.print("Min Pos = ");
     Serial.print(minPos);
     Serial.print(" Min Distance = ");
     Serial.println(minDistance);
     if(minPos > 80 &&  minPos < 100) {
       Serial.println("We centered!");
       break;
     }
     minDistance =  irRange(IRsensor0);
     //Serial.print("Min Pos = ");
     //Serial.print(minPos);
     //Serial.print(" Min Distance = ");
     //Serial.println(minDistance);
     
     pos = 180;
     myservo.write(pos);
     delay(250);
    }
    
    //If at 0 degrees pan back up to 180
    else if(pos == 0) {
      turnDirection = 1;
      myservo.write(minPos);
      delay(250);
     
     for(pos = minPos; pos <= 180; pos++) {
       myservo.write(pos);
       delay(3);
       Distance = irRange(IRsensor0);
       if(Distance > minDistance + 5) {
         rightPos = pos;
         break;
       }
     }
     //Serial.print("Right Edge = ");
     //Serial.println(rightPos);
     myservo.write(minPos);
     delay(250);
     for(pos = minPos; pos >= 0; pos--) {
       myservo.write(pos);
       delay(3);
       Distance = irRange(IRsensor0);
       if(Distance > minDistance + 5) {
         leftPos = pos;
         break;
       }
     }
     //Serial.print("Left Edge = ");
     //Serial.println(leftPos);
     
     minPos = (rightPos + leftPos)/2;
     myservo.write(minPos);
     delay(250);
     Serial.print("Min Pos = ");
     Serial.print(minPos);
     Serial.print(" Min Distance = ");
     Serial.println(minDistance);
     if(minPos > 80 &&  minPos < 100) {
       Serial.println("We centered!");
       break;
     }
     minDistance =  irRange(IRsensor0);
     //Serial.print("Min Pos = ");
     //Serial.print(minPos);
     //Serial.print(" Min Distance = ");
     //Serial.println(minDistance);
     pos = 0;
     myservo.write(pos);
     delay(250);
    }
    myservo.write(pos);
    
    //Every 5 degrees take a range measurement
    if(pos%5 == 0) {
      Distance = irRange(IRsensor0);
      if (Distance <= 0) {
         Distance = 500;
      }
      if (Distance < minDistance) {
        minPos = pos;
        //Serial.println(minPos);
        minDistance = Distance;
      }
      //print the distance to the screen
      Serial.print("Position = ");
      Serial.print(pos);
      Serial.print(" Distance = ");
     Serial.println(Distance);
    }
    delay(3);                       // waits 3ms for the servo to reach the position
  } 
  while(1);
}

/**** Functions to read and average the sensors! ****/
int avgSensor(int Sensor, int numAvg) {
  int avg = 0;
  for(int i = 0; i < numAvg; i++) {
    avg = analogRead(Sensor) + avg;
  }
  avg = avg/numAvg;
  return avg;
}
  
int irRange(int IR) {
  int dis;
  while(1) {
    int IRval = avgSensor(IR, 3);
    dis = 9148.23997*(pow(IRval, -1.09443));
    if(dis <= 120) {
      break;
    }
  }
  return dis; 
}
