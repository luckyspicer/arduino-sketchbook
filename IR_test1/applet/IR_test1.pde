/* Test to read IR values and print them serially!
*
*
*
*/

#include <LCD4Bit.h> 
//create object to control an LCD.  
//number of lines in display=1
LCD4Bit lcd = LCD4Bit(2);
int button = 0;
int buttonState;
int IR = 0;
int i = 0;
int val = 0;
int cm = 0;
char cmchar[4];
char value[4];

void setup()
  { 
    Serial.begin(9600);         // Setting up serial library
    pinMode(13, OUTPUT);  //we'll use the debug LED to output a heartbeat
    pinMode(0, INPUT);
    lcd.init();
  }

void loop()
{
if(digitalRead(button) != buttonState)
  {
    if(digitalRead(button) == LOW)
    {
        
    }
    else
    {
      for(i = 0; i < 20; i++)
      {
        val = val + analogRead(IR);
      }
      val = val/20;
      
      cm = 16489 * pow(val,-1.2);
      lcd.clear();

      lcd.printIn("   IR Value IS: ");
      lcd.cursorTo(2, 0);
      
      itoa(val, value, 10);
      itoa(cm, cmchar, 10);
      lcd.printIn(cmchar);
      lcd.printIn(" cm    ");
      lcd.printIn(value);

      Serial.print("Your current IR value is: ");
      Serial.print(cm);
      Serial.print(" cm    ");
      Serial.print(val);
    }
    buttonState = digitalRead(button);  
  }
}
