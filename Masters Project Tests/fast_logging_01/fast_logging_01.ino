/*
  SD card read/write
 
 This example shows how to read and write data to and from an SD card file 	
 The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created   Nov 2010
 by David A. Mellis
 updated 2 Dec 2010
 by Tom Igoe
 
 This example code is in the public domain.
 	 
 */
 
#include <SD.h>
File myFile;
int PPG = 0;

void setup()
{
  Serial.begin(115200);
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(10, OUTPUT);
   analogReference(EXTERNAL);
   
  if (!SD.begin(10)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("logTest.txt", FILE_WRITE);
  int analogVal = 0;
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.println("Writing to logTest.txt...");
    //**********//

    long period = micros();
    long startTime = millis();
    long number = 0;
    pinMode(2, OUTPUT);
    digitalWrite(2, HIGH);
    while(digitalRead(15) == 1) {
      int i = 0;
      while(i < 1000) {
        if((micros() - period) >= 835) {
          analogVal = analogRead(PPG);
          period = micros();
          myFile.println(analogVal);
          Serial.println(analogVal);
          i++;
          number++;
        }
      }
    }
    long endTime = millis();
    digitalWrite(2, LOW);
    
    //**********//
    myFile.close();  // close the file:
    Serial.println("\ndone writing to logTest.txt");
    Serial.print("It took ");
    Serial.print(endTime - startTime);
    Serial.print(" mSec to write ");
    Serial.print(number);
    Serial.println(" integers to the SD card");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening logTest.txt");
  }
}

void loop()
{
	// nothing happens after setup
}


