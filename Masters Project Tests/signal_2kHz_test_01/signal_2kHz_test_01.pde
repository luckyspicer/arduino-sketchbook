int output_pin = 2;
int out_state = 1;
int analogSensor = 0;

void setup() {
  Serial.begin(9600);
  
  Serial.println("Test of 2KhZ Analog Read Rate");
  
  pinMode(output_pin, OUTPUT);
}

long time = micros();
long time2;
int value;

void loop() {
  time2 = micros();
  if(time2 - time >= 250) {
    time = time2;
    if (out_state == 1) {
      digitalWrite(output_pin, HIGH);
      value = analogRead(analogSensor);
      out_state = 0;
    }
    else if (out_state == 0) {
      digitalWrite(output_pin, LOW);
      out_state = 1;
    }
  }
}
