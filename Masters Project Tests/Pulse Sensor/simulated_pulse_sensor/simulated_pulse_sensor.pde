void setup() {
  Serial.begin(115200);
  
}

int BPM = 0;
float angle = 0.0;
float angle_inc = PI/100.0;
void loop() {
  angle += angle_inc;
  if(angle > 2*PI) {
    angle = 0.0;
    BPM++;
    if(BPM >= 200) {
      BPM = 0;
    }
    Serial.print("Q");
    Serial.println(BPM);
  }
  Serial.print("S");
  float HRV = 25.0*sin(angle) + 300.0; 
  Serial.println(HRV);
  delay(3);
}
