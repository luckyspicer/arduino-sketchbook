long time;
char randCharacter;
void setup() {
  Serial.begin(9600);
  Serial.println("Arduino Serial to Processing Test");
  time = millis();
  randomSeed(analogRead(0));
}


void loop() {
  if (millis() - time > 1500) {
    time = millis();
    Serial.print("Test Transmission -- ");
    Serial.print("Time = ");
    Serial.print(millis()/1000);
    Serial.print(" Sec");
    Serial.print(" random character = ");
    
    randCharacter = random(65, 68);
    Serial.println(randCharacter);
  }
}
