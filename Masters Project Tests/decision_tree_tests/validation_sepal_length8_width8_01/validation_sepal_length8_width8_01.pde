#include <iris.h>

int output_class[] = {0,1,2,1,1,2,1,0,2,0,1,0,2,2,2};
int sepal_length[] = {2,5,5,3,5,4,5,1,6,1,4,3,4,3,6};
int sepal_width[]  = {1,3,4,3,4,5,3,0,4,0,3,0,5,5,4};

void setup() {
  Serial.begin(9600);
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 15; i++) {
    if(iris(sepal_length[i], sepal_width[i]) == output_class[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(iris(sepal_length[i], sepal_width[i]));
    Serial.print(" | Answer = ");
    Serial.println(output_class[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
