#include <iris.h>

int output_class[] = {2, 0, 0, 1, 2, 1, 1, 1, 0, 2, 0, 0, 1, 1, 2};
int sepal_length[] = {4, 2, 1, 3, 6, 3, 4, 3, 2, 6, 2, 2, 4, 5, 5};
int sepal_width[]  = {3, 4, 2, 2, 2, 1, 2, 1, 5, 2, 3, 4, 2, 2, 1};
int petal_length[] = {5, 0, 0, 4, 6, 4, 4, 3, 0, 7, 0, 0, 4, 5, 6};
int petal_width[]  = {6, 0, 0, 3, 5, 3, 3, 2, 1, 4, 0, 0, 3, 4, 4};

void setup() {
  Serial.begin(9600);
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 15; i++) {
    if(iris(sepal_length[i], sepal_width[i], petal_length[i], petal_width[i]) == output_class[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(iris(sepal_length[i], sepal_width[i], petal_length[i], petal_width[i]));
    Serial.print(" | Answer = ");
    Serial.println(output_class[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
