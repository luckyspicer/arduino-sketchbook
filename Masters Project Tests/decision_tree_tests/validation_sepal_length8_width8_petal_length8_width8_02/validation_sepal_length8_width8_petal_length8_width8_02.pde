#include <iris.h>
#include <MemoryFree.h>
// 2268 Bytes Flash,  without Tree
// 3568 Bytes Flash,  with Tree

int output_class[] = {1, 2, 0, 0, 0, 1, 0, 2, 0, 2, 2, 0, 1, 1, 0};
int sepal_length[] = {3, 5, 2, 2, 2, 3, 0, 4, 3, 5, 3, 2, 3, 4, 1};
int sepal_width[]  = {3, 2, 4, 4, 4, 2, 3, 2, 5, 3, 1, 3, 1, 2, 3};
int petal_length[] = {5, 6, 0, 0, 0, 3, 0, 5, 0, 6, 5, 0, 4, 4, 0};
int petal_width[]  = {4, 5, 0, 1, 0, 3, 0, 4, 0, 6, 5, 0, 3, 3, 0};

void setup() {
  Serial.begin(9600);
  Serial.println(freeMemory());
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 15; i++) {
    int decision;
    decision = iris(sepal_length[i], sepal_width[i], petal_length[i], petal_width[i], analogRead(0));
    if(decision == output_class[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(decision);
    Serial.print(" | Answer = ");
    Serial.println(output_class[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
