#include <play_tennis.h>

int playTennis[] = {1, 1};
int Outlook[] = {0, 0};
int Temp[] = {1, 0};
int Humidity[] = {1, 0};
int Wind[] = {1, 1};

void setup() {
  Serial.begin(9600);
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 2; i++) {
    int decision; 
    decision = play_tennis(Outlook[i], Temp[i], Humidity[i], Wind[i], analogRead(0));
    if(decision == playTennis[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(decision);
    Serial.print(" | Answer = ");
    Serial.println(playTennis[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
