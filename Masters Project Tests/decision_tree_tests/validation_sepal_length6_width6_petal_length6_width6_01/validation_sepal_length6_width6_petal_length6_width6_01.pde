#include <iris.h>

int output_class[] = {2, 0, 0, 1, 2, 1, 1, 1, 0, 2, 0, 0, 1, 1, 2};
int sepal_length[] = {1, 2, 3, 1, 2, 0, 1, 0, 4, 1, 3, 2, 1, 1, 5};
int sepal_width[]  = {3, 2, 1, 2, 1, 2, 2, 1, 2, 3, 2, 1, 2, 2, 3};
int petal_length[] = {0, 3, 3, 0, 3, 0, 0, 0, 3, 0, 3, 3, 0, 0, 5};
int petal_width[]  = {0, 3, 2, 1, 2, 0, 0, 0, 2, 0, 3, 4, 0, 0, 4};

void setup() {
  Serial.begin(9600);
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 15; i++) {
    int decision = iris(sepal_length[i], sepal_width[i], petal_length[i], petal_width[i], analogRead(0));
    if(decision == output_class[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(decision);
    Serial.print(" | Answer = ");
    Serial.println(output_class[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
