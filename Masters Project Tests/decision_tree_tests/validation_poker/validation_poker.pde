#include <Poker_Hand.h>

int S1[] = {2, 2};
int C1[] = {0, 4};
int S2[] = {4, 6};
int C2[] = {1, 3};
int S3[] = {1, 3};
int C3[] = {10, 9};
int S4[] = {2, 0};
int C4[] = {3, 4};
int S5[] = {3, 3};
int C5[] = {5, 3};
int output_poker[] = {0, 1};

void setup() {
  Serial.begin(9600);
  
  int correct_count = 0;
  int wrong_count = 0;
  for (int i = 0; i < 2; i++) {
    int decision = Poker_Hand(S1[i], C1[i], S2[i], C2[i], S3[i], C3[i], S4[i], C4[i], S5[i], C5[i], analogRead(0));
    if(decision == output_poker[i]){
      correct_count++;
    }
    else{
      wrong_count++;
    }
    Serial.print("Decision = ");
    Serial.print(decision);
    Serial.print(" | Answer = ");
    Serial.println(output_poker[i]);
  }
    Serial.print("\nCorrect = ");
    Serial.print(correct_count);
    Serial.print(" | Wrong = ");
    Serial.println(wrong_count);
}

void loop(){
}
