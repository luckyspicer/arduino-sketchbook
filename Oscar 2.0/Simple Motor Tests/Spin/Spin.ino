// Adafruit Motor shield library
// copyright Adafruit Industries LLC, 2009
// this code is public domain, enjoy!

#include <AFMotor.h>

AF_DCMotor RightMotor(1);
AF_DCMotor LeftMotor(2);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");

  // turn on motor
  RightMotor.setSpeed(75);
  LeftMotor.setSpeed(75);
 
  RightMotor.run(RELEASE);
  LeftMotor.run(RELEASE);
  RightMotor.run(FORWARD);
  LeftMotor.run(BACKWARD);
  delay(3000);
  RightMotor.run(RELEASE);
  LeftMotor.run(RELEASE);
}

void loop() {
}
