/*
 * Play .wav files for robot voice
 * Output form:
 * "Room Number" a "Position" b "x" c "y" 
 * "The Victim is councious/unconcious/dead" 
 */
#include "WaveHC.h"
#include "WaveUtil.h"
#include <AFMotor.h>
#include <ks0108.h>
#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font

char filename[13];

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
FatReader file;   // This object represent the WAV file for a pi digit or period
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
/*
 * Define macro to put error messages in flash memory
 */
#define error(msg) error_P(PSTR(msg))

//MOTOR Test STuff
AF_DCMotor motor1(1, MOTOR12_64KHZ);
AF_DCMotor motor2(2, MOTOR12_64KHZ);
AF_DCMotor motor3(3, MOTOR34_64KHZ);
AF_DCMotor motor4(4, MOTOR34_64KHZ);

//////////////////////////////////// SETUP

void setup() {
  motor1.setSpeed(255);
  motor2.setSpeed(255);
  motor3.setSpeed(255);
  motor4.setSpeed(255);
  
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  motor3.run(FORWARD);
  motor4.run(FORWARD);
  delay(5000);
  
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  PgmPrintln("Rescue Robot Voice Sartup");
  
  if (!card.init()) {
    error("Card init. failed!");
  }
  if (!vol.init(card)) {
    error("No partition!");
  }
  if (!root.openRoot(vol)) {
    error("Couldn't open dir");
  }

  PgmPrintln("Files found:");
  root.ls();
  
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");
}

/////////////////////////////////// LOOP

unsigned digit = 0;
void loop() {
  motor1.run(FORWARD); 
  motor2.run(BACKWARD);
  motor3.run(FORWARD); 
  motor4.run(BACKWARD);
  
  RobotSpeak(2,6,22,1);
  GLCD.SetInverted(0);
  motor1.run(BACKWARD);
  motor2.run(FORWARD);
  motor3.run(BACKWARD);
  motor4.run(FORWARD);
  
  RobotSpeak(4,12,3,2);
  GLCD.SetInverted(1);
  RobotSpeak(1,10,14,3); 
}

/////////////////////////////////// HELPERS

void RobotSpeak(int w, int x, int y, int z)
{
  char aud [2];
 
  //"Room Number w"
  strcpy_P(filename, PSTR("ROOMNU~1.WAV"));
  playcomplete(filename);
  delay(50);
  itoa(w, aud , 10); 
  speaknum(aud[0]);
 
  //"Position" x "x" y "y"
  delay(300);
  strcpy_P(filename, PSTR("POSITION.WAV"));
  playcomplete(filename);
  delay(100);
  itoa(x, aud , 10); 
  speaknum(aud[0]);
  speaknum(aud[1]);
  strcpy_P(filename, PSTR("X.WAV"));
  playcomplete(filename);
  delay(150);
  itoa(y, aud , 10); 
  speaknum(aud[0]);
  speaknum(aud[1]);
  strcpy_P(filename, PSTR("Y.WAV"));
  playcomplete(filename);
  delay(150);
 
  //"The Victim is" concious, unconcious, dead
  if(z == 1){
  strcpy_P(filename, PSTR("CONCIOUS.WAV"));
  playcomplete(filename);
  }
  else if(z == 2){
  strcpy_P(filename, PSTR("UNCONCIOUS.WAV"));
  playcomplete(filename); 
  }
  else if(z == 3){
  strcpy_P(filename, PSTR("DEAD.WAV"));
  playcomplete(filename);
  }
}

void speaknum(char c) 
{   uint8_t i=0;
    strcpy_P(filename, PSTR("             "));
    strcpy_P(filename, PSTR("1.WAV"));
    filename[0] = c;
    i = 1;

  playcomplete(filename);
}
/*
 * print error message and halt
 */
void error_P(const char *str)
{
  PgmPrint("Error: ");
  SerialPrint_P(str);
  sdErrorCheck();
  while(1);
}
/*
 * print error message and halt if SD I/O error
 */
void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  PgmPrint("\r\nSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  PgmPrint(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
/*
 * Play a file and wait for it to complete
 */
void playcomplete(char *name) {
  playfile(name);
  while (wave.isplaying);
  
  // see if an error occurred while playing
  sdErrorCheck();
}
/*
 * Open and start playing a WAV file
 */
void playfile(char *name) 
{
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  if (!file.open(root, name)) {
    PgmPrint("Couldn't open file ");
    Serial.print(name); 
    return;
  }
  if (!wave.create(file)) {
    PgmPrintln("Not a valid WAV");
    return;
  }
  // ok time to play!
  wave.play();
}
