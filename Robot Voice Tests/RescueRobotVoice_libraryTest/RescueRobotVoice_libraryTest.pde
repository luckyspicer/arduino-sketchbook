/*
 * Play .wav files for robot voice
 * Output form:
 * "Room Number" a "Position" b "x" c "y" 
 * "The Victim is councious/unconcious/dead" 
 */
#include "robotSpeech.h"

//////////////////////////////////// SETUP

void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  speechInit();
}

/////////////////////////////////// LOOP
void loop() { 
  RobotSpeak(2,6,22,1);
  RobotSpeak(4,12,3,2);
  RobotSpeak(1,10,14,3); 
}
