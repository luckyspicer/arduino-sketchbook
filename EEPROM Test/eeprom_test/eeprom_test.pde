#include <EEPROM.h>
#include <Music.h>             // Custom Library to play Music (melody) from piezo

// Music Object and HMI Variables
Music speaker = Music(10);           // Speaker attached to pin 10, for playing tunes with Music
int buzzer = 10;              // Buzzer used for Arduino Tone() function, also pin 10
int button = 9;               // Pushbutton attached to pin 9
long button_timer;            // Variable to store thearduino_timethe pushbutton is pressed
long button_stop_time;        // Variable to store thearduino_timethe pushbutton is released
long arduino_time;            // Variable to store the global Arduinoarduino_timeach cycle of loop()
int guitarTune[] = {200, B3,q, d4,q, g4,q, B3,e, c4,q, d4,q, g4,q, c4,e};

int home_d_lat;
int home_m_lat;
float home_s_lat;
int home_d_long;
int home_m_long;
float home_s_long;
int isHomeSet;

void setup(){
  Serial.begin(9600);
  
  // Check the EEPROM for coordinates
  int EEPROM_values[10];
  for (int n = 0; n < 10; n++) {
    EEPROM_values[n] = EEPROM.read(n);
    Serial.print("EEPROM Address ");
    Serial.print(n);
    Serial.print(": ");
    Serial.println(EEPROM_values[n]);
  }
}

void loop(){
  //check_button();                     // Check the HMI Button for Presses and Home location logging
  if(digitalRead(button) == 0) {                    // Check if button pressed
    button_timer = millis();                        // Start the button timer
    if(isHomeSet == 1) {                            // If isHomeSet is already set
      tone(buzzer, 1000000/a5);                     // Play a little three tone pluse to let us know
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/b5);
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/c6);
      delay(250);
      noTone(buzzer);
      delay(250);
    } 
    tone(buzzer, 1000000/a5);                       // Start playing the tone
    delay(20);                                      // Delay to debounce pushbutton
    while(digitalRead(button) == 0) {               // While button pressed check to see
      button_stop_time = millis() - button_timer;   // if button has been pressed for 3 seconds
      if(button_stop_time/1000 >= 3) {              // If button has been pressed for 3 seconds
        Serial.println("Button Held for 3 Seconds");// Tell us
        noTone(buzzer);                             // turn off the buzzer
                                                    // Save the home location
        /***** Save Home Location Coordinates *****/
        home_d_lat = 38;
        home_m_lat = 13;
        home_s_lat = 51.42555;
        
        home_d_long = 85;
        home_m_long = 45;
        home_s_long = 29.12665;
 
        isHomeSet = 1;
        
        // Save Latitude to EEPROM
        EEPROM.write(0, home_d_lat);
        EEPROM.write(1, home_m_lat);
        
        int seconds_int = int(home_s_lat);
        Serial.print("Seconds Integer Part = ");
        Serial.println(seconds_int);
        EEPROM.write(2, seconds_int);
        
        float seconds_fraction = home_s_lat - seconds_int;
        Serial.print("Second Fractional Part = ");
        Serial.println(seconds_fraction, 5);
        seconds_fraction = seconds_fraction*100.0;
        seconds_int = int(seconds_fraction);
        Serial.print("Second Fractional Part as Int = ");
        Serial.println(seconds_int);
        EEPROM.write(3, seconds_int);
        
        seconds_fraction = seconds_fraction - seconds_int;
        seconds_fraction = seconds_fraction*100.0;
        seconds_int = int(seconds_fraction);
        Serial.print("Second Fractional Part as Int = ");
        Serial.println(seconds_int);
        EEPROM.write(4, seconds_int);
        
        // Save Longitude to EEPROM
        EEPROM.write(5, home_d_long);
        EEPROM.write(6, home_m_long);
        
        seconds_int = int(home_s_long);
        Serial.print("Seconds Integer Part = ");
        Serial.println(seconds_int);
        EEPROM.write(7, seconds_int);
        
        seconds_fraction = home_s_long - seconds_int;
        Serial.print("Second Fractional Part = ");
        Serial.println(seconds_fraction, 5);
        seconds_fraction = seconds_fraction*100.0;
        seconds_int = int(seconds_fraction);
        Serial.print("Second Fractional Part as Int = ");
        Serial.println(seconds_fraction);
        EEPROM.write(8, seconds_int);
        
        seconds_fraction = seconds_fraction - seconds_int;
        seconds_fraction = seconds_fraction*100.0;
        seconds_int = int(seconds_fraction);
        Serial.print("Second Fractional Part as Int = ");
        Serial.println(seconds_int);
        EEPROM.write(9, seconds_int);
        
        
        /*****                                *****/
                                                  // And play a tune to let us know
        speaker.playMusic(guitarTune, sizeof(guitarTune)); 
        break;                                    // Then break from the while loop
      }
    }
    noTone(buzzer);                                 // Turn off the buzzer
    while(digitalRead(button) == 0);                // And wait for the button to be let up
  }
  
  Serial.print("Time = ");
  Serial.print(arduino_time);
  Serial.println(" sec");
  delay(500);
}
