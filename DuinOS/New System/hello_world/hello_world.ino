#include <Arduino.h>
#include "DuinOS/DuinOS.h"

int GRNLED = 2;
int ORGLED = 3;

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Arduino FreeRTOS Test"));
  Serial.print(F("configCPU_CLOCK_HZ = "));
  Serial.println(configCPU_CLOCK_HZ);
  Serial.print(F("configTICK_RATE_HZ = "));
  Serial.println(configTICK_RATE_HZ);
  pinMode(GRNLED, OUTPUT);
  pinMode(ORGLED, OUTPUT);
}

static void vGreen(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    Serial.println(F("Green On"));
    digitalWrite(GRNLED, LOW);
    vTaskDelay( 500 );
    Serial.println(F("Green Off"));
    digitalWrite(GRNLED, HIGH);
    vTaskDelay( 500 );
  }
}

static void vOrange(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    Serial.println(F("Orange On"));
    digitalWrite(ORGLED, LOW);
    vTaskDelay( 250 );
    Serial.println(F("Orange Off"));
    digitalWrite(ORGLED, HIGH);
    vTaskDelay( 250 );
  }
}

int main(void)
{
	init();

#if defined(USBCON)
	USBDevice.attach();
#endif
	
	setup();
        
        xTaskCreate( vGreen, ( signed char * ) "Green", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
        xTaskCreate( vOrange, ( signed char * ) "Orange", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );        
//	for (;;) {
//		loop();
//		if (serialEventRun) serialEventRun();
//	}
        Serial.println("Tasks Created, Scheduler about to start...");
        vTaskStartScheduler();
        
        Serial.println("Unexpected Exit from OS");
        
	return 0;
}
