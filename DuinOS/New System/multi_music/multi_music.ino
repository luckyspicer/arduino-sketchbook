#include "DuinOS/DuinOS.h"
#include <Music.h>

int HEARTBEAT = 13;   /* Heartbeat LED */
int ORGLED = 3;
Music speaker1 = 7;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */
Music speaker2 = 8;

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */

int tune[] = {120, b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  b4,q+e,  a4,e,  a4,h,
                   b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h,
                   a4,q,  a4,q,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,    a4,q,  g4,q,  a4,q,  d4,h,
                   b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h};
                   
int bass_tune[] = {120, B3,q,  B3,q,  c4,q,  d4,q,  d4,q,  c4,q,  B3,q,  A3,q,  G3,q,  G3,q,  A3,q,  B3,q,  B3,q+e,  A3,e,  A3,h,
                   B3,q,  B3,q,  c4,q,  d4,q,  d4,q,  c4,q,  B3,q,  A3,q,  G3,q,  G3,q,  A3,q,  B3,q,  A3,q+e,  A3,e,  G3,h,
                   A3,q,  A3,q,  B3,q,  G3,q,  A3,q,  B3,e,  c4,e,  B3,q,  G3,q,  A3,q,  B3,e,  c4,e,  A3,q,    A3,q,  G3,q,  A3,q,  D3,h,
                   B3,q,  B3,q,  c4,q,  d4,q,  d4,q,  c4,q,  B3,q,  A3,q,  G3,q,  G3,q,  A3,q,  B3,q,  A3,q+e,  G3,e,  G3,h};


static void Melody(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    speaker1.playMusic(tune, sizeof(tune));      //nameOfYourPiezo.playMusic(nameOfYourSong[], sizeof(nameOfYourSong), this is all you need to make some music, Enjoy!
  }
}

static void Bass(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    speaker2.playMusic(bass_tune, sizeof(bass_tune));      //nameOfYourPiezo.playMusic(nameOfYourSong[], sizeof(nameOfYourSong), this is all you need to make some music, Enjoy!
  }
}

static void vOrange(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;; )
  {
    Serial.println(F("Orange On"));
    digitalWrite(ORGLED, LOW);
    vTaskDelay( 250 );
    Serial.println(F("Orange Off"));
    digitalWrite(ORGLED, HIGH);
    vTaskDelay( 250 );
  }
}

void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Arduino FreeRTOS Test | Multi Music!"));
  pinMode(HEARTBEAT, OUTPUT);
  pinMode(ORGLED, OUTPUT);
  xTaskCreate( Heartbeat, ( signed char * ) "Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  xTaskCreate( vOrange, ( signed char * ) "Orange", configMINIMAL_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate( Melody, ( signed char * ) "Melody", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  xTaskCreate( Bass, ( signed char * ) "Bass", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  vTaskStartScheduler(); 
}

void loop()
{
}
