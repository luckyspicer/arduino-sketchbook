#include "DuinOS/DuinOS.h"
 #include "pitches.h"

int HEARTBEAT = 13;   /* Heartbeat LED */
int ORGLED = 3;
int speaker1 = 7;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */
int speaker2 = 8;

// notes in the melody:
int melody1[] = {NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3,0, NOTE_B3, NOTE_C4};
int melody2[] = {0, NOTE_B3, NOTE_C4,NOTE_C4, NOTE_G3,NOTE_G3, NOTE_A3, NOTE_G3};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations1[] = {4, 8, 8, 4, 4, 4, 4, 4 };
int noteDurations2[] = {4, 4, 4, 4, 4, 8, 8, 4 };

static void Melody(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    for (int thisNote = 0; thisNote < 8; thisNote++) {
      // to calculate the note duration, take one second 
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      int noteDuration = 1000/noteDurations1[thisNote];
      tone(speaker1, melody1[thisNote],noteDuration);
  
      // to distinguish the notes, set a minimum time between them.
      // the note's duration + 30% seems to work well:
      int pauseBetweenNotes = noteDuration * 1.30;
      vTaskDelay(pauseBetweenNotes);
      // stop the tone playing:
      noTone(speaker1);
    }
  }
}

static void Bass(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    for (int thisNote = 0; thisNote < 8; thisNote++) {
      // to calculate the note duration, take one second 
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      int noteDuration = 1000/noteDurations2[thisNote];
      tone(speaker2, melody2[thisNote],noteDuration);
  
      // to distinguish the notes, set a minimum time between them.
      // the note's duration + 30% seems to work well:
      int pauseBetweenNotes = noteDuration * 1.30;
      vTaskDelay(pauseBetweenNotes);
      // stop the tone playing:
      noTone(speaker2);
    }
  }
}

static void vOrange(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;; )
  {
    Serial.println(F("Orange On"));
    digitalWrite(ORGLED, LOW);
    vTaskDelay( 250 );
    Serial.println(F("Orange Off"));
    digitalWrite(ORGLED, HIGH);
    vTaskDelay( 250 );
  }
}

void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Arduino FreeRTOS Test | Multi Music!"));
  pinMode(HEARTBEAT, OUTPUT);
  pinMode(ORGLED, OUTPUT);
  xTaskCreate( Heartbeat, ( signed char * ) "Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  //xTaskCreate( vOrange, ( signed char * ) "Orange", configMINIMAL_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate( Melody, ( signed char * ) "Melody", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  xTaskCreate( Bass, ( signed char * ) "Bass", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  vTaskStartScheduler(); 
}

void loop()
{
}
