#include <DuinOS/DuinOS.h>
#include <Interrupt_Types.h>
#include <WheelEncoder.h>

int GRNLED = 7;
int ORGLED = 8;
int HEARTBEAT = 13;
static const int dir_pin = 4;

static void vGreen(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    digitalWrite(GRNLED, LOW);
    vTaskDelay( 500 );
    digitalWrite(GRNLED, HIGH);
    vTaskDelay( 500 );
  }
}

static void vOrange(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    digitalWrite(ORGLED, LOW);
    vTaskDelay( 250 );
    digitalWrite(ORGLED, HIGH);
    vTaskDelay( 250 );
  }
}

void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void Encoder0(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    Serial.print(F("Count = "));
    Serial.print(WheelEncoder0.GetCount());
    Serial.print(F("  Direction = "));
    Serial.print(WheelEncoder0.GetDirection());
    Serial.print(F("  Speed = "));
    Serial.println(WheelEncoder0.GetFrequency());   
    vTaskDelay( 100 );
  }
}

void setup()
{
  Serial.begin(9600);
  WheelEncoder0.Connect(Interrupt_Zero, Interrupt_Change, dir_pin);
  Serial.println(F("Arduino FreeRTOS Test"));
  pinMode(GRNLED, OUTPUT);
  pinMode(ORGLED, OUTPUT);
  pinMode(HEARTBEAT, OUTPUT);
  xTaskCreate( Heartbeat, ( signed char * ) "Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  xTaskCreate( Encoder0, ( signed char * ) "Encoder0", 128, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate( vGreen, ( signed char * ) "Green", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  xTaskCreate( vOrange, ( signed char * ) "Orange", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  vTaskStartScheduler(); 
}

void loop()
{
}
