#include "DuinOS/DuinOS.h"

int GRNLED = 2;
int ORGLED = 3;
int HEARTBEAT = 13;

static void vGreen(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    Serial.println(F("Green On"));
    digitalWrite(GRNLED, LOW);
    vTaskDelay( 500 );
    Serial.println(F("Green Off"));
    digitalWrite(GRNLED, HIGH);
    vTaskDelay( 500 );
  }
}

static void vOrange(void *pvParameters)
{
  ( void ) pvParameters;
  
  for( ;;)
  {
    Serial.println(F("Orange On"));
    digitalWrite(ORGLED, LOW);
    vTaskDelay( 250 );
    Serial.println(F("Orange Off"));
    digitalWrite(ORGLED, HIGH);
    vTaskDelay( 250 );
  }
}

void Heartbeat(void *pvParameters)
{
  ( void ) pvParameters;
  
  for ( ;; )
  {
    digitalWrite(HEARTBEAT, HIGH);
    vTaskDelay( 500 );
    digitalWrite(HEARTBEAT, LOW);
    vTaskDelay( 3000 );
  }
}

void setup()
{
  Serial.begin(9600);
  Serial.println(F("Arduino FreeRTOS Test"));
  pinMode(GRNLED, OUTPUT);
  pinMode(ORGLED, OUTPUT);
  pinMode(HEARTBEAT, OUTPUT);
  xTaskCreate( Heartbeat, ( signed char * ) "Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
  xTaskCreate( vGreen, ( signed char * ) "Green", configMINIMAL_STACK_SIZE, NULL, HIGH_PRIORITY, NULL );
  xTaskCreate( vOrange, ( signed char * ) "Orange", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
  vTaskStartScheduler(); 
}

void loop()
{
}
