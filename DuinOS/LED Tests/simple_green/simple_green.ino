const int greenLED = 2;

taskLoop(LEDFlash1) {
  Serial.println("LED ON");
  digitalWrite(greenLED, LOW);
  static long startTime = millis();
  while(millis()-startTime < 500);
  Serial.println("LED OFF");
  digitalWrite(greenLED, HIGH);
  startTime = millis();
  while(millis()-startTime < 500);
}

void setup() {
  Serial.begin(9600);
  Serial.println("Test of LED and DuinOS");
  Serial.print("configCPU_CLOCK_HZ = ");
  Serial.println(configCPU_CLOCK_HZ);
  Serial.print("configTICK_RATE_HZ = ");
  Serial.println(configTICK_RATE_HZ);
  Serial.print("configMINIMAL_STACK_SIZE = ");
  Serial.println(configMINIMAL_STACK_SIZE);
  Serial.print("configTOTAL_HEAP_SIZE = ");
  Serial.println(configTOTAL_HEAP_SIZE);
  long startTime = millis();
  while(millis()-startTime < 1000);
  long endTime = millis();
  Serial.print("Delay Lasted ");
  Serial.print(endTime-startTime);
  Serial.println(" mS");
  pinMode(greenLED, OUTPUT);
  createTaskLoop(LEDFlash1, NORMAL_PRIORITY);
}

void loop() {
  nextTask();
}
