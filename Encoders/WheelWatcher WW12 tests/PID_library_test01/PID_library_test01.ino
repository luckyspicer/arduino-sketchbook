#include <Interrupt_Types.h>
#include <WheelEncoder.h>
#include <AFMotor.h>
#include <PID_v1.h>

AF_DCMotor motor(1);
static const uint8_t maxSpeed = 192;
static const uint8_t minSpeed = 32;
static const int dir_pin = 5;

// PID Stuff
//Define Variables we'll be connecting to
double Setpoint, Input, Output;
//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint,1,0,0, DIRECT);

void setup()
{
  Serial.begin(9600);
  Serial.println("P controller Test !");
  WheelEncoder0.Connect(Interrupt_One, Interrupt_Change, dir_pin);
  //turn the PID on
  Input = WheelEncoder0.GetCount();
  Setpoint = 400;
  myPID.SetOutputLimits(-192, 192);
  myPID.SetSampleTime(25);
  myPID.SetMode(AUTOMATIC);
}

// 192 Top Motor Speed
// 32 Lower Motor Speed
void loop() 
{
  Input = WheelEncoder0.GetCount();
  myPID.Compute();
  if(Output < 0)
  {
    motor.setSpeed(Output);
    motor.run(BACKWARD);
  }
  else
  {
    motor.setSpeed(Output);
    motor.run(FORWARD);
  }
  
  if(millis() % 1000 == 0) 
  {
    Serial.print("Count = ");
    Serial.println(WheelEncoder0.GetCount());
    Serial.print("Motor Speed = ");
    Serial.println(Output);
    Serial.println();
  }
}

