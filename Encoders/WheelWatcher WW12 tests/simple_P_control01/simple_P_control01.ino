#include <Interrupt_Types.h>
#include <WheelEncoder.h>
#include <AFMotor.h>

AF_DCMotor motor(1);
static const uint8_t maxSpeed = 192;
static const uint8_t minSpeed = 32;
static const int dir_pin = 5;

void setup()
{
  Serial.begin(9600);
  Serial.println("P controller Test !");
  WheelEncoder0.Connect(Interrupt_One, Interrupt_Change, dir_pin);
}

// 192 Top Motor Speed
// 32 Lower Motor Speed
int desiredPos = 400;
void loop() 
{
  int motorSpeed = desiredPos - WheelEncoder0.GetCount();
  if (motorSpeed < 0)
  {
    motor.run(BACKWARD);
    motorSpeed = -motorSpeed;
  }
  else
  {
    motor.run(FORWARD);
  }
  
  if (motorSpeed > maxSpeed)
  {
    motorSpeed = maxSpeed;
  }
  else if (motorSpeed < minSpeed && motorSpeed > 5)
  {
    motorSpeed = minSpeed;
  }
  else if (motorSpeed < 5) {
    motorSpeed = 0;
  }
  
  motor.setSpeed(motorSpeed);
  if(millis() % 1000 == 0) 
  {
    Serial.print("Count = ");
    Serial.println(WheelEncoder0.GetCount());
    Serial.print("Motor Speed = ");
    Serial.println(motorSpeed);
    Serial.println();
  }
}

