#include <Interrupt_Types.h>
#include <WheelEncoder.h>
#include <AFMotor.h>

AF_DCMotor motor(1);
static const uint8_t topSpeed = 255;
static const int dir_pin = 5;

void setup()
{
  Serial.begin(9600);
  Serial.println("Motor/Encoder Speed Test !");
  WheelEncoder0.Connect(Interrupt_One, Interrupt_Change, dir_pin);
  
  // turn on motor
  Serial.println("Forward");
  
  motor.run(FORWARD);
  for (int i=0; i < topSpeed; i++) {
    motor.setSpeed(i);  
    delay(10);
 }
 Serial.println("Spinning!");
}

void loop() 
{
  Serial.print("Encoder0  Count = ");
  Serial.print(WheelEncoder0.GetCount());
  Serial.print("  Direction = ");
  Serial.print(WheelEncoder0.GetDirection());
  Serial.print("  Speed = ");
  Serial.println(WheelEncoder0.GetFrequency());
  delay(100);
}

