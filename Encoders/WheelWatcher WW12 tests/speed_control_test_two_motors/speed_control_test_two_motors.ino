#include <Interrupt_Types.h>
#include <WheelEncoder.h>
#include <AFMotor.h>
#include <PID_v1.h>

static const int right_dir_pin = 22;
static const int left_dir_pin = 23;
AF_DCMotor rightMotor(2);
AF_DCMotor leftMotor(1);

// PID Stuff
//Define Variables we'll be connecting to
double rightSetpoint, rightInput, rightOutput, leftSetpoint, leftInput, leftOutput;
//Specify the links and initial tuning parameters
PID rightPID(&rightInput, &rightOutput, &rightSetpoint, 10, 5, 1, DIRECT);
PID leftPID(&leftInput, &leftOutput, &leftSetpoint, 10, 5, 1, DIRECT);

void setup()
{
  Serial.begin(9600);
  Serial.println("Wheel Watcher Encoder Class Test !");
  WheelEncoder0.Connect(Interrupt_Two, Interrupt_Falling, right_dir_pin);
  WheelEncoder1.Connect(Interrupt_Three, Interrupt_Falling, left_dir_pin);  
   //turn the PID on
  rightInput = WheelEncoder0.GetFrequency();
  leftInput = WheelEncoder1.GetFrequency();
  rightSetpoint = 50;
  leftSetpoint = 50;
  rightPID.SetOutputLimits(0, 255);
  leftPID.SetOutputLimits(0, 255);
  rightPID.SetSampleTime(10);
  leftPID.SetSampleTime(10);
  rightPID.SetMode(AUTOMATIC);
  leftPID.SetMode(AUTOMATIC);
}

void loop() 
{
  rightInput = WheelEncoder0.GetFrequency();
  leftInput = WheelEncoder1.GetFrequency();
  rightPID.Compute();
  leftPID.Compute();
  
  rightMotor.setSpeed(rightOutput);
  leftMotor.setSpeed(leftOutput);
  rightMotor.run(FORWARD);
  leftMotor.run(BACKWARD);
  if(millis() % 1000 == 0) 
  {
    Serial.print("Setpoint = ");
    Serial.println(rightSetpoint);
    Serial.print("Right Frequency = ");
    Serial.print(WheelEncoder0.GetFrequency());
    Serial.print(" | Left Frequency = ");
    Serial.println(WheelEncoder1.GetFrequency());
    Serial.print("Right Motor Speed = ");
    Serial.print(rightOutput);
    Serial.print(" | Left Motor Speed = ");
    Serial.println(leftOutput);
    Serial.println("\n");
  }
}
