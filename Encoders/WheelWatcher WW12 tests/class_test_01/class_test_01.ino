#include <Interrupt_Types.h>
#include <WheelEncoder.h>

static const int dir_pin = 22;

void setup()
{
  Serial.begin(9600);
  Serial.println("Wheel Watcher Encoder Class Test !");
  WheelEncoder0.Connect(Interrupt_Two, Interrupt_Falling, dir_pin);
}

void loop() 
{
  Serial.print("Encoder0  Count = ");
  Serial.print(WheelEncoder0.GetCount());
  Serial.print("  Direction = ");
  Serial.print(WheelEncoder0.GetDirection());
  Serial.print("  Speed = ");
  Serial.println(WheelEncoder0.GetFrequency());
  delay(100);
}
