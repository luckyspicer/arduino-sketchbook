#include <Interrupt_Types.h>

// For the new class
static const unsigned long US_PER_S = 1000000;
static const unsigned int MULTIPLIER = 4;
static const unsigned int MULTIPLIER_POW2 = 2;
volatile long _position;
volatile unsigned int  _direction;
volatile int _speedReady = 0;
volatile unsigned long _periodBuf[MULTIPLIER];
volatile unsigned int _periodBufIdx = 0;
volatile unsigned long _lastEncoderPulseTime = 0;

// Testing the new types
InterruptNumber_t inter_num = Interrupt_Zero;
InterruptMode_t inter_mode = Interrupt_Change;

// For this simulation
static const unsigned int dir_pin = 4;

void setup() {
  Serial.begin(9600);
  Serial.println("Wheel Watcher Encoder Test!");
  attachInterrupt(inter_num, encoder_isr, inter_mode);
}

void loop() { 
  Serial.print("Count = ");
  Serial.print(_position);
  Serial.print("  Speed = ");
  Serial.print(GetFrequency());
  Serial.print("  _periodBufIdx =");
  Serial.println(_periodBufIdx);
}

void encoder_isr(void)
{
  // Get the Current time since reset in micro-seconds
  unsigned long tempMicroTime = micros();
  
  // Position: count up or count down based on the direction
  // Clockwise when looking at the attached wheel/motor is postive
  if (digitalRead(dir_pin) == 1)
  {
    ++_position;
    _direction = 1;
  }
  else
  {
    --_position;
    _direction = 0;
  }
  
  // Period: Measure the time since the last encoder pulse
  //         Store in the running average ring buffer
  _periodBuf[_periodBufIdx] = tempMicroTime - _lastEncoderPulseTime;
  _periodBufIdx = (_periodBufIdx + 1) % MULTIPLIER;
  if(_periodBufIdx == 0) 
  {
    _speedReady = 1;
  }
  _lastEncoderPulseTime = tempMicroTime;
}

unsigned long GetFrequency(void)
{
  if(_speedReady == 0) {
    return 0;
  }
  else
  {
    unsigned long tempAvgPeriod = 0;
    for(int i = 0; i < MULTIPLIER; ++i)
    {
      tempAvgPeriod += _periodBuf[i];
    }
    tempAvgPeriod = (tempAvgPeriod >> MULTIPLIER_POW2);
    
    _speedReady = 0;
    return (US_PER_S / tempAvgPeriod);
  }
}
