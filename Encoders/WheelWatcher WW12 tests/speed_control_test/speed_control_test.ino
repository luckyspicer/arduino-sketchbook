#include <Interrupt_Types.h>
#include <WheelEncoder.h>
#include <AFMotor.h>
#include <PID_v1.h>

static const int dir_pin = 22;
AF_DCMotor rightMotor(2);

// PID Stuff
//Define Variables we'll be connecting to
double Setpoint, Input, Output;
//Specify the links and initial tuning parameters
PID myPID(&Input, &Output, &Setpoint, 10, 5, 0, DIRECT);

void setup()
{
  Serial.begin(9600);
  Serial.println("Wheel Watcher Encoder Class Test !");
  WheelEncoder0.Connect(Interrupt_Two, Interrupt_Falling, dir_pin);
  
   //turn the PID on
  Input = WheelEncoder0.GetCount();
  Setpoint = 50;
  myPID.SetOutputLimits(0, 255);
  myPID.SetSampleTime(10);
  myPID.SetMode(AUTOMATIC);
}

void loop() 
{
  Input = WheelEncoder0.GetFrequency();
  myPID.Compute();
  
  rightMotor.setSpeed(Output);
  rightMotor.run(FORWARD);
  if(millis() % 1000 == 0) 
  {
    Serial.print("Setpoint = ");
    Serial.println(Setpoint);
    Serial.print("Frequency = ");
    Serial.println(WheelEncoder0.GetFrequency());
    Serial.print("Motor Speed = ");
    Serial.println(Output);
    Serial.println();
  }
}
