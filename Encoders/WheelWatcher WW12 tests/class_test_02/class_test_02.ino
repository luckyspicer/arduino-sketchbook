#include <Interrupt_Types.h>
#include <WheelEncoder.h>

static const int dir_pin = 4;

void setup()
{
  Serial.begin(9600);
  Serial.println("Wheel Watcher Encoder Class Test !");
  WheelEncoder0.Connect(Interrupt_Zero, Interrupt_Change, dir_pin);
  
  while(millis()/1000 < 5)
  {
    Serial.print("Count = ");
    Serial.print(WheelEncoder0.GetCount());
    Serial.print("  Direction = ");
    Serial.print(WheelEncoder0.GetDirection());
    Serial.print("  Speed = ");
    Serial.println(WheelEncoder0.GetFrequency());   
  }
  WheelEncoder0.Reset();
  while(millis()/1000 < 10)
  {
    Serial.print("Count = ");
    Serial.print(WheelEncoder0.GetCount());
    Serial.print("  Direction = ");
    Serial.print(WheelEncoder0.GetDirection());
    Serial.print("  Speed = ");
    Serial.println(WheelEncoder0.GetFrequency());   
  }
  WheelEncoder0.Disconnect();
}

void loop() 
{
  Serial.print("Count = ");
  Serial.print(WheelEncoder0.GetCount());
  Serial.print("  Direction = ");
  Serial.print(WheelEncoder0.GetDirection());
  Serial.print("  Speed = ");
  Serial.println(WheelEncoder0.GetFrequency());   
}

