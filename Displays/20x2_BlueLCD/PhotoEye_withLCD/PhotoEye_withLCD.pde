// include the library code:
#include <LiquidCrystal.h>

// Analgo Pins with the photo-resistor eyes
int photoEyeRight = 9;
int photoEyeLeft = 10;
int RightEyeVal;
int LeftEyeVal;

// initialize the library with the numbers of the interface pins
//LiquidCrystal lcd(rs [orange], R/W [blue], E [green], d4 [red], d5 [gray], d6 [yellow] ,d7 [orange])
// also the single yellow line by itself is the contrast pin!
LiquidCrystal lcd(34, 36, 38, 26, 28, 30, 32);

void setup() {
  // set up the LCD's number of rows and columns: 
  lcd.begin(20, 2);
  // Print a message to the LCD.
  lcd.print("   Hello, World!");
  lcd.setCursor(1,1);
  lcd.print("PhotoResistorEyes!");
  
  // Also settup the serial interface for debuggin and such!
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("PhotoResistorEyes!");
  
  delay(3000);
  lcd.clear();
}

void loop() {
  RightEyeVal = readPR(photoEyeRight);
  LeftEyeVal = readPR(photoEyeLeft);
  Serial.print("Photo-Resistor Right ");
  Serial.println(RightEyeVal);
  Serial.print("Photo-Resistor Left ");
  Serial.println(LeftEyeVal);
  Serial.println("");
  
  lcd.clear();
  lcd.print("Left Eye   Right Eye");
  lcd.setCursor(2,1);
  lcd.print(LeftEyeVal);
  lcd.setCursor(14,1);
  lcd.print(RightEyeVal);
  //delay(100);
}

int readPR(int photoR) {
  int avgPR = 0;
  for(int i = 0; i < 10; i++) {
    avgPR = analogRead(photoR) + avgPR;
  }
  avgPR = avgPR/10;
  return avgPR;
}
