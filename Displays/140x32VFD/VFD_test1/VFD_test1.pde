//VFD Control Pins
int  _wr_pin = 11;                //VFD pin 5, Data Write, Input to VFD
int  _rd_pin = 10;                //VFD pin 6, Data Read,  Input to VFD

//VFD data pins
int _data_pins[8];

void setup()  {
  pinMode(_wr_pin, OUTPUT);
  pinMode(_rd_pin, OUTPUT);
  _data_pins[0] = 9;           //VFD pin  7, D0 Input/Output
  _data_pins[1] = 8;           //VFD pin  8, D1 Input/Output 
  _data_pins[2] = 7;           //VFD pin  9, D2 Input/Output
  _data_pins[3] = 6;           //VFD pin 10, D3 Input/Output
  _data_pins[4] = 5;           //VFD pin 11, D4 Input/Output
  _data_pins[5] = 4;           //VFD pin 12, D5 Input/Output
  _data_pins[6] = 3;           //VFD pin 13, D6 Input/Output
  _data_pins[7] = 2;           //VFD pin 14, D7(PBUSY) Input/Output
  
  Serial.begin(9600);             //Serial Monitor for debugging
}

void loop()  {
  delay(500);
  VFDwrite(0x0C);               //clear display
  
  VFDwrite('H');               // H
  delay(500);
  VFDwrite('e');               // e
  delay(500);
  VFDwrite('l');               // l
  delay(500);
  VFDwrite('l');               // l
  delay(500);
  VFDwrite('o');               // o
  delay(500);
  VFDwrite(' ');               // Space
  delay(500);
  VFDwrite('W');               // W
  delay(500);
  VFDwrite('o');               // o
  delay(500);
  VFDwrite('r');               // r
  delay(500);
  VFDwrite('l');               // l
  delay(500);
  VFDwrite('d');                // d
  delay(500);
  VFDwrite(0x0C);
  delay(1500);
  
  VFDwrite('U');
  VFDwrite('o');
  VFDwrite('f');
  VFDwrite('L');
  delay(1000);
  VFDwrite(0x0A);
  VFDwrite(0x0D);
  
  VFDwrite('2');
  VFDwrite('0');
  VFDwrite('1');
  VFDwrite('1');
  VFDwrite(' ');
  VFDwrite('I');
  VFDwrite('E');
  VFDwrite('E');
  VFDwrite('E');
  delay(1000);
  VFDwrite(0x0A);
  VFDwrite(0x0D);
  
  VFDwrite('S');
  VFDwrite('o');
  VFDwrite('u');
  VFDwrite('t');
  VFDwrite('h');
  VFDwrite('e');
  VFDwrite('a');
  VFDwrite('s');
  VFDwrite('t');
  VFDwrite('C');
  VFDwrite('o');
  VFDwrite('n');
  delay(1000);
  VFDwrite(0x0A);
  VFDwrite(0x0D);
  
  VFDwrite('H');
  VFDwrite('a');
  VFDwrite('r');
  VFDwrite('d');
  VFDwrite('w');
  VFDwrite('a');
  VFDwrite('r');
  VFDwrite('e');
  VFDwrite(' ');
  VFDwrite('C');
  VFDwrite('o');
  VFDwrite('m');
  VFDwrite('p');
  VFDwrite('!'); 
  for(;;)  {
  }
}

//Function to send commands out to VFD
void VFDwrite(byte value) {
  digitalWrite(_wr_pin, LOW);
  digitalWrite(_rd_pin, HIGH);
  
  for (int i = 0; i <= 7; i++) {
    pinMode(_data_pins[i], OUTPUT);
    digitalWrite(_data_pins[i], (value >> i) & 0x01);
  }
  digitalWrite(_wr_pin, HIGH);    //data or command is sent on rising edge of /WR
  //digitalWrite(_rd_pin, LOW);
  delayMicroseconds(35000);         //should change this to reading pbusy;
}
    
