#include <VFD.h>  //including VFD.h allows access to the special vfd library functions

// Create a new instance of the VFD object, this one is named vfdisp, but choose whatever you want
// The constructor needs the pin numbers of the pins: (wr, rd, d0, d1, d2, d3, d4, d5, d6, d7)
// The pins are numbered 5 through 14 on the VFD pcb and spec sheet
VFD vfdisp(53, 51, 49, 47, 45, 43, 41, 39, 37, 35);

void setup() {
  vfdisp.clear();
  vfdisp.print("   This is a Test");
  delay(1000);
  vfdisp.newLine();
  vfdisp.print("VFD Library Examples");
  delay(3000);
}
void loop() {
  // clear the entire display
  vfdisp.clear();
 
  // Example of the cursor on/off
  vfdisp.cursorOff();
  vfdisp.print("Command: cursorOff()");
  vfdisp.newLine();
  delay(3000);
  vfdisp.clear();
  vfdisp.cursorOn();
  vfdisp.print("Command: cursorOn()");
  vfdisp.newLine();
  delay(3000);
  
  
  // Example of tab() and backspace()
  vfdisp.clear();
  vfdisp.print("Command: tab()");
  vfdisp.newLine();
  delay(1500);
  vfdisp.tab();
  delay(1500);
  vfdisp.tab();
  delay(1500);
  vfdisp.tab();
  delay(2500);
  
  
  vfdisp.clear();
  vfdisp.print("Command: backspace()");
  vfdisp.tab();
  vfdisp.tab();
  vfdisp.tab();
  delay(1500);
  vfdisp.backspace();
  delay(1500);
  vfdisp.backspace();
  delay(1500);
  vfdisp.backspace();
  delay(2500);
  
  // Example of carriageReturn(), lineFeed() and newLine()
  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("carriageReturn()");
  delay(2000);
  vfdisp.carriageReturn();
  delay(2000);

  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("lineFeed()");
  delay(2000);
  vfdisp.lineFeed();
  delay(2000);
  
  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("newLine()");
  delay(2000);
  vfdisp.newLine();
  delay(2000);
  
  // Examples of home() and setCursor()
  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("home()");
  delay(2000);
  vfdisp.home();
  delay(2000);
  
  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("setCursor(16, 2)");
  delay(2000);
  vfdisp.setCursor(16, 2);
  delay(2000);
  vfdisp.setCursor(0, 1);
  vfdisp.print("setCursor(64, 3)");
  delay(2000);
  vfdisp.setCursor(64, 3);
  delay(2000);

  // Examples of brightness( );
  vfdisp.clear();
  for(int i = 8; i > 0; i--) {
    vfdisp.home();
    vfdisp.print("Command:");
    vfdisp.newLine();
    vfdisp.print("brightness(");
    vfdisp.print(i);
    vfdisp.print(")");
    vfdisp.brightness(i);
    delay(1500);
  }
  for(int i = 1; i <= 8; i++) {
    vfdisp.home();
    vfdisp.print("Command:");
    vfdisp.newLine();
    vfdisp.print("brightness(");
    vfdisp.print(i);
    vfdisp.print(")");
    vfdisp.brightness(i);
    delay(1500);
  }
  
  // Examples of invertOn() and invertOff()
  vfdisp.clear();
  vfdisp.invertOn();
  vfdisp.print("Command: invertOn()");
  delay(3000);
  
  vfdisp.invertOff();
  vfdisp.home();
  vfdisp.print("Command: invertOff()");
  delay(3000);
  
  // Examples of charWidth( );
  for(int i = 0; i < 4; i++) {
    vfdisp.clear();
    vfdisp.print("Command:");
    vfdisp.newLine();
    vfdisp.print("charWidth(");
    vfdisp.print(i);
    vfdisp.print(")");
    vfdisp.charWidth(i);
    delay(2500);
  }

  // Examples of charMagnify( );
  for(int i = 1; i <= 4; i++) {
    for(int j = 1; j <= 2; j++) {
      vfdisp.clear();
      vfdisp.charMagnify(1,1);
      vfdisp.print("Command:");
      vfdisp.newLine();
      vfdisp.print("charMagnify(");
      vfdisp.print(i);
      vfdisp.print(", ");
      vfdisp.print(j);
      vfdisp.print(")");
      delay(2500);
      
      vfdisp.clear();
      vfdisp.charMagnify(i,j);
      vfdisp.print("Test!");
      delay(2500);
    }
  }
  
  // Examples of blink()
  vfdisp.charMagnify(1,1);
  vfdisp.cursorOff();
  vfdisp.clear();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("blink(P = 1)");
  delay(1000);
  vfdisp.blink(1, 500, 500, 3000);
  
  vfdisp.home();
  vfdisp.print("Command:");
  vfdisp.newLine();
  vfdisp.print("blink(P = 2)");
  delay(1000);
  vfdisp.blink(2, 500, 500, 3000);
    
  // Example of scroll()
  vfdisp.clear();
  vfdisp.newLine();
  vfdisp.print("Command: scroll()");
  delay(1500);
  vfdisp.scroll(4, 256, 14);
  delay(1500); 
}
