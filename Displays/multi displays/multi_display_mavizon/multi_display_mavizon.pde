// include the library code:
#include <LiquidCrystal.h>
#include <VFD.h>  //including VFD.h allows access to the special vfd library functions
#include <avr/pgmspace.h>

#include <ks0108.h>
//#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font
#include "baskerville.h"     // homemade big font
#include "cardinal.h"

// Create a new instance of the VFD object, this one is named svfdisp, but choose whatever you want
// The constructor needs the pin numbers of the pins: (wr, rd, d0, d1, d2, d3, d4, d5, d6, d7)
// The pins are numbered 5 through 14 on the VFD pcb and spec sheet
VFD svfdisp(SERIAL2);

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(30, 31, 43, 45, 47, 49);

uint8_t imageBuffer[560];

PROGMEM prog_uint8_t banner[] = { 0xCB, 0xBB, 0xBB, 0xB3, 0x81, 0x11, 0x11, 0x11, 0x0B, 0xBB, 0xBB, 0xB0, 0x0F, 0xFF, 0xFF, 0xF0,
                     0xB8, 0x00, 0x00, 0x3B, 0x13, 0xFF, 0xFF, 0x91, 0xB7, 0xFF, 0xFF, 0xDB, 0xF7, 0xFF, 0xFF, 0xDF,
                     0xB7, 0xE3, 0xFF, 0xDB, 0x17, 0xC3, 0xEF, 0xD1, 0xB7, 0xC7, 0xE7, 0xDB, 0xF7, 0x87, 0xE7, 0xDF,
                     0xB7, 0x80, 0x00, 0x5B, 0x17, 0x80, 0x00, 0x51, 0xB7, 0x80, 0x00, 0x5B, 0xF7, 0x80, 0x00, 0x5F,
                     0xB7, 0x87, 0xE7, 0xDB, 0x17, 0xC7, 0xE7, 0xD1, 0xB7, 0xC3, 0xEF, 0xDB, 0xF7, 0xE3, 0xFF, 0xDF,
                     0xB7, 0xEF, 0xF7, 0xDB, 0x17, 0xC0, 0x07, 0xD1, 0xB7, 0xC0, 0x07, 0xDB, 0xF7, 0xC0, 0x07, 0xDF,
                     0xB7, 0xFD, 0xE7, 0xDB, 0x17, 0xF9, 0xFF, 0xD1, 0xB7, 0xF8, 0x07, 0xDB, 0xF7, 0xF8, 0x07, 0xDF,
                     0xB7, 0xFC, 0x07, 0xDB, 0x17, 0xFF, 0xE7, 0xD1, 0xB7, 0xFF, 0xFF, 0xDB, 0xF7, 0xFE, 0x1F, 0xDF,
                     0xB7, 0xFC, 0x0F, 0xDB, 0x17, 0xF8, 0x07, 0xD1, 0xB7, 0xFB, 0x47, 0xDB, 0xF7, 0xF8, 0x67, 0xDF,
                     0xB7, 0xF8, 0x67, 0xDB, 0x17, 0xFC, 0x4F, 0xD1, 0xB7, 0xFF, 0xFF, 0xDB, 0xF7, 0xFF, 0xFF, 0xDF,
                     0xB7, 0xFF, 0xFF, 0xDB, 0x17, 0xFF, 0xFF, 0xD1, 0xB7, 0xFF, 0xFF, 0xDB, 0xF7, 0xFF, 0xE7, 0xDF,
                     0xB7, 0xFF, 0xC7, 0xDB, 0x17, 0xFF, 0x07, 0xD1, 0xB7, 0xFC, 0x27, 0xDB, 0xF7, 0xC0, 0x3F, 0xDF,
                     0xB7, 0xC3, 0x3F, 0xDB, 0x17, 0xE0, 0x27, 0xD1, 0xB7, 0xF0, 0x07, 0xDB, 0xF7, 0xF8, 0x01, 0xDF,
                     0xB7, 0xFF, 0x01, 0xDB, 0x17, 0xFF, 0xC0, 0xD1, 0xB7, 0xFF, 0xE0, 0xDB, 0xF7, 0xFF, 0xFC, 0x5F,
                     0xB7, 0xFD, 0xF4, 0x5B, 0x17, 0xFC, 0x04, 0x51, 0xB7, 0xF8, 0x04, 0x5B, 0xF7, 0xF8, 0x04, 0x5F,
                     0xB7, 0xFC, 0xF4, 0x5B, 0x17, 0xF8, 0x7C, 0x51, 0xB7, 0xF8, 0x7C, 0x5B, 0xF7, 0xFC, 0xFC, 0x5F,
                     0xB7, 0xFF, 0xFC, 0x5B, 0x17, 0xFD, 0xFC, 0x51, 0xB7, 0xF8, 0x0C, 0x5B, 0xF7, 0xF0, 0x06, 0x5F,
                     0xB7, 0xE0, 0x06, 0x5B, 0x17, 0xF9, 0xE6, 0x51, 0xB7, 0xF9, 0xEE, 0x5B, 0xF7, 0xFE, 0xFE, 0x5F,
                     0xB7, 0xFE, 0x3F, 0x5B, 0x17, 0xFE, 0x1F, 0x51, 0xB7, 0xFF, 0x0F, 0x5B, 0xF7, 0xFF, 0x87, 0x5F,
                     0xB7, 0xC1, 0xC3, 0x5B, 0x17, 0x80, 0xE1, 0x51, 0xB7, 0x00, 0x71, 0x5B, 0xF6, 0x00, 0x71, 0x5F,
                     0xB6, 0x0C, 0x31, 0x5B, 0x16, 0x0C, 0x01, 0x51, 0xB7, 0x04, 0x01, 0x5B, 0xF7, 0x02, 0x03, 0x5F,
                     0xB7, 0x83, 0x3F, 0x5B, 0x17, 0xFF, 0xFF, 0x51, 0xB7, 0xEF, 0xEF, 0x5B, 0xF7, 0xE0, 0x07, 0xDF,
                     0xB7, 0xC0, 0x07, 0x5B, 0x17, 0xC0, 0x07, 0x51, 0xB7, 0xFD, 0xEF, 0x5B, 0xF7, 0xFD, 0xFF, 0x5F,
                     0xB7, 0xF8, 0x0F, 0xDB, 0x17, 0xF8, 0x07, 0x51, 0xB7, 0xFC, 0x07, 0x5B, 0xF7, 0xFF, 0xEF, 0x5F,
                     0xB7, 0xFF, 0xFF, 0xDB, 0x17, 0xFF, 0x1F, 0x51, 0xB7, 0xFE, 0x0F, 0x5B, 0xF7, 0xFC, 0x0F, 0xDF,
                     0xB7, 0xF9, 0xC7, 0x5B, 0x17, 0xF9, 0xE7, 0xD1, 0xB7, 0xF8, 0xE7, 0x5B, 0xF7, 0xFC, 0x0F, 0xDF,
                     0xB7, 0xFC, 0x1F, 0x5B, 0x17, 0xFF, 0x3F, 0xD1, 0xB7, 0xFF, 0xFF, 0x5B, 0xF7, 0xFD, 0xFF, 0xDF,
                     0xB7, 0xF9, 0xFF, 0xDB, 0x17, 0xF8, 0x7F, 0x51, 0xB7, 0xF8, 0x1F, 0xDB, 0xF7, 0xFD, 0x0F, 0xDF,
                     0xB7, 0xFF, 0x87, 0x5B, 0x17, 0xFD, 0x9F, 0xD1, 0xB7, 0xF8, 0x3F, 0xDB, 0xF7, 0xF8, 0x3F, 0xDF,
                     0xB7, 0xF8, 0x1F, 0x5B, 0x17, 0xFD, 0x07, 0xD1, 0xB7, 0xFF, 0x8F, 0xDB, 0xF7, 0xFD, 0x1F, 0xDF,
                     0xB7, 0xF8, 0x7F, 0x5B, 0x17, 0xF9, 0xFF, 0xD1, 0xB7, 0xFD, 0xFF, 0xDB, 0xF7, 0x07, 0xCF, 0xDF,
                     0xB6, 0x00, 0x83, 0xDB, 0x16, 0x00, 0x83, 0x51, 0xB7, 0x07, 0xCF, 0xDB, 0xF7, 0xFF, 0xFF, 0xDF,
                     0xB7, 0xFF, 0xFF, 0xDB, 0x17, 0xFF, 0xFF, 0xD1, 0xB7, 0xFF, 0xFF, 0x5B, 0xF7, 0xFF, 0xFF, 0xDF,
                     0xB7, 0xFF, 0xFF, 0xDB, 0x17, 0xFF, 0xFF, 0xD1, 0xB3, 0xFF, 0xFF, 0x9B, 0xF8, 0x00, 0x00, 0x3F,
                     0x0F, 0xFF, 0xFF, 0xF0, 0x0B, 0xBB, 0xBB, 0xB0, 0x81, 0x11, 0x11, 0x11, 0xCB, 0xBB, 0xBB, 0xB3};
                     
PROGMEM prog_uint8_t Alan1[] =  { 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF,
                     0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFC, 0x37,
                     0xFF, 0xFF, 0x18, 0xA6, 0xFF, 0xFC, 0xD2, 0x84, 0xFF, 0xF8, 0x04, 0x04, 0xFF, 0xF8, 0xA1, 0x1C,
                     0xFF, 0xF9, 0xE4, 0x3C, 0xFF, 0xF9, 0xE4, 0x3C, 0xFF, 0xF8, 0x41, 0x1C, 0xFF, 0xFC, 0x88, 0x04,
                     0xFF, 0xFD, 0x4A, 0x84, 0xFF, 0xFD, 0x58, 0xA4, 0xFF, 0xFF, 0xBE, 0x37, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF,
                     0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xC0, 0x03, 0xFF, 0xFC,
                     0xBF, 0xFC, 0xFF, 0xFC, 0xAF, 0xF4, 0xFF, 0xFC, 0xBF, 0xFC, 0xFF, 0xFF, 0xB1, 0xFC, 0xFF, 0xFC,
                     0xA1, 0xFC, 0xFF, 0xFC, 0xAF, 0x24, 0xFF, 0xFC, 0xAE, 0x24, 0xFF, 0xFC, 0xB0, 0xFC, 0xFF, 0xFF,
                     0xB8, 0xFC, 0xFF, 0xFE, 0xBF, 0xFC, 0xFF, 0xFC, 0xAF, 0xF4, 0xFF, 0xFC, 0xBF, 0xFC, 0xFF, 0xFC,
                     0xC0, 0x03, 0xFF, 0xFC, 0xC0, 0x03, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF,
                     0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xC0, 0x0F, 0xFC, 0xFF, 0xC0, 0x0F, 0xFC,
                     0xFF, 0xD7, 0xE0, 0x00, 0xFF, 0xD7, 0xE0, 0x00, 0xFF, 0xD5, 0x43, 0xFC, 0xFF, 0xD2, 0xA1, 0x54,
                     0xFF, 0xD8, 0x02, 0xA8, 0xFF, 0xDF, 0xE0, 0x00, 0xFF, 0xDF, 0xE3, 0xFC, 0xFF, 0xD0, 0x03, 0xFC,
                     0xFF, 0xD7, 0xE0, 0x00, 0xFF, 0xD0, 0x03, 0xFC, 0xFF, 0xD0, 0x00, 0x00, 0xFF, 0xD0, 0x00, 0x00,
                     0xFF, 0xD0, 0x00, 0x00, 0xFF, 0xD0, 0x00, 0x00, 0xFF, 0xD0, 0x00, 0x00, 0xFF, 0xD0, 0x02, 0xA8,
                     0xFF, 0xD0, 0x03, 0xFC, 0xFF, 0xD5, 0x41, 0x54, 0xFF, 0xD7, 0xE3, 0xFC, 0xFF, 0xD2, 0xA3, 0xFC,
                     0xFF, 0xD7, 0xE0, 0x00, 0xFF, 0xDF, 0xE0, 0x00, 0xFF, 0xC0, 0x0F, 0xFC, 0xFF, 0xC0, 0x0F, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFE,
                     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFF,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xF0, 0x1F, 0xFC, 0xFC, 0x04, 0x0F, 0xFE,
                     0xFA, 0x20, 0x7B, 0xF7, 0xF7, 0xF0, 0xC0, 0x26, 0xF3, 0xE0, 0x07, 0xC4, 0xF8, 0x00, 0x3C, 0x64,
                     0xFC, 0x02, 0xCA, 0x24, 0xFF, 0xFF, 0x04, 0x2C, 0xFF, 0xFE, 0x8A, 0x14, 0xFF, 0xFE, 0xD1, 0x54,
                     0xFF, 0xFE, 0x80, 0x94, 0xFF, 0xFE, 0xF1, 0x2C, 0xFF, 0xFE, 0xE2, 0x24, 0xFF, 0xFF, 0x7C, 0x67,
                     0xFF, 0xFF, 0x82, 0x44, 0xFF, 0xFF, 0xC0, 0x54, 0xFF, 0xFF, 0xFF, 0x1C, 0xFF, 0xFF, 0xFF, 0xFE,
                     0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC,
                     0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC, 0xFF, 0xFF, 0xFF, 0xFC};
                     
PROGMEM prog_uint8_t Aaron[] = { 0xAA, 0xAA, 0xAA, 0xAA, 0x00, 0x00, 0x00, 0x01, 0xBF, 0xFF, 0xFF, 0xFC, 0x30, 0x00, 0x00, 0x0D,
                     0xA0, 0x00, 0x00, 0x04, 0x20, 0x00, 0x00, 0x05, 0xA7, 0xFF, 0xFF, 0xE4, 0x24, 0x00, 0x00, 0x25,
                     0xA4, 0x00, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x80, 0x00, 0x24, 0x24, 0x80, 0x00, 0x25,
                     0xA4, 0x9F, 0xFF, 0xE4, 0x24, 0x90, 0x00, 0x05, 0xA4, 0x97, 0xFF, 0xE4, 0x24, 0x94, 0x00, 0x25,
                     0xA4, 0x94, 0x00, 0x24, 0x24, 0x94, 0xFF, 0x25, 0xA4, 0x94, 0x01, 0x24, 0x24, 0x94, 0x01, 0x25,
                     0xA4, 0x97, 0xF9, 0x24, 0x24, 0x90, 0x09, 0x25, 0xA4, 0x9F, 0xE9, 0x24, 0x24, 0x80, 0x29, 0x25,
                     0xA4, 0x80, 0x29, 0x24, 0x24, 0xFF, 0x29, 0x25, 0xA4, 0x00, 0x29, 0x24, 0x24, 0x00, 0x29, 0x25,
                     0xA7, 0xFF, 0xE9, 0x24, 0x20, 0x00, 0x09, 0x25, 0xA7, 0xFF, 0xF9, 0x24, 0x24, 0x00, 0x01, 0x25,
                     0xA4, 0x00, 0x01, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x00, 0x24, 0x24, 0x00, 0x00, 0x25,
                     0xA7, 0xFF, 0xFF, 0xE4, 0x20, 0x00, 0x00, 0x05, 0xA7, 0xFF, 0xFF, 0xE4, 0x24, 0x00, 0x00, 0x25,
                     0xA4, 0x00, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x80, 0x00, 0x24, 0x24, 0x80, 0x00, 0x25,
                     0xA4, 0x9F, 0xFF, 0xE4, 0x24, 0x90, 0x00, 0x05, 0xA4, 0x97, 0xFF, 0xE4, 0x24, 0x94, 0x00, 0x25,
                     0xA4, 0x94, 0x00, 0x24, 0x24, 0x94, 0xFF, 0x25, 0xA4, 0x94, 0x01, 0x24, 0x24, 0x94, 0x01, 0x25,
                     0xA4, 0x97, 0xF9, 0x24, 0x24, 0x90, 0x09, 0x25, 0xA4, 0x9F, 0xE9, 0x24, 0x24, 0x80, 0x29, 0x25,
                     0xA4, 0x80, 0x29, 0x24, 0x24, 0xFF, 0x29, 0x25, 0xA4, 0x00, 0x29, 0x24, 0x24, 0x00, 0x29, 0x25,
                     0xA7, 0xFF, 0xE9, 0x24, 0x20, 0x00, 0x09, 0x25, 0xA7, 0xFF, 0xF9, 0x24, 0x24, 0x00, 0x01, 0x25,
                     0xA4, 0x00, 0x01, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x00, 0x24, 0x24, 0x00, 0x00, 0x25,
                     0xA7, 0xFF, 0xFF, 0xE4, 0x20, 0x00, 0x00, 0x05, 0xA7, 0xFF, 0xFF, 0xE4, 0x24, 0x00, 0x00, 0x25,
                     0xA4, 0x00, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x01, 0x24, 0x24, 0x00, 0x01, 0x25,
                     0xA7, 0xFF, 0xF9, 0x24, 0x20, 0x00, 0x09, 0x25, 0xA7, 0xFF, 0xE9, 0x24, 0x24, 0x00, 0x29, 0x25,
                     0xA4, 0x00, 0x29, 0x24, 0x24, 0xFF, 0x29, 0x25, 0xA4, 0x80, 0x29, 0x24, 0x24, 0x80, 0x29, 0x25,
                     0xA4, 0x9F, 0xE9, 0x24, 0x24, 0x90, 0x09, 0x25, 0xA4, 0x97, 0xF9, 0x24, 0x24, 0x94, 0x01, 0x25,
                     0xA4, 0x94, 0x01, 0x24, 0x24, 0x94, 0xFF, 0x25, 0xA4, 0x94, 0x00, 0x24, 0x24, 0x94, 0x00, 0x25,
                     0xA4, 0x97, 0xFF, 0xE4, 0x24, 0x90, 0x00, 0x05, 0xA4, 0x9F, 0xFF, 0xE4, 0x24, 0x80, 0x00, 0x25,
                     0xA4, 0x80, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x00, 0x24, 0x24, 0x00, 0x00, 0x25,
                     0xA7, 0xFF, 0xFF, 0xE4, 0x20, 0x00, 0x00, 0x05, 0xA7, 0xFF, 0xFF, 0xE4, 0x24, 0x00, 0x00, 0x25,
                     0xA4, 0x00, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x01, 0x24, 0x24, 0x00, 0x01, 0x25,
                     0xA7, 0xFF, 0xF9, 0x24, 0x20, 0x00, 0x09, 0x25, 0xA7, 0xFF, 0xE9, 0x24, 0x24, 0x00, 0x29, 0x25,
                     0xA4, 0x00, 0x29, 0x24, 0x24, 0xFF, 0x29, 0x25, 0xA4, 0x80, 0x29, 0x24, 0x24, 0x80, 0x29, 0x25,
                     0xA4, 0x9F, 0xE9, 0x24, 0x24, 0x90, 0x09, 0x25, 0xA4, 0x97, 0xF9, 0x24, 0x24, 0x94, 0x01, 0x25,
                     0xA4, 0x94, 0x01, 0x24, 0x24, 0x94, 0xFF, 0x25, 0xA4, 0x94, 0x00, 0x24, 0x24, 0x94, 0x00, 0x25,
                     0xA4, 0x97, 0xFF, 0xE4, 0x24, 0x90, 0x00, 0x05, 0xA4, 0x9F, 0xFF, 0xE4, 0x24, 0x80, 0x00, 0x25,
                     0xA4, 0x80, 0x00, 0x24, 0x24, 0xFF, 0xFF, 0x25, 0xA4, 0x00, 0x00, 0x24, 0x24, 0x00, 0x00, 0x25,
                     0xA7, 0xFF, 0xFF, 0xE4, 0x20, 0x00, 0x00, 0x05, 0xA0, 0x00, 0x00, 0x04, 0x20, 0x00, 0x00, 0x05,
                     0xB0, 0x00, 0x00, 0x0C, 0x3F, 0xFF, 0xFF, 0xFD, 0x80, 0x00, 0x00, 0x00, 0x55, 0x55, 0x55, 0x55};

PROGMEM prog_uint8_t Chelsea[] = { 0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x00, 0x00, 0x66, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0x03, 0xC0, 0xFF, 0x66, 0x1C, 0x38, 0x66,
                       0x60, 0x20, 0x04, 0x06, 0xF0, 0x40, 0x02, 0x0F, 0xF0, 0x80, 0x07, 0x0F, 0x61, 0x00, 0x0C, 0x86,
                       0x61, 0x00, 0x18, 0x86, 0xF1, 0x00, 0x30, 0x8F, 0xF2, 0x00, 0x20, 0x4F, 0x62, 0x00, 0x60, 0x46,
                       0x63, 0xFF, 0xFF, 0xC6, 0xF2, 0x00, 0x20, 0x4F, 0xF1, 0x00, 0x30, 0x8F, 0x61, 0x00, 0x18, 0x86,
                       0x61, 0x00, 0x08, 0x86, 0xF0, 0x80, 0x0D, 0x0F, 0xF0, 0x40, 0x06, 0x0F, 0x60, 0x20, 0x04, 0x06,
                       0x66, 0x1C, 0x38, 0x66, 0xFF, 0x03, 0xC0, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0x66, 0x00, 0x00, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x60, 0x00, 0x00, 0x06, 0xF0, 0x00, 0x00, 0x0F, 0xF3, 0xFF, 0xFF, 0xCF, 0x62, 0x06, 0x00, 0x06,
                       0x62, 0x04, 0x00, 0x06, 0xF2, 0x04, 0x00, 0x0F, 0xF2, 0x0C, 0x00, 0x0F, 0x62, 0x08, 0x78, 0x06,
                       0x62, 0x18, 0x4C, 0x06, 0xF3, 0x10, 0x4E, 0x0F, 0xF1, 0xF0, 0x4A, 0x0F, 0x60, 0x00, 0x4B, 0x06,
                       0x66, 0x66, 0x69, 0x06, 0xFF, 0xFF, 0x39, 0x0F, 0xFF, 0xFF, 0x19, 0x0F, 0x66, 0x66, 0x01, 0x06,
                       0x66, 0x66, 0x00, 0x06, 0xFF, 0xFF, 0x00, 0x0F, 0xFF, 0xFF, 0x3F, 0x0F, 0x66, 0x66, 0x21, 0x06,
                       0x66, 0x66, 0x21, 0x06, 0xFF, 0xFF, 0x27, 0x0F, 0xFF, 0xFF, 0x3F, 0x8F, 0x66, 0x66, 0x00, 0x06,
                       0x66, 0x66, 0x00, 0x06, 0xFF, 0xFF, 0x1E, 0x0F, 0xFF, 0xFF, 0x33, 0x0F, 0x66, 0x66, 0x21, 0x06,
                       0x66, 0x66, 0x21, 0x06, 0xFF, 0xFF, 0x31, 0x0F, 0xFF, 0xFF, 0x01, 0x0F, 0x66, 0x66, 0x00, 0x06,
                       0x66, 0x66, 0x00, 0x06, 0xFF, 0xFF, 0x08, 0x0F, 0xFF, 0xFF, 0x7E, 0x0F, 0x66, 0x66, 0x4A, 0x06,
                       0x66, 0x66, 0x4B, 0x06, 0xFF, 0xFF, 0x69, 0x0F, 0xFF, 0xFF, 0x39, 0x0F, 0x66, 0x66, 0x03, 0x06,
                       0x66, 0x66, 0x60, 0x66, 0xFF, 0xFF, 0xF0, 0xFF, 0xFF, 0xFF, 0xF0, 0xFF, 0x66, 0x66, 0x60, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66,
                       0x66, 0x66, 0x66, 0x66, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x66, 0x66, 0x66, 0x66};
                       
PROGMEM prog_uint8_t Emily[] =  { 0xBB, 0xFF, 0xFF, 0xE7, 0xE9, 0xFF, 0xFF, 0xCF, 0xFD, 0xFF, 0xFF, 0x8F, 0xB4, 0xFF, 0xFF, 0x1F,
                       0xFE, 0xFF, 0xFE, 0x1F, 0xFD, 0x3F, 0xFC, 0x9F, 0xFF, 0x8F, 0xF0, 0x3F, 0x6D, 0x63, 0xE0, 0x7F,
                       0x3F, 0x90, 0x04, 0x7F, 0x9A, 0xA9, 0x22, 0xFF, 0xC4, 0x08, 0x40, 0xFF, 0xF2, 0x83, 0x11, 0xFF,
                       0xF8, 0xB4, 0x07, 0xF8, 0xFE, 0x00, 0x7F, 0xE1, 0xFF, 0xFF, 0xFF, 0xCD, 0x3F, 0xFF, 0xFF, 0x3F,
                       0x3F, 0xFF, 0xFE, 0x76, 0x1F, 0xFF, 0xFC, 0x9F, 0x0F, 0xFF, 0xFB, 0xB7, 0x0F, 0xFF, 0xE4, 0xF7,
                       0x07, 0xFF, 0xCA, 0xBE, 0x07, 0xFF, 0x9E, 0xEF, 0x07, 0xFF, 0xA9, 0xBF, 0x03, 0xFE, 0x17, 0xFD,
                       0x03, 0xFC, 0xD1, 0x6F, 0x01, 0xFC, 0x26, 0xFF, 0x01, 0xFB, 0x40, 0x0B, 0x01, 0xF8, 0x1F, 0xE7,
                       0x01, 0xF2, 0x7F, 0xFB, 0x01, 0xE0, 0x7F, 0xFC, 0x01, 0xE0, 0xFF, 0xFE, 0x01, 0xC0, 0xFF, 0xFF,
                       0x01, 0xC1, 0xFF, 0xFF, 0x01, 0x83, 0xFF, 0xFF, 0x01, 0x83, 0xFF, 0xFF, 0x01, 0x83, 0xFF, 0xFF,
                       0x03, 0x83, 0xFF, 0xFF, 0x03, 0xC3, 0xFF, 0xFF, 0x03, 0xC1, 0xFF, 0xFF, 0x07, 0xE1, 0xFF, 0xFF,
                       0x0F, 0xFC, 0xFF, 0xFF, 0x1F, 0xFE, 0xFF, 0xFF, 0x1F, 0xFF, 0x3F, 0xFF, 0x3F, 0xFF, 0x8F, 0xFF,
                       0x7F, 0xFF, 0xC7, 0xFF, 0xFF, 0x0F, 0xC3, 0xFF, 0xF8, 0x01, 0xE1, 0xFF, 0xF0, 0x00, 0x70, 0xFF,
                       0xE0, 0x00, 0x30, 0xFF, 0xC0, 0x00, 0x08, 0xFF, 0x80, 0x00, 0x00, 0x7F, 0x80, 0x78, 0x00, 0x7F,
                       0x03, 0xFE, 0x00, 0x3F, 0x0F, 0xDF, 0x00, 0x3F, 0x1E, 0xFF, 0x80, 0x3F, 0x3F, 0x1F, 0x80, 0xBF,
                       0x36, 0xC3, 0xC0, 0x3F, 0x78, 0x09, 0xC1, 0x5F, 0x7E, 0x3C, 0xC0, 0x1F, 0x78, 0x7E, 0xC1, 0x8F,
                       0xFC, 0x7E, 0xC1, 0x2F, 0xF4, 0x7E, 0xC1, 0x8F, 0xFE, 0x7E, 0xC2, 0x9F, 0xFA, 0x3F, 0xC0, 0x1F,
                       0xFE, 0x1F, 0xC2, 0x1F, 0xEC, 0xCF, 0x88, 0x1F, 0xFB, 0x80, 0x33, 0x5F, 0xAD, 0x32, 0x82, 0x9F,
                       0xFC, 0xC0, 0xA4, 0x2F, 0xF2, 0xFF, 0x68, 0x0F, 0x1F, 0xB8, 0xA1, 0x0F, 0xFD, 0xF7, 0x04, 0x0F,
                       0xD6, 0xFC, 0xB2, 0x0F, 0xBE, 0xC6, 0x64, 0x07, 0xEA, 0xFC, 0xD0, 0x07, 0x7F, 0x30, 0x00, 0x07,
                       0x73, 0xC4, 0x38, 0x03, 0xF6, 0x51, 0xFC, 0x03, 0xFF, 0xE3, 0xFE, 0x03, 0xFD, 0x07, 0xFF, 0x81,
                       0xFD, 0x4F, 0xFF, 0xC0, 0xFD, 0x1F, 0xFF, 0xF0, 0xFF, 0x9F, 0xFF, 0xFC, 0xFE, 0x3F, 0xFF, 0xFF,
                       0xFE, 0x7F, 0xFF, 0xFF, 0xFC, 0x7F, 0xFF, 0xFF, 0xFE, 0x7F, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF,
                       0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF,
                       0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFF, 0xFF, 0xFE, 0xFF, 0xFC, 0x7F, 0xFE, 0xFF, 0xFF, 0x3F,
                       0xFE, 0xFF, 0xFF, 0xBF, 0xFE, 0xFF, 0xFF, 0xBF, 0xFE, 0x7F, 0xFF, 0xBF, 0xFF, 0x7F, 0xFF, 0x3F,
                       0xFF, 0x3F, 0xFF, 0x7E, 0xFF, 0x9F, 0xFE, 0x7E, 0xFB, 0xCF, 0xFC, 0xFC, 0xF3, 0xF1, 0xF9, 0xFC,
                       0xF3, 0xFC, 0x03, 0xF8, 0xF3, 0xFF, 0xFF, 0xF0, 0xF3, 0xFF, 0xFF, 0xE0, 0xF1, 0xFF, 0xFF, 0xE0,
                       0xF0, 0xFF, 0xFF, 0xC0, 0xF0, 0x7F, 0xFE, 0x00, 0xF8, 0x1F, 0xF8, 0x00, 0xFC, 0x00, 0x00, 0x01,
                       0xFC, 0x00, 0x00, 0x03, 0xFC, 0x00, 0x00, 0x05, 0xFE, 0x00, 0x00, 0x16, 0xFE, 0x00, 0x00, 0x17,
                       0xFF, 0x00, 0x02, 0xAB, 0xFF, 0x80, 0x08, 0x33, 0xFF, 0xC0, 0x04, 0x96, 0xFF, 0xC0, 0x00, 0x2F,
                       0xFF, 0xE0, 0x00, 0xD3, 0xFF, 0xF8, 0x00, 0x57, 0xFF, 0xFE, 0x00, 0x2A, 0xFF, 0xFF, 0x00, 0x3F,
                       0xFF, 0xFF, 0x80, 0x09, 0xFF, 0xFF, 0xE0, 0x03, 0xFF, 0xFF, 0xF0, 0x00, 0xFF, 0xFF, 0xF0, 0x00,
                       0xFF, 0xFF, 0xFC, 0x00, 0xFF, 0xFF, 0xFE, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0xFF, 0x00,
                       0xFF, 0xFF, 0xFF, 0x80, 0xFF, 0xFF, 0xFF, 0xC0, 0xFF, 0xFF, 0xFF, 0xE0, 0xFF, 0xFF, 0xFF, 0xE0,
                       0xFF, 0xFF, 0xFF, 0xF0, 0xFF, 0xFF, 0xFF, 0xF8, 0xFF, 0xFF, 0xFF, 0xF8, 0xFF, 0xFF, 0xFF, 0xFC};
                       
PROGMEM prog_uint8_t Luke[] = { 0xFD, 0x8C, 0x42, 0x40, 0xFC, 0xC4, 0x22, 0x44, 0xFE, 0x42, 0x12, 0x64, 0xF8, 0x62, 0x02, 0x34,
                       0xF7, 0x21, 0x13, 0x03, 0xED, 0x31, 0x09, 0x81, 0xE9, 0x91, 0x88, 0x81, 0xFB, 0x18, 0x88, 0x98,
                       0xC6, 0xC8, 0xC4, 0xCD, 0xEC, 0x8C, 0x46, 0x04, 0xFF, 0xE6, 0x42, 0x02, 0xAA, 0xA2, 0x20, 0x20,
                       0x55, 0x53, 0x21, 0x15, 0x2A, 0xA1, 0x11, 0x10, 0x35, 0x59, 0x19, 0x8A, 0x0A, 0xA9, 0x88, 0x80,
                       0x15, 0x54, 0x88, 0xD5, 0x0A, 0xA8, 0xC4, 0x00, 0x0D, 0x56, 0x46, 0x29, 0x02, 0xAA, 0x62, 0x00,
                       0x05, 0x55, 0x33, 0x55, 0x02, 0xAA, 0x11, 0x00, 0x03, 0x55, 0x99, 0xAB, 0x00, 0xAA, 0x88, 0x00,
                       0x01, 0x55, 0x49, 0x57, 0x00, 0x6A, 0x8C, 0x00, 0xFF, 0xD5, 0x66, 0xBF, 0xFF, 0xAA, 0xA6, 0x00,
                       0x7F, 0xD5, 0x55, 0xFF, 0x3F, 0xEA, 0xA8, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0x1F, 0xE0, 0x08, 0x00,
                       0x1F, 0xF0, 0x1F, 0xFF, 0x0F, 0xF0, 0x1C, 0x00, 0x0F, 0xF8, 0x3F, 0xFF, 0x07, 0xF8, 0x3E, 0x00,
                       0x07, 0xFC, 0x7F, 0xFF, 0x03, 0xFC, 0xFF, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0x01, 0xFF, 0xFF, 0x00,
                       0x01, 0xFF, 0xFF, 0xFF, 0x01, 0x55, 0xAA, 0x80, 0x02, 0xAA, 0x55, 0x7F, 0x01, 0x54, 0x2A, 0xA0,
                       0x06, 0xAC, 0x35, 0x5F, 0x05, 0x50, 0x0A, 0xA0, 0x0A, 0xA8, 0x15, 0x5F, 0x05, 0x50, 0x0A, 0xA8,
                       0x1A, 0xB0, 0x0D, 0x57, 0x15, 0x40, 0x02, 0xA8, 0x2A, 0xA0, 0x05, 0x57, 0x15, 0x40, 0x02, 0xAA,
                       0x6A, 0xC0, 0x03, 0x55, 0x55, 0x00, 0x00, 0xAA, 0xFF, 0x80, 0x01, 0xFF, 0xAA, 0x80, 0x01, 0xFF,
                       0x55, 0x40, 0x03, 0xFF, 0x2A, 0x80, 0x03, 0xFE, 0x35, 0x60, 0x07, 0xFE, 0x2A, 0xA0, 0x07, 0xFC,
                       0x15, 0x50, 0x0F, 0xFD, 0x0A, 0xA0, 0x0F, 0xF8, 0x0D, 0x58, 0x1F, 0xFA, 0x0A, 0xA8, 0x1F, 0xF0,
                       0x05, 0x54, 0x3F, 0xF5, 0x02, 0xA8, 0x3F, 0xE0, 0x03, 0x56, 0x7F, 0xEA, 0x02, 0xAA, 0x7F, 0xC0,
                       0x01, 0x55, 0xFF, 0xD5, 0x01, 0xAB, 0xFF, 0x80, 0x03, 0xFF, 0xFF, 0xAA, 0x03, 0xFF, 0xAA, 0x80,
                       0x07, 0xFF, 0xD5, 0x55, 0x07, 0xFE, 0xAA, 0xA0, 0x0F, 0xFE, 0x55, 0x4A, 0x0F, 0xFC, 0x2A, 0xA0,
                       0x1F, 0xFC, 0x35, 0x55, 0x1F, 0xF8, 0x0A, 0xA8, 0x3F, 0xF8, 0x15, 0x55, 0x3F, 0xF0, 0x0A, 0xA8,
                       0x7F, 0xF0, 0x0D, 0x55, 0x7F, 0xE0, 0x02, 0xAA, 0xFF, 0xE0, 0x05, 0x55, 0xFF, 0xC0, 0x02, 0xAA,
                       0xFF, 0xC0, 0x03, 0x55, 0xFF, 0x80, 0x00, 0xAA, 0xFF, 0x80, 0x01, 0xFF, 0xD5, 0x00, 0x01, 0xFF,
                       0x55, 0x40, 0x03, 0xFF, 0x55, 0x40, 0x03, 0xFE, 0xF5, 0x60, 0x07, 0xFF, 0x55, 0x40, 0x07, 0xFC,
                       0x55, 0x50, 0x0F, 0xFF, 0x55, 0x50, 0x0F, 0xF8, 0xFD, 0x58, 0x1F, 0xFF, 0x55, 0x54, 0x1F, 0xF0,
                       0x55, 0x54, 0x3F, 0xFF, 0x55, 0x54, 0x3F, 0xE0, 0xFF, 0x56, 0x7F, 0xFF, 0x55, 0x54, 0x7F, 0xC0,
                       0x55, 0xFF, 0xFF, 0xFF, 0x55, 0x56, 0x15, 0x00, 0x55, 0x56, 0xFF, 0xFF, 0x55, 0x56, 0xAA, 0x00,
                       0x57, 0xFE, 0xFF, 0xFF, 0x55, 0x52, 0xAA, 0x00, 0x55, 0x5A, 0xBF, 0xFF, 0x55, 0x5A, 0xA8, 0x00,
                       0x5F, 0xFA, 0xBF, 0xFF, 0x55, 0x4A, 0xA8, 0x00, 0x55, 0x6A, 0xAF, 0xFF, 0x55, 0x6A, 0xA0, 0x00,
                       0x7F, 0xEA, 0xBF, 0x3B, 0x55, 0x2A, 0xA1, 0xEB, 0x55, 0xAA, 0xAA, 0x0E, 0x55, 0xAA, 0x89, 0x80,
                       0xFF, 0xAA, 0xF9, 0x7D, 0x54, 0xAA, 0x81, 0x04, 0x56, 0xAA, 0xAD, 0xF6, 0x56, 0xAA, 0x29, 0x12,
                       0xFE, 0xAB, 0xE1, 0x52, 0x52, 0xAA, 0x09, 0x5A, 0x5A, 0xAA, 0xBB, 0x72, 0x5A, 0xA8, 0xA2, 0x46,
                       0xFA, 0xAF, 0xAE, 0x52, 0x4A, 0xA8, 0x28, 0x56, 0x6A, 0xAA, 0xE9, 0xDC, 0x6A, 0xA2, 0x8B, 0x10,
                       0xEA, 0xBE, 0xAA, 0x37, 0x2A, 0xA0, 0xAA, 0xE4, 0xAA, 0xAB, 0xAA, 0x8E, 0xAA, 0x8A, 0x2A, 0xBA,
                       0xAA, 0xFA, 0xAA, 0xBB, 0xAA, 0x82, 0xAA, 0xA3, 0xAA, 0xAE, 0xAA, 0xAF, 0xAA, 0x28, 0x2A, 0xAC,
                       0xAB, 0xEA, 0x8A, 0xAD, 0xAA, 0x0A, 0xAA, 0xAD, 0xAA, 0xBA, 0xAA, 0xAD, 0xA8, 0xA2, 0xA2, 0xAD};
                       
PROGMEM prog_uint8_t Alan2[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x80, 0x00, 0x00, 0x0C, 0x60, 0x00,
                       0x00, 0x18, 0x30, 0x00, 0x00, 0x10, 0x10, 0x00, 0x00, 0x30, 0x08, 0x00, 0x00, 0x27, 0xC8, 0x00,
                       0x00, 0x3F, 0x78, 0x00, 0x00, 0x1C, 0xF0, 0x00, 0x00, 0x1B, 0xB0, 0x00, 0x00, 0x0D, 0xE0, 0x00,
                       0x00, 0x03, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x25, 0x54, 0x00, 0x00, 0x9F, 0xF9, 0x00,
                       0x02, 0x60, 0x06, 0x40, 0x05, 0x9F, 0xF1, 0xA0, 0x0A, 0x7E, 0x3E, 0x50, 0x15, 0xFE, 0x6F, 0xA8,
                       0x2B, 0xBE, 0x0F, 0xD4, 0x57, 0x7F, 0x4F, 0xE8, 0x2E, 0xD0, 0x46, 0x2A, 0x6E, 0xC0, 0x00, 0x04,
                       0xAF, 0x00, 0x00, 0x05, 0x5B, 0x00, 0x00, 0x02, 0xD8, 0x00, 0x00, 0x03, 0x40, 0x0F, 0xF0, 0x02,
                       0xC0, 0xFF, 0xFF, 0x03, 0x4F, 0xFF, 0xFF, 0xF2, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0xFF, 0xFF, 0xFE,
                       0xFF, 0xFF, 0xFF, 0xFE, 0x7F, 0x5F, 0xFF, 0xFF, 0x73, 0x11, 0x83, 0xFE, 0xA3, 0xF1, 0x01, 0xFD,
                       0x23, 0x01, 0x01, 0xFC, 0xB3, 0x07, 0x00, 0xFA, 0x52, 0x0F, 0x00, 0x08, 0x29, 0x01, 0x00, 0x14,
                       0x14, 0x07, 0x00, 0x28, 0x0A, 0x03, 0x80, 0x50, 0x05, 0x80, 0x01, 0xA0, 0x01, 0x70, 0x0E, 0x40,
                       0x00, 0x9F, 0xF9, 0x00, 0x00, 0x2A, 0xA4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                       0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00,
                       0x00, 0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x00,
                       0x00, 0x40, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x03, 0x40, 0x00,
                       0x00, 0x01, 0xC1, 0x00, 0x02, 0x19, 0x00, 0x00, 0x00, 0x09, 0x04, 0x00, 0x00, 0x0F, 0x20, 0x40,
                       0x00, 0x93, 0x00, 0x00, 0x02, 0x11, 0x32, 0x00, 0x00, 0x09, 0x20, 0x00, 0x20, 0xC9, 0xE4, 0x00,
                       0x04, 0x09, 0x00, 0x40, 0x00, 0x09, 0x42, 0x00, 0x24, 0xBD, 0x12, 0x04, 0x06, 0x21, 0x0A, 0x90,
                       0x05, 0x33, 0x1E, 0x01, 0x10, 0x1F, 0xF4, 0x20, 0x10, 0xCF, 0xC4, 0x80, 0xD7, 0x2F, 0xC4, 0xA4,
                       0x65, 0x07, 0xD2, 0xB4, 0x6C, 0x43, 0xC0, 0x10, 0x30, 0x1B, 0xEA, 0x38, 0x31, 0x23, 0xF0, 0x98,
                       0x22, 0x07, 0x99, 0x30, 0x31, 0xD7, 0x03, 0x32, 0x9D, 0x67, 0xD6, 0x66, 0x07, 0xAF, 0x9C, 0x7E,
                       0xA2, 0xFF, 0x98, 0xBE, 0xF2, 0xFF, 0xF4, 0x1E, 0xF8, 0x7B, 0xF0, 0x5C, 0xE0, 0x3B, 0xC4, 0x3C,
                       0x6C, 0x7F, 0x12, 0x38, 0xF0, 0x3F, 0x3F, 0x5A, 0xF0, 0x7F, 0x36, 0x50, 0x38, 0x3F, 0x35, 0x3C,
                       0x3B, 0x87, 0xE7, 0xFA, 0xFC, 0xD7, 0xEB, 0x7E, 0x3E, 0x7F, 0xB1, 0x3B, 0xAE, 0x3F, 0x78, 0x61,
                       0xBF, 0x17, 0xF0, 0xF8, 0xCF, 0x1F, 0xF0, 0xE0, 0xDC, 0x1F, 0xF1, 0xCA, 0x57, 0x9E, 0x61, 0xE6,
                       0x4F, 0xC7, 0xE1, 0xC0, 0x56, 0x5F, 0xE7, 0xD7, 0xC3, 0xCF, 0x67, 0xDC, 0x83, 0xE7, 0xA7, 0xDE,
                       0x63, 0xEF, 0xF7, 0xFE, 0x29, 0xCF, 0x86, 0xFE, 0x3F, 0xDF, 0x07, 0xFA, 0x6F, 0x9F, 0xB1, 0xFE,
                       0x6F, 0xAF, 0xB8, 0x10, 0xF7, 0x7E, 0x39, 0xDF, 0x77, 0xFF, 0xBD, 0xFA, 0x70, 0xFF, 0xDF, 0xE2,
                       0xFA, 0xFF, 0xD7, 0xF7, 0x5B, 0x7F, 0xFF, 0xEC, 0xF3, 0xFF, 0xFF, 0x5E, 0xB7, 0xFF, 0xFF, 0x96,
                       0xF7, 0xFF, 0xFF, 0x84, 0xFF, 0xF8, 0x9E, 0x16, 0xFF, 0xEF, 0x27, 0xF3, 0xFF, 0xD0, 0x01, 0xF7,
                       0xFF, 0x16, 0x06, 0xFF, 0xFE, 0x47, 0x02, 0xFF, 0xFE, 0x0D, 0x60, 0x7F, 0xFC, 0x8A, 0x00, 0x3F,
                       0xFC, 0x1E, 0x03, 0x3F, 0xF8, 0x00, 0x60, 0x1F, 0xF8, 0x00, 0x20, 0x1F, 0xF9, 0x89, 0xB9, 0x1F};

void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(20, 4);
  // Print a message to the LCD.
  lcd.setCursor(7, 0);
  lcd.print("Time");
  lcd.setCursor(0, 1);
  lcd.print("Seconds Since Reset:");
  lcd.setCursor(7, 2);
  lcd.print("      ");
  lcd.setCursor(7, 2); 
  lcd.print(millis()/1000);
  lcd.setCursor(10,3);
  
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  
  svfdisp.clear();
  svfdisp.print("       Enjoy!");
  DisplayTeam();
  delay(1500);
}
int i = 0;
void loop() {
  if(i >= 9999) {
    i = 0;
  }
  lcd_time();
  // Information and Instructions and Banner
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  lcd_time();
  svfdisp.clear();
  delay(100);
  svfdisp.print("  The First Annual");
  svfdisp.newLine();
  svfdisp.print("   Electronic Art");
  svfdisp.newLine();
  svfdisp.print("      Contest");
  lcd_time();
  delay(1500);
  lcd_time();
  // art show banner
  DisplayVictim(i);
  i++;
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  lcd_time();
  svfdisp.clear();
  delay(100);
  imageFromFlash(imageBuffer, banner, 140, 32);
  delay(2500);
  lcd_time();
  DisplayVictim(i);
  i++;
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  lcd_time();
  delay(100);
  lcd_time();
  svfdisp.charMagnify(2,2);
  svfdisp.print("  Judged");
  svfdisp.newLine();
  svfdisp.print(" Entries!");
  DisplayCardinal();
  delay(1500);
  lcd_time();
  svfdisp.charMagnify(1,1);
  
  // Scored and Judged Submissions
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  lcd_time();
  svfdisp.clear();
  delay(100);
  lcd_time();
  DisplayCardinal();
  imageFromFlash(imageBuffer, Alan1, 140, 32);
  delay(2500);
  lcd_time();
  // Aaron's Image
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  DisplayCardinal();
  imageFromFlash(imageBuffer, Aaron, 140, 32);
  delay(2500);
  lcd_time();
  // Chelsea's Image
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  DisplayCardinal();
  imageFromFlash(imageBuffer, Chelsea, 140, 32);
  delay(2500);
  lcd_time();
  // Emily's Image
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  lcd_time();
  DisplayCardinal();
  imageFromFlash(imageBuffer, Emily, 140, 32);
  delay(2500);
  lcd_time();
  // Open Division Submissions
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  svfdisp.charMagnify(2,2);
  svfdisp.print("   Open");
  svfdisp.newLine();
  svfdisp.print(" Entries!");
  DisplayCardinal();
  delay(1500);
  lcd_time();
  svfdisp.charMagnify(1,1);

  // Luke's Image
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  DisplayCardinal();
  imageFromFlash(imageBuffer, Luke, 140, 32);
  delay(2500); 
  lcd_time();
  // Alan's Second Image 
  for(int j = 0; j < 5; j++) {
      DisplayVictim(i);
      i++;
      delay(10);
  }
  svfdisp.clear();
  delay(100);
  DisplayCardinal();
  imageFromFlash(imageBuffer, Alan2, 140, 32);
  delay(2500);
  lcd_time();
}

void imageFromFlash(uint8_t buffer[], uint8_t image[], uint8_t x, uint8_t y) {
  memcpy_P(buffer, image, (y/8)*x); 	
  svfdisp.displayImage(buffer, x, y);
}

void DisplayTeam() {
  GLCD.ClearScreen();
  
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts(" Mavizon ");
  //GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  //GLCD.Puts("Robot Team!");
}

void DisplayCardinal() {
  GLCD.ClearScreen();
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
}

void DisplayVictim(int num)
{
  int room = num/1000;
  num = num%1000;
  int x = num/100;
  num = num%100;
  int y = num/10;
  num = num%10;
  int status = num;
  
  GLCD.ClearScreen();
  GLCD.GotoXY(0, 7);
  GLCD.SelectFont(baskerville);
  GLCD.PrintNumber(room);
  GLCD.GotoXY(33, 7);
  GLCD.PrintNumber(x);
  GLCD.GotoXY(66, 7);
  GLCD.PrintNumber(y);
  GLCD.GotoXY(99, 7);
  GLCD.PrintNumber(status);
}

void lcd_time() {
  lcd.noAutoscroll();
  lcd.setCursor(7, 2);
  lcd.print("      ");
  lcd.setCursor(7, 2); 
  lcd.print(millis()/1000);
}
