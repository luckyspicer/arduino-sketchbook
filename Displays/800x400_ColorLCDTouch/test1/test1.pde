// CTRL
int ADJ   = 2;
int DE    = 3;
int DCLK  = 4;

// RED
int R5    = 40; 
int R4    = 38;
int R3    = 36;
int R2    = 34;
int R1    = 32;
int R0    = 30;

// GREEN
int G5    = 52;
int G4    = 50;
int G3    = 48;
int G2    = 46;
int G1    = 44;
int G0    = 42;

// BLUE
int B5    = 53;
int B4    = 51;
int B3    = 49;
int B2    = 47;
int BB1    = 45;
int BB0    = 43;


void setup() {
  //Backlight Brightness
  analogWrite(ADJ, 168);
  
  // CTRL
  digitalWrite(DCLK, LOW);
  digitalWrite(DE, LOW);
  
  Serial.begin(9600);
}

void loop() {
  //Green Screen!
  
  Serial.println("Green");
  
  //Solid Color (Green!)
  digitalWrite(R5, LOW);
  digitalWrite(R4, LOW);
  digitalWrite(R3, LOW);
  digitalWrite(R2, LOW);
  digitalWrite(R1, LOW);
  digitalWrite(R0, LOW);
  
  digitalWrite(G5, HIGH);
  digitalWrite(G4, HIGH);
  digitalWrite(G3, HIGH);
  digitalWrite(G2, HIGH);
  digitalWrite(G1, HIGH);
  digitalWrite(G0, HIGH);
  
  digitalWrite(B5, LOW);
  digitalWrite(B4, LOW);
  digitalWrite(B3, LOW);
  digitalWrite(B2, LOW);
  digitalWrite(BB1, LOW);
  digitalWrite(BB0, LOW);
  
  for(int v = 0; v < 480; v++) {
    digitalWrite(DE, LOW);
    for(int blank = 0; blank < 20; blank++) {
      digitalWrite(DCLK, HIGH);
      digitalWrite(DCLK, LOW);
    }
    digitalWrite(DE, HIGH);
    for(int h = 0; h < 800; h++) {
      digitalWrite(DCLK, HIGH);
      digitalWrite(DCLK, LOW);
    }
  }

  
//  //BLUE Screen!
//  
//  //Solid Color (BLUE!)
//  digitalWrite(R5, LOW);
//  digitalWrite(R4, LOW);
//  digitalWrite(R3, LOW);
//  digitalWrite(R2, LOW);
//  digitalWrite(R1, LOW);
//  digitalWrite(R0, LOW);
//  
//  digitalWrite(G5, LOW);
//  digitalWrite(G4, LOW);
//  digitalWrite(G3, LOW);
//  digitalWrite(G2, LOW);
//  digitalWrite(G1, LOW);
//  digitalWrite(G0, LOW);
//  
//  digitalWrite(B5, HIGH);
//  digitalWrite(B4, HIGH);
//  digitalWrite(B3, HIGH);
//  digitalWrite(B2, HIGH);
//  digitalWrite(BB1, HIGH);
//  digitalWrite(BB0, HIGH);
//  
//  for(int v = 0; v < 480; v++) {
//    digitalWrite(DE, LOW);
//    for(int blank = 0; blank < 20; blank++) {
//      digitalWrite(DCLK, HIGH);
//      delay(1);
//      digitalWrite(DCLK, LOW);
//      delay(1);
//    }
//    digitalWrite(DE, HIGH);
//    for(int h = 0; h < 800; h++) {
//      digitalWrite(DCLK, HIGH);
//      delay(1);
//      digitalWrite(DCLK, LOW);
//      delay(1);
//    }
//  }
  
//  //Red Screen!
//  
//  //Solid Color (RED!)
//  digitalWrite(R5, HIGH);
//  digitalWrite(R4, HIGH);
//  digitalWrite(R3, HIGH);
//  digitalWrite(R2, HIGH);
//  digitalWrite(R1, HIGH);
//  digitalWrite(R0, HIGH);
//  
//  digitalWrite(G5, LOW);
//  digitalWrite(G4, LOW);
//  digitalWrite(G3, LOW);
//  digitalWrite(G2, LOW);
//  digitalWrite(G1, LOW);
//  digitalWrite(G0, LOW);
//  
//  digitalWrite(B5, LOW);
//  digitalWrite(B4, LOW);
//  digitalWrite(B3, LOW);
//  digitalWrite(B2, LOW);
//  digitalWrite(BB1, LOW);
//  digitalWrite(BB0, LOW);
//  
//  //for(int red = 0; red < 100; red++) {
//    digitalWrite(DE, LOW);
//    for(int v = 0; v < 480; v++) {
//      for(int blank = 0; blank < 20; blank++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//      digitalWrite(DE, HIGH);
//      for(int h = 0; h < 800; h++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//    }
//  //}
  
//  //White Screen!
//  
//  //Solid Color (White!)
//  digitalWrite(R5, HIGH);
//  digitalWrite(R4, HIGH);
//  digitalWrite(R3, HIGH);
//  digitalWrite(R2, HIGH);
//  digitalWrite(R1, HIGH);
//  digitalWrite(R0, HIGH);
//  
//  digitalWrite(G5,  HIGH);
//  digitalWrite(G4,  HIGH);
//  digitalWrite(G3,  HIGH);
//  digitalWrite(G2,  HIGH);
//  digitalWrite(G1,  HIGH);
//  digitalWrite(G0,  HIGH);
//  
//  digitalWrite(B5,  HIGH);
//  digitalWrite(B4,  HIGH);
//  digitalWrite(B3,  HIGH);
//  digitalWrite(B2,  HIGH);
//  digitalWrite(BB1,  HIGH);
//  digitalWrite(BB0,  HIGH);
//  
//  //for(int white = 0; white < 100; white++) {
//    digitalWrite(DE, LOW);
//    for(int v = 0; v < 480; v++) {
//      for(int blank = 0; blank < 20; blank++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//      digitalWrite(DE, HIGH);
//      for(int h = 0; h < 800; h++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//    }
//  //}

////Black Screen!
//  
//  Serial.println("Black!");
//  
//  //Solid Color (black!)
//  digitalWrite(R5, LOW);
//  digitalWrite(R4, LOW);
//  digitalWrite(R3, LOW);
//  digitalWrite(R2, LOW);
//  digitalWrite(R1, LOW);
//  digitalWrite(R0, LOW);
//  
//  digitalWrite(G5,  LOW);
//  digitalWrite(G4,  LOW);
//  digitalWrite(G3,  LOW);
//  digitalWrite(G2,  LOW);
//  digitalWrite(G1,  LOW);
//  digitalWrite(G0,  LOW);
//  
//  digitalWrite(B5,  LOW);
//  digitalWrite(B4,  LOW);
//  digitalWrite(B3,  LOW);
//  digitalWrite(B2,  LOW);
//  digitalWrite(BB1,  LOW);
//  digitalWrite(BB0,  LOW);
//  
//    digitalWrite(DE, LOW);
//    for(int v = 0; v < 480; v++) {
//      for(int blank = 0; blank < 20; blank++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//      digitalWrite(DE, HIGH);
//      for(int h = 0; h < 800; h++) {
//        digitalWrite(DCLK, HIGH);
//        digitalWrite(DCLK, LOW);
//      }
//    }
  
  
}
