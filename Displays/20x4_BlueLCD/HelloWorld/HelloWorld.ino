// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
// rs, enable, d0, d1, d2, d3
LiquidCrystal lcd(30, 31, 43, 45, 47, 49);

void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(20, 4);
  // Print a message to the LCD.
  lcd.print("hello, world! help I am trapped in this display! Please get me out I mean it!");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  //lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  //lcd.print(millis()/1000);
}

