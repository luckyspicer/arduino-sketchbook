/*
 * GLCDexample
 *
 * Basic test code for the Arduino KS0108 GLCD library.
 * This code exercises a range of graphic functions supported
 * by the library and is an example of its use.
 * It also gives an indication of performance, showing the
 *  number of frames drawn per second.  
 */

#include <ks0108.h>
#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font
#include "anne_smile.h"     // bitmap 

unsigned long startMillis;
unsigned int loops = 0;
unsigned int iter = 0;

void setup(){
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  GLCD.ClearScreen();  
  GLCD.DrawBitmap(anne_smile, 0,0, WHITE); //draw the bitmap at the given x,y position
}

void  loop(){   // run over and over again
  
}
