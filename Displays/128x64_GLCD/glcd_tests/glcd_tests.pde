/*
 * GLCDexample
 *
 * Basic test code for the Arduino KS0108 GLCD library.
 * This code exercises a range of graphic functions supported
 * by the library and is an example of its use.
 * It also gives an indication of performance, showing the
 *  number of frames drawn per second.  
 */

#include <ks0108.h>
#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font

void setup(){
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  GLCD.ClearScreen();
  
}

void  loop() {   // run over and over again
  GLCD.FillRect(0, 0, 127,  8, BLACK);
  delay(2000);
  GLCD.FillRect(0, 0,   8, 63, BLACK);
  while(1);
  
}
