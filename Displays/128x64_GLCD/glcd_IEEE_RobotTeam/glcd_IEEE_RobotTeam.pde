/*
 * GLCDexample
 *
 * Basic test code for the Arduino KS0108 GLCD library.
 * This code exercises a range of graphic functions supported
 * by the library and is an example of its use.
 * It also gives an indication of performance, showing the
 *  number of frames drawn per second.  
 */

#include <ks0108.h>
#include "Arial14.h"         // proportional font
#include "SystemFont5x7.h"   // system font

void setup(){
  GLCD.Init(NON_INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  GLCD.ClearScreen();
  
}

void  loop() {   // run over and over again
  GLCD.FillRect(0, 0, 127,  6, BLACK);
  GLCD.FillRect(0, 0,   6, 63, BLACK);
  GLCD.FillRect(121, 0, 6,  63, BLACK);
  GLCD.FillRect(0, 57,  127, 6, BLACK);
  
  GLCD.FillRect(12, 12, 103,  6, BLACK);
  GLCD.FillRect(12, 12,   6, 39, BLACK);
  GLCD.FillRect(109, 12, 6,  39, BLACK);
  GLCD.FillRect(12, 45,  103, 6, BLACK);
  
  GLCD.GotoXY(36, 24);
  GLCD.SelectFont(System5x7);
  GLCD.Puts("UofL IEEE");
  GLCD.GotoXY(32, 34);
  GLCD.Puts("Robot Team!");

  while(1) {
    GLCD.SetInverted(0);
    delay(1500);
    GLCD.SetInverted(1);
    delay(1500);
  }
}

