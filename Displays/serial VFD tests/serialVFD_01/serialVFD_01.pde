#include <serialVFD.h>

serialVFD svfdisp(SERIAL2);

void setup() {
  Serial.begin(9600);
  
  Serial.println("This is a Test of the Serial VFD");
  svfdisp.clear();
  svfdisp.print("Testing Serial VFD");
  svfdisp.setCursor(0, 1);
  svfdisp.print("Woot! Hello! ");
  delay(1000);
}

void loop() {
  svfdisp.setCursor(7*14, 1);
  svfdisp.print("     ");
  svfdisp.setCursor(7*14, 1);
  svfdisp.print(millis()/1000);
  delay(1000);
}
