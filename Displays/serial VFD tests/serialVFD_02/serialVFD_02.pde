/* Lucas Spicer 8/18/2011
 * This program runs two serial controlled VFDs at the same time
 */

#include <VFD.h>  //including serialVFD.h allows access to the special serial vfd library functions
#include <avr/pgmspace.h>

// Create two new instances of the VFD object, these are named svfdisp1 and svfdisp2, but choose whatever you want
VFD svfdisp1(SERIAL1);
VFD svfdisp2(SERIAL2);

char x[] = "AAA";
                      
void setup() {
  svfdisp1.clear();
  svfdisp2.clear();
  
  svfdisp1.brightness(4);
  svfdisp2.brightness(4);
  svfdisp1.charMagnify(2,2);
  svfdisp2.charMagnify(2,2);
  
  svfdisp1.print(" Display 1");
  svfdisp2.print(" Display 2");
  delay(1000);
}

void loop() {
  if(x[0] > 90) {
    x[0] = 'A';
    x[1] = 'A';
    x[2] = 'A';
  }
  svfdisp1.setCursor(7*8, 2);  
  svfdisp1.print("     ");
  svfdisp1.setCursor(7*8, 2);
  svfdisp1.print(millis()/1000);
  
  svfdisp2.setCursor(7*8, 2);  
  svfdisp2.print("   ");
  svfdisp2.setCursor(7*8, 2);
  svfdisp2.print(x);
  
  x[2]++;
  if(x[2] == 91) {
    x[2] = 'A';
    x[1]++;
    if(x[1] == 91) {
      x[1] = 'A';
      x[0]++;
    }
  }
  delay(1000);
}
