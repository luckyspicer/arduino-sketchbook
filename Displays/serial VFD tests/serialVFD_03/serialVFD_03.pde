/* Lucas Spicer 8/18/2011
 * This program runs two serial controlled VFDs and one parallel controlled
 * VFDs at the same time. Be carefule however, three displays may exceed the
 * the maximum power rating of the arduio's 5v power supply
 */

#include <VFD.h>  //including serialVFD.h allows access to the special serial vfd library functions
#include <avr/pgmspace.h>

// Create two new instances of the VFD object, these are named svfdisp1 and svfdisp2, but choose whatever you want
VFD svfdisp1(SERIAL1);
VFD svfdisp2(SERIAL2);

// Create a new instance of the VFD object, this one is named vfdisp, but choose whatever you want
// The constructor needs the pin numbers of the pins: (wr, rd, d0, d1, d2, d3, d4, d5, d6, d7)
// The pins are numbered 5 through 14 on the VFD pcb and spec sheet
VFD vfdisp(PARALLEL, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2);
int randomNumber;

char x[] = "AAA";
                      
void setup() {
  randomSeed(analogRead(0));
  svfdisp1.clear();
  svfdisp2.clear();
  
  svfdisp1.brightness(4);
  svfdisp2.brightness(4);
  vfdisp.brightness(4);
  svfdisp1.charMagnify(2,2);
  svfdisp2.charMagnify(2,2);
  vfdisp.charMagnify(2,2);
  
  svfdisp1.print(" Display 1");
  svfdisp2.print(" Display 2");
  vfdisp.print(" Display 3");
  delay(1000);
}

void loop() {
  if(x[0] > 90) {
    x[0] = 'A';
    x[1] = 'A';
    x[2] = 'A';
  }
  svfdisp1.setCursor(7*8, 2);  
  svfdisp1.print("     ");
  svfdisp1.setCursor(7*8, 2);
  svfdisp1.print(millis()/1000);
  
  svfdisp2.setCursor(7*8, 2);  
  svfdisp2.print("   ");
  svfdisp2.setCursor(7*8, 2);
  svfdisp2.print(x);
 
  randomNumber = random(128,255);
  vfdisp.setCursor(7*7, 2);
  vfdisp.print("     ");
  vfdisp.setCursor(7*7, 2);
  vfdisp.print(randomNumber);
  vfdisp.print(" ");
  vfdisp.print(randomNumber, BYTE);
  
  x[2]++;
  if(x[2] == 91) {
    x[2] = 'A';
    x[1]++;
    if(x[1] == 91) {
      x[1] = 'A';
      x[0]++;
    }
  }
  delay(1000);
}
