#include <IMU.h>

int xa = 3;
int ya = 4;
int za = 5;
int yrate = 6;
int xrate = 7;
float Vref = 5.02;
float V3_3 = 3.308;

IMU my_imu(xa, ya, za, yrate, xrate, V3_3, Vref);

int x_acc;
int y_acc;
int z_acc;
int y_rate;
int x_rate;

float x_acceleration;
float y_acceleration;
float z_acceleration;
float y_rotation_rate;
float x_rotation_rate;

void setup() {
  Serial.begin(9600);
  Serial.println("***** IMU Test *****");
  Serial.println("Zero IMU...");
  my_imu.zero(20);
  
  Serial.print("X Acceleration Zero Value = ");
  Serial.println(my_imu.get_x_acc_zero());
  Serial.print("Y Acceleration Zero Value = ");
  Serial.println(my_imu.get_y_acc_zero());
  Serial.print("Z Acceleration Zero Value = ");
  Serial.println(my_imu.get_z_acc_zero());
  Serial.print("Y Rotation Rate Zero Value = ");
  Serial.println(my_imu.get_y_rate_zero());
  Serial.print("X Rotation Rate Zero Value = ");
  Serial.println(my_imu.get_x_rate_zero());
  Serial.println();
}

void loop() {
  x_acc = my_imu.get_x_acc_AD();
  y_acc = my_imu.get_y_acc_AD();
  z_acc = my_imu.get_z_acc_AD();
  y_rate = my_imu.get_y_rate_AD();
  x_rate = my_imu.get_x_rate_AD();
  
  x_acceleration = my_imu.get_x_acc();
  y_acceleration = my_imu.get_y_acc();
  z_acceleration = my_imu.get_z_acc();
  y_rotation_rate = my_imu.get_y_rate();
  x_rotation_rate = my_imu.get_x_rate();
  
  Serial.print("X Acceleration = ");
  Serial.print(x_acc);
  Serial.print(" A/D Value ");
  Serial.print(x_acceleration);
  Serial.println(" g");
  
  Serial.print("Y Acceleration = ");
  Serial.print(y_acc);
  Serial.print(" A/D Value ");
  Serial.print(y_acceleration);
  Serial.println(" g");
  
  Serial.print("Z Acceleration = ");
  Serial.print(z_acc);
  Serial.print(" A/D Value ");
  Serial.print(z_acceleration);
  Serial.println(" g");
  
  Serial.print("Y Rotation Rate = ");
  Serial.print(y_rate);
  Serial.print(" A/D Value ");
  Serial.print(y_rotation_rate);
  Serial.println(" Degrees/Sec");
  
  Serial.print("X Rotation Rate = ");
  Serial.print(x_rate);
  Serial.print(" A/D Value ");
  Serial.print(x_rotation_rate);
  Serial.println(" Degrees/Sec");
  
  Serial.println();
  delay(1500);
  
}
