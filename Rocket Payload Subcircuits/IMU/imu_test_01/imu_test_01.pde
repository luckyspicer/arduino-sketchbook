int xa = 3;
int ya = 4;
int za = 5;
int yrate = 6;
int xrate = 7;

float Vref = 4.82; 
int x_acc;
int y_acc;
int z_acc;
int y_rate;
int x_rate;

float x_acc_voltage;
float y_acc_voltage;
float z_acc_voltage;
float y_rate_voltage;
float x_rate_voltage;

void setup() {
  Serial.begin(115200);
  Serial.println("***** IMU Test *****");
  
  
}

void loop() {
  x_acc = analogRead(xa);
  y_acc = analogRead(ya);
  z_acc = analogRead(za);
  y_rate = analogRead(yrate);
  x_rate = analogRead(xrate);
  
  x_acc_voltage = x_acc * Vref / 1024;
  y_acc_voltage = y_acc * Vref / 1024;
  z_acc_voltage = z_acc * Vref / 1024;
  y_rate_voltage = y_rate * Vref / 1024;
  x_rate_voltage = x_rate * Vref / 1024;
  
  Serial.print("X Acceleration = ");
  Serial.print(x_acc);
  Serial.print(" A/D Value ");
  Serial.print(x_acc_voltage);
  Serial.println(" V");
  
  Serial.print("Y Acceleration = ");
  Serial.print(y_acc);
  Serial.print(" A/D Value ");
  Serial.print(y_acc_voltage);
  Serial.println(" V");
  
  Serial.print("Z Acceleration = ");
  Serial.print(z_acc);
  Serial.print(" A/D Value ");
  Serial.print(z_acc_voltage);
  Serial.println(" V");
  
  Serial.print("Y Rotation Rate = ");
  Serial.print(y_rate);
  Serial.print(" A/D Value ");
  Serial.print(y_rate_voltage);
  Serial.println(" V");
  
  Serial.print("X Rotation Rate = ");
  Serial.print(x_rate);
  Serial.print(" A/D Value ");
  Serial.print(x_rate_voltage);
  Serial.println(" V");
  
  Serial.println();
  delay(1500);
}
