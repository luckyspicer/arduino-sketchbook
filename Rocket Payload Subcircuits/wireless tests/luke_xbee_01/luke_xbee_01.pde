void setup() {
  Serial.begin(9600);
  
  Serial.println("Xbee Wireless Test!\n");
}

void loop() {
  Serial.print("This is a test of the Xbee Modules -- Time = ");
  Serial.print(millis()/1000);
  Serial.println("Sec");
  delay(3000);
  Serial.print("Messages Recieved: ");
  while(Serial.available() > 0) {
    Serial.print((char)Serial.read());
  }
  Serial.println();
}
