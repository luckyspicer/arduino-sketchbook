#include <WirelessComm.h>
#define SERIALNUM 3

WirelessComm myWireless(SERIALNUM);
float humVars[1] = {55.2};

String hum = "HUM";
int LED = 53;

void setup() 
{
  myWireless.beginSerial();
  pinMode(LED, OUTPUT);
  
  digitalWrite(LED, LOW);
  int i = 0;
  for(i; i < 100; i++){
    myWireless.transmitData(millis(), hum, humVars);
  }
  digitalWrite(LED, HIGH);
}

void loop()
{
}
