#include <WirelessComm.h>
#define SERIALNUM 0

WirelessComm myWireless(SERIALNUM);
float testVars[9] = {84.023, 35.031, 22, 88.023, 30.031, 29, 102, 12, 7};
unsigned long time;
String sensor = "GPS";

void setup() 
{

  // Serial.begin should have already been called when the WirelessComm constructor
  // was executed.
  //Serial.begin(9600);
  
  Serial.println("SETUP : Testing wireless comm module"); 
}

void loop()
{
  time = millis();
  myWireless.transmitData(time, sensor, testVars);
  delay(1000);
}
