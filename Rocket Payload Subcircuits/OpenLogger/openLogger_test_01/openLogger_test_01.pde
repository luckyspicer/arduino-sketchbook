void setup(){
  Serial.begin(9600);
  Serial1.begin(9600);
  
  appendFile("luke1.txt");
  Serial.println("Appending to the new file!");
  Serial1.println("Print this test");
  Serial1.println("Print this test");
  
  appendFile("luke2.txt");
  Serial.println("Appending to the new file!");
  Serial1.println("Print this test");
  Serial1.println("Print this test");
  Serial.println("Done with setup()");
}

int count = 0;
void loop(){
  count++;
  appendFile("odd.txt");
  Serial1.print("Count = ");
  Serial1.println(count);
  count++;
  appendFile("even.txt");
  Serial1.print("Count = ");
  Serial1.println(count);
  
  Serial.print("Count = ");
  Serial.println(count);
  delay(250);
}

void appendFile(char fileName[]){
  //Send three control z to enter OpenLog command mode
  Serial3.print(byte(26));
  Serial3.print(byte(26));
  Serial3.print(byte(26));

  //Wait for OpenLog to respond with '>' to indicate we are in command mode
  while(1) {
    if(Serial3.available())
      if(Serial3.read() == '>') break;
  }

  String file_name = String(fileName);
  file_name = String("append ") + fileName + String("\r");
  Serial3.print(file_name);
  Serial.println(file_name);

  //Wait for OpenLog to indicate file is open and ready for writing
  while(1) {
    if(Serial3.available())
      if(Serial3.read() == '<') break;
  }
}
