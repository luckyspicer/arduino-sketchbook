#include <NewSoftSerial.h>

NewSoftSerial softSerial(50, 51);

void setup(){
  Serial.begin(9600);
  Serial.println("Begin the Test");
  softSerial.begin(9600);
  softSerial.println("Test Print, Test Print, Test Print");
  softSerial.println("Pest Trint, Pest Trint, Pest Trint");
  
  appendFile("luke1.txt");
  Serial.println("Appending to the new file!");
  softSerial.println("Print this test");
  softSerial.println("Print this test");
  
  appendFile("luke2.txt");
  Serial.println("Appending to the new file!");
  softSerial.println("Print this test");
  softSerial.println("Print this test");
  Serial.println("Done with setup()");
}

int count = 0;
void loop(){
  count++;
  appendFile("odd.txt");
  softSerial.print("Count = ");
  softSerial.println(count);
  count++;
  appendFile("even.txt");
  softSerial.print("Count = ");
  softSerial.println(count);
  
  Serial.print("Count = ");
  Serial.println(count);
  delay(250);
  Serial.println("Print Print Print Print Print");
  softSerial.println("Print Print Print Print Print");
  delay(250);
}

void appendFile(char fileName[]){
  char c;
  int test;
  //Send three control z to enter OpenLog command mode
  softSerial.print(byte(26));
  softSerial.print(byte(26));
  softSerial.print(byte(26));

  //Wait for OpenLog to respond with '>' to indicate we are in command mode
  long timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if (softSerial.read() == '>') break;
    }
  }

  String file_name = String(fileName);
  file_name = String("append ") + fileName + String("\r");
  softSerial.print(file_name);
  Serial.println(file_name);

  //Wait for OpenLog to indicate file is open and ready for writing
  timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if(softSerial.read() == '<') break;
    }
  }
}
