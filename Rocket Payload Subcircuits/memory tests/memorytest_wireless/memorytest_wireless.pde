#include <MemoryFree.h>
//#include <SD.h>                // Arduino SD card Library (for datalogger)
//#include <Servo.h>             // Arduino Servo Motor Library

//#include <Altimeter.h>         // Custom Altimeter Library for PerfectFlite StratoLogger
//#include <Coordinates.h>       // Custom Library to Generate a Square Coordinate System
//#include <GPS.h>               // Custom GPS Parser Library
//#include <Music.h>             // Custom Library to play Music (melody) from piezo
//#include <Vector.h>            // Custom LIbrary to Calculate Coorinate Vectors
//#include <VinMonitor.h>        // Custom Battery Voltage Monitor Library
#include <WirelessComm.h>
#define SERIALNUM 3

WirelessComm myWireless(SERIALNUM);
float gpsVars[9] = {84.023, 35.031, 22, 88.023, 30.031, 29, 102, 12, 7};
int LED = 53;

/*
File myFile;                   // Log File to Keep track of servo_position and stat
String logFileString;          // Log File name String object
String logNum;                 // Log File number String object
char logFileCharArray[13];     // Log File character array
int i;                         // Log File number 

// Servo Object and Variables
Servo myservo;                 // Servo object connected to Xbee connector
int initServoPos = 1500;       // Variable to store the servo position
int servoRight = 1425;         // Represents Servo Turned 1/3 rotation to the right
int servoLeft = 1575;          // Represents Servo Turned 1/3 rotation to teh left
int isRight = -1;              // Variable tracks if servo is postioned for right turn (or left turn)

// Altimeter Object and Variables
//Altimeter my_altimeter(SERIAL1);

// GPS and Supporting Objects and Variables
GPS my_gps;                       // GPS object connected to GPS connector
int gps_latitude_degrees = 0;
int gps_latitude_minutes = 0;
float gps_latitude_seconds = 0.0;
int gps_longitude_degrees = 0;
int gps_longitude_minutes = 0;
float gps_longitude_seconds = 0.0;
int gps_time_hours = 0;
int gps_time_minutes = 0;
float gps_time_seconds = 0.0;
float gps_altitude;
float gps_speed;
float gps_course;
int isFixed = 0;               // isFixed = 1, GPS has valid fix data
*/
// Music Object and HMI Variables
//Music speaker = Music(10);           // Speaker attached to pin 10, for playing tunes with Music
//int buzzer = 10;              // Buzzer used for Arduino Tone() function, also pin 10
//int button = 9;               // Pushbutton attached to pin 9
//long button_timer;            // Variable to store thearduino_timethe pushbutton is pressed
//long button_stop_time;        // Variable to store thearduino_timethe pushbutton is released
//long arduino_time;            // Variable to store the global Arduinoarduino_timeach cycle of loop()
//int guitarTune[] = {200, B3,q, d4,q, g4,q, B3,e, c4,q, d4,q, g4,q, c4,e};

// Vin Battery Voltage Monitor Object and Variables
//int Vin_analog_input_pin = 0; // Vin (Battery) Voltage Monitor connected to analog pin 0
//float Vref = 5.00;            // Vref measured value
//float Rref = 42.80;           // Rref measured value
//float Rup = 42.80;            // Rup measured value
//float diode_drop = 0.84;      // Input protection diode voltage drop
//VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

// Control System Variables
//Coordinates coords = Coordinates();
//int home_d_lat;
//int home_m_lat;
//float home_s_lat;
//int home_d_long;
//int home_m_long;
//float home_s_long;
//int isHomeSet;
//float spiral_angle = 20;
//float mappedX;
//float mappedY;
//float mapped_course_angle;
//float home_angle;
//float idealAngle;

void setup() {
  Serial.begin(9600);
  Serial.println(freeMemory());
  /*
  Serial2.begin(9600);         // Initialize Serial2 (for GPS)
  
  //Serial.println("\n*** Recovery System is Go! ***\n");
  Serial.println("\nGO!\n");
  
  if (!SD.begin(53)) {
    Serial.println("SD init fail!");
    return;
  }
  Serial.println("SD init done");
  
  //Create a file called 'logi.txt' on the SD card.
  logNum = String(i);
  logFileString = String("test" + logNum + ".txt");
  logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  
  // This while loop check for pictures already on the SD card and increments the index until
  // we have a unique filename, that is the next image number
  while (SD.exists(logFileCharArray)) {
    i++;
    logNum = String(i);
    logFileString = String("test" + logNum + ".txt");
    logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  }
  Serial.print("\nCreating ");
  Serial.print(logFileCharArray);
  Serial.println("...");
  myFile = SD.open(logFileCharArray, FILE_WRITE);
  if (myFile) {
    myFile.println("New Pushbutton Log File");
    myFile.println("Arduino Time, GPS Time, Battery Voltage,"
                   "Latitude Degrees, Latitude Minutes, Latitude Seconds,"
                  "Longitude Degrees, Longitude Minutes, Longitude Seconds,"
                   "GPS Altitude,Altimeter Altitude, Course, Speed,"
                   "X, Y, COG, Ideal COG, Home, Turn Right");
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(logFileCharArray);
  }
  myFile.close();
  
  myservo.attach(14);  // attaches the servo on pin 18 to the servo object 
  myservo.writeMicroseconds(initServoPos);
  */
  myWireless.beginSerial();
  
  pinMode(LED, OUTPUT);
  
  digitalWrite(LED, LOW);
  int i = 0;
  for(i; i < 100; i++){
    myWireless.transmitData(millis(), String("GPS"), gpsVars);
    Serial.println(freeMemory());
  }
  digitalWrite(LED, HIGH);
  
  Serial.println(freeMemory());
}

void loop() {
}
