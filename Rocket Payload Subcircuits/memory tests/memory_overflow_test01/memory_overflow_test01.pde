#include <MemoryFree.h>

String string1;
int n;

void setup() {
  Serial.begin(9600);
  Serial.println(freeMemory());
  n++;
  string1 = String("This is String " + String(n));
  Serial.println(string1);
  Serial.println(freeMemory());
  n++;
  string1 = String("This is String " + String(n));
  Serial.println(string1);
  Serial.println(freeMemory());
  pinMode(53, OUTPUT);
}

void loop() {
  digitalWrite(53, LOW);
  n++;
  string1 = String(String(n) + " " + string1);
  //Serial.println(string1);
  Serial.print(millis());
  Serial.print(" ");
  Serial.println(freeMemory());
  digitalWrite(53, HIGH);
  n++;
  string1 = String(String(n) + " " + string1);
  //Serial.println(string1);
  Serial.print(millis());
  Serial.print(" ");
  Serial.println(freeMemory());
  function1();
}

void function1() {
  char buff[] = {"This is a buffer"};
  function2();
}

void function2() {
  char buff[] = "This is a buffer";
  function3();
}

void function3() {
  char buff[] = "This is a buffer";
  function4();
}

void function4() {
  char buff[] = "This is a buffer";
  function5();
}

void function5() {
  char buff[] = "This is a buffer";
  function6();
}

void function6() {
  char buff[] = "This is a buffer";
  function7();
}

void function7() {
  char buff[] = "This is a buffer";
  function8();
}

void function8() {
  char buff[] = "This is a buffer";
  function9();
}

void function9() {
  char buff[] = "This is a buffer";
  function10();
}

void function10() {
  char buff[] = "This is a buffer A Huge Buffer this is a buffer A Huge Buffer this is a buffer A Huge Buffer this is a buffer A Huge Buffer this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer  this is a buffer A Huge Buffer ";
  Serial.print(buff);
  Serial.println(freeMemory());
  return;
}
