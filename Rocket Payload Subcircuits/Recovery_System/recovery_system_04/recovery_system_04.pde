#include <MemoryFree.h>
#include <GPS.h>
#include <SD.h>
#include <VinMonitor.h>
#include <Altimeter.h>
#include <Servo.h>
#include <Music.h>
#include <Coordinates.h>
#include <Vector.h>

// Create an instance of our GPS class (name the GPS object my_gps)
int button = 9;
int buzzer = 10;
long arduino_time;
int pos = 1500;    // variable to store the servo position 
int clockwise = 1;

//Create a String Object to hold the log filename
String logFileString;
String logNum;
char logFileCharArray[13];
int i; // to keep track of the picture we're on
int Vin_analog_input_pin = 0;
float Vref = 5.00;
float Rref = 42.80;
float Rup = 42.80;
float diode_drop = 0.84;

VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);
Altimeter my_altimeter(SERIAL1);
Servo myservo;

// GPS and Supporting Objects and Variables
GPS my_gps;                       // GPS object connected to GPS connector
int gps_latitude_degrees = 0;
int gps_latitude_minutes = 0;
float gps_latitude_seconds = 0.0;
int gps_longitude_degrees = 0;
int gps_longitude_minutes = 0;
float gps_longitude_seconds = 0.0;
int gps_time_hours = 0;
int gps_time_minutes = 0;
float gps_time_seconds = 0.0;
float gps_altitude;
float gps_speed;
float gps_course;
int isFixed = 0;               // isFixed = 1, GPS has valid fix data

Coordinates coords = Coordinates();

File myFile;
Music speaker = 10;

int guitarTune[] = {200, B3,q, d4,q, g4,q, B3,e, c4,q, d4,q, g4,q, c4,e};
long button_timer;
long button_stop_time;

// Control System Variables
int home_d_lat;
int home_m_lat;
float home_s_lat;
int home_d_long;
int home_m_long;
float home_s_long;
int isHomeSet;
float spiral_angle = 20;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600);
 
  // Initialize Serial2 (the default port for attaching a GPS device in this library) at
  // whatever baud rate your GPS device communicates at. Mine is 9600 baud, but other common
  // rates are 4800, and 19200. Also, to use a different port than Serial2 or to use a software
  // serial port, please see the GPS.cpp file for information on how to modify the library to do
  // this.
  Serial2.begin(9600); 
  Serial.println("\n*** SD Card Test ***\n");
  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(53, OUTPUT);
   
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  //Create a file called 'GPS_LOGi.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  logNum = String(i);
  logFileString = String("test" + logNum + ".txt");
  logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  
  // This while loop check for pictures already on the SD card and increments the index until
  // we have a unique filename, that is the next image number
  while (SD.exists(logFileCharArray)) {
    i++;
    logNum = String(i);
    logFileString = String("test" + logNum + ".txt");
    logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  }
  Serial.print("\nCreating ");
  Serial.print(logFileCharArray);
  Serial.println("...");
  myFile = SD.open(logFileCharArray, FILE_WRITE);
  if (myFile) {
    myFile.println("New Pushbutton Log File");
    myFile.println("Time, Battery Voltage, Latitude Degrees, Latitude Minutes, Latitude Seconds, Longitude Degrees, Longitude Minutes, Longitude Seconds,GPS Altitude,Altimeter Altitude, Course, Speed");
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(logFileCharArray);
  }
  myFile.close();
  
  myservo.attach(14);  // attaches the servo on pin 18 to the servo object 
  myservo.writeMicroseconds(pos);
  delay(3000);
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
  arduino_time = millis()/1000;
  Serial.print("\nMemory Free = ");
  Serial.println(freeMemory());
  // Call a custom funtion (which is defined below) that will read the GPS
  read_gps();
  
  //check_button();                     // Check the HMI Button for Presses and Home location logging
  if(digitalRead(button) == 0) {                    // Check if button pressed
    button_timer = millis();                        // Start the button timer
    if(isHomeSet == 1) {                            // If isHomeSet is already set
      tone(buzzer, 1000000/a5);                     // Play a little three tone pluse to let us know
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/b5);
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/c6);
      delay(250);
      noTone(buzzer);
      delay(250);
    } 
    tone(buzzer, 1000000/a5);                       // Start playing the tone
    delay(20);                                      // Delay to debounce pushbutton
    while(digitalRead(button) == 0) {               // While button pressed check to see
      button_stop_time = millis() - button_timer;   // if button has been pressed for 3 seconds
      if(button_stop_time/1000 >= 3) {              // If button has been pressed for 3 seconds
        Serial.println("Button Held for 3 Seconds");// Tell us
        noTone(buzzer);                             // turn off the buzzer
        if(isFixed == 1) {                          // If there is a valid GPS fix
                                                    // Save the home location
          /***** Save Home Location Coordinates *****/
          home_d_lat = gps_latitude_degrees;
          home_m_lat = gps_latitude_minutes;
          home_s_lat = gps_latitude_seconds;
          
          home_d_long = gps_longitude_degrees;
          home_m_long = gps_longitude_minutes;
          home_s_long = gps_longitude_seconds;
 
          isHomeSet = 1;
          
          coords = Coordinates(home_d_lat, home_m_lat, home_s_lat,
                      home_d_long, home_m_long, home_s_long);
          /*****                                *****/
                                                    // And play a tune to let us know
          speaker.playMusic(guitarTune, sizeof(guitarTune)); 
          break;                                    // Then break from the while loop
        }
        else {                                      // If there is no valid GPS fix
          tone(buzzer, 1000000/c4);                 // Play a 3 second long tone
          delay(3000);                              // To let us know that
          noTone(buzzer);                           // Then turn the buzzer off
        } 
      }
    }
    noTone(buzzer);                                 // Turn off the buzzer
    while(digitalRead(button) == 0);                // And wait for the button to be let up
  }
  
  myFile = SD.open(logFileCharArray, FILE_WRITE);
  // if the file opened okay, write to it:
  if (myFile) {
    Serial.print("Writing to ");
    Serial.println(logFileCharArray);
    Serial.print("Time = ");
    Serial.print(arduino_time);
    Serial.println(" sec");
    
    
    myFile.print(arduino_time);
    myFile.print(", ");
    
    myFile.print(gps_time_hours);
    myFile.print(":");
    myFile.print(gps_time_minutes);
    myFile.print(":");
    myFile.print(gps_time_seconds);
    myFile.print(", ");
    
    myFile.print(battery.get_Vin());
    myFile.print(", ");
    
    myFile.print(gps_latitude_degrees);
    myFile.print(", ");
    myFile.print(gps_latitude_minutes);
    myFile.print(", ");
    myFile.print(gps_latitude_seconds,5);
    myFile.print(", ");
    
    myFile.print(gps_longitude_degrees);
    myFile.print(", ");
    myFile.print(gps_longitude_minutes);
    myFile.print(", ");
    myFile.print(gps_longitude_seconds,5);
    myFile.print(", ");
    
    myFile.print(gps_altitude);
    myFile.print(", ");
    
    int alt = my_altimeter.get_altitude();
    myFile.print(alt);
    // If the function returns an altitude of -999 ft it means the device timed out
    if (alt == -999) {
      // When the device times out it could mean the device is not configured properly
      // Or is disconnected or damanged
      Serial.println("Altimeter Disconnected or Dead!");
    }
    // Otherwise the altitude should be accurate
    else {
      // So we display the altitude on the serial monitor
      Serial.print("Altimeter Altitude = ");
      Serial.println(alt);
    }
    myFile.print(", ");
    myFile.print(gps_course);
    myFile.print(", ");
    myFile.print(gps_speed);
    myFile.print(", ");
    
    myFile.println();
    
    // close the file:
    myFile.close();
    Serial.print("...done writing to ");
    Serial.println(logFileCharArray);
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(logFileCharArray);
  }
  
  if (isHomeSet == 1) {
    Serial.print("Home Latitude = ");
    Serial.print(home_d_lat);
    Serial.print(char(176));
    Serial.print(home_m_lat);
    Serial.print("'");
    Serial.print(home_s_lat);
    Serial.println("\"");
    
    Serial.print("Home Longitude = ");
    Serial.print(home_d_long);
    Serial.print(char(176));
    Serial.print(home_m_long);
    Serial.print("'");
    Serial.print(home_s_long);
    Serial.println("\"");
    
    Serial.print("Square Ratio=");
    Serial.println(coords.getSqRatio(),5);
    Serial.print("Origin x seconds=");
    Serial.println(coords.getX0());
    Serial.print("Origin y seconds=");
    Serial.println(coords.getY0());
    
    Serial.println("Mapping Current Point");
    coords.map_point(gps_latitude_minutes, gps_latitude_seconds,
                     gps_longitude_minutes, gps_longitude_seconds);
    Serial.print("Mapped x = ");
    Serial.println(coords.getX());
    Serial.print("Mapped y = ");
    Serial.println(coords.getY());
    Serial.println("Mapping COG to coordinate system:");
    Serial.print("Mapped angle = ");
    float mapped_course_angle = coords.map_COG(gps_course);
    Serial.println(mapped_course_angle);
    
    Vector cv = Vector(mapped_course_angle);
    Vector hv = Vector(coords.getX(), coords.getY());
    hv.rotate(spiral_angle);
    
    if(cv.cmpAngle(&hv) > 0 ){
      Serial.println("Turn Left");
      myservo.writeMicroseconds(pos+100);
    }
    else {
      Serial.println("Turn Right");
      myservo.writeMicroseconds(pos-100);
    }
  }
  else {
    Serial.println("*** No HOME LOCATION SET ***");
  }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
 
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message, VERBOSE);
      if(test_valid == 1) {
        Serial.println("Checksum Valid\n");
        
        /***** Update GPS Data Objects *****/
        Latitude test_latitude = Latitude(my_gps.get_latitude_string());
        gps_latitude_degrees = test_latitude.get_degrees();
        gps_latitude_minutes = test_latitude.get_minutes();
        gps_latitude_seconds = test_latitude.get_seconds();
        
        Longitude test_longitude = Longitude(my_gps.get_longitude_string());
        gps_longitude_degrees = test_longitude.get_degrees();
        gps_longitude_minutes = test_longitude.get_minutes();
        gps_longitude_seconds = test_longitude.get_seconds();
        
        Time test_time = Time(my_gps.get_time_string());
        gps_time_hours = test_time.get_hours();
        gps_time_minutes = test_time.get_minutes();
        gps_time_seconds = test_time.get_seconds();
        
        gps_altitude = my_gps.get_altitude();
        
        gps_course = my_gps.get_course();
        
        gps_speed = my_gps.get_speed(MPH);
        /*****                         *****/
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          isFixed = 0;
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          isFixed = 1;
        }
        else {
          Serial.println("Data status unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          isFixed = 1;
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          isFixed = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          isFixed = 1;
        }
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}
