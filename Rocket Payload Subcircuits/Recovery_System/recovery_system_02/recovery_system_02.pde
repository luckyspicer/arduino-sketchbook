/***** Libraries Necessary to run the Recovery System *****/
#include <SD.h>                // Arduino SD card Library (for datalogger)
#include <Servo.h>             // Arduino Servo Motor Library

#include <Altimeter.h>         // Custom Altimeter Library for PerfectFlite StratoLogger
#include <GPS.h>               // Custom GPS Parser Library
#include <Music.h>             // Custom Library to play Music (melody) from piezo
#include <VinMonitor.h>        // Custom Battery Voltage Monitor Library

/***** Create Instances of Custom Objects and Supporting Globabl Variables *****/
// SC Card/File Objects and Variables
File logf;                 // Log File to Keep track of position and state
String log_file_String;        // Log File name String object
String log_num;                // Log File number String object
int log_file_num;              // Log File number
char log_file_char_array[13];  // Log File character array
int SS = 53;                   // Slave Select (SS) is pin 53, used to control SD card
int isSD = 0;                  // Flag isSD = 1 if SD card initialized properly, 0 otherwise.
int sd_alert_state = 0;        // Flag that keeps track of the SD card alert (buzzer tells us SD not connected)
long sd_alert_time;            // Long Time to keep track for alert_state buzzer

// Servo Object and Variables
Servo servo;                   // Servo object connected to Xbee connector
int servo_pin;                 // Servo is controlled by pin 14
int servo_pos = 1500;          // int servo position, intially set to midpoint = 1500uS pulse
int isClockwise = 1;           // isClockwise = 1, servo will rotate clockwise, otherwise anti-clockwise 

// Altimeter Object and Variables
Altimeter altimeter(SERIAL1);
int altimeter_altitude;

// GPS and Supporting Objects and Variables
GPS gps;                       // GPS object connected to GPS connector
                               // Objects for GPS: Latitude, Longitude, Time
Latitude gps_latitude = Latitude(gps.get_latitude_string());
Longitude gps_longitude = Longitude(gps.get_longitude_string());
Time gps_time = Time(gps.get_time_string());
int isFixed = 0;               // isFixed = 1, GPS has valid fix data

// Music Object and HMI Variables
Music speaker = 10;            // Speaker attached to pin 10, for playing tunes with Music
int buzzer = 10;               // Buzzer used for Arduino Tone() function, also pin 10
int button = 9;                // Pushbutton attached to pin 9
long button_timer;             // Variable to store the time the pushbutton is pressed
long button_stop_time;         // Variable to store the time the pushbutton is released
long arduino_time;             // Variable to store the global Arduino time each cycle of loop()
int isHomeSet = 0;             // Variable that tells us if the Home location has been set
int speaker_tune[] = {200, B3,q,  d4,q,  g4,q,  B3,e, c4,q,  d4,q,  g4,q,  c4,e,
                           d4,q,  d4,e,  g4,q,  d4,q, d4,q,  d4,e,  g4,q,  d4,q,
                           g4,q,  d4,q,  g4,q,  g4,e, fs4,q, d4,q,  g4,q,  fs4,e,
                           a4,q,  d4,q,  g4,q,  a4,e, b4,q,  d4,q,  g4,q,  b4,q,
                           d5,q,  g4,q,  b4,q,  d5,e, c5,q,  g4,q,  b4,q,  c5,e,
                           b4,q,  g4,q,  b4,q,  b4,e, a4,q,  g4,q,  b4,q,  g4,q};

// Vin Battery Voltage Monitor Object and Variables
int Vin_analog_input_pin = 0;  // Vin (Battery) Voltage Monitor connected to analog pin 0
float Vref = 5.00;             // Vref measured value
float Rref = 42.80;            // Rref measured value
float Rup = 42.80;             // Rup measured value
float diode_drop = 0.84;       // Input protection diode voltage drop
VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

// Control System Variables
int home_m_lat;
float home_s_lat;
int home_m_long;
float home_s_long;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
// Setup Can run operation that only need to occur once in the program lifecyle
void setup() 
{ 
  Serial.begin(9600);          // Initialaze Serial (for debugging and USB interface)
                               // Serial1 is used for Altimeter and was initialized in its constructor  
  Serial2.begin(9600);         // Initialize Serial2 (for GPS)
  
  Serial.println("\n*** Recovery System is Go! ***\n");
  
  Serial.print("Initializing SD card...");
  pinMode(SS, OUTPUT);         // Slave Select Pin must be output for SD card to work
  
  initialize_sd();             // Run custom function to intitialize SD card and log file 
  
  servo.attach(servo_pin);            // Attach the Servo object to the servo on pin 14 
  servo.writeMicroseconds(servo_pos); // Command the servo to return it its initial (midpoint) position
  delay(3000);                        // Give the Servo time to reset back to midpoint
  sd_alert_time = millis();
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
// The Loop reads the gps and altimeter, performs human machine interface operations
// and implements the recovery controller
void loop() {
  arduino_time = millis();            // Read the Arduino time
  read_gps();                         // Read the GPS
  check_sd_flag();                    // Check SD flag to see if SD card is initialized
  read_altimeter();                 // Read the altimeter
  check_button();                     // Check the HMI Button for Presses and Home location logging
  
  if (isHomeSet == 1) {
    Serial.print("Home Longitude = ");
    Serial.print(home_m_long);
    Serial.print("' ");
    Serial.print(home_s_long);
    Serial.println("\"");
    
    Serial.print("Home Latitude = ");
    Serial.print(home_m_lat);
    Serial.print("' ");
    Serial.print(home_s_lat);
    Serial.println("\"");
  }
  else {
    Serial.println("*** No HOME LOCATION SET ***");
  }
  
  log_system_state();
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum.
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remaining parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String gps_message = gps.get_gps_line();
 
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      int test_valid = gps.parse_NMEA_message(gps_message);
      if(test_valid == 1) {                      // If NMEA message has valid checksum, update data objects
        Serial.println("Checksum Valid\n");
        
        /***** Update GPS Data Objects *****/
        gps_latitude = Latitude(gps.get_latitude_string());
        gps_longitude = Longitude(gps.get_longitude_string());
        gps_time = Time(gps.get_time_string());
        /*****                         *****/
        
        int data_status = 0;
        // Test Status Indicators
        if(gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          data_status = 0;
        }
        else if(gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          data_status = 1;
        }
        else {
          Serial.println("Data status unknown");
          data_status = 0;
        }
        
        int fix_mode;
        if(gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          fix_mode = 0;
        }
        else if(gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          fix_mode = 1;
        }
        else if(gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          fix_mode = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          fix_mode = 0;
        }
        
        int fix_status;
        if(gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          fix_status = 0;
        }
        else if(gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          fix_status = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          fix_status = 0;
        }
        
        int mod_ind;
        if(gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          mod_ind = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          mod_ind = 1;
        }
        
        if(data_status == 1 || fix_mode == 1 || fix_status == 1 || mod_ind == 1) {
          isFixed = 1;
        }
        else{
          isFixed = 0;
        }
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the gps watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}

//
// Function: initialize_sd()
//
// Input: void
// Return: void
//
// Description: Use this function to initialize the sd card and the log file with the propper
//              comma separated data fields
//
void initialize_sd() {
  if (!SD.begin(SS)) {         // Begin the SD card and check for sucessful initialization 
    Serial.println("initialization failed!");
    isSD = 0;                  // If initialization fails, flag the system that it failed
    return;
  }
  else {
    Serial.println("initialization successful");
    isSD = 1;                  // If initialization successful, flag the system that it succeeded
  } 
  
  //Create a file called 'logi.txt' on the SD card.
  log_num = String(log_file_num);
  log_file_String = String("test" + log_num + ".txt");
  log_file_String.toCharArray(log_file_char_array, log_file_String.length()+1);
  
  // This while loop check for log files already on the SD card and increments the index until
  // we have a unique filename, that is the next log file number
  while (SD.exists(log_file_char_array)) {
    log_file_num++;
    log_num = String(log_file_num);
    log_file_String = String("test" + log_num + ".txt");
    log_file_String.toCharArray(log_file_char_array, log_file_String.length()+1);
  }
  Serial.print("\nCreating ");
  Serial.print(log_file_char_array);
  Serial.println("...");
  logf = SD.open(log_file_char_array, FILE_WRITE);
  if (logf) {
    logf.println("New Pushbutton Log File");
    logf.println("Arduino Time, GPS Time, Battery Voltage, Latitude Degrees, Latitude Minutes, Latitude Seconds, Longitude Degrees, Longitude Minutes, Longitude Seconds,"
                   "GPS Altitude, Altimeter Altitude, Course, Speed");
  } else {         
    Serial.print("error opening ");   // if the file didn't open, print an error:
    Serial.println(log_file_char_array);
  }
  logf.close();                   // Close the log file
}

//
// Function: check_sd_flag()
//
// Input: void
// Return: void
//
// Description: Use this function to check if the sd flag is set, and use the buzzer to alert
//              that no SD card is correctly initialized (if that is the case)
//
void check_sd_flag() {
  // If the SD card initialized flag is not set and SD alert timer is out, try SD init again
  if (isSD == 0 && ((millis() - sd_alert_time) > 1000)) {
    if (sd_alert_state == 1) {        // If sd_alert_state = 1, play tone on buzzer
      tone(buzzer, 1000000/d4);       // Play tone on buzzer
      sd_alert_state = 0;             // Set sd_alert_state = 0, to turn tone off
    }
    else {
      noTone(buzzer);                 // If sd_alert_state = 0, turn off tone
      sd_alert_state = 1;             // Set sd_alert_state = 1, to turn tone on
    }
    sd_alert_time = millis();         // Reset the sd alert timer
  }
}

//
// Function: read_altimeter()
//
// Input: void
// Return: void
//
// Description: Read the altimeter and print a debug message if the altimeter is disconnected
//              or else print the altitude
//
void read_altimeter() {
  altimeter_altitude = altimeter.get_altitude();
  // If the function returns an altitude of -999 ft it means the device timed out
  if (altimeter_altitude == -999) {
    // When the device times out it could mean the device is not configured properly
    // Or is disconnected or damanged
    Serial.println("Altimeter Disconnected or Dead!");
  }
  else {
    Serial.print("Altimeter Altitude = ");
    Serial.println(altimeter_altitude);
  }
}

//
// Function: check_button()
//
// Input: void
// Return: void
//
// Description: Checks the pushbutton for press. If pressed it the buzzer plays a tone
//              while pressed. If the button is pressed for 3 seconds the system checks
//              if there is a valid GPS fix. If there is a valid fix the current GPS
//              latitude and longitude minutes and seconds are saved as the home location.
//
void check_button() {
  if(digitalRead(button) == 0) {                    // Check if button pressed
    button_timer = millis();                        // Start the button timer
    if(isHomeSet == 1) {                            // If isHomeSet is already set
      tone(buzzer, 1000000/a5);                     // Play a little three tone pluse to let us know
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/b5);
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/c6);
      delay(250);
      noTone(buzzer);
      delay(250);
    } 
    tone(buzzer, 1000000/a5);                       // Start playing the tone
    delay(20);                                      // Delay to debounce pushbutton
    while(digitalRead(button) == 0) {               // While button pressed check to see
      button_stop_time = millis() - button_timer;   // if button has been pressed for 3 seconds
      if(button_stop_time/1000 >= 3) {              // If button has been pressed for 3 seconds
        Serial.println("Button Held for 3 Seconds");// Tell us
        noTone(buzzer);                             // turn off the buzzer
        if(isFixed == 1) {                          // If there is a valid GPS fix
                                                    // Save the home location
          /***** Save Home Location Coordinates *****/
          home_m_long = gps_longitude.get_minutes();
          home_s_long = gps_longitude.get_seconds();
        
          home_m_lat = gps_latitude.get_minutes();
          home_s_lat = gps_latitude.get_seconds();
          isHomeSet = 1;
          /*****                                *****/
                                                    // And play a tune to let us know
          speaker.playMusic(speaker_tune, sizeof(speaker_tune)); 
          break;                                    // Then break from the while loop
        }
        else {                                      // If there is no valid GPS fix
          tone(buzzer, 1000000/c4);                 // Play a 3 second long tone
          delay(3000);                              // To let us know that
          noTone(buzzer);                           // Then turn the buzzer off
        } 
      }
    }
    noTone(buzzer);                                 // Turn off the buzzer
    while(digitalRead(button) == 0);                // And wait for the button to be let up
  }
}

//
// Function: log_system_state()
//
// Input: void
// Return: void
//
// Description: Opends the log file and saves all the specified data as a comma separated
//              text file. Enjoy!
//
void log_system_state() {
  logf = SD.open(log_file_char_array, FILE_WRITE);
  // if the file opened okay, write to it:
  if (logf) {
    Serial.print("Writing to ");
    Serial.println(log_file_char_array);
    Serial.print("Time = ");
    Serial.print(arduino_time);
    Serial.println(" sec");
    
    logf.print(arduino_time);
    logf.print(", ");
    
    logf.print(gps_time.get_hours());
    logf.print(":");
    logf.print(gps_time.get_minutes());
    logf.print(":");
    logf.print(gps_time.get_seconds());
    logf.print(", ");
    
    logf.print(battery.get_Vin());
    logf.print(", ");
    
    logf.print(gps_latitude.get_degrees());
    logf.print(", ");
    logf.print(gps_latitude.get_minutes());
    logf.print(", ");
    logf.print(gps_latitude.get_seconds(),5);
    logf.print(", ");
    
    logf.print(gps_longitude.get_degrees());
    logf.print(", ");
    logf.print(gps_longitude.get_minutes());
    logf.print(", ");
    logf.print(gps_longitude.get_seconds(),5);
    logf.print(", ");

    logf.print(gps.get_altitude());
    logf.print(", ");
    logf.print(altimeter_altitude);
    logf.print(", ");

    logf.print(gps.get_course());
    logf.print(", ");
    logf.print(gps.get_speed(MPH));
    logf.print(", ");
    logf.println();
    
    // close the file:
    logf.close();
    
    Serial.print("...done writing to ");
    Serial.println(log_file_char_array);
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(log_file_char_array);
  }
}
