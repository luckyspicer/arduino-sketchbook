
// Libraries Necessary to run the Recovery System
#include <SD.h>             // Arduino SD card Library (for datalogger)
#include <Servo.h>          // Arduino Servo Motor Library
#include <GPS.h>            // Custom GPS Parser Library
#include <VinMonitor.h>     // Custom Battery Voltage Monitor Library
#include <Altimeter.h>      // Custom Altimeter Library for PerfectFlite StratoLogger
#include <Music.h>          // Custom Library to play Music (melody) from piezo

// Create an instance of our GPS class (name the GPS object my_gps)
int button = 9;
int buzzer = 10;
long time;
int isFixed = 0;
int pos = 1500;    // variable to store the servo position 
int clockwise = 1;

//Create a String Object to hold the log filename
String logFileString;
String logNum;
char logFileCharArray[13];
int i; // to keep track of the picture we're on
int Vin_analog_input_pin = 0;
float Vref = 5.00;
float Rref = 42.80;
float Rup = 42.80;
float diode_drop = 0.84;

VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);
Altimeter my_altimeter(SERIAL1);
Servo myservo;
GPS my_gps;

Latitude test_latitude = Latitude(my_gps.get_latitude_string());
Longitude test_longitude = Longitude(my_gps.get_longitude_string());
Time test_time = Time(my_gps.get_time_string());
Date test_date = Date(my_gps.get_date_string());

File myFile;
Music speaker = 10;

int guitarTune[] = {200, B3,q,  d4,q,  g4,q,  B3,e,
                     c4,q,  d4,q,  g4,q,  c4,e,
                     d4,q,  d4,e,  g4,q,  d4,q,  d4,q,  d4,e,  g4,q,  d4,q,
                     g4,q,  d4,q,  g4,q,  g4,e,  fs4,q,  d4,q,  g4,q,  fs4,e,
                     a4,q,  d4,q,  g4,q,  a4,e,
                     b4,q,  d4,q,  g4,q,  b4,q,
                   
                     d5,q,  g4,q,  b4,q,  d5,e,
                     c5,q,  g4,q,  b4,q,  c5,e,
                     b4,q,  g4,q,  b4,q,  b4,e,
                     a4,q,  g4,q,  b4,q,  g4,q};
long timer;
long button_stop_time;
int home_m_lat;
float home_s_lat;
int home_m_long;
float home_s_long;
int isHomeSet = 0;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600);
 
  // Initialize Serial2 (the default port for attaching a GPS device in this library) at
  // whatever baud rate your GPS device communicates at. Mine is 9600 baud, but other common
  // rates are 4800, and 19200. Also, to use a different port than Serial2 or to use a software
  // serial port, please see the GPS.cpp file for information on how to modify the library to do
  // this.
  Serial2.begin(9600); 
  Serial.println("\n*** SD Card Test ***\n");
  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(53, OUTPUT);
   
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  //Create a file called 'GPS_LOGi.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  logNum = String(i);
  logFileString = String("test" + logNum + ".txt");
  logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  
  // This while loop check for pictures already on the SD card and increments the index until
  // we have a unique filename, that is the next image number
  while (SD.exists(logFileCharArray)) {
    i++;
    logNum = String(i);
    logFileString = String("test" + logNum + ".txt");
    logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  }
  Serial.print("\nCreating ");
  Serial.print(logFileCharArray);
  Serial.println("...");
  myFile = SD.open(logFileCharArray, FILE_WRITE);
  if (myFile) {
    myFile.println("New Pushbutton Log File");
    myFile.println("Time, Battery Voltage, Latitude Degrees, Latitude Minutes, Latitude Seconds, Longitude Degrees, Longitude Minutes, Longitude Seconds,GPS Altitude,Altimeter Altitude, Course, Speed");
  } else {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(logFileCharArray);
  }
  myFile.close();
  
  myservo.attach(14);  // attaches the servo on pin 18 to the servo object 
  myservo.writeMicroseconds(pos);
  delay(3000);
  time = millis();
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
  // Call a custom funtion (which is defined below) that will read the GPS
  read_gps();
  if(digitalRead(button) == 0) {
    timer = millis();
    tone(buzzer, 1000000/a5);
    delay(20);
    while(digitalRead(button) == 0) {
      button_stop_time = millis() - timer;
      if(button_stop_time/1000 >= 3) {
        Serial.println("Button Held for 3 Seconds");
        noTone(buzzer);
        if(isFixed == 1) {
          home_m_long = test_longitude.get_minutes();
          home_s_long = test_longitude.get_seconds();
        
          home_m_lat = test_latitude.get_minutes();
          home_s_lat = test_latitude.get_seconds();
          isHomeSet = 1;
          speaker.playMusic(guitarTune, sizeof(guitarTune));
          break;
        }
        else {
          tone(buzzer, 1000000/c4);
          delay(3000);
          noTone(buzzer);
        } 
      }
    }
    noTone(buzzer);
    while(digitalRead(button) == 0);
  }
  if (isHomeSet == 1) {
    Serial.print("Home Longitude = ");
    Serial.print(home_m_long);
    Serial.print("' ");
    Serial.print(home_s_long);
    Serial.println("\"");
    
    Serial.print("Home Latitude = ");
    Serial.print(home_m_lat);
    Serial.print("' ");
    Serial.print(home_s_lat);
    Serial.println("\"");
  }
  else {
    Serial.println("*** No HOME LOCATION SET ***");
  }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
 
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
      if(test_valid == 1) {
        Serial.println("Checksum Valid\n");
        
        test_latitude = Latitude(my_gps.get_latitude_string());
        test_longitude = Longitude(my_gps.get_longitude_string());
        test_time = Time(my_gps.get_time_string());
        test_date = Date(my_gps.get_date_string());
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          isFixed = 0;
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          isFixed = 1;
        }
        else {
          Serial.println("Data status unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          isFixed = 1;
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          isFixed = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          isFixed = 1;
        }
        Serial.println();
        time = millis();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}
