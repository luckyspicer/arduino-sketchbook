void setup() {
  Serial.begin(9600);
  Serial.println("Beginning Test");
}

void loop() {
  for(int i = 62; i <= 69; i++) {
    Serial.print("Testing Pin ");
    Serial.print(i);
    pinMode(i, OUTPUT);
    Serial.print(" Low...");
    digitalWrite(i, LOW);
    delay(2500);
    Serial.print(" High...");
    digitalWrite(i, HIGH);
    delay(2500);
    Serial.println(" Low...");
    digitalWrite(i, LOW);
    pinMode(i, INPUT);
  }
}
