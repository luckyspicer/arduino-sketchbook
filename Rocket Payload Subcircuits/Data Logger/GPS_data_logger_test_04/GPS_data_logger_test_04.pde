#include <SD.h>

File myFile;

//Create a String Object to hold the log filename
String logFileString;
String logNum;
char logFileCharArray[13];
int i; // to keep track of the picture we're on

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600); 
  
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin 
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output 
  // or the SD library functions will not work. 
   pinMode(53, OUTPUT);
   
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  
  //Create a file called 'GPS_LOGi.txt' on the SD card.
  //NOTE: The memoryCard libary can only create text files.
  logNum = String(i);
  logFileString = String("log_test" + logNum + ".txt");
  logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  
  // This while loop check for pictures already on the SD card and increments the index until
  // we have a unique filename, that is the next image number
  while (SD.exists(logFileCharArray)) {
    i++;
    logNum = String(i);
    logFileString = String("log_test" + logNum + ".txt");
    logFileString.toCharArray(logFileCharArray, logFileString.length()+1);
  }
  Serial.print("\nCreating ");
  Serial.print(logFileCharArray);
  Serial.println("...");
  myFile = SD.open(logFileCharArray, FILE_WRITE);
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
   delay(1000);
   // re-open the file for reading:
   if(digitalRead(button) == 0) {
     while(digitalRead(button) == 0);
        myFile = SD.open(logFileCharArray, FILE_WRITE);
        // if the file opened okay, write to it:
        if (myFile) {
          Serial.print("Writing to ");
          Serial.print(logFileCharArray);
          Serial.print("Time = ");
          Serial.print(millis()/1000);
          Serial.println(" sec");
          
          myFile.print("Time = ");
          myFile.print(millis()/1000);
          myFile.println(" sec");
          
          // close the file:
          myFile.close();
          Serial.print("...done writing to ");
          Serial.println(logFileCharArray);
        } else {
          // if the file didn't open, print an error:
          Serial.println("error opening GPS_log.txt");
        }
   }
}
