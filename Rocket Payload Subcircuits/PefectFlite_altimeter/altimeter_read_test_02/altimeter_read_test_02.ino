int serial_port_number = 1;

void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial3.begin(9600);
  
  Serial.setTimeout(100);
  Serial1.setTimeout(100);
  Serial2.setTimeout(100);
  Serial3.setTimeout(100);
  
  Serial.println("***Test of Altimeter 01***"); 
}

int altitude;

void loop()
{
  if (serial_port_number == 0) {
    while(Serial.available() > 10) {
      altitude = Serial.parseInt();
    }
  }
  else if (serial_port_number == 1) {
    while(Serial1.available() > 10) {
      altitude = Serial1.parseInt();
    }
  }
  else if (serial_port_number == 2) {
    while(Serial2.available() > 10) {
      altitude = Serial2.parseInt();
    }
  }
  else if (serial_port_number == 3) {
    while(Serial3.available() > 10) {
      altitude = Serial3.parseInt();
    }
  }
  Serial.println(altitude);
  Serial.print("Bytes Available = ");
  Serial.println(Serial1.available());
  delay(1500);
}
