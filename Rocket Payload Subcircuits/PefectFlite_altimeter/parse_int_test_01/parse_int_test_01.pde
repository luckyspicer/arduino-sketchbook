void setup() {
  Serial.begin(9600);
  Serial2.begin(9600);
}

void loop() {
  Serial.print("Available = ");
  Serial.println(Serial2.available());
  while(Serial2.available() >= 10) {
    Serial2.read();
  }
  Serial.print("Available = ");
  Serial.println(Serial2.available());
  Serial.println(parse_int());
  delay(250);
}

int parse_int(void) {
  int isvalid = -999;                               // Flag to determine if we got a valid input 1 = valid, -1 = timeout
  int i = 0;                                        // char_buff index counter
  unsigned long timeout = 0;                        // Variable to store the timeout time           
  char char_buff[10];								// Variable to store the values characters read in
  timeout = millis();                             	// Initialize the timeout counter
  while(i < 2) {
  char_buff[i] = Serial2.read();                 	// Read Serial again for next char
    while(char_buff[i] != '\n' && i < 9) {           // While char != end of line, or have a full buffer, continue
		if(char_buff[i] != -1) {                    // If char != -1 (Serial.read() == -1 indicates no data available
			i++;                                    // Increment index buffer
		}                                           // This ensure Arduino does not go faster than GPS transmission
		char_buff[i] = Serial2.read();              // Read in next character in char_buff[i]
        if(millis() - timeout > 150) {              // If millis() - timeout > 150, this means more than 150mSec      
          return isvalid;                           // dead or dying, return isvalid == -1 (timeout)
        }                                           
    }
  }
    if(char_buff[i] != '\n') {
      return isvalid;
    }
    char_buff[i] = 0; 								// Make last character of char_buff[i] = NULL
    Serial.print("i = ");
    Serial.println(i);
    Serial.println(char_buff);
	return atoi(char_buff);                       	// Use atoi() to convert char array to integer
}
