void setup() 
{
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial1.setTimeout(100);
  
  Serial.println("***Test of Altimeter 01***"); 
}

void loop()
{
  Serial.print("Time is ");
  Serial.println(millis()/1000);
  int altitude = Serial1.parseInt();
  Serial.println(altitude);
  Serial.print("Bytes Available = ");
  Serial.println(Serial1.available());
}
