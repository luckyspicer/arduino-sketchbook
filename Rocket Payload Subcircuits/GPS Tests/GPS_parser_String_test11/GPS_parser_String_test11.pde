/* This is simply a test of the Arduino String object commands a simple way to parse GPS sentence*/

// Face NMEA GPS test strings
String test_NMEA[] = {  "$GPRMC,201547.000,A,3014.5527,N,09749.5808,W,0.24,163.05,040109,,*1A",
                        "$GPGGA,201548.000,3014.5529,N,09749.5808,W,1,07,1.5,225.6,M,-22.5,M,18.8,0000*78",
                        "$GPRMC,201548.000,A,3014.5529,N,09749.5808,W,0.17,53.25,040109,,*2B",
                        "$GPGGA,201549.000,3014.5533,N,09749.5812,W,1,07,1.5,223.5,M,-22.5,M,18.8,0000*7C",
                        "$GPGSA,A,3,23,29,07,08,09,18,26,28,,,,,1.94,1.18,1.54*0D",
                        "$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,8,1.01,499.6,M,48.0,M,,0*5B"};

// PRIVATE VARIABLES IN LIBRARY

// All available GPS NMEA Message Fields (in alphabetical order)     
char   _altref[12],_cog[12],_cs_gga[12],_cs_gsa[12],_cs_rmc[12],_date[12],_diffage[12],_diffstation[12],
       _e_w[12],_fs[12],_hdop[12],_latitude[12],_longitude[12],_mode[12],_msl[12],_mv[12],_mve[12],_n_s[12],
       _nosv[12],_pdop[12],_smode[12],_speed[12],_status[12],_sv1[12],_sv2[12],_sv3[12],_sv4[12],_sv5[12],
       _sv6[12],_sv7[12],_sv8[12],_sv9[12],_sv10[12],_sv11[12],_sv12[12],_time[12],_umsl[12],_usep[12],_vdop[12];

// Arrays hold the particular fields present in each NMEA message type
//   These arrays must be arranged with fields in the order the fields appear in that particular NMEA messags strin
char *_GPGGA_fields[] = {_time,_latitude,_n_s,_longitude,_e_w,_fs,_nosv,_hdop,_msl,_umsl,_altref,_usep,
                         _diffage,_diffstation,_cs_gga};

char *_GPGSA_fields[] = {_smode,_fs,_sv1,_sv2,_sv3,_sv4,_sv5,_sv6,_sv7,_sv8,_sv9,_sv10,_sv11,_sv12,_pdop,
                         _hdop,_vdop,_cs_gsa};
                         
char *_GPRMC_fields[] = {_time,_status,_latitude,_n_s,_longitude,_e_w,_speed,_cog,_date,_mv,_mve,_cs_rmc};


// Arrays to hold the labels for the fields present in each NMEA message type
String _GPGGA_labels[] = {"Current Time","Latitude","N/S Indicator","Longitude","E/W Indicator","Position Fix Status",
                         "Num. Satellites Used","Horizontal Dilution of Precision","MSL Altitude","MSL Altitude Units",
                         "Geoid Separation","Geoid Separation Units","Age of Diff Corretions",
                         "Diff Reference Station ID","GPGGA Checksum"};

String _GPGSA_labels[] = {"Smode","Position Fix Status","SV  1","SV  2","SV  3","SV  4","SV  5","SV  6","SV  7","SV  8",
                         "SV  9","SV  10","SV  11","SV  12","PDoP","HDoP","VDoP","GPGSA Checksum"};

String _GPRMC_labels[] = {"Current Time","Status","Latitude","N/S Indicator","Longitude","E/W Indicator",
                         "Speed over Ground","Course over Ground","Date","Magnetic Variantion, N.A.",
                         "Magnetic Varation E/W, N.A.","GPRMC Checksum"};

// Arrays to hold the values of the indexes of the delimiters in each message type;
int _GPGGA_indexes[15];
int _GPGSA_indexes[18];
int _GPRMC_indexes[12];

// PRIVATE FUNCTIONS IN LIBRARY

//
// Function: parse_delimiter_indexes()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes will be stored in)
// Return: int num_delim (the number of delimters, both "," and "*" found in the message)
//
// Description: Use this function to save the indexes of the NMEA field delimiters into a passed
//   array and also to return the number of delimters in the message
//
int parse_delimiter_indexes(String message_string, int index_array[]) {
  int index = 0;                                  // index is number of delimter index in string
  int last_index = 0;                             // last index is previous index number
  int num_delim = 0;                              // counter for number of delimiters parsed
  while(1) {                                      // loop until index == -1 which breaks while(1)
    last_index = index;                           // set last_index = index, prior to updating index
    index = message_string.indexOf(",",index+1);  // index = indexOf next "," in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop    
  }
  index = last_index;                             // update index with the last index (now index != -1)
  while(1) {                                      // loop until index == -1 which breaks while(1)
    index = message_string.indexOf("*",index+1);  // index = indexOf next "*" in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array                                  
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop
  }
  return num_delim;                               // return the number of delimiters found
}

//
// Function: parse_field_substrings()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes are stored in)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
//        String fields[] (the array of strings which contains the fields for a given NMEA message type)
// Return: Void
//
// Description: Use this function to parse the substrings from the NMEA message and store them in
//  the appropriate fields
//
void parse_field_substrings(String message_string, int index_array[], int num_fields, char *fields[]) {
  int i = 0;
  String tempString;
  int buff_length;  
  for(i; i < num_fields-1; i++) {
    // the current field = the substring located between the next two field delimiter indexes
    tempString = message_string.substring(index_array[i]+1, index_array[i+1]);
    buff_length = index_array[i+1] - (index_array[i]);
    tempString.toCharArray(fields[i],buff_length);
  }
  // the final field = the substring located from the last delimiter until the end of the message
  tempString = message_string.substring(index_array[i]+1);
  buff_length = message_string.length() - (index_array[i]);
  tempString.toCharArray(fields[i],buff_length);
}

//
// Function: print_field_substrings()
//
// Input: String fields[] (the array of strings which contains the fields for a given NMEA message type)
//        String labels[] (the array of strings which label the fields for a given NMEA message tyep)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
// Return: Void
//
// Description: Use this function to print the contents of a messages fields, along with the labels
//  for those fields
//
void print_field_substrings(char *fields[], String labels[], int num_fields) {
  for (int i; i < num_fields; i++) {
    Serial.print("Field ");
    Serial.print(labels[i]);
    Serial.print(" = ");
    Serial.println(fields[i]);
  }
}

//
// Function: parse_NMEA_message()
//
// Input: String message (the NMEA message to parse for delimiters and data substrings)
// Return: int (something to do with checksum)
//
// Description: Use this function take a NMEA style message String and parse it as a way
//  to populate the various String data fields that the GPS measures
//
int parse_NMEA_message(String message) {
  int isvalid = 0;
  if(verify_checksum(message) == 1) {
      isvalid = 1;
  }
  else {
      return isvalid;
  }
  if(message.startsWith("$GPGGA")) {
    Serial.print(message);
    Serial.println(" is a $GPGGA NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPGGA_indexes);
    parse_field_substrings(message, _GPGGA_indexes, num_delimiters, _GPGGA_fields);
    print_field_substrings(_GPGGA_fields, _GPGGA_labels, num_delimiters);
  }
  else  if(message.startsWith("$GPGSA")) {
    Serial.print(message);
    Serial.println(" is a $GPGSA NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPGSA_indexes);
    parse_field_substrings(message, _GPGSA_indexes, num_delimiters, _GPGSA_fields);
    print_field_substrings(_GPGSA_fields, _GPGSA_labels, num_delimiters);
  }
  else  if(message.startsWith("$GPRMC")) {
    Serial.print(message);
    Serial.println(" is a $GPRMC NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPRMC_indexes);
    parse_field_substrings(message, _GPRMC_indexes, num_delimiters, _GPRMC_fields);
    print_field_substrings(_GPRMC_fields, _GPRMC_labels, num_delimiters);
  }
  return isvalid;
}

//
// Function: verify_checksum()
//
// Input: String message (the NMEA message we want to vefigy the checksum of)
//        
// Return: int (1 = valid checksum, 0 = invalid checksum)
//
// Description: Use this function vefiy that the checksum value in the message, matches
//   the calculated checksum value for  the characters recieved
//
int verify_checksum(String message) {
  char checksum = 0;                               // char to hold value of calucated checksum
  char char_checksum[3];                           // 3 character buffer to hold simple checksum substring
  char simple_string[100];                         // 100 character buffer to hold simple string message
  int simple_string_length;                        // length of char array string version of message
  int converted_checksum;                          // Integer, value of checksum substring
  String String_checksum;                          // String object to contain checksum substring
  
  simple_string_length = message.indexOf("*");                  // Calculate index of checksum substring
  String_checksum = message.substring(simple_string_length+1);  // Parse NMEA message for checksum substring
  
  message.toCharArray(simple_string,simple_string_length+1);    // Convert message to char array, store in simple_string
  
  for(int i = 1; i <  simple_string_length; i++) {  // For each character in simple string between '$' and '*'
    checksum ^= simple_string[i];                   // Calculate checksum as checksum = checksum XOR string[i]
  }
  String_checksum.toCharArray(char_checksum, 3);    // Convert the message checksum String to char array
  // Calculate value of message substring char array, by converting hex char to integer value
  converted_checksum = 16 * convert_from_hex(char_checksum[0]) + convert_from_hex(char_checksum[1]);
  if(checksum == converted_checksum) {              // If our calcuated checksum == given converted checksum
    return 1;                                       // Return 1 (this means checksum is verfied successfully)
  }
  else {                                            
    return 0;                                       // Else return 0 (this means checksum is not valid)
  }
}

//
// Function: convert_from_hex()
//
// Input: char c (character we wish to convert from hex)
// Return: int (integer value of c as if it were HEX, not char)
//
// Description: Use this function to take a char between 0-9 or a-f or A-F and 
//   convert it to the integer equivalent of that hex value
int convert_from_hex(char c) 
{
  if (c >= 'A' && c <= 'F') {
    return c - 'A' + 10;
  }
  else if (c >= 'a' && c <= 'f') {
    return c - 'a' + 10;
  }
  else {
    return c - '0';
  }
}

//
// Function: read_gps_line()
//
// Input: char char_buff[] (character array buffer we wish to read GPS string into)
// Return: int isvalid (isvalid = 0 if invalid string returned, not $,,,,*CS\n\r,
//   isvalid = 1 if a valid NMEA string is read into char_buff[])
//
// Description: Use this function read Serial port for NMEA strings and store them as
//   char array char_buff[] for further parsing. Valids input string to ensure it is of format
//   $,,...data...,,*CS\n\r and returns a flag to indicate if string was valid
//   TO DO: Add timeout function so that if data stops being input function does not lockup!
//
int read_gps_line(char char_buff[]) {
  int isvalid = 0;                                  // Flag to determine if we got a valid input 1 = valid, -1 = timeout
  int i = 0;                                        // char_buff index counter
  unsigned long timeout = 0;                                  // Variable to store the timeout time
  char_buff[0] = Serial.read();                     // Read serial into char_buff index 0
  if (char_buff[0] == '$') {                        // If char_buff[0] == '$' we're starting a valid string
    i++;                                            // Increment char_buff index counter by 1
    char_buff[i] = Serial.read();                   // Read Serial again for next char
    timeout = millis();                             // Initialize the timeout counter
    while(char_buff[i] != 'X' & i < 85){            // While char != end of line, or have a full buffer, continue
	if(char_buff[i] != -1) {                    // If char != -1 (Serial.read() == -1 indicates no data available
          i++;                                      // Increment index buffer
        }                                           // This ensure Arduino does not go faster than GPS transmission
	char_buff[i] = Serial.read();               // Read in next character in char_buff[i]
        if(millis() - timeout > 150) {              // If millis() - timeout > 150, this means more than 150mSec      
          isvalid = -1;                             // have passed since '$' was since on serial line, so GPS unit is
          return isvalid;                           // dead or dying, return isvalid == -1 (timeout)
        }                                           
    }
    if(char_buff[i] == 'X') {                       // If the last character we read was the end of line characer
      isvalid = 1;                                  // Then the data is valid. Else we just ran out of buffer
    }                                               // So valid stays equal to 0
    char_buff[i] = 0; //make end to string          // Make last character of char_buff[i] = NULL
  }                                                 // So we have a null terminated string for conversion
                                                    // to string object or for serial priting
  return isvalid;                                   // Return status (valid == 0 or valid == 1)
}

/*_altref[12],_cog[12],_cs_gga[12],_cs_gsa[12],_cs_rmc[12],_date[12],_diffage[12],_diffstation[12],
       _e_w[12],_fs[12],_hdop[12],_latitude[12],_longitude[12],_mode[12],_msl[12],_mv[12],_mve[12],_n_s[12],
       _nosv[12],_pdop[12],_smode[12],_speed[12],_status[12],_sv1[12],_sv2[12],_sv3[12],_sv4[12],_sv5[12],
       _sv6[12],_sv7[12],_sv8[12],_sv9[12],_sv10[12],_sv11[12],_sv12[12],_time[12],_umsl[12],_usep[12],_vdop[12];
*/

// PUBLIC "Get" methods

//   All "get_variable_String" functions have same form
//
// Function: get_"variable name"_String()
//
// Input: void
// Return: String object with contents of "variable name" character array in String
//
// Description: Use this function to get the raw contents of the character array string
//   for the given "variable name" which labels that given GPS data field. The data is
//   returned as an arduino String object because returning a pointer to the character array
//   does not protect the private status of that data string
//
String get_altref_string() {
  String tempString = String(_altref);
  return tempString;
}

String get_cog_string() {
  String tempString = String(_cog);
  return tempString;
}

String get_cs_gga_string() {
  String tempString = String(_cs_gga);
  return tempString;
}

String get_cs_gsa_string() {
  String tempString = String(_cs_gsa);
  return tempString;
}

String get_cs_rmc_string() {
  String tempString = String(_cs_rmc);
  return tempString;
}

String get_date_string() {
  String tempString = String(_date);
  return tempString;
}

String get_diffage_string() {
  String tempString = String(_diffage);
  return tempString;
}

String get_diffstation_string() {
  String tempString = String(_diffstation);
  return tempString;
}

String get_e_w_string() {
  String tempString = String(_e_w);
  return tempString;
}

String get_fs_string() {
  String tempString = String(_fs);
  return tempString;
}

String get_hdop_string() {
  String tempString = String(_hdop);
  return tempString;
}

String get_latitude_string() {
  String tempString = String(_latitude);
  return tempString;
}

String get_longitude_string() {
  String tempString = String(_longitude);
  return tempString;
}

String get_mode_string() {
  String tempString = String(_mode);
  return tempString;
}

String get_msl_string() {
  String tempString = String(_msl);
  return tempString;
}

String get_mv_string() {
  String tempString = String(_mv);
  return tempString;
}

String get_mve_string() {
  String tempString = String(_mve);
  return tempString;
}

String get_n_s_string() {
  String tempString = String(_n_s);
  return tempString;
}

String get_nosv_string() {
  String tempString = String(_nosv);
  return tempString;
}

String get_pdop_string() {
  String tempString = String(_pdop);
  return tempString;
}

String get_smode_string() {
  String tempString = String(_smode);
  return tempString;
} 

String get_speed_string() {
  String tempString = String(_speed);
  return tempString;
}

String get_status_string() {
  String tempString = String(_status);
  return tempString;
}

String get_sv1_string() {
  String tempString = String(_sv1);
  return tempString;
}

String get_sv2_string() {
  String tempString = String(_sv2);
  return tempString;
}

String get_sv3_string() {
  String tempString = String(_sv3);
  return tempString;
}

String get_sv4_string() {
  String tempString = String(_sv4);
  return tempString;
}

String get_sv5_string() {
  String tempString = String(_sv5);
  return tempString;
}

String get_sv6_string() {
  String tempString = String(_sv6);
  return tempString;
}

String get_sv7_string() {
  String tempString = String(_sv7);
  return tempString;
}

String get_sv8_string() {
  String tempString = String(_sv8);
  return tempString;
}

String get_sv9_string() {
  String tempString = String(_sv9);
  return tempString;
}

String get_sv10_string() {
  String tempString = String(_sv10);
  return tempString;
}

String get_sv11_string() {
  String tempString = String(_sv11);
  return tempString;
}

String get_sv12_string() {
  String tempString = String(_sv12);
  return tempString;
}

String get_time_string() {
  String tempString = String(_time);
  return tempString;
}

String get_umsl_string() {
  String tempString = String(_umsl);
  return tempString;
}

String get_usep_string() {
  String tempString = String(_usep);
  return tempString;
}

String get_vdop_string() {
  String tempString = String(_vdop);
  return tempString;
}

// Get Data methods (these are for testing right now)
float get_longitude() {
  return atof(_longitude);
}

float get_latitude() {
  return atof(_latitude);
}

// 
// Latitude Object which contains internal private variables, degrees, minutes and seconds
//   To calculate the degrees, minute and seconds of latitude just pass the constructor
//   a String object representing a standard NMEA latitude that is 3,2.4 or 3,2.5 where
//   the string has 3 places for the integer degrees, 2 for the integer minutes and 4 or 5
//   after te decimal point for fractional minutes. By converting the NMEA string to degrees
//   minutes and seconds instead of just fractional degrees no loss of precision occurs due to
//   limited size of the Arduino float data type (which is only precise to 6 or 7 total figures,
//   that is 6 or 7 numbers total on both sides of the decimal point).
//
class Latitude {
  public:
  Latitude(String latitude_string);                 // Constructor, calculates degrees, minutes & seconds
  int get_degrees();                                // returns integer number of degrees of latitude
  int get_minutes();                                // returns integer number of minutes of latitude
  float get_seconds();                              // returns float seconds
  
  private:
  int _degrees;                                     // private variable contains number of degrees
  int _minutes;                                     // private variable contains number of minutes
  float _seconds;                                   // private variable contains number of minutes
};

//
// Latitude Object Constructor
//   Pass the constructor a string representing a NMEA GPS latitude string of standard form
//   3,2.5 where the latitude string had 3 digits for the degrees, 2 for integer number of minutes
//   and 5 places after the decimal for fractional seconds. This string is then parsed and the 
//   variables _degree, _minutes and _seconds are populated with the numeric equivilent
//
Latitude::Latitude(String latitude_string) {
  String temp_string;                               // Temp string to hold parsed sections of latitude_string
  char temp_char_array[10];                         // Temp char array for use as go between String object and atoi() or atof()
  int decimal_index;                                // The index location of the decimal point in the latitude string  
  decimal_index = latitude_string.indexOf(".");
  temp_string = latitude_string.substring(0,decimal_index-2); // First substring = degrees, values up to 2 places before point
  temp_string.toCharArray(temp_char_array,decimal_index-1);   // Convert degrees substring to char array
  _degrees = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _degrees
  
  temp_string = latitude_string.substring(decimal_index-2,decimal_index);  // Second substring = degrees, next two places
  temp_string.toCharArray(temp_char_array,(decimal_index+1)-decimal_index-2); // Convert minutes substring to char array
  _minutes = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _minutes               
  
  temp_string = latitude_string.substring(decimal_index);     // Final substring = fractional minutes
  temp_string.toCharArray(temp_char_array,latitude_string.length()+1-decimal_index); // Convert substring to char array
  _seconds = atof(temp_char_array);                 // Use atof() to convert char array to float, store in _seconds                 
  _seconds = _seconds * 60;                         // Multiply fractional seconds by 60 to convert to fractional seconds
}

//
// Public "Get" Methods for Latitude Object
//
int Latitude::get_degrees() {
  return _degrees;
}

int Latitude::get_minutes() {
  return _minutes;
}

float Latitude::get_seconds() {
  return _seconds;
}

// 
// Longitude Object which contains internal private variables, degrees, minutes and seconds
//   To calculate the degrees, minutes and seconds of longitude just pass the constructor
//   a String object representing a standard NMEA longitude that is 3,2.4 or 3,2.5 where
//   the string has 3 places for the integer degrees, 2 for the integer minutes and 4 or 5
//   after the decimal point for fractional minutes. By converting the NMEA string to degrees
//   minutes and seconds instead of just fractional degrees no loss of precision occurs due to
//   limited size of the Arduino float data type (which is only precise to 6 or 7 total figures,
//   that is 6 or 7 numbers total on both sides of the decimal point).
//
class Longitude {
  public:
  Longitude(String latitude_string);                // Constructor, calculates degrees, minutes & seconds
  int get_degrees();                                // returns integer number of degrees of longitude
  int get_minutes();                                // returns integer number of minutes of longitude
  float get_seconds();                              // returns float seconds of longitude
  
  private:
  int _degrees;                                     // private variable contains number of degrees
  int _minutes;                                     // private variable contains number of minutes
  float _seconds;                                   // private variable contains number of minutes
};

//
// Longitude Object Constructor
//   Pass the constructor a string representing a NMEA GPS longitude string of standard form
//   3,2.5 where the latitude string had 3 digits for the degrees, 2 for integer number of minutes
//   and 5 places after the decimal for fractional seconds. This string is then parsed and the 
//   variables _degree, _minutes and _seconds are populated with the numeric equivilent
//
Longitude::Longitude(String longitude_string) {
  String temp_string;                               // Temp string to hold parsed sections of longitude_string
  char temp_char_array[10];                         // Temp char array for use as go between String object and atoi() or atof()
  int decimal_index;                                // The index location of the decimal point in the latitude string  
  decimal_index = longitude_string.indexOf(".");
  temp_string = longitude_string.substring(0,decimal_index-2); // First substring = degrees, values up to 2 places before point
  temp_string.toCharArray(temp_char_array,decimal_index-1);    // Convert degrees substring to char array
  _degrees = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _degrees
  
  temp_string = longitude_string.substring(decimal_index-2,decimal_index);    // Second substring = degrees, next two places
  temp_string.toCharArray(temp_char_array,(decimal_index+1)-decimal_index-2); // Convert minutes substring to char array
  _minutes = atoi(temp_char_array);                 // Use atoi() to convert char array to integer, store in _minutes               
  
  temp_string = longitude_string.substring(decimal_index);     // Final substring = fractional minutes
  temp_string.toCharArray(temp_char_array,longitude_string.length()+1-decimal_index); // Convert substring to char array
  _seconds = atof(temp_char_array);                 // Use atof() to convert char array to float, store in _seconds                 
  _seconds = _seconds * 60;                         // Multiply fractional seconds by 60 to convert to fractional seconds
}

//
// Public "Get" Methods for Longitude Object
//
int Longitude::get_degrees() {
  return _degrees;
}

int Longitude::get_minutes() {
  return _minutes;
}

float Longitude::get_seconds() {
  return _seconds;
}


// Arduino sketch setup()
void setup() {
  Serial.begin(9600);
  
  Serial.println("Testing simple String based GPS parser with static strings to test");
  
  for(int i = 0; i < 6; i++) {
    if(parse_NMEA_message(test_NMEA[i])) {
      Serial.println("Checksum Valid\n");
    }
    else {
      Serial.println("Checksum Invalid\n");
    }
  }
}

// Arduino sketch main loop()
void loop()
{
  char char_buff[85];
  int test = read_gps_line(char_buff);
  if(test == 1) {
    String test_message = String(char_buff);
    Serial.println(test_message);
    if(parse_NMEA_message(test_message)) {
      Serial.println("Checksum Valid\n");
      Serial.print("test latitude = ");
      Serial.print(get_latitude_string());
      Serial.print("  ");
      Serial.println(get_latitude(),6);
      Serial.print("test longitude = ");
      Serial.print(get_longitude_string());
      Serial.print("  ");
      Serial.println(get_longitude(),6);
      Serial.println("\n");
      
      // Test prints for latitude object
      Latitude test_latitude = Latitude(get_latitude_string());
      Serial.print("Latitude Degrees = ");
      Serial.println(test_latitude.get_degrees());
      Serial.print("Latitude Minutes = ");
      Serial.println(test_latitude.get_minutes());
      Serial.print("Latitude Seconds = ");
      Serial.println(test_latitude.get_seconds(),5);
      Serial.println();
      
      // Test prints for longitude object
      Longitude test_longitude = Longitude(get_longitude_string());
      Serial.print("Longitude Degrees = ");
      Serial.println(test_longitude.get_degrees());
      Serial.print("Longitude Minutes = ");
      Serial.println(test_longitude.get_minutes());
      Serial.print("Longitude Seconds = ");
      Serial.println(test_longitude.get_seconds(),5);
      Serial.println();
    }
    else {
      Serial.println("Checksum Invalid\n");
    }
  }
  else if (test == -1) {
    Serial.println("Got incomplete message and timed out\n");
  }
}
