//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  GPS_library_device_test
//  Written by  :  Lucas Spicer
//  Date        :  06 November 2011
//  Description :  Arduino Example Sketch of the GPS library, with a GPS module
//                 attached to Serial2 (the libraries default port)
//                 For detailed description of the GPS class or its funtions please
//                 see the fully commented GPS.h and GPS.cpp files in the /libraries/GPS folder
//

// Include the Library we wish to use in this example
#include <GPS.h>

// Create an instance of our GPS class (name the GPS object my_gps)
GPS my_gps;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600); 
  
  // Initialize Serial2 (the default port for attaching a GPS device in this library) at
  // whatever baud rate your GPS device communicates at. Mine is 9600 baud, but other common
  // rates are 4800, and 19200. Also, to use a different port than Serial2 or to use a software
  // serial port, please see the GPS.cpp file for information on how to modify the library to do
  // this.
  Serial2.begin(9600);
  
  // A friendly message to begin our test! 
  Serial.println("GPS Library Device Test");
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
   // Call a custom funtion (which is defined below) that will read the GPS
   read_gps();
   
   // Set this delay to anything you want or comment it out. It's here to simulate
   // the amount of time the rest of the code in a full loop() may take
   delay(1000);
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
      Serial.println(test_message);
      
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message, VERBOSE);
      if(test_valid == 1) {
        Serial.println("Checksum Valid\n");
        
        // Test prints for latitude object
        Latitude test_latitude = Latitude(my_gps.get_latitude_string());
        Serial.print("Latitude Degrees = ");
        Serial.println(test_latitude.get_degrees());
        Serial.print("Latitude Minutes = ");
        Serial.println(test_latitude.get_minutes());
        Serial.print("Latitude Seconds = ");
        Serial.println(test_latitude.get_seconds(),5);
        Serial.println();
        
        // Test prints for longitude object
        Longitude test_longitude = Longitude(my_gps.get_longitude_string());
        Serial.print("Longitude Degrees = ");
        Serial.println(test_longitude.get_degrees());
        Serial.print("Longitude Minutes = ");
        Serial.println(test_longitude.get_minutes());
        Serial.print("Longitude Seconds = ");
        Serial.println(test_longitude.get_seconds(),5);
        Serial.println();
        
        // Test prints for Time object
        Time test_time = Time(my_gps.get_time_string());
        Serial.print("Time Hours = ");
        Serial.println(test_time.get_hours());
        Serial.print("Time Minutes = ");
        Serial.println(test_time.get_minutes());
        Serial.print("Time Seconds = ");
        Serial.println(test_time.get_seconds(),5);
        Serial.println(test_time.get_formatted_time());
        Serial.println();
        
        // Test prints for Date object
        Date test_date = Date(my_gps.get_date_string());
        Serial.print("Date Day = ");
        Serial.println(test_date.get_day());
        Serial.print("Date Month = ");
        Serial.println(test_date.get_month());
        Serial.print("Date Year = ");
        Serial.println(test_date.get_year());
        Serial.println(test_date.get_formatted_date());
        Serial.println();
        
        // Test prints for GPS value get functions
        Serial.print("Altitude = ");
        Serial.print(my_gps.get_altitude());
        Serial.println(my_gps.get_altu_string());
        
        Serial.print("Course = ");
        Serial.println(my_gps.get_course());
        
        Serial.print("Number of Satellites Used = ");
        Serial.println(my_gps.get_nosat());
        
        Serial.print("Speed = ");
        Serial.print(my_gps.get_speed(KNOTS));
        Serial.println(" knots");
        
        Serial.print("Speed = ");
        Serial.print(my_gps.get_speed(MPH));
        Serial.println(" mph");
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
        }
        else {
          Serial.println("Data status unknown");
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
        }
        else {
          Serial.println("Fix Mode unknown");
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
        }
        else {
          Serial.println("Fix Status Unknown");
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
        }
        else {
          Serial.println("Mode Indicates a fix");
        }
        
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA or RMC *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}
