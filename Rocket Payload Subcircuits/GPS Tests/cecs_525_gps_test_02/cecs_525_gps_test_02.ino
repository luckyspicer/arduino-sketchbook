//
//  University of Louisville Fall 2011
//  CECS 525 Group 1 Final Project
//
//  Filename    :  cecs_525_gps_test_02
//  Written by  :  Lucas Spicer
//  Date        :  12 December 2011
//  Description :  Arduino Sketch for use of the GPS library, with a GPS module
//                 attached to Serial2 (the libraries default port). And Xbee module
//                 attached to default serial port (Serial). A pushbutton has also
//                 been attached to digital I/O pin 2. When button is pressed the
//                 The latest Latitude, Longitude, Time and Date are transmitted wireless
//                 by the Xbee module.
//

// Include the Library we wish to use in this example
#include <GPS.h>

// Create an instance of our GPS class (name the GPS object my_gps)
GPS my_gps;

// Create variable button which is physcially connected to digital I/O pin 2
int button = 2;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface and Xbee Wireless Module)
  // at 9600 for debugging and receiving message on the computer's serial monitor
  // as well as wireless transmission to M68K
  Serial.begin(9600); 
  
  // Initialize Serial2 (the default port for attaching a GPS device in this library) at
  // whatever baud rate your GPS device communicates at. Ours is 9600 baud, but other common
  // rates are 4800, and 19200
  Serial2.begin(9600);
  
  // A friendly message to begin our test! 
  Serial.println("*** CECS 525 Final Project Wireless & GPS ***");
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
   // Call a custom funtion (which is defined below) that will read the GPS
   read_gps();
   
   // If the button is pressed its digital value goes to zero
   if (digitalRead(button) == 0) {
     // We wait until the button is released to continue
     while(digitalRead(button) == 0);
     // Print the Latitude Values (this data will be sent by Xbee)
     Latitude test_latitude = Latitude(my_gps.get_latitude_string());
     Serial.print("Latitude Degrees = ");
     Serial.println(test_latitude.get_degrees());
     Serial.print("Latitude Minutes = ");
     Serial.println(test_latitude.get_minutes());
     Serial.print("Latitude Seconds = ");
     Serial.println(test_latitude.get_seconds(),5);
     Serial.println();
    
     // Print the Longitude Values (this data will be sent by Xbee)
     Longitude test_longitude = Longitude(my_gps.get_longitude_string());
     Serial.print("Longitude Degrees = ");
     Serial.println(test_longitude.get_degrees());
     Serial.print("Longitude Minutes = ");
     Serial.println(test_longitude.get_minutes());
     Serial.print("Longitude Seconds = ");
     Serial.println(test_longitude.get_seconds(),5);
     Serial.println();
    
     // // Print the Time (this data will be sent by Xbee)
     Time test_time = Time(my_gps.get_time_string());
     Serial.print("UTC Time: ");
     Serial.println(test_time.get_formatted_time());
     Serial.println();
    
     // // Print the Date (this data will be sent by Xbee)
     Date test_date = Date(my_gps.get_date_string());
     Serial.print("Date: ");
     Serial.println(test_date.get_formatted_date());
     Serial.println();
   }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
      
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
    }
  }
}
