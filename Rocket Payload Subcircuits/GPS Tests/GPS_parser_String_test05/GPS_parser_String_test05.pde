/* This is simply a test of the Arduino String object commands a simple way to parse GPS sentence*/

// Face NMEA GPS test strings
String test_NMEA[] = { "$GPRMC,201547.000,A,3014.5527,N,09749.5808,W,0.24,163.05,040109,,*1A",
                        "$GPGGA,201548.000,3014.5529,N,09749.5808,W,1,07,1.5,225.6,M,-22.5,M,18.8,0000*78",
                        "$GPRMC,201548.000,A,3014.5529,N,09749.5808,W,0.17,53.25,040109,,*2B",
                        "$GPGGA,201549.000,3014.5533,N,09749.5812,W,1,07,1.5,223.5,M,-22.5,M,18.8,0000*7C",
                        "$GPGSA,A,3,23,29,07,08,09,18,26,28,,,,,1.94,1.18,1.54*0D",
                        "$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,8,1.01,499.6,M,48.0,M,,0*5B"};

// GPS NMEA Message Fields
String _altref,_cog,_cs_gga,_cs_gsa,_cs_rmc,_date,_diffage,_diffstation,_e_w,_fs,_hdop,_latitude,_longitude,
       _mode,_msl,_mv,_mve,_n_s,_nosv,_pdop,_smode,_speed,_status,_sv1,_sv2,_sv3,_sv4,_sv5,_sv6,_sv7,_sv8,
       _sv9,_sv10,_sv11,_sv12,_time,_umsl,_usep,_vdop;

// Arrays hold the particular fields present in each NMEA message type
String _GPGGA_fields[] = {_time,_latitude,_n_s,_longitude,_e_w,_fs,_nosv,_hdop,_msl,_umsl,_altref,_usep,
                             _diffage,_diffstation,_cs_gga};

String _GPGSA_fields[] = {_smode,_fs,_sv1,_sv2,_sv3,_sv4,_sv5,_sv6,_sv7,_sv8,_sv9,_sv10,_sv11,_sv12,_pdop,
                         _hdop,_vdop,_cs_gsa};
                         
String _GPRMC_fields[] = {_time,_status,_latitude,_n_s,_longitude,_e_w,_speed,_cog,_mv,_mve,_date,_mode,_cs_rmc};


// Arrays to hold the labels for the fields present in each NMEA message type
String _GPGGA_labels[] = {"Current Time","Latitude","N/S Indicator","Longitude","E/W Indicator","Position Fix Status",
                         "Num. Satellites Used","Horizontal Dilution of Precision","MSL Altitude","MSL Altitude Units",
                         "Geoid Separation","Geoid Separation Units","Age of Diff Corretions",
                         "Diff Reference Station ID","GPGGA Checksum"};

String _GPGSA_labels[] = {"Smode","Position Fix Status","SV  1","SV  2","SV  3","SV  4","SV  5","SV  6","SV  7","SV  8",
                         "SV  9","SV  10","SV  11","SV  12","PDoP","HDoP","VDoP","GPGSA Checksum"};

String _GPRMC_labels[] = {"Current Time","Status","Latitude","N/S Indicator","Longitude","E/W Indicator",
                         "Speed over Ground","Course over Ground","Magnetic Variantion, N.A."
                         "Magnetic Varation E/W, N.A.","Date","Mode","GPRMC Checksum"};

// Arrays to hold the values of the indexes of the delimiters in each message type;
int _GPGGA_indexes[15];
int _GPGSA_indexes[18];
int _GPRMC_indexes[13];

//
// Function: parse_delimiter_indexes()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes will be stored in)
// Return: int num_delim (the number of delimters, both "," and "*" found in the message)
//
// Description: Use this function to save the indexes of the NMEA field delimiters into a passed
//   array and also to return the number of delimters in the message
//
int parse_delimiter_indexes(String message_string, int index_array[]) {
  int index = 0;                                  // index is number of delimter index in string
  int last_index = 0;                             // last index is previous index number
  int num_delim = 0;                              // counter for number of delimiters parsed
  while(1) {                                      // loop until index == -1 which breaks while(1)
    last_index = index;                           // set last_index = index, prior to updating index
    index = message_string.indexOf(",",index+1);  // index = indexOf next "," in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop    
  }
  index = last_index;                             // update index with the last index (now index != -1)
  while(1) {                                      // loop until index == -1 which breaks while(1)
    index = message_string.indexOf("*",index+1);  // index = indexOf next "*" in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array                                  
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop
  }
  return num_delim;                               // return the number of delimiters found
}

//
// Function: parse_field_substrings()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes are stored in)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
//        String fields[] (the array of strings which contains the fields for a given NMEA message type)
// Return: Void
//
// Description: Use this function to parse the substrings from the NMEA message and store them in
//  the appropriate fields
//
void parse_field_substrings(String message_string, int index_array[], int num_fields, String fields[]) {
  int i = 0;                                      
  for(i; i < num_fields-1; i++) {
    // the current field = the substring located between the next two field delimiter indexes
    fields[i] = message_string.substring(index_array[i]+1, index_array[i+1]);
  }
  // the final field = the substring located from the last delimiter until the end of the message
  fields[i] = message_string.substring(index_array[i]+1);
}

//
// Function: print_field_substrings()
//
// Input: String fields[] (the array of strings which contains the fields for a given NMEA message type)
//        String labels[] (the array of strings which label the fields for a given NMEA message tyep)
//        int num_fields (the num of fields, this is the output of parse_delimiter_indexes() function)
// Return: Void
//
// Description: Use this function to print the contents of a messages fields, along with the labels
//  for those fields
//
void print_field_substrings(String fields[], String labels[], int num_fields) {
  for (int i; i < num_fields; i++) {
    Serial.print("Field ");
    Serial.print(labels[i]);
    Serial.print(" = ");
    Serial.println(fields[i]);
  }
  Serial.println();
}

//
// Function: parse_NMEA_message()
//
// Input: String message (the NMEA message to parse for delimiters and data substrings)
// Return: int (something to do with checksum)
//
// Description: Use this function take a NMEA style message String and parse it as a way
//  to populate the various String data fields that the GPS measures
//
void parse_NMEA_message(String message) {
  if(message.startsWith("$GPGGA")) {
    Serial.print(message);
    Serial.println(" is a $GPGGA NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPGGA_indexes);
    parse_field_substrings(message, _GPGGA_indexes, num_delimiters, _GPGGA_fields);
    print_field_substrings(_GPGGA_fields, _GPGGA_labels, num_delimiters);
    
    Serial.print("Verify Checksum = ");
    Serial.println(verify_checksum(message, _GPGGA_indexes, num_delimiters, _GPGGA_fields));
    Serial.println();
  }
  else  if(message.startsWith("$GPGSA")) {
    Serial.print(message);
    Serial.println(" is a $GPGSA NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPGSA_indexes);
    parse_field_substrings(message, _GPGSA_indexes, num_delimiters, _GPGSA_fields);
    print_field_substrings(_GPGSA_fields, _GPGSA_labels, num_delimiters);
    
    Serial.print("Verify Checksum = ");
    Serial.println(verify_checksum(message, _GPGSA_indexes, num_delimiters, _GPGSA_fields));
    Serial.println();
  }
  else  if(message.startsWith("$GPRMC")) {
    Serial.print(message);
    Serial.println(" is a $GPRMC NMEA message");
    int num_delimiters = parse_delimiter_indexes(message, _GPRMC_indexes);
    parse_field_substrings(message, _GPRMC_indexes, num_delimiters, _GPRMC_fields);
    print_field_substrings(_GPRMC_fields, _GPRMC_labels, num_delimiters);
    
    Serial.print("Verify Checksum = ");
    Serial.println(verify_checksum(message, _GPRMC_indexes, num_delimiters, _GPRMC_fields));
    Serial.println();
  }
}

int verify_checksum(String message, int index_array[], int num_fields, String fields[]) {
  char simple_string[100];
  int simple_string_length = index_array[num_fields-1]+1;
  char checksum = 0;
  char char_checksum[3];
  int converted_checksum;
  message.toCharArray(simple_string,  simple_string_length);
  
  for(int i = 1; i <  simple_string_length-1; i++) {
    checksum ^= simple_string[i];
  }
  fields[num_fields-1].toCharArray(char_checksum, 3);
  converted_checksum = 16 * convert_from_hex(char_checksum[0]) + convert_from_hex(char_checksum[1]);
  if(checksum == converted_checksum) {
    return 1;
  }
  else {
    return 0;
  }
}

int convert_from_hex(char c) 
{
  if (c >= 'A' && c <= 'F') {
    return c - 'A' + 10;
  }
  else if (c >= 'a' && c <= 'f') {
    return c - 'a' + 10;
  }
  else {
    return c - '0';
  }
}

void setup()
{
  Serial.begin(19200);
  
  Serial.println("Testing simple String based GPS parser with static strings to test");
  
  for(int i = 0; i < 6; i++) {
    parse_NMEA_message(test_NMEA[i]);
  }
}

void loop()
{
}
