//
//  University of Louisville USLI, 2011-2012
//
//  Filename    :  GPS_library_device_test
//  Written by  :  Lucas Spicer
//  Date        :  06 November 2011
//  Description :  Arduino Example Sketch of the GPS library, with a GPS module
//                 attached to Serial2 (the libraries default port)
//                 For detailed description of the GPS class or its funtions please
//                 see the fully commented GPS.h and GPS.cpp files in the /libraries/GPS folder
//

// Include the Library we wish to use in this example
#include <GPS.h>

// Create an instance of our GPS class (name the GPS object my_gps)
GPS my_gps;
int button = 2;

// All Arduino Sketches require both a setup() and loop() funtions, this is the setup()
void setup() 
{ 
  // Initialize the Serial port (the one connected to the USB interface) at 9600 for debugging and
  // receiving message on the computer's serial monitor
  Serial.begin(9600); 
  
  // Initialize Serial2 (the default port for attaching a GPS device in this library) at
  // whatever baud rate your GPS device communicates at. Mine is 9600 baud, but other common
  // rates are 4800, and 19200. Also, to use a different port than Serial2 or to use a software
  // serial port, please see the GPS.cpp file for information on how to modify the library to do
  // this.
  Serial2.begin(9600);
  Serial3.begin(9600);
  
  // A friendly message to begin our test! 
  Serial.println("GPS Library Device Test");
  Serial3.println("GPS Library Device Test");
} 

// All Arduino Sketches require both a setup() and loop() funtions, this is the loop()
void loop() {
   // Call a custom funtion (which is defined below) that will read the GPS
   read_gps();
   
   // Set this delay to anything you want or comment it out. It's hear to simulate
   // the amount of time the rest of the code in a full loop() may take
   //delay(250);
   if (digitalRead(button) == 0) {
     while(digitalRead(button) == 0);
     // Test prints for latitude object
     Latitude test_latitude = Latitude(my_gps.get_latitude_string());
     Serial.print("Latitude Degrees = ");
     Serial.println(test_latitude.get_degrees());
     Serial.print("Latitude Minutes = ");
     Serial.println(test_latitude.get_minutes());
     Serial.print("Latitude Seconds = ");
     Serial.println(test_latitude.get_seconds(),5);
     Serial.println();
    
     // Test prints for longitude object
     Longitude test_longitude = Longitude(my_gps.get_longitude_string());
     Serial.print("Longitude Degrees = ");
     Serial.println(test_longitude.get_degrees());
     Serial.print("Longitude Minutes = ");
     Serial.println(test_longitude.get_minutes());
     Serial.print("Longitude Seconds = ");
     Serial.println(test_longitude.get_seconds(),5);
     Serial.println();
    
     // Test prints for Time object
     Time test_time = Time(my_gps.get_time_string());
     Serial.print("UTC Time: ");
     Serial.println(test_time.get_formatted_time());
     Serial.println();
    
     // Test prints for Date object
     Date test_date = Date(my_gps.get_date_string());
     Serial.print("Date: ");
     Serial.println(test_date.get_formatted_date());
     Serial.println();
     
     // Test prints for latitude object
     Serial3.print("Latitude Degrees = ");
     Serial3.println(test_latitude.get_degrees());
     Serial3.print("Latitude Minutes = ");
     Serial3.println(test_latitude.get_minutes());
     Serial3.print("Latitude Seconds = ");
     Serial3.println(test_latitude.get_seconds(),5);
     Serial3.println();
    
     // Test prints for longitude object
     Serial3.print("Longitude Degrees = ");
     Serial3.println(test_longitude.get_degrees());
     Serial3.print("Longitude Minutes = ");
     Serial3.println(test_longitude.get_minutes());
     Serial3.print("Longitude Seconds = ");
     Serial3.println(test_longitude.get_seconds(),5);
     Serial3.println();
    
     // Test prints for Time object
     Serial3.print("UTC Time: ");
     Serial3.println(test_time.get_formatted_time());
     Serial3.println();
    
     // Test prints for Date object
     Serial3.print("Date: ");
     Serial3.println(test_date.get_formatted_date());
     Serial3.println();
   }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
  // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
      
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
    }
  }
}
