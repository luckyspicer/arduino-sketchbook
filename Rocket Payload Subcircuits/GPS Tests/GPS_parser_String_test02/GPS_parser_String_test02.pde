/* This is simply a test of the Arduino String object commands a simple way to parse GPS sentence*/

String string1 = "$GPRMC,201547.000,A,3014.5527,N,09749.5808,W,0.24,163.05,040109,,*1A";
String string2 = "$GPGGA,201548.000,3014.5529,N,09749.5808,W,1,07,1.5,225.6,M,-22.5,M,18.8,0000*78";
String string3 = "$GPRMC,201548.000,A,3014.5529,N,09749.5808,W,0.17,53.25,040109,,*2B";
String string4 = "$GPGGA,201549.000,3014.5533,N,09749.5812,W,1,07,1.5,223.5,M,-22.5,M,18.8,0000*7C";

// GPS NMEA Message Fields
String _altref,_cog,_cs_gga,_cs_gsa,_cs_rmc,_date,_diffage,_diffstation,_e_w,_fs,_hdop,_latitude,_longitude,
       _mode,_msl,_mv,_mve,_n_s,_nosv,_pdop,_smode,_speed,_status,_sv1,_sv2,_sv3,_sv4,_sv5,_sv6,_sv7,_sv8,
       _sv9,_sv10,_sv11,_sv12,_time,_umsl,_usep,_vdop;

// Arrays hold the particular fields present in each NMEA message type
String _GPGGA_fields[] = {_time,_latitude,_n_s,_longitude,_e_w,_fs,_nosv,_hdop,_msl,_umsl,_altref,_usep,
                             _diffage,_diffstation,_cs_gga};

String _GPGSA_fields[] = {_smode,_fs,_sv1,_sv2,_sv3,_sv4,_sv5,_sv6,_sv7,_sv8,_sv9,_sv10,_sv11,_sv12,_pdop,
                         _hdop,_vdop,_cs_gsa};
                         
String _GPRMC_fields[] = {_time,_status,_latitude,_n_s,_longitude,_e_w,_speed,_cog,_mv,_mve,_date,_mode,_cs_rmc};


// Arrays to hold the labels for the fields present in each NMEA message type
String _GPGGA_labels[] = {"Current Time","Latitude","N/S Indicator","Longitude","E/W Indicator","Position Fix Status",
                         "Num. Satellites Used","Horizontal Dilution of Precision","MSL Altitude","MSL Altitude Units",
                         "Geoid Separation","Geoid Separation Units","Age of Diff Corretions",
                         "Diff Reference Station ID","GPGGA Checksum"};

String _GPGSA_labels[] = {"Smode","Position Fix Status","SV  1","SV  2","SV  3","SV  4","SV  5","SV  6","SV  7","SV  8",
                         "SV  9","SV  10","SV  11","SV  12","PDoP","HDoP","VDoP","GPGSA Checksum"};

String _GPRMC_labels[] = {"Current Time","Status","Latitude","N/S Indicator","Longitude","E/W Indicator",
                         "Speed over Ground","Course over Ground","Magnetic Variantion, N.A."
                         "Magnetic Varation E/W, N.A.","Date","Mode","GPRMC Checksum"};

// Arrays to hold the values of the indexes of the delimiters in each message type;
int _GPGGA_indexes[15];
int _GPGSA_indexes[18];
int _GPRMC_indexes[13];

//
// Function: parse_delimiter_indexes()
//
// Input: String message_string (the NMEA sentence you wish to parse, as an Arduino String object)
//        int index_array[] (the int array the delimiter indexes will be stored in)
// Return: int num_delim (the number of delimters, both "," and "*" found in the message)
//
// Description: Use this function to save the indexes of the NMEA field delimiters into a passed
//   array and also to return the number of delimters in the message
//
int parse_delimiter_indexes(String message_string, int index_array[]) {
  int index = 0;                                  // index is number of delimter index in string
  int last_index = 0;                             // last index is previous index number
  int num_delim = 0;                              // counter for number of delimiters parsed
  while(1) {                                      // loop until index == -1 which breaks while(1)
    last_index = index;                           // set last_index = index, prior to updating index
    index = message_string.indexOf(",",index+1);  // index = indexOf next "," in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop    
  }
  index = last_index;                             // update index with the last index (now index != -1)
  while(1) {                                      // loop until index == -1 which breaks while(1)
    index = message_string.indexOf("*",index+1);  // index = indexOf next "*" in the message string
    index_array[num_delim] = index;               // add value of delimiter index to index_array                                  
    if(index == -1) {                             // if index == -1, no "," delimiter found
      break;                                      //   therefore break from this while(1) loop
    }
    num_delim++;                                  // increment the num of delims by 1 each loop
  }
  return num_delim;                               // return the number of delimiters found
}

// This little loop is supposed to save the contents of all the field Strings
void parse_field_substrings(String message_string, int index_array[], int num_fields, String fields[]) {
  int i = 0;
  for(i; i < num_fields-1; i++) {
    Serial.print("Save the substring from: ");
    Serial.print(index_array[i]+1);
    Serial.print(" to ");
    Serial.println(index_array[i+1]);
    fields[i] = message_string.substring(index_array[i]+1, index_array[i+1]);
  }
  fields[i] = message_string.substring(index_array[i]+1);
  Serial.print("Save the substring from: ");
  Serial.print(_GPGGA_indexes[i]);
  Serial.println(" to the end of the string\n");
}

void setup()
{
  Serial.begin(19200);
  
  Serial.println("Testing simple String based GPS parser with static strings to test");
  
  if (string2.startsWith("$GPGGA")){
    Serial.println("\n   We've got a live $GPGGA GPS string here!\n");
  }
  
  // This a test of the parse_delimiter_indexes() function
  int num_delimiters = parse_delimiter_indexes(string2, _GPGGA_indexes);
  Serial.print(num_delimiters);
  Serial.println(" delimiters were found");
  Serial.println();
  
  for (int k = 0; k < num_delimiters; k++) {
    Serial.print("Delimeter at message index: ");
    Serial.println(_GPGGA_indexes[k]);
  }
  Serial.println("\n");
  
  // This is a test of the parse_field_substrings() function
  parse_field_substrings(string2, _GPGGA_indexes, num_delimiters, _GPGGA_fields);
  Serial.println("\n");
  
  int j = 0;
  for (j; j < num_delimiters; j++) {
    Serial.print("Field ");
    Serial.print(_GPGGA_labels[j]);
    Serial.print(" = ");
    Serial.println(_GPGGA_fields[j]);
  }
  Serial.println();
}

void loop()
{
}
