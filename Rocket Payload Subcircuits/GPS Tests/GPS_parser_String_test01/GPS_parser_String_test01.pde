/* This is simply a test of the Arduino String object commands a simple way to parse GPS sentence*/

String string1 = "$GPRMC,201547.000,A,3014.5527,N,09749.5808,W,0.24,163.05,040109,,*1A";
String string2 = "$GPGGA,201548.000,3014.5529,N,09749.5808,W,1,07,1.5,225.6,M,-22.5,M,18.8,0000,*78";
String string3 = "$GPRMC,201548.000,A,3014.5529,N,09749.5808,W,0.17,53.25,040109,,*2B";
String string4 = "$GPGGA,201549.000,3014.5533,N,09749.5812,W,1,07,1.5,223.5,M,-22.5,M,18.8,0000,*7C";

// $GPGGA  Fields
//int hours;
//int minutes;
//float seconds;
//float latitude;
String time;
//char N_S;
String N_S;
//float longitude;
String longitude;
//char E_W;
String E_W;
//int pfi;
String pfi;
//int sats_used;
String sats_used;
//float HDOP;
String HDOP;
//float MSL_alt;
String MSL_alt;
//char units1;
String units1;
//int geoid_sep;
String geoid_sep;
//char units2;
String units2;
//int age;
String age;
//int diff;
String diff;
//int checksum;
String checksum;

String GPGGA_fieldArray[] = {time, N_S, longitude, E_W, pfi, sats_used, HDOP, MSL_alt, units1, geoid_sep, units2, age, diff, checksum};
String GPGGA_fieldNames[] = {"UTC Time","Latitude","N/S Indicator","Longitude",
                              "E/W Indicator","Position Fix Indicator","Satellites Used","HDOP",
                              "MSL Altitude","Units","Geoid Separation","Units","Age of Differential Corrections",
                              "Diff Reference Corrections","Checksum"};
int GPGGA_indexes[15];

void setup()
{
  Serial.begin(19200);
  
  Serial.println("Testing simple String based GPS parser with static strings");
  
  if (string2.startsWith("$GPGGA")); {
    Serial.println("\n   We've got a live $GPGGA GPS string here!\n");
  }
  
  // This little loop pulls out all the ',' which are the delimeters 
  int x = 0;
  int num_fields = 0;
  while(1) {
    x = string2.indexOf(",",x+1);
    GPGGA_indexes[num_fields] = x;
    num_fields++;
    if(x == -1) {
      break;
    }
    Serial.print("There was a ',' at index ");
    Serial.println(x);
  }
  Serial.println("No more commas found");
  Serial.println();
  
  // This little loop just prints out all the delimeter indexes we just captured
  int j;
  for(j = 0; j < num_fields; j++) {
    Serial.print("Index at: ");
    Serial.println(GPGGA_indexes[j]);
  }
  Serial.println("\n");
  
  // This little loop is supposed to save the contents of all the field Strings
  for(j = 0; j < num_fields-2; j++) {
    Serial.print("Save the substring from: ");
    Serial.print(GPGGA_indexes[j]+1);
    Serial.print(" to ");
    Serial.println(GPGGA_indexes[j+1]);
    GPGGA_fieldArray[j] = string2.substring(GPGGA_indexes[j]+1, GPGGA_indexes[j+1]);
  }
  GPGGA_fieldArray[j] = string2.substring(GPGGA_indexes[j]+1);
  Serial.print("Save the substring from: ");
  Serial.print(GPGGA_indexes[j]);
  Serial.println(" to the end of the string\n");
  
  // This little loop is supposed to print the saved field Strings
  j = 0;
  for (j; j < num_fields-1; j++) {
    Serial.print("Field ");
    Serial.print(GPGGA_fieldNames[j]);
    Serial.print(" = ");
    Serial.println(GPGGA_fieldArray[j]);
  }
  
}

void loop()
{
  
}
