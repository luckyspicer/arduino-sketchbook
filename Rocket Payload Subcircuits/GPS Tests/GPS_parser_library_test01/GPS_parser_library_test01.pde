#include <GPS.h>

GPS my_gps;
                      


// Arduino sketch setup()
void setup() {
  Serial.begin(9600);
  
  Serial.println("GPS Library!!!");
  delay(2000);
}

// Arduino sketch main loop()
void loop()
{ 
  int test = my_gps.read_gps_line();
  if(test == 1) {
    String test_message = my_gps.get_gps_string();
    Serial.println(test_message);
    if(my_gps.parse_NMEA_message(test_message)) {
      Serial.println("Checksum Valid\n");
      
      /*
      // Test prints for latitude object
      Latitude test_latitude = Latitude(get_latitude_string());
      Serial.print("Latitude Degrees = ");
      Serial.println(test_latitude.get_degrees());
      Serial.print("Latitude Minutes = ");
      Serial.println(test_latitude.get_minutes());
      Serial.print("Latitude Seconds = ");
      Serial.println(test_latitude.get_seconds(),5);
      Serial.println();
      
      // Test prints for longitude object
      Longitude test_longitude = Longitude(get_longitude_string());
      Serial.print("Longitude Degrees = ");
      Serial.println(test_longitude.get_degrees());
      Serial.print("Longitude Minutes = ");
      Serial.println(test_longitude.get_minutes());
      Serial.print("Longitude Seconds = ");
      Serial.println(test_longitude.get_seconds(),5);
      Serial.println();
      
      // Test prints for Time object
      Time test_time = Time(get_time_string());
      Serial.print("Time Hours = ");
      Serial.println(test_time.get_hours());
      Serial.print("Time Minutes = ");
      Serial.println(test_time.get_minutes());
      Serial.print("Time Seconds = ");
      Serial.println(test_time.get_seconds(),5);
      Serial.println(test_time.get_formatted_time());
      Serial.println();
      
      // Test prints for Date object
      Date test_date = Date(get_date_string());
      Serial.print("Date Day = ");
      Serial.println(test_date.get_day());
      Serial.print("Date Month = ");
      Serial.println(test_date.get_month());
      Serial.print("Date Year = ");
      Serial.println(test_date.get_year());
      Serial.println(test_date.get_formatted_date());
      Serial.println();
      */
      
      // Test prints for GPS value get functions
      Serial.print("Altitude = ");
      Serial.print(my_gps.get_altitude());
      Serial.println(my_gps.get_altu_string());
      
      Serial.print("Course = ");
      Serial.println(my_gps.get_course());
      
      Serial.print("Number of Satellites Used = ");
      Serial.println(my_gps.get_nosat());
      
      Serial.print("Speed = ");
      Serial.print(my_gps.get_speed_knots());
      Serial.println(" knots");
      
      Serial.print("Speed = ");
      Serial.print(my_gps.get_speed_mph());
      Serial.println(" mph");
      
      // Test Status Indicator
      if(my_gps.get_data_status() == 0) {
        Serial.println("Data is invalid!");
      }
      else if(my_gps.get_data_status() == 1) {
        Serial.println("Data is valid");
      }
      else {
        Serial.println("Data status unknown");
      }
      
      if(my_gps.get_fix_mode() == 1) {
        Serial.println("No Fix Available");
      }
      else if(my_gps.get_fix_mode() == 2) {
        Serial.println("2D Fix Available");
      }
      else if(my_gps.get_fix_mode() == 3){
        Serial.println("3D Fix Available");
      }
      else {
        Serial.println("Fix Mode unknown");
      }
      
      if(my_gps.get_fix_status() == 0) {
        Serial.println("No Fix Available");
      }
      else if(my_gps.get_fix_status() == 1) {
        Serial.println("Standard GPS Fix Available");
      }
      else {
        Serial.println("Fix Status Unknown");
      }
      
      if(my_gps.get_mode_ind() == 0) {
        Serial.println("Mode Indicates 'No Fix'");
      }
      else {
        Serial.println("Mode Indicates a fix");
      }
      
      Serial.println();
    }
    else {
      Serial.println("Checksum Invalid\n");
    }
  }
  else if (test == -1) {
    Serial.println("Got incomplete message and timed out\n");
  }
}
