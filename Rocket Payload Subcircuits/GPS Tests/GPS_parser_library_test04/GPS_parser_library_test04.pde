#include <GPS.h>
#include <latitude.h>
#include <longitude.h>
#include <time.h>
#include <date.h>

GPS my_gps;

// Face NMEA GPS test strings
String test_NMEA[] = {  "$GPRMC,201547.000,A,3014.5527,N,09749.5808,W,0.24,163.05,040109,,*1A",
                        "$GPGGA,201548.000,3014.5529,N,09749.5808,W,1,07,1.5,225.6,M,-22.5,M,18.8,0000*78",
                        "$GPRMC,201548.000,A,3014.5529,N,09749.5808,W,0.17,53.25,040109,,*2B",
                        "$GPGGA,201549.000,3014.5533,N,09749.5812,W,1,07,1.5,223.5,M,-22.5,M,18.8,0000*7C",
                        "$GPGSA,A,3,23,29,07,08,09,18,26,28,,,,,1.94,1.18,1.54*0D",
                        "$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,8,1.01,499.6,M,48.0,M,,0*5B"};

void setup() 
{ 
  Serial.begin(9600); 
  Serial2.begin(9600);
   
  Serial.println("GPS Library Test");
  
  for(int i = 0; i < 6; i++) {
    if(my_gps.parse_NMEA_message(test_NMEA[i],VERBOSE)) {
      Serial.println("Checksum Valid\n");
    }
    else {
      Serial.println("Checksum Invalid\n");
    }
  }
} 

void loop() {
   read_gps();
}

void read_gps() {
  int test = my_gps.read_gps_line();
  if(test == 1) {
    String test_message = my_gps.get_gps_line();
    Serial.println(test_message);
    if(my_gps.parse_NMEA_message(test_message, VERBOSE)) {
      Serial.println("Checksum Valid\n");
      
      // Test prints for latitude object
      Latitude test_latitude = Latitude(my_gps.get_latitude_string());
      Serial.print("Latitude Degrees = ");
      Serial.println(test_latitude.get_degrees());
      Serial.print("Latitude Minutes = ");
      Serial.println(test_latitude.get_minutes());
      Serial.print("Latitude Seconds = ");
      Serial.println(test_latitude.get_seconds(),5);
      Serial.println();
      
      // Test prints for longitude object
      Longitude test_longitude = Longitude(my_gps.get_longitude_string());
      Serial.print("Longitude Degrees = ");
      Serial.println(test_longitude.get_degrees());
      Serial.print("Longitude Minutes = ");
      Serial.println(test_longitude.get_minutes());
      Serial.print("Longitude Seconds = ");
      Serial.println(test_longitude.get_seconds(),5);
      Serial.println();
      
      // Test prints for Time object
      Time test_time = Time(my_gps.get_time_string());
      Serial.print("Time Hours = ");
      Serial.println(test_time.get_hours());
      Serial.print("Time Minutes = ");
      Serial.println(test_time.get_minutes());
      Serial.print("Time Seconds = ");
      Serial.println(test_time.get_seconds(),5);
      Serial.println(test_time.get_formatted_time());
      Serial.println();
      
      // Test prints for Date object
      Date test_date = Date(my_gps.get_date_string());
      Serial.print("Date Day = ");
      Serial.println(test_date.get_day());
      Serial.print("Date Month = ");
      Serial.println(test_date.get_month());
      Serial.print("Date Year = ");
      Serial.println(test_date.get_year());
      Serial.println(test_date.get_formatted_date());
      Serial.println();
      
      // Test prints for GPS value get functions
      Serial.print("Altitude = ");
      Serial.print(my_gps.get_altitude());
      Serial.println(my_gps.get_altu_string());
      
      Serial.print("Course = ");
      Serial.println(my_gps.get_course());
      
      Serial.print("Number of Satellites Used = ");
      Serial.println(my_gps.get_nosat());
      
      Serial.print("Speed = ");
      Serial.print(my_gps.get_speed_knots());
      Serial.println(" knots");
      
      Serial.print("Speed = ");
      Serial.print(my_gps.get_speed_mph());
      Serial.println(" mph");
      
      // Test Status Indicator
      if(my_gps.get_data_status() == 0) {
        Serial.println("Data is invalid!");
      }
      else if(my_gps.get_data_status() == 1) {
        Serial.println("Data is valid");
      }
      else {
        Serial.println("Data status unknown");
      }
      
      if(my_gps.get_fix_mode() == 1) {
        Serial.println("No Fix Available");
      }
      else if(my_gps.get_fix_mode() == 2) {
        Serial.println("2D Fix Available");
      }
      else if(my_gps.get_fix_mode() == 3){
        Serial.println("3D Fix Available");
      }
      else {
        Serial.println("Fix Mode unknown");
      }
      
      if(my_gps.get_fix_status() == 0) {
        Serial.println("No Fix Available");
      }
      else if(my_gps.get_fix_status() == 1) {
        Serial.println("Standard GPS Fix Available");
      }
      else {
        Serial.println("Fix Status Unknown");
      }
      
      if(my_gps.get_mode_ind() == 0) {
        Serial.println("Mode Indicates 'No Fix'");
      }
      else {
        Serial.println("Mode Indicates a fix");
      }
      
      Serial.println();
    }
    else {
      Serial.println("Checksum Invalid\n");
    }
  }
  else if (test == -1) {
    Serial.println("Got incomplete message and timed out\n");
  }
}
