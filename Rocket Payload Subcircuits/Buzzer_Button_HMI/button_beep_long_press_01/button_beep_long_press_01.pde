#include <Music.h>

int button = 9;
int buzzer = 10;

Music speaker = 10;
int guitarTune[] = {200, B3,q,  d4,q,  g4,q,  B3,e,
                     c4,q,  d4,q,  g4,q,  c4,e,
                     d4,q,  d4,e,  g4,q,  d4,q,  d4,q,  d4,e,  g4,q,  d4,q,
                     g4,q,  d4,q,  g4,q,  g4,e,  fs4,q,  d4,q,  g4,q,  fs4,e,
                     a4,q,  d4,q,  g4,q,  a4,e,
                     b4,q,  d4,q,  g4,q,  b4,q,
                   
                     d5,q,  g4,q,  b4,q,  d5,e,
                     c5,q,  g4,q,  b4,q,  c5,e,
                     b4,q,  g4,q,  b4,q,  b4,e,
                     a4,q,  g4,q,  b4,q,  g4,q};
long timer;
long button_stop_time;

void setup() {
  Serial.begin(9600);
  Serial.println("Buzzer/Button Test");
  
}

void loop() {
  if(digitalRead(button) == 0) {
    timer = millis();
    tone(buzzer, 1000000/a5);
    delay(20);
    while(digitalRead(button) == 0) {
      button_stop_time = millis() - timer;
      if(button_stop_time/1000 >= 3) {
        Serial.println("Button Held for 3 Seconds");
        noTone(buzzer);
        speaker.playMusic(guitarTune, sizeof(guitarTune));
        break;
      }
    }
    noTone(buzzer);
    while(digitalRead(button) == 0);
  }
  
}
