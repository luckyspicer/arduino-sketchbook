/* Music Library Arduino Sketchbook Example
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element (or speakers) and your Arduino!
 */
 
#include <Music.h>    //including the library that this code exemplifies seems like a good idea
#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 

Music speaker = 10;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */
int pos = 1500;    // variable to store the servo position 
int button = 9;
int clockwise = 1;
int LED = 53;

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */
int low_tune[] = {200, b4,e,  c5,e,  d5,e,  e5,e};
               

void setup() 
{ 
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  myservo.attach(18);  // attaches the servo on pin 18 to the servo object 
  Serial.println("Servo Test 01");
  myservo.writeMicroseconds(pos);
  delay(3000);
} 

void loop()
{
  if(digitalRead(button) == 0) {
    while(digitalRead(button) == 0);
    if (clockwise == 1) {
      pos += 100;
    }
    else {
      pos -= 100;
    }
    if (pos >= 2400) {
      clockwise = 0;
    }
    if (pos <= 600) {
      clockwise = 1;
    }
    Serial.print("Position = ");
    Serial.println(pos);
    myservo.writeMicroseconds(pos);
    digitalWrite(LED, HIGH);
    delay(1000);
    speaker.playMusic(low_tune, sizeof(low_tune));
    digitalWrite(LED, LOW);
  }
}
