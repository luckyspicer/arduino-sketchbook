/* Music Library Arduino Sketchbook Example
 * Luke Spicer, March 17th, 2009
 * Use this Libary to play music/tones with
 * a piezo element (or speakers) and your Arduino!
 */
 
#include <Music.h>    //including the library that this code exemplifies seems like a good idea

Music speaker = 10;    /* We create the Music object, call an instance of that class
                       * and then I named it speaker and set it to digital I/0 pin 7,
                       * You can name your speaker anything you want and set it to any pin that is free */

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */
int low_tune[] = {120, b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  b4,q+e,  a4,e,  a4,h,
                   b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h,
                   a4,q,  a4,q,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,  g4,q,  a4,q,  b4,e,  c5,e,  b4,q,    a4,q,  g4,q,  a4,q,  d4,h,
                   b4,q,  b4,q,  c5,q,  d5,q,  d5,q,  c5,q,  b4,q,  a4,q,  g4,q,  g4,q,  a4,q,  b4,q,  a4,q+e,  g4,e,  g4,h};

int high_tune[] = {120, b6,q,  b6,q,  c7,q,  d7,q,  d7,q,  c7,q,  b6,q,  a6,q,  g6,q,  g6,q,  a6,q,  b6,q,  b6,q+e,  a6,e,  a6,h,
                   b6,q,  b6,q,  c7,q,  d7,q,  d7,q,  c7,q,  b6,q,  a6,q,  g6,q,  g6,q,  a6,q,  b6,q,  a6,q+e,  g6,e,  g6,h,
                   a6,q,  a6,q,  b6,q,  g6,q,  a6,q,  b6,e,  c7,e,  b6,q,  g6,q,  a6,q,  b6,e,  c7,e,  b6,q,    a6,q,  g6,q,  a6,q,  d6,h,
                   b6,q,  b6,q,  c7,q,  d7,q,  d7,q,  c7,q,  b6,q,  a6,q,  g6,q,  g6,q,  a6,q,  b6,q,  a6,q+e,  g6,e,  g6,h};

void setup()       //If you only want your arduino to play music, no further setup is required
{
}

void loop()
{
  speaker.playMusic(low_tune, sizeof(low_tune));
  
  delay(2000);
  
  speaker.playMusic(high_tune, sizeof(high_tune));      //nameOfYourPiezo.playMusic(nameOfYourSong[], sizeof(nameOfYourSong), this is all you need to make some music, Enjoy!

  delay(2000);
}
