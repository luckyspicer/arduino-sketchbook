/* Subcicuit Test
 * Battery Voltage Monitor
 * 
 * Luke Spicer 9/21/2011
 */ 
 void setup() {
   Serial.begin(9600);
   Serial.println("Battery Voltage Monitor Test\n");
 }
 
 void loop() {
   Serial.print("Battery Voltage: ");
   Serial.print(getBatteryVoltage());
   Serial.println(" V");
   delay(1000);
 }
 
 float getBatteryVoltage() {
   float RTest = 32.71;
   float RUp  = 32.60;
   float VRef = 5.04;    // With external power source
   //float VRef = 4.75;  // With USB power source
   float voltage = analogRead(A8);
   voltage = voltage*VRef/1024;
   voltage = voltage*(RUp + RTest)/RTest;
   voltage = (voltage + 0.589);
   return voltage;
 }
