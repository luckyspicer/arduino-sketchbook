#include <VinMonitor.h>

/* Subcicuit Test
 * Battery Voltage Monitor
 * 
 * Luke Spicer 9/25/2011
 * Test with VinMonitor library
 */ 
 
 // Create Global Objects
 
 // Global VinMonitor object, battery
 int Vin_analogInput_Pin = 0;
 float Vref = 5.04;
 float Rref = 32.60;
 float Rup = 32.71;
 float DiodeDrop = 0.595;
 
 VinMonitor battery(Vin_analogIjnput_Pin, Vref, Rref, Rup, DiodeDrop);
 
 void setup() {
   Serial.begin(9600);
   Serial.println("Battery Voltage Monitor Test\n"); 
 }
 
 void loop() {
   Serial.print("Battery Voltage: ");
   Serial.print(battery.getVin());
   Serial.println(" V");
   delay(1000);
 }
