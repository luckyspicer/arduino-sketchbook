// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 
 
void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(18);  // attaches the servo on pin 18 to the servo object 
  Serial.println("Servo Test 01");
} 
 
 
void loop() 
{ 
  //Serial.println("CCW Full 500 pulse");
  //myservo.writeMicroseconds(500);
  //delay(2000);
  
  //Serial.println("CW Full 2500 pulse");
  //myservo.writeMicroseconds(2500);
  //delay(2000);
  
  Serial.println("0 degrees Stopped 600 pulse");
  myservo.writeMicroseconds(1500);
  delay(2000);
  
  //Serial.println("1260 degrees Stopped 2400 pulse");
  //myservo.writeMicroseconds(2400);
  //delay(2000);
} 
