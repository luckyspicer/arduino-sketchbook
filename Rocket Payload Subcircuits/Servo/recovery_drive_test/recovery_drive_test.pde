// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 1500;    // variable to store the servo position 
int button = 9;
int isRight = 1;

void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(14);  // attaches the servo on pin 18 to the servo object 
  Serial.println("Servo Test 01");
  myservo.writeMicroseconds(pos);
  delay(3000);
} 
 
void loop() {
  if (digitalRead(9) == 0) {
    while(digitalRead(9) == 0);
    if(isRight == 1) {
      Serial.print("Turn Right ");
      pos = 1400;
      isRight = 0;
    }
    else {
      Serial.print("Turn Left ");
      pos = 1600;
      isRight = 1;
    }
    Serial.print("Position = ");
    Serial.println(pos);
    myservo.writeMicroseconds(pos);
    delay(1500);
  }
} 
