#include <EEPROM.h>            // Arduino Libary to allow acces to EEPROM
#include <Servo.h>             // Arduino Servo Motor Library

#include <Altimeter.h>         // Custom Altimeter Library for PerfectFlite StratoLogger
#include <Coordinates.h>       // Custom Library to Generate a Square Coordinate System
#include <GPS.h>               // Custom GPS Parser Library
#include <Music.h>             // Custom Library to play Music (melody) from piezo
#include <Vector.h>            // Custom LIbrary to Calculate Coorinate Vectors
#include <VinMonitor.h>        // Custom Battery Voltage Monitor Library
#define SERVOROTATE (175)

char file_name[10];

// Servo Object and Variables
Servo myservo;                 // Servo object connected to Xbee connector
int servo_pin = 3;             // Digital Pin used to control the servo
int initServoPos = 1500;       // Variable to store the servo position
int servoRight = initServoPos - SERVOROTATE;         // Represents Servo Turned 1/3 rotation to the right
int servoLeft = initServoPos + SERVOROTATE;          // Represents Servo Turned 1/3 rotation to teh left
int isRight = -1;              // Variable tracks if servo is postioned for right turn (or left turn)
int right_LED = 53;

//  Altimeter Object and Variables
Altimeter my_altimeter(SERIAL1);

// GPS and Supporting Objects and Variables
GPS my_gps;                    // GPS object connected to GPS connector
int gps_latitude_degrees = 0;
int gps_latitude_minutes = 0;
float gps_latitude_seconds = 0.0;
int gps_longitude_degrees = 0;
int gps_longitude_minutes = 0;
float gps_longitude_seconds = 0.0;
int gps_time_hours = 0;
int gps_time_minutes = 0;
float gps_time_seconds = 0.0;
float gps_altitude;
float gps_speed;
float gps_course;
int gps_nosat;
int isFixed = 0;               // isFixed = 1, GPS has valid fix data

// Music Object and HMI Variables
Music speaker = Music(10);           // Speaker attached to pin 10, for playing tunes with Music
int buzzer = 10;              // Buzzer used for Arduino Tone() function, also pin 10
int button = 9;               // Pushbutton attached to pin 9
long button_timer;            // Variable to store thearduino_timethe pushbutton is pressed
long button_stop_time;        // Variable to store thearduino_timethe pushbutton is released
long arduino_time;            // Variable to store the global Arduinoarduino_timeach cycle of loop()
int guitarTune[] = {200, B3,q, d4,q, g4,q, B3,e, c4,q, d4,q, g4,q, c4,e};

// Vin Battery Voltage Monitor Object and Variables
int Vin_analog_input_pin = 0; // Vin (Battery) Voltage Monitor connected to analog pin 0
float Vref = 5.01;            // Vref measured value
float Rref = 42.80;           // Rref measured value
float Rup = 42.80;            // Rup measured value
float diode_drop = 0.0;       // Input protection diode voltage drop
VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

// Control System Variables
Coordinates coords = Coordinates();
int home_d_lat;
int home_m_lat;
float home_s_lat;
int home_d_long;
int home_m_long;
float home_s_long;
int isHomeSet;
float spiral_angle = 20;
float mappedX;
float mappedY;
float mapped_course_angle;
float home_angle;
float idealAngle;

void setup() {
  Serial.begin(9600);
  Serial2.begin(9600);         // Initialize Serial2 (for GPS)
  Serial3.begin(9600);         // Initialize the Serial3 for the OpenLogger
  
  //Serial.println("\n*** Recovery System is Go! ***\n");
  Serial.println("\nGO!\n");
  
  byte fileNum = EEPROM.read(25);
  fileNum++;
  EEPROM.write(25,int(fileNum));
  sprintf(file_name,"log%d.txt", fileNum);
  appendFile(file_name);
  Serial3.print("Arduino Time, GPS Time, Battery Voltage,"
                   " Latitude Degrees, Latitude Minutes, Latitude Seconds,"
                   " Longitude Degrees, Longitude Minutes, Longitude Seconds,"
                   " Num Satellites, GPS Altitude, Altimeter Altitude, Course, Speed,"
                   " X, Y, COG, Ideal COG, Home, Turn Right,");
  
  myservo.attach(servo_pin);  // attaches the servo on pin 18 to the servo object
  pinMode(left_LED, OUTPUT);
  pinMode(right_LED, OUTPUT);
  digitalWrite(left_LED, HIGH);
  digitalWrite(right_LED, HIGH); 
  myservo.writeMicroseconds(initServoPos);
  delay(3000);
  myservo.writeMicroseconds(servoRight);
  digitalWrite(left_LED, HIGH);
  digitalWrite(right_LED, LOW);
  delay(3000);
  myservo.writeMicroseconds(initServoPos);
  delay(1500);
  myservo.writeMicroseconds(servoLeft);
  digitalWrite(left_LED, LOW);
  digitalWrite(right_LED, HIGH);
  delay(3000);
  myservo.writeMicroseconds(initServoPos);
  delay(1500);
  // Restore Last Saved GPS Goal Location
  home_d_lat = EEPROM.read(0);
  home_m_lat = EEPROM.read(1);
  float fractional_seconds = float(EEPROM.read(4));
  fractional_seconds = fractional_seconds/10000.0;
  fractional_seconds = fractional_seconds + float(EEPROM.read(3))/100.0;
  fractional_seconds = fractional_seconds + float(EEPROM.read(2));
  home_s_lat = fractional_seconds;
  
  home_d_long = EEPROM.read(5);
  home_m_long = EEPROM.read(6);
  fractional_seconds = float(EEPROM.read(9));
  fractional_seconds = fractional_seconds/10000.0;
  fractional_seconds = fractional_seconds + float(EEPROM.read(8))/100.0;
  fractional_seconds = fractional_seconds + float(EEPROM.read(7));
  home_s_long = fractional_seconds;
}

void loop() {
  arduino_time = millis()/1000;
  // Call a custom funtion (which is defined below) that will read the GPS
  read_gps();
  
  //check_button();                     // Check the HMI Button for Presses and Home location logging
  if(digitalRead(button) == 0) {                    // Check if button pressed
    button_timer = millis();                        // Start the button timer
    if(isHomeSet == 1) {                            // If isHomeSet is already set
      tone(buzzer, 1000000/a5);                     // Play a little three tone pluse to let us know
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/b5);
      delay(250);
      noTone(buzzer);
      delay(250);
      tone(buzzer, 1000000/c6);
      delay(250);
      noTone(buzzer);
      delay(250);
    } else {
      tone(buzzer, 1000000/a5);                       // Start playing the tone
      delay(20);                                      // Delay to debounce pushbutton
      while(digitalRead(button) == 0) {               // While button pressed check to see
        button_stop_time = millis() - button_timer;   // if button has been pressed for 3 seconds
        if(button_stop_time/1000 >= 3) {              // If button has been pressed for 3 seconds
          Serial.println("Button Held for 3 Seconds");// Tell us
          noTone(buzzer);                             // turn off the buzzer
          if(isFixed == 1) {                          // If there is a valid GPS fix
                                                      // Save the home location
            /***** Save Home Location Coordinates *****/
            home_d_lat = gps_latitude_degrees;
            home_m_lat = gps_latitude_minutes;
            home_s_lat = gps_latitude_seconds;
            
            home_d_long = gps_longitude_degrees;
            home_m_long = gps_longitude_minutes;
            home_s_long = gps_longitude_seconds;
   
            isHomeSet = 1;
            
            // Save Latitude to EEPROM
            EEPROM.write(0, home_d_lat);
            EEPROM.write(1, home_m_lat);
            
            int seconds_int = int(home_s_lat);
            Serial.print("Seconds Integer Part = ");
            Serial.println(seconds_int);
            EEPROM.write(2, seconds_int);
            
            float seconds_fraction = home_s_lat - seconds_int;
            Serial.print("Second Fractional Part = ");
            Serial.println(seconds_fraction, 5);
            seconds_fraction = seconds_fraction*100.0;
            seconds_int = int(seconds_fraction);
            Serial.print("Second Fractional Part as Int = ");
            Serial.println(seconds_int);
            EEPROM.write(3, seconds_int);
            
            seconds_fraction = seconds_fraction - seconds_int;
            seconds_fraction = seconds_fraction*100.0;
            seconds_int = int(seconds_fraction);
            Serial.print("Second Fractional Part as Int = ");
            Serial.println(seconds_int);
            EEPROM.write(4, seconds_int);
            
            // Save Longitude to EEPROM
            EEPROM.write(5, home_d_long);
            EEPROM.write(6, home_m_long);
            
            seconds_int = int(home_s_long);
            Serial.print("Seconds Integer Part = ");
            Serial.println(seconds_int);
            EEPROM.write(7, seconds_int);
            
            seconds_fraction = home_s_long - seconds_int;
            Serial.print("Second Fractional Part = ");
            Serial.println(seconds_fraction, 5);
            seconds_fraction = seconds_fraction*100.0;
            seconds_int = int(seconds_fraction);
            Serial.print("Second Fractional Part as Int = ");
            Serial.println(seconds_fraction);
            EEPROM.write(8, seconds_int);
            
            seconds_fraction = seconds_fraction - seconds_int;
            seconds_fraction = seconds_fraction*100.0;
            seconds_int = int(seconds_fraction);
            Serial.print("Second Fractional Part as Int = ");
            Serial.println(seconds_int);
            EEPROM.write(9, seconds_int);
            EEPROM.write(25, 0);
            /*****                                *****/
            
            coords = Coordinates(home_d_lat, home_m_lat, home_s_lat,
                        home_d_long, home_m_long, home_s_long);
            /*****                                *****/
                                                      // And play a tune to let us know
            speaker.playMusic(guitarTune, sizeof(guitarTune)); 
            break;                                    // Then break from the while loop
          }
          else {                                      // If there is no valid GPS fix
            tone(buzzer, 1000000/c4);                 // Play a 3 second long tone
            delay(3000);                              // To let us know that
            noTone(buzzer);                           // Then turn the buzzer off
          } 
        }
      }
    }
    noTone(buzzer);                                 // Turn off the buzzer
    while(digitalRead(button) == 0);                // And wait for the button to be let up
  }
  
  appendFile(file_name);
  Serial3.println();
  Serial.print("Time = ");
  Serial.print(arduino_time);
  Serial.println(" sec");
  
  Serial.print("Time = ");
  Serial.print(arduino_time);
  Serial.println(" sec");
  
  Serial3.print(arduino_time);
  Serial3.print(", ");
  
  Serial3.print(gps_time_hours);
  Serial3.print(":");
  Serial3.print(gps_time_minutes);
  Serial3.print(":");
  Serial3.print(gps_time_seconds);
  Serial3.print(", ");
  
  Serial3.print(battery.get_Vin());
  Serial3.print(", ");
  
  Serial3.print(gps_latitude_degrees);
  Serial3.print(", ");
  Serial3.print(gps_latitude_minutes);
  Serial3.print(", ");
  Serial3.print(gps_latitude_seconds,5);
  Serial3.print(", ");
  
  Serial3.print(gps_longitude_degrees);
  Serial3.print(", ");
  Serial3.print(gps_longitude_minutes);
  Serial3.print(", ");
  Serial3.print(gps_longitude_seconds,5);
  Serial3.print(", ");
  
  Serial3.print(gps_nosat);
  Serial3.print(", ");
  
  Serial3.print(gps_altitude);
  Serial3.print(", ");
  
  int alt = my_altimeter.get_altitude();
  // If the function returns an altitude of -999 ft it means the device timed out
  if (alt == -999) {
    // When the device times out it could mean the device is not configured properly
    // Or is disconnected or damanged
    Serial.println("Altimeter Disconnected or Dead!");
  }
  // Otherwise the altitude should be accurate
  else {
    // So we display the altitude on the serial monitor
    Serial.print("Altimeter Altitude = ");
    Serial.println(alt);
  }
  Serial3.print(alt);
  Serial3.print(", ");
  Serial3.print(gps_course);
  Serial3.print(", ");
  Serial3.print(gps_speed);
  Serial3.print(", ");
  
  Serial3.print(mappedX);
  Serial3.print(", ");
  Serial3.print(mappedY);
  Serial3.print(", ");
  Serial3.print(mapped_course_angle);
  Serial3.print(", ");
  Serial3.print(idealAngle);
  Serial3.print(", ");
  Serial3.print(home_angle);
  Serial3.print(", ");
  Serial3.print(isRight);
  Serial3.print(", ");
  
  Serial.println("...done writing to log file");
  
  Serial.print("Home Latitude = ");
  Serial.print(home_d_lat);
  Serial.print(char(176));
  Serial.print(home_m_lat);
  Serial.print("'");
  Serial.print(home_s_lat, 6);
  Serial.println("\"");
  
  Serial.print("Home Longitude = ");
  Serial.print(home_d_long);
  Serial.print(char(176));
  Serial.print(home_m_long);
  Serial.print("'");
  Serial.print(home_s_long, 6);
  Serial.println("\"");
  
  Serial.print("Square Ratio=");
  Serial.println(coords.getSqRatio(),5);
  Serial.print("Origin x seconds=");
  Serial.println(coords.getX0());
  Serial.print("Origin y seconds=");
  Serial.println(coords.getY0());
  
  Serial.println("Mapping Current Point");
  coords.map_point(gps_latitude_minutes, gps_latitude_seconds,
                   gps_longitude_minutes, gps_longitude_seconds);
  Serial.print("Mapped x = ");
  mappedX = coords.getX();
  Serial.println(mappedX);
  Serial.print("Mapped y = ");
  mappedY = coords.getY();
  Serial.println(mappedY);
  Serial.println("Mapping COG to coordinate system:");
  Serial.print("Mapped angle = ");
  mapped_course_angle = coords.map_COG(gps_course);
  Serial.println(mapped_course_angle);
  
  Vector cv = Vector(mapped_course_angle);
  Vector hv = Vector(mappedX, mappedY);
  home_angle = hv.getAngle();
  hv.rotate(spiral_angle);
  idealAngle = hv.getAngle();
  
  if(cv.cmpAngle(&hv) < 0){
    Serial.println("Turn Left");
    myservo.writeMicroseconds(servoLeft);
    digitalWrite(left_LED, LOW);
    digitalWrite(right_LED, HIGH);
    isRight = 0;
    Serial.println("   **** Turning Left!");
  }
  else {
    Serial.println("Turn Right");
    myservo.writeMicroseconds(servoRight);
    digitalWrite(left_LED, HIGH);
    digitalWrite(right_LED, LOW);
    isRight = 1;
    Serial.println("   **** Turning Right!");
  }
  if (isHomeSet != 1) {
    Serial.println("*** No HOME LOCATION SET ***");
  }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
   // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
   
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
      if(test_valid == 1) {
        Serial.println("Checksum Valid");
        
        /***** Update GPS Data Objects *****/
        Latitude test_latitude = Latitude(my_gps.get_latitude_string());
        gps_latitude_degrees = test_latitude.get_degrees();
        gps_latitude_minutes = test_latitude.get_minutes();
        gps_latitude_seconds = test_latitude.get_seconds();

        
        Longitude test_longitude = Longitude(my_gps.get_longitude_string());
        gps_longitude_degrees = test_longitude.get_degrees();
        gps_longitude_minutes = test_longitude.get_minutes();
        gps_longitude_seconds = test_longitude.get_seconds();
        
        Time test_time = Time(my_gps.get_time_string());
        gps_time_hours = test_time.get_hours();
        gps_time_minutes = test_time.get_minutes();
        gps_time_seconds = test_time.get_seconds();
        
        gps_nosat = my_gps.get_nosat();
        
        gps_altitude = my_gps.get_altitude();
        
        gps_course = my_gps.get_course();
        
        gps_speed = my_gps.get_speed(MPH);
        /*****                         *****/
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          isFixed = 0;
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          isFixed = 1;
        }
        else {
          Serial.println("Data status unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          isFixed = 1;
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          isFixed = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          isFixed = 1;
        }
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}


// This function opens the openlogger in command mode and creates or opens the logFile
// to append to
void appendFile(char fileName[]){
  char c;
  int test;
  //Send three control z to enter OpenLog command mode
  Serial3.print(byte(26));
  Serial3.print(byte(26));
  Serial3.print(byte(26));

  //Wait for OpenLog to respond with '>' to indicate we are in command mode
  long timer = millis();
  while((millis() - timer) < 150) {
    if(Serial3.available()) {
      if (Serial3.read() == '>') break;
    }
  }

  String file_name = String(fileName);
  file_name = String("append ") + fileName + String("\r");
  Serial3.print(file_name);
  Serial.println(file_name);

  //Wait for OpenLog to indicate file is open and ready for writing
  timer = millis();
  while((millis() - timer) < 150) {
    if(Serial3.available()) {
      if(Serial3.read() == '<') break;
    }
  }
}
