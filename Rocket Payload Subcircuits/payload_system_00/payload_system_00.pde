#include <Wire.h>                //arduino library for I2C comm (req'd for bmp, adc, and mux)
#include <BMP085.h>              //adafruit library for baro
#include <MUX.h>                 //custom library for I2C mux (PCA9544A)
#include <ADS7828.h>             //custom library for I2C ADC (ADS7828)
#include <WirelessComm.h>        //custom library for wireless comm
#include <GPS.h>                 // Custom GPS Parser Library
#include <VinMonitor.h>          // Custom Battery Voltage Monitor Library
#include <NewSoftSerial.h>     // Arduino Libary to allow for software UART interfaces
#include <IMU.h>                 //custom library for Sparkfun 5DOF IMU

#define SERIALNUM 0              //Serial port used by myWireless

NewSoftSerial softSerial(50, 51); //initialize softserial and file_name for UART
char file_name[10];

ADS7828 ads0(0);                 //initialize ADC

BMP085 bmp;                      //initialize baro
float presOffset = 101988;       //pressure at sea level (baro)

long arduino_time;            // Variable to store the global Arduinoarduino_timeach cycle of loop()

// GPS and Supporting Objects and Variables
GPS my_gps;                    // GPS object connected to GPS connector
int gps_latitude_degrees = 0;
int gps_latitude_minutes = 0;
float gps_latitude_seconds = 0.0;
int gps_longitude_degrees = 0;
int gps_longitude_minutes = 0;
float gps_longitude_seconds = 0.0;
int gps_time_hours = 0;
int gps_time_minutes = 0;
float gps_time_seconds = 0.0;
float gps_altitude;
float gps_speed;
float gps_course;
int isFixed = 0;               // isFixed = 1, GPS has valid fix data

//myWireless and supporting objects and vars
WirelessComm myWireless(SERIALNUM);

/***********TODO: verify with conor order of gps vars and imu vars*****************************/
float gpsVars[9] = {84.023, 35.031, 22, 88.023, 30.031, 29, 102, 12, 7}; //verify with Conor order
float barVars[3] = {128.023, 124, 23.4}; //pressure, altitude, temp
float humVars[1] = {55.2}; //relative humidity
float sirVars[1] = {60.01232}; //solar irradiance
float uvVars[1]  = {11.19988}; //UV irradiance
float rgbVars[3] = {10, 20, 30}; //red, blue, green
float imuVars[5] = {0.2, 0.7, 20, 0.1, 0.3}; //verify with Conor order

String gps = "GPS";
String bar = "BAR";
String hum = "HUM";
String sir = "SIR";
String uv  = "UV";
String rgb = "RGB";
String imu = "IMU";

// Vin Battery Voltage Monitor Object and Variables
int Vin_analog_input_pin = 0; // Vin (Battery) Voltage Monitor connected to analog pin 0
float Vref = 5.00;            // Vref measured value
float Rref = 42.80;           // Rref measured value
float Rup = 42.80;            // Rup measured value
float diode_drop = 0.84;      // Input protection diode voltage drop
VinMonitor battery(Vin_analog_input_pin, Vref, Rref, Rup, diode_drop);

//analog pins, and reference voltages and other supporting vars along with IMU object 
int xa = 3;
int ya = 4;
int za = 5;
int yrate = 6;
int xrate = 7;
//float Vref = 4.82;  //use Vref from VinMonitor
float V3_3 = 3.312;
IMU my_imu(xa, ya, za, yrate, xrate, V3_3, Vref);
float x_acceleration;
float y_acceleration;
float z_acceleration;
float y_rotation_rate;
float x_rotation_rate;

void setup() 
{
  myWireless.beginSerial();      //initialize myWireless
  Serial2.begin(9600);           //initialize Serial2 (for GPS)
  Serial.begin(9600);            //initialize debug serial
}

void loop()
{
  arduino_time = millis()/1000;
  // Call a custom funtion (which is defined below) that will read the GPS
  read_gps();
  
  //Read 8 Science Boards
  for(int i = 0; i <8; i++){
    MUX mux0(i);
    mux0.open_chan(0);
    humVars[0] = ads0.read_chan(0);
    sirVars[0] = ads0.read_chan(2);
    uvVars[0] = ads0.read_chan(3);
    rgbVars[0] = ads0.read_chan(4);
    rgbVars[1] = ads0.read_chan(5);
    rgbVars[2] = ads0.read_chan(6);
    //pick chan 1 or chan 2 for Baro
    mux0.open_chan(1);
    bmp.begin();
    barVars[0] = bmp.readPressure();
    barVars[1] = bmp.readAltitude(presOffset);
    barVars[2] = bmp.readTemperature();
    mux0.open_chan(2);
    bmp.begin();
    barVars[0] = bmp.readPressure();
    barVars[1] = bmp.readAltitude(presOffset);
    barVars[2] = bmp.readTemperature();
    myWireless.transmitData(millis(), bar, barVars);
    myWireless.transmitData(millis(), hum, humVars);
    myWireless.transmitData(millis(), sir, sirVars);
    myWireless.transmitData(millis(), uv, uvVars);
    myWireless.transmitData(millis(), rgb, rgbVars);
  }
  
/***********TODO: map gps and imu according to conor*****************************/
  gpsVars[0] = 0;
  gpsVars[1] = 0;
  gpsVars[1] = 0;
  gpsVars[3] = 0;
  gpsVars[4] = 0;
  gpsVars[5] = 0;
  gpsVars[6] = 0;
  gpsVars[7] = 0;
  gpsVars[8] = 0;
  
  x_acceleration = my_imu.get_x_acc();
  y_acceleration = my_imu.get_y_acc();
  z_acceleration = my_imu.get_z_acc();
  y_rotation_rate = my_imu.get_y_rate();
  x_rotation_rate = my_imu.get_x_rate();
  imuVars[0] = 0;
  imuVars[1] = 0;
  imuVars[2] = 0;
  imuVars[3] = 0;
  imuVars[4] = 0;
  
  myWireless.transmitData(millis(), gps, gpsVars);
  myWireless.transmitData(millis(), imu, imuVars);
  
  appendFile(file_name);
  softSerial.println();
  Serial.print("Time = ");
  Serial.print(arduino_time);
  Serial.println(" sec");
  softSerial.print(arduino_time);
  softSerial.print(", ");
  softSerial.print(gps_time_hours);
  softSerial.print(":");
  softSerial.print(gps_time_minutes);
  softSerial.print(":");
  softSerial.print(gps_time_seconds);
  softSerial.print(", ");
  softSerial.print(battery.get_Vin());
  softSerial.print(", ");
  softSerial.print(gps_latitude_degrees);
  softSerial.print(", ");
  softSerial.print(gps_latitude_minutes);
  softSerial.print(", ");
  softSerial.print(gps_latitude_seconds,5);
  softSerial.print(", ");
  softSerial.print(gps_longitude_degrees);
  softSerial.print(", ");
  softSerial.print(gps_longitude_minutes);
  softSerial.print(", ");
  softSerial.print(gps_longitude_seconds,5);
  softSerial.print(", ");
  softSerial.print(gps_altitude);
  softSerial.print(", ");
  //int alt = my_altimeter.get_altitude();
//  int alt = -999;
//  softSerial.print(alt);
//  // If the function returns an altitude of -999 ft it means the device timed out
//  if (alt == -999) {
//    // When the device times out it could mean the device is not configured properly
//    // Or is disconnected or damanged
//    Serial.println("Altimeter Disconnected or Dead!");
//  }
//  // Otherwise the altitude should be accurate
//  else {
//    // So we display the altitude on the serial monitor
//    Serial.print("Altimeter Altitude = ");
//    Serial.println(alt);
//  }
  softSerial.print(", ");
  softSerial.print(gps_course);
  softSerial.print(", ");
  softSerial.print(gps_speed);
  softSerial.print(", ");
//  softSerial.print(mappedX);
//  softSerial.print(", ");
//  softSerial.print(mappedY);
//  softSerial.print(", ");
//  softSerial.print(mapped_course_angle);
//  softSerial.print(", ");
//  softSerial.print(idealAngle);
//  softSerial.print(", ");
//  softSerial.print(home_angle);
//  softSerial.print(", ");
//  softSerial.print(isRight);
//  softSerial.print(", ");
  
  Serial.println("...done writing to log file");
  
//  Serial.print("Home Latitude = ");
//  Serial.print(home_d_lat);
//  Serial.print(char(176));
//  Serial.print(home_m_lat);
//  Serial.print("'");
//  Serial.print(home_s_lat, 6);
//  Serial.println("\"");
//  
//  Serial.print("Home Longitude = ");
//  Serial.print(home_d_long);
//  Serial.print(char(176));
//  Serial.print(home_m_long);
//  Serial.print("'");
//  Serial.print(home_s_long, 6);
//  Serial.println("\"");
//  
//  Serial.print("Square Ratio=");
//  Serial.println(coords.getSqRatio(),5);
//  Serial.print("Origin x seconds=");
//  Serial.println(coords.getX0());
//  Serial.print("Origin y seconds=");
//  Serial.println(coords.getY0());
  
//  Serial.println("Mapping Current Point");
//  coords.map_point(gps_latitude_minutes, gps_latitude_seconds,
//                   gps_longitude_minutes, gps_longitude_seconds);
//  Serial.print("Mapped x = ");
//  mappedX = coords.getX();
//  Serial.println(mappedX);
//  Serial.print("Mapped y = ");
//  mappedY = coords.getY();
//  Serial.println(mappedY);
//  Serial.println("Mapping COG to coordinate system:");
//  Serial.print("Mapped angle = ");
//  mapped_course_angle = coords.map_COG(gps_course);
//  Serial.println(mapped_course_angle);
  
//  Vector cv = Vector(mapped_course_angle);
//  Vector hv = Vector(mappedX, mappedY);
//  home_angle = hv.getAngle();
//  hv.rotate(spiral_angle);
//  idealAngle = hv.getAngle();
//  
//  if(cv.cmpAngle(&hv) < 0 ){
//    Serial.println("Turn Left");
//    myservo.writeMicroseconds(servoLeft);
//    isRight = 0;
//  }
//  else {
//    Serial.println("Turn Right");
//    myservo.writeMicroseconds(servoRight);
//    isRight = 1;
//  }
//  if (isHomeSet != 1) {
//    Serial.println("*** No HOME LOCATION SET ***");
//  }
}

//
// Function: read_gps()
//
// Input: void
// Return: void
//
// Description: Use this function to read your GPS device for a NMEA message using
//              read_gps_line(), check it for a valid checksum, and display with
//              full verbosity the conents of the message input. 
//              The while((millis() - start_time)<500) will read the GPS for 500mSec
//              before continuing the loop, this time can be set as low as you want and is simply
//              used to give the Arduino time to read the GPS, especially if the remainting parts 
//              of the loop() take a lot of time.
//
void read_gps() {
   // Save the time the the read_gps() function began
  long start_time = millis();
  
  // Keep reading/updating the GPS for up to 500mSec
  while((millis() - start_time) < 500) {
    // read_gps_line() reads Serial2 and take in a NMEA string
    // if it returns something other than 1, it did not get a full message
    int test = my_gps.read_gps_line();
    if(test == 1) {
      // We know we got a full message so we use get_gps_line() to return the
      // NMEA message for disply on the serial monitor
      String test_message = my_gps.get_gps_line();
   
      // Next we use par_NMEA_message to get the data from the message and validate
      // its checksum. If this function call returns 0 the checksum was invalid, so don't
      // bother getting the data from it.
      // By selecting VERBOSE mode, the parse_NMEA_message displays on the serial monitor
      // all the fields it just parsed. If you leave this input empty, it won't do that.
      int test_valid = my_gps.parse_NMEA_message(test_message);
      if(test_valid == 1) {
        Serial.println("Checksum Valid");
        
        /***** Update GPS Data Objects *****/
        Latitude test_latitude = Latitude(my_gps.get_latitude_string());
        gps_latitude_degrees = test_latitude.get_degrees();
        gps_latitude_minutes = test_latitude.get_minutes();
        gps_latitude_seconds = test_latitude.get_seconds();

        
        Longitude test_longitude = Longitude(my_gps.get_longitude_string());
        gps_longitude_degrees = test_longitude.get_degrees();
        gps_longitude_minutes = test_longitude.get_minutes();
        gps_longitude_seconds = test_longitude.get_seconds();
        
        Time test_time = Time(my_gps.get_time_string());
        gps_time_hours = test_time.get_hours();
        gps_time_minutes = test_time.get_minutes();
        gps_time_seconds = test_time.get_seconds();
        
        gps_altitude = my_gps.get_altitude();
        
        gps_course = my_gps.get_course();
        
        gps_speed = my_gps.get_speed(MPH);
        /*****                         *****/
        
        // Test Status Indicators
        if(my_gps.get_data_status() == 0) {
          Serial.println("Data is invalid!");
          isFixed = 0;
        }
        else if(my_gps.get_data_status() == 1) {
          Serial.println("Data is valid");
          isFixed = 1;
        }
        else {
          Serial.println("Data status unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_mode() == 1) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_mode() == 2) {
          Serial.println("2D Fix Available");
          isFixed = 1;
        }
        else if(my_gps.get_fix_mode() == 3){
          Serial.println("3D Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Mode unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_fix_status() == 0) {
          Serial.println("No Fix Available");
          isFixed = 0;
        }
        else if(my_gps.get_fix_status() == 1) {
          Serial.println("Standard GPS Fix Available");
          isFixed = 1;
        }
        else {
          Serial.println("Fix Status Unknown");
          isFixed = 0;
        }
        
        if(my_gps.get_mode_ind() == 0) {
          Serial.println("Mode Indicates 'No Fix'");
          isFixed = 0;
        }
        else {
          Serial.println("Mode Indicates a fix");
          isFixed = 1;
        }
        Serial.println();
      }
      // If parse_NMEA_message() returns -1 the message was not of valid type
      else if(test_valid == -1) {
        Serial.println("***** Message was not of type GGA, GSA, RMC or VTG *****\n");
      }
      // If parse_NMEA_message() returns 0 the message checksum was invalid
      else {
        Serial.println("Checksum Invalid\n");
      }
    }
    // If read_gps_line() returns -1 it means before a full message
    // was read in the watchdog timmed out. This is useful, so that
    // a broken or malfunctioning GPS wan't lockup the entire system
    else if (test == -1) {
      Serial.println("***** Got incomplete message and timed out *****\n");
    }
  }
}

void appendFile(char fileName[]){
  char c;
  int test;
  //Send three control z to enter OpenLog command mode
  softSerial.print(byte(26));
  softSerial.print(byte(26));
  softSerial.print(byte(26));

  //Wait for OpenLog to respond with '>' to indicate we are in command mode
  long timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if (softSerial.read() == '>') break;
    }
  }

  String file_name = String(fileName);
  file_name = String("append ") + fileName + String("\r");
  softSerial.print(file_name);
  Serial.println(file_name);

  //Wait for OpenLog to indicate file is open and ready for writing
  timer = millis();
  while((millis() - timer) < 150) {
    if(softSerial.available()) {
      if(softSerial.read() == '<') break;
    }
  }
}
