#include <AFMotor.h>

AF_DCMotor motor3(3, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
AF_DCMotor motor4(4, MOTOR12_64KHZ); //create motor #4, 64KHz pwn
int front = 0; //define the pin for the IR sensor
int fval; //IR sensor value
int bottom = 5;
int bval;


void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");
  
  motor3.setSpeed(200);     // set the speed to 200/255
  motor4.setSpeed(200);    //set the speed to 100/255
  fval = analogRead(front);
  bval = analogRead(bottom);

}
void loop()
{
  if((fval < 370)&&(bval < 100)) //no obstacle nearby in front
  {
    motor3.run(FORWARD);      //Drive Forward
    motor4.run(FORWARD);
    Serial.println("Driving Forward!");
    delay(250);
  }
  else if((fval >= 370)||(bval >= 100))
  {
    motor3.run(BACKWARD);
    motor4.run(BACKWARD);
    delay(500);
    while((fval > 350)||(bval >= 80))
    {
      motor3.run(BACKWARD);
      motor4.run(RELEASE);
      delay(100);
      motor3.run(RELEASE);
      motor4.run(BACKWARD);
      delay(100);
      fval = analogRead(front);
      bval = analogRead(bottom);
    }
    motor3.run(FORWARD);
    motor4.run(BACKWARD);
    delay(1500);
  }
  fval = analogRead(front);
  bval = analogRead(bottom);
}
