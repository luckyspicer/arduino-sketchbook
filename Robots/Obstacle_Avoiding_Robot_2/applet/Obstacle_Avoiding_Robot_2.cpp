#include <AFMotor.h>

#include "WProgram.h"
void setup();
void loop();
AF_DCMotor motor3(3, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
AF_DCMotor motor4(4, MOTOR12_64KHZ); //create motor #4, 64KHz pwn
int front = 0; //define the pin for the IR sensor
int fval; //IR sensor value
int bottom = 5;
int bval;


void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");
  
  motor3.setSpeed(200);     // set the speed to 200/255
  motor4.setSpeed(200);    //set the speed to 100/255
  fval = analogRead(front);
  bval = analogRead(bottom);

}
void loop()
{
  if((bval < 200)&&(bval > 30)) //no obstacle nearby in front
  {
    motor3.run(FORWARD);      //Drive Forward
    motor4.run(FORWARD);
    Serial.println("Driving Forward!");
    delay(50);
  }
  else if(bval <= 30)
  {
    motor3.run(BACKWARD);
    motor4.run(FORWARD);
    delay(75);
  }
  else if(bval >= 200)
  {
    motor3.run(FORWARD);
    motor4.run(BACKWARD);
    delay(75);
  }
  //fval = analogRead(front);
  bval = analogRead(bottom);
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

