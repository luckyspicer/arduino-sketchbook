#include <LSMotor.h>
#include <Music.h>

#include "WProgram.h"
void setup();
void loop();
LS_DCMotor motor3(3, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
LS_DCMotor motor4(4, MOTOR12_64KHZ); //create motor #4, 64KHz pwn
int IR = 0; //define the pin for the IR sensor
int val; //IR sensor value

int GreenLED = 15; //define the pin for the GreenLED (lights when going forward)
int RedLED = 16;   //define the pin for the RedLED (lights when going backward)
int YellowLED = 17; //defin the pin for the YellowLED (lights when turning)
int randomLED; //for the Muse party light show!!!

int x = 1;


void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");
  
  motor3.setSpeed(200);     // set the speed to 200/255
  motor4.setSpeed(200);     // set the speed to 200/255
  val = analogRead(IR);
  
  pinMode(GreenLED, OUTPUT); //The LED's and Speaker are OUTPUTS!!!
  pinMode(RedLED, OUTPUT);
  pinMode(YellowLED, OUTPUT);
}

void loop()
{
  if(val < 400) //no obstacle nearby in front
  {
    digitalWrite(GreenLED, HIGH);
    motor3.run(FORWARD);      //Drive Forward
    motor4.run(FORWARD);
    Serial.println("Driving Forward!");
    delay(100);
    digitalWrite(GreenLED, LOW);
    delay(100);
  }
  else if((val >= 400)&&(val != 700))
  {
    digitalWrite(RedLED, HIGH); // Backing Up from the Obstacle
    motor3.run(BACKWARD);
    motor4.run(BACKWARD);
    Serial.println("Backing UP!");
    delay(4000);
    digitalWrite(RedLED, LOW);
    
    digitalWrite(YellowLED, HIGH);
    motor3.run(FORWARD);      // Turn
    motor4.run(BACKWARD);
    Serial.println("Turning!");
    delay(1700);
    digitalWrite(YellowLED, LOW);
  }
  val = analogRead(IR);
}

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

