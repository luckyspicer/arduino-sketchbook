#include <LSMotor.h>
#include <Music.h>

LS_DCMotor motor3(3, MOTOR12_64KHZ); // create motor #3, 64KHz pwm
LS_DCMotor motor4(4, MOTOR12_64KHZ); //create motor #4, 64KHz pwn
int IR = 0; //define the pin for the IR sensor
int val; //IR sensor value

int GreenLED = 15; //define the pin for the GreenLED (lights when going forward)
int RedLED = 16;   //define the pin for the RedLED (lights when going backward)
int YellowLED = 17; //defin the pin for the YellowLED (lights when turning)
int randomLED; //for the Muse party light show!!!

int x = 1;

Music speaker = 18; //setup a pin for the speaker on Analog pin 4 (I/0 pin 18)

void setup()
{
  Serial.begin(9600);           // set up Serial library at 9600 bps
  Serial.println("Motor test!");
  
  motor3.setSpeed(200);     // set the speed to 200/255
  motor4.setSpeed(200);    //set the speed to 100/255
  val = analogRead(IR);
  
  pinMode(GreenLED, OUTPUT); //The LED's and Speaker are OUTPUTS!!!
  pinMode(RedLED, OUTPUT);
  pinMode(YellowLED, OUTPUT);
}

// Melody and Timing =============================================================
int truckNoise[] = {75,  a4,q,   R,q,  a4,q,  R,q,  a4,q};

//Party Time music and timing!====================================================
int partyMusic[] = {150,  E3,h,  E3,q,  B3,h,  B3,q,  Fs3,q+3,  G3,e,  Fs3,q,  E3,h+q,  R,q,  B3,q,  d4,q, e4,h, d4,q,
                          B3,q,  cs4,q, A3,q,  B3,h+q, R,h,     e4,q,  e4,h,   e4,q,    d4,h, B3,q,  B3,q, A3,q, G3,q,
                          Fs3,e, D3,W+h, E3,h,  B3,q,    A3,h,  G3,q,   Fs3,q,   E3,q,  D3,q,  E3,h+q};
void loop()
{
  if(x == 1)
  {
    speaker.playMusic(partyMusic, sizeof(partyMusic));
    x = -500;
  }
  if(val < 400) //no obstacle nearby in front
  {
    digitalWrite(GreenLED, HIGH);
    motor3.run(FORWARD);      //Drive Forward
    motor4.run(FORWARD);
    Serial.println("Driving Forward!");
    delay(250);
    digitalWrite(GreenLED, LOW);
  }
  else if((val >= 400)&&(val != 700))
  {
    digitalWrite(RedLED, HIGH); // Backing Up from the Obstacle
    motor3.run(BACKWARD);
    motor4.run(BACKWARD);
    Serial.println("Backing UP!");
    speaker.playMusic(truckNoise, sizeof(truckNoise));
    digitalWrite(RedLED, LOW);
    
    digitalWrite(YellowLED, HIGH);
    motor3.run(FORWARD);      // Turn
    motor4.run(BACKWARD);
    Serial.println("Turning!");
    delay(1700);
    digitalWrite(YellowLED, LOW);
  }
  
  val = random(10, 900);
  if(val == 700) //If val happens to randomly fall on 700 then this Muse party will occur.
  {
    motor3.run(BACKWARD);
    motor4.run(FORWARD);
    Serial.println("Party Time!");
    
    speaker.playMusic(partyMusic, sizeof(partyMusic));
    
    motor3.run(FORWARD);
    motor4.run(BACKWARD);
    Serial.println("More Party Time!");
    
    speaker.playMusic(partyMusic, sizeof(partyMusic));
  }
  val = analogRead(IR);
}
