//#include <SD.h>
#include "WaveHC.h"
#include "WaveUtil.h"


char filename[13];

SdReader card;    // This object holds the information for the card
FatVolume vol;    // This holds the information for the partition on the card
FatReader root;   // This holds the information for the volumes root directory
FatReader file;   // This object represent the WAV file for a pi digit or period
WaveHC wave;      // This is the only wave (audio) object, since we will only play one at a time
/*
 * Define macro to put error messages in flash memory
 */
#define error(msg) error_P(PSTR(msg))

//////////////////////////////////// SETUP

void setup() {
  // set up Serial library at 9600 bps
  Serial.begin(9600);           
  
  PgmPrintln("Rescue Robot Voice Startup");
  
  if (!card.init()) {
    error("Card init. failed!");
  }
  if (!vol.init(card)) {
    error("No partition!");
  }
  if (!root.openRoot(vol)) {
    error("Couldn't open dir");
  }

  PgmPrintln("Files found:");
  root.ls();
  
  // Set up the SD card
  /*Serial.print("Initializing SD card...");
  pinMode(53, OUTPUT);
  if (!SD.begin(53)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");
  */
}

/////////////////////////////////// LOOP

unsigned digit = 0;
void loop() {
  // Say "Victim Located"
  playcomplete("VICTIM.WAV");
  delay(50);
  
  // Say Room Number
  playcomplete("ROOM3.WAV");
  delay(50);
  
  // Say Position
  playcomplete("POSITION.WAV");
  delay(50);
  playnum(7);
  delay(50);
  playnum(0);
  delay(50);
  
  // Say Status
  playcomplete("STATUS.WAV");
  delay(50);
  
  //strcpy_P(filename, PSTR("PORTAL3.WAV"));
  playcomplete("PORTAL3.WAV");
  delay(50);
}

/////////////////////////////////// HELPERS
void playnum(int number) {
  switch (number) {
    case 0:
      playcomplete("0.WAV");
      break;
    case 1:
      playcomplete("1.WAV");
      break;
    case 2:
      playcomplete("2.WAV");
      break;
    case 3:
      playcomplete("3.WAV");
      break;
    case 4:
      playcomplete("4.WAV");
      break;
    case 5:
      playcomplete("5.WAV");
      break;
    case 6:
      playcomplete("6.WAV");
      break;
     case 7:
      playcomplete("7.WAV");
      break;
     case 8:
      playcomplete("8.WAV");
      break;
     case 9:
      playcomplete("9.WAV");
      break;
  }
}
/*
 * print error message and halt
 */
void error_P(const char *str)
{
  PgmPrint("Error: ");
  SerialPrint_P(str);
  sdErrorCheck();
  while(1);
}
/*
 * print error message and halt if SD I/O error
 */
void sdErrorCheck(void)
{
  if (!card.errorCode()) return;
  PgmPrint("\r\nSD I/O error: ");
  Serial.print(card.errorCode(), HEX);
  PgmPrint(", ");
  Serial.println(card.errorData(), HEX);
  while(1);
}
/*
 * Play a file and wait for it to complete
 */
void playcomplete(char *name) {
  playfile(name);
  while (wave.isplaying);
  
  // see if an error occurred while playing
  sdErrorCheck();
}
/*
 * Open and start playing a WAV file
 */
void playfile(char *name) 
{
  if (wave.isplaying) {// already playing something, so stop it!
    wave.stop(); // stop it
  }
  if (!file.open(root, name)) {
    PgmPrint("Couldn't open file ");
    Serial.print(name); 
    return;
  }
  if (!wave.create(file)) {
    PgmPrintln("Not a valid WAV");
    return;
  }
  // ok time to play!
  wave.play();
}
