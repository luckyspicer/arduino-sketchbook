#include <ks0108.h>
#include "cardinal.h"

//////////////////////////////////// SETUP
void setup() { 
  GLCD.Init(INVERTED);   // initialise the library, non inverted writes pixels onto a clear screen
  DisplayCardinal();
}

/////////////////////////////////// LOOP
void loop() {
}

void DisplayCardinal() {
  GLCD.ClearScreen();
  delay(1500);
  GLCD.DrawBitmap(cardinal, 35, 0, BLACK);
}
